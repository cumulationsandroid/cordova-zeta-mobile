//
//  ProxyCommPlugin.m
//  HelloWorld
//
//  Created by Madhu V Swamy on 30/05/16.
//
//

#import "ProxyCommPlugin.h"
#import <Cordova/CDV.h>

@interface ProxyCommPlugin ()

@property (strong, nonatomic) CDVInvokedUrlCommand* commandCallBack;
@property (strong, nonatomic) CDVInvokedUrlCommand* communicationCallBack;
@property (strong, nonatomic) CommunicationBle *iComm;

@end

@implementation ProxyCommPlugin
@synthesize commandCallBack,iComm, communicationCallBack;

- (void)sendMessage:(CDVInvokedUrlCommand*)command
{
    NSString* methodName = [command.arguments objectAtIndex:0];
    NSString *message = @"";
    if([command.arguments count]==2)
        message = [command.arguments objectAtIndex:1];
    
    NSLog(@"%@ is the comm proxy",methodName);
    
    ZetaSenseHealthKitManager *healthManager = [ZetaSenseHealthKitManager sharedManager];
    if(![healthManager hasRequestedUserAccess])
        [healthManager requestAuthorization];
    
    if([methodName isEqualToString:kTRANSMIT_DATA]){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataReadyExternal:) name:kNOTIFICATION_DATA_RECIEVED object:nil];
        commandCallBack = command;
        iComm = [[CommunicationBle alloc] init];
        [iComm transmitData:message];
    }else if ([methodName isEqualToString:kCONNECTION_LISTNER]){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionStateChange:) name:kNOTIFICATION_CONNECTION_STATUS_CHANGE object:nil];
        communicationCallBack = command;
    }
}

- (void)connectionStateChange:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    NSLog(@"connectionStateChange %@",dict);
    NSString *response=[dict objectForKey:kDEVICE_CONNECTION_STATUS];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:response];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:communicationCallBack.callbackId];
}

- (void)dataReadyExternal:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    NSLog(@"datareadyexternal %@",dict);
    NSString *response=[dict objectForKey:kBLE_DATA_RECEIVED];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:response];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:commandCallBack.callbackId];
}


@end
