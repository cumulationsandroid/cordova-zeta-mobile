//
//  ZetaSenseHealthKitManager.m
//  zetasense
//
//  Created by Madhu V Swamy on 14/05/16.
//  Copyright © 2016 Tuts+. All rights reserved.
//

#import "ZetaSenseHealthKitManager.h"
#import <HealthKit/HealthKit.h>

@interface ZetaSenseHealthKitManager()

@property (nonatomic, retain) HKHealthStore *healthStore;

@end

@implementation ZetaSenseHealthKitManager

+ (ZetaSenseHealthKitManager *)sharedManager {
    static dispatch_once_t pred = 0;
    static ZetaSenseHealthKitManager *instance = nil;
    dispatch_once(&pred, ^{
        instance = [[ZetaSenseHealthKitManager alloc] init];
        instance.healthStore = [[HKHealthStore alloc] init];
    });
    return instance;
}

- (void)requestAuthorization {
    
    NSLog(@"requestAuthorization");
    
    if ([HKHealthStore isHealthDataAvailable] == NO) {
        // If our device doesn't support HealthKit -> return.
        return;
    }
    
    NSArray *readTypes = @[[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate],[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned]];
    
    NSArray *writeTypes = @[[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierBodyMass]];
    
    
    [self.healthStore requestAuthorizationToShareTypes:nil
                                             readTypes:[NSSet setWithArray:readTypes] completion:^(BOOL success, NSError * _Nullable error){
                                                 
         [[NSUserDefaults standardUserDefaults] setBool:success forKey:kHEALTHKIT_ACCESS_REQUESTED];
         [[NSUserDefaults standardUserDefaults] synchronize];
                                                 
    }];    
}

- (BOOL)hasRequestedUserAccess{
    BOOL accessFlag = [[NSUserDefaults standardUserDefaults] boolForKey:kHEALTHKIT_ACCESS_REQUESTED];
    NSLog(@"%hhd Access",accessFlag);
    return accessFlag;
}


@end
