//
//  CommunicationBle.h
//  zetasense
//
//  Created by Madhu V Swamy on 04/04/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@import WatchConnectivity;

#define kBLE_DATA_RECEIVED @"COMM_BLE"
#define kBLE_RESPONSE @"BLE_RESPONSE"

#define kDEVICE_CONNECTION_STATUS @"DEVICE_CONNECTION_STATUS"
#define kDEVICE_CONNECTED @"CONNECTED"
#define kDEVICE_DISCONNECTED @"DISCONNECTED"

#define kNOTIFICATION_CONNECTION_STATUS_CHANGE @"NOTIFICATION_CONNECTION_STATUS_CHANGE"
#define kNOTIFICATION_DATA_RECIEVED @"NOTIFICATION_DATA_RECIEVED"


@interface CommunicationBle : NSObject<WCSessionDelegate>{
    WCSession *session;
}
@property (strong, nonatomic) WCSession *session;
- (void)transmitData:(NSString *)data;


@end
