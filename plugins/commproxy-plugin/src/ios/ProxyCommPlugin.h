//
//  ProxyCommPlugin.h
//  HelloWorld
//
//  Created by Madhu V Swamy on 30/05/16.
//
//

#import <Cordova/CDV.h>
#import "CommunicationBle.h"
#import "ZetaSenseHealthKitManager.h"

@import WatchConnectivity;

@interface ProxyCommPlugin : CDVPlugin

#define kTRANSMIT_DATA @"transmitData"
#define kCONNECTION_LISTNER @"setOnConnectionChangeListener"

@property (strong, nonatomic) WCSession *session;
- (void)sendMessage:(CDVInvokedUrlCommand*)command;
- (void)dataReadyExternal:(NSString *)input;

@end
