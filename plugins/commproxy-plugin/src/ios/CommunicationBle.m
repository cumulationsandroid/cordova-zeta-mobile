//
//  CommBle.m
//  zetasense
//
//  Created by Madhu V Swamy on 04/04/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "CommunicationBle.h"
#import "ProxyCommPlugin.h"

@implementation CommunicationBle
@synthesize session;
- (id)init{
    self = [super init];
    if (self) {
        if ([WCSession isSupported]) {
            session = [WCSession defaultSession];
            session.delegate = self;
            [session activateSession];
        }
    }
    return self;
}

- (void)transmitData:(NSString *)data{
    NSLog(@"transmit data is called %@",data);
    if ([WCSession isSupported]) {
        self.session = [WCSession defaultSession];
        self.session.delegate = self;
        [self.session activateSession];
        NSMutableDictionary *applicationDict = [[NSMutableDictionary alloc] init];
        [applicationDict setValue:data forKey:@"COMMAND"];
        [[WCSession defaultSession] updateApplicationContext:applicationDict error:nil];
    }
}


- (void)session:(WCSession *)session
didReceiveApplicationContext:(NSDictionary<NSString *,
                              id> *)applicationContext
{
    NSLog(@"PHONE DATA %@",applicationContext);
    if([applicationContext valueForKey:kBLE_DATA_RECEIVED]!=nil){
        NSLog(@"COMM_BLE %@",[applicationContext valueForKey:@"COMM_BLE"]);
        [[NSNotificationCenter defaultCenter] postNotificationName:kNOTIFICATION_DATA_RECIEVED object:self userInfo:applicationContext];
    }
    if([applicationContext valueForKey:@"DEBUG_COMMBLE"]!=nil){
        NSLog(@"COMM_BLE %@",[applicationContext valueForKey:@"DEBUG_COMMBLE"]);
    }
}

- (void)sessionReachabilityDidChange:(WCSession *)reachSession
{
    NSMutableDictionary *status = [[NSMutableDictionary alloc] init];
    if(reachSession.reachable){
        NSLog(@"===CONNECTED===");
        [status setValue:kDEVICE_CONNECTED forKey:kDEVICE_CONNECTION_STATUS];
    }else{
        NSLog(@"===NOT CONNECTED===");
        [status setValue:kDEVICE_DISCONNECTED forKey:kDEVICE_CONNECTION_STATUS];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kNOTIFICATION_CONNECTION_STATUS_CHANGE object:self userInfo:status];
}

@end
