//
//  ZetaSenseHealthKitManager.h
//  zetasense
//
//  Created by Madhu V Swamy on 14/05/16.
//  Copyright © 2016 Tuts+. All rights reserved.
//

#import <Foundation/Foundation.h>
@import WatchConnectivity;

@interface ZetaSenseHealthKitManager : NSObject

#define kHEALTHKIT_ACCESS_REQUESTED @"HEALTHKIT_ACCESS_REQUESTED"

+ (ZetaSenseHealthKitManager *)sharedManager;
- (void)requestAuthorization;
- (BOOL)hasRequestedUserAccess;

@end
