package com.at.zeta.zetamobile;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.at.communication.GenericBle;
import com.at.utils.Constants;

import java.util.ArrayList;
import java.util.List;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class DeviceListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final String KEY_MAC_ADDRESS = "KEY_MAC_ADDRESS";
    private static final String KEY_NAME = "NAME";
    private static final String[] KEYS = {"NAME", "KEY_MAC_ADDRESS"};
    private static final int[] IDS = {R.id.device_name, R.id.device_id};

    private static final String TAG = "JL-" + DeviceListActivity.class.getSimpleName();
    private static final int REQUEST_ENABLE_BT = 0;
    TextView device_name, device_id;
    private ListView listView;

    private ArrayList<BluetoothDevice> deviceList = new ArrayList<BluetoothDevice>();
    private DeviceAdapter mAdapter;
    private ProgressDialog mDialog;

    private BluetoothManager bluetoothManager;
    private BluetoothAdapter btAdapter;

    private SharedPreferences mSharedPrefs;
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            Log.d(TAG, "!inside onLeScan, device: " + device);
            addDevice(device, rssi);
        }
    };
    private ImageView img_delete;
    private LinearLayout layoutID;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_list_activity);

        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = bluetoothManager.getAdapter();

        listView = (ListView) findViewById(android.R.id.list);
        listView.setOnItemClickListener(this);

        layoutID = (LinearLayout) findViewById(R.id.layoutID);
        img_delete = (ImageView) findViewById(R.id.img_delete);

        device_name = (TextView) findViewById(R.id.device_name);
        device_id = (TextView) findViewById(R.id.device_id);

        if (mSharedPrefs.getString(Constants.PREF_KEY_MAC_ADDRESS, "").length() > 0) {
            layoutID.setVisibility(View.VISIBLE);
//            device_name.setText("Metawear");
            device_name.setText(mSharedPrefs.getString(Constants.PREF_KEY_DEVICE_NAME, ""));
            device_id.setText(mSharedPrefs.getString(Constants.PREF_KEY_MAC_ADDRESS, ""));
        } else {
            layoutID.setVisibility(View.GONE);
        }

        img_delete = (ImageView) findViewById(R.id.img_delete);
        img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSharedPrefs.edit().putString(Constants.PREF_KEY_MAC_ADDRESS, "").apply();
                layoutID.setVisibility(View.GONE);
                startService(new Intent(DeviceListActivity.this, GenericBle.class).setAction(GenericBle.ACTION_DISCONNECT));
            }
        });

        scanLeDevice(true);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (btAdapter != null && !btAdapter.isEnabled()) {
            enableBt();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        scanLeDevice(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_ENABLE_BT) {
            scanLeDevice(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.device_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.scan_devices) {
            if (btAdapter != null && !btAdapter.isEnabled()) {
                enableBt();
            } else {
                scanLeDevice(true);
                showScanDialog("Scanning devices");
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void enableBt() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void scanLeDevice(boolean enable) {
        Log.d(TAG, "!inside scanLeDevice: " + enable);
        if (btAdapter != null && btAdapter.isEnabled()) {
            if (enable) {
                btAdapter.startLeScan(mLeScanCallback);
            } else {
                btAdapter.stopLeScan(mLeScanCallback);
            }
        }
    }

    private void addDevice(BluetoothDevice device, int rssi) {
        if (!deviceList.contains(device)) {
            deviceList.add(device);
        }
        if (mAdapter == null) {
            mAdapter = new DeviceAdapter(this, R.layout.device_list_item, deviceList);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    listView.setAdapter(mAdapter);
                }
            });
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void showScanDialog(String stringId) {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage(stringId);
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        mDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        scanLeDevice(false);
                    }
                });
        mDialog.show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        scanLeDevice(false);
        BluetoothDevice device = mAdapter.getItem(position);
        mSharedPrefs.edit()
                .putString(Constants.PREF_KEY_MAC_ADDRESS, device.getAddress())
                .putString(Constants.PREF_KEY_DEVICE_NAME, device.getName())
                .commit();
        setResult(RESULT_OK, new Intent().putExtra(Constants.EXTRA_MAC_ADDRESS, device.getAddress()));
        finish();
    }

    private class DeviceAdapter extends ArrayAdapter<BluetoothDevice> {

        public DeviceAdapter(Context context, int resource, List<BluetoothDevice> objects) {
            super(context, resource, objects);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(DeviceListActivity.this).inflate(R.layout.device_list_item, null);
            }

            TextView deviceNameView = (TextView) convertView.findViewById(R.id.device_name);
            TextView deviceIdView = (TextView) convertView.findViewById(R.id.device_id);

            BluetoothDevice device = getItem(position);
            deviceNameView.setText(device.getName());
            deviceIdView.setText(device.getAddress());
            return convertView;
        }
    }
}
