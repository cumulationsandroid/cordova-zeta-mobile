package com.at.zeta.zetamobile;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.at.communication.CommBle;
import com.at.communication.ProxyComm;
import com.at.model.app.ConnectionStates;
import com.at.model.app.IDataReadyExternal;
import com.at.model.app.SensorAgent;
import com.at.model.app.States;
import com.at.model.domain.Channel;
import com.at.model.domain.Device;
import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;
import com.at.model.meta.NodeType;
import com.at.model.meta.Notification;
import com.at.plugin.ProxyCommPlugin;
import com.at.plugin.ProxyCommPluginCallback;
import com.at.utils.Constants;
import com.at.utils.NodeGsonDeserializer;
import com.at.utils.NodeGsonSerializer;
import com.at.utils.db.DatabaseWorker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class MainActivityMetaWear extends AppCompatActivity implements ProxyCommPluginCallback {

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static final String TAG = MainActivityMetaWear.class.getSimpleName();
    private static final int REQUEST_ENABLE_BT = 1;
    private static final String GET_DEV_CAPABILITIES = "{\"name\":\"getDeviceCapabilities\",\"description\":\"SensorAgent getDeviceCapabilities\",\"guid\":\"5c2f4ed5-39f9-4162-b91f-b034f38d9e9d\",\"uri\":\"Method://3d0523bd64000f81.MetaWear.SensorAgent.getDeviceCapabilities\",\"type\":\"METHOD\",\"data\":{\"timeStamp\":0},\"nodes\":[]}";
    private static final String GET_CONF = "{\"name\":\"getConfiguration\",\"description\":\"SensorAgent getDeviceCapabilities\",\"guid\":\"5c2f4ed5-39f9-4162-b91f-b034f38d9e9d\",\"uri\":\"Method://3d0523bd64000f81.BPMonitor.SensorAgent.getConfiguration\",\"type\":\"METHOD\",\"data\":{\"timeStamp\":0},\"nodes\":[]}";
    private static final String SET_CONF = "{ \"name\": \"setConfiguration\", \"description\": \"SensorAgent setConfiguration\", \"guid\": \"5c2f4ed5-39f9-4162-b91f-b034f38d9e9d\", \"uri\": \"Method://3d0523bd64000f81.MetaWear.SensorAgent.setConfiguration\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"description\": \"This is MetaWear\", \"type\": \"OBJECT\", \"guid\": \"AECD4EB7-FBAA-4CB6-8368-212AA733E6C2\", \"name\": \"MetaWear\", \"uri\": \"Device:\\\\/\\\\/3d0523bd64000f81.MetaWear\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"description\": \"metawear.sensor.temperature\", \"type\": \"OBJECT\", \"guid\": \"BFA39631-3EBF-4F1B-A5B3-A9B8DA97B7A0\", \"name\": \"Temperature Sensor\", \"uri\": \"Sensor:\\\\/\\\\/metawear.sensor.temperature\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"description\": \"metawear.sensor.temperature.1\", \"isEnabled\": true, \"samplingDuration\": 4000000, \"guid\": \"14A0B0EC-9E9C-477A-B432-B31AAED76E35\", \"name\": \"Temperature Sensor Channel 1\", \"type\": \"OBJECT\", \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [], \"uri\": \"Channel:\\\\/\\\\/metawear.sensor.temperature.channel1\" }] }, { \"description\": \"metawear.sensor.heartrate\", \"type\": \"OBJECT\", \"guid\": \"BFA39631-3EBF-4F1B-A5B3-A9B8DA97B7A0\", \"name\": \"Heartrate Sensor\", \"data\": { \"timeStamp\": 0 }, \"uri\": \"Sensor:\\\\/\\\\/metawear.sensor.heartrate\", \"nodes\": [{ \"description\": \"metawear.sensor.heartrate.1\", \"isEnabled\": true, \"samplingDuration\": 7000000, \"guid\": \"14A0B0EC-9E9C-477A-B432-B31AAED76E35\", \"name\": \"Heartrate Sensor Channel 1\", \"type\": \"OBJECT\", \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [], \"uri\": \"Channel:\\\\/\\\\/metawear.sensor.heartrate.channel1\" }] }, { \"description\": \"metawear.sensor.gyroscope\", \"type\": \"OBJECT\", \"guid\": \"BFA39631-3EBF-4F1B-A5B3-A9B8DA97B7A0\", \"name\": \"Gyroscope Sensor\", \"data\": { \"timeStamp\": 0 }, \"uri\": \"Sensor:\\\\/\\\\/metawear.sensor.gyroscope\", \"nodes\": [{ \"description\": \"metawear.sensor.gyroscope.1\", \"isEnabled\": true, \"samplingDuration\": 5000000, \"guid\": \"14A0B0EC-9E9C-477A-B432-B31AAED76E35\", \"name\": \"Gyroscope Sensor Channel 1\", \"type\": \"OBJECT\", \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [], \"uri\": \"Channel:\\\\/\\\\/metawear.sensor.gyroscope.channel1\" }, { \"description\": \"metawear.sensor.gyroscope.2\", \"isEnabled\": true, \"samplingDuration\": 5000000, \"guid\": \"14A0B0EC-9E9C-477A-B432-B31AAED76E35\", \"name\": \"Gyroscope Sensor Channel 2\", \"type\": \"OBJECT\", \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [], \"uri\": \"Channel:\\\\/\\\\/metawear.sensor.gyroscope.channel2\" }, { \"description\": \"metawear.sensor.gyroscope.3\", \"isEnabled\": true, \"samplingDuration\": 5000000, \"guid\": \"14A0B0EC-9E9C-477A-B432-B31AAED76E35\", \"name\": \"Gyroscope Sensor Channel 3\", \"type\": \"OBJECT\", \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [], \"uri\": \"Channel:\\\\/\\\\/metawear.sensor.gyroscope.channel3\" }] }] }] }";
    private static final String START = "{\"name\": \"start\", \"description\": \"SensorAgent Start\",\"guid\": \"54899870-d9e8-445d-b015-00e4e68e10fd\",\"uri\": \"Method://3d0523bd64000f81.MetaWear.SensorAgent.start\",\"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"start\", \"description\": \"start param Node\",\"guid\": \"19b2a939-5201-4b6c-8a2e-bd786bb09e93\",\"uri\": \"Parameter://3d0523bd64000f81.MetaWear.SensorAgent.start.Node\",\"type\": \"PARAMETER\",\"data\": { \"dataType\": \"STRING\",\"values\": \"U2Vzc2lvbklE\",\"timeStamp\": 0 }, \"nodes\": [] }] }";
    private static final String STOP = "{\"name\":\"stop\",\"description\":\"SensorAgent Stop\",\"guid\":\"5c2f4ed5-39f9-4162-b91f-b034f38d9e9d\",\"uri\":\"Method://3d0523bd64000f81.MetaWear.SensorAgent.stop\",\"type\":\"METHOD\",\"data\":{\"timeStamp\":0},\"nodes\":[]}";
    private static final String RESET = "{\"name\":\"reset\",\"description\":\"SensorAgent Reset\",\"guid\":\"5c2f4ed5-39f9-4162-b91f-b034f38d9e9d\",\"uri\":\"Method://3d0523bd64000f81.MetaWear.SensorAgent.reset\",\"type\":\"METHOD\",\"data\":{\"timeStamp\":0},\"nodes\":[]}";
    boolean isGotDeviceConfig = false;
    String connectionStates = null;
    private Handler handler = new Handler();
    private TextView readValueView;
    private BluetoothAdapter bluetoothAdapter;
    private ArrayList<BluetoothGattCharacteristic> characteristicQueue = new ArrayList<BluetoothGattCharacteristic>();
    private IDataReadyExternal iSensorAgent;
    private SharedPreferences mSharedPrefs;
    private StringBuilder stringBuilder;
    private boolean isConnectionInvoked = false;
    private String lastMsg = "";
    private ProxyComm mProxyComm;
    private DataSenderThread dbThread;
    private DatabaseWorker databaseWorker;
    private String dbThreadName = "ZMAcitivityThread";

    public static byte[] hexStringToBytes(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[0];
        if (bytes != null) {
            hexChars = new char[bytes.length * 2];
            for (int j = 0; j < bytes.length; j++) {
                int v = bytes[j] & 0xFF;
                hexChars[j * 2] = hexArray[v >>> 4];
                hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            }
        }
        return new String(hexChars);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        mProxyComm = new ProxyComm(getApplicationContext(), null, null, this);

        readValueView = (TextView) findViewById(R.id.read_service);

        stringBuilder = new StringBuilder();

        storeDeviceConfig(SET_CONF);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE Not Supported", Toast.LENGTH_SHORT).show();
            finish();
        }
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        String macAddress = mSharedPrefs.getString(Constants.PREF_KEY_MAC_ADDRESS, "");

        if (macAddress != null && macAddress.length() > 0) {
            connect(macAddress);
        } else {
            setTitle("No device");
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (iSensorAgent == null) {

                } else {
                    if (connectionStates == null)
                        iSensorAgent.dataReadyExternal(serialize(getConnectionStates()));
                }
            }
        }, 1000);

    }

    private void storeDeviceConfig(String setConf) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), ProxyCommPlugin.FOLDER_NAME);
            boolean status = false;
            if (!root.exists()) {
                status = root.mkdirs();
            }
            File logFile = new File(root, "device_config.txt");
            if (!logFile.exists()) {
                status = logFile.createNewFile();
            } else {
                return;
            }
            FileWriter writer = new FileWriter(logFile, true);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(setConf);
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void connect(String macAddress) {
        setTitle(macAddress);
        iSensorAgent = ((ZetaApplication) getApplication()).getSensorAgent(macAddress, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.main_activity2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() != R.id.clear && item.getItemId() != R.id.choose) {
            if (iSensorAgent == null) {
                showMsg("Device is not connected", true);
                return true;
            }
        }

        switch (item.getItemId()) {
            case R.id.clear:
                stringBuilder.setLength(0);
                readValueView.setText(stringBuilder.toString());
                return true;

            case R.id.get_dev_capabilities:
                stringBuilder.setLength(0);
                readValueView.setText(stringBuilder.toString());
                iSensorAgent.dataReadyExternal(GET_DEV_CAPABILITIES);
                return true;

            case R.id.get_conf:
                stringBuilder.setLength(0);
                readValueView.setText(stringBuilder.toString());
                iSensorAgent.dataReadyExternal(GET_CONF);
                return true;

            case R.id.set_conf:
                loadDataFromFile();
                return true;

            case R.id.start:
                iSensorAgent.dataReadyExternal(START);
                checkThread();
                return true;

            case R.id.get_connection:
                iSensorAgent.dataReadyExternal(serialize(getConnectionStates()));
                return true;

            case R.id.get_state:
                iSensorAgent.dataReadyExternal(serialize(getStateNode()));
                return true;

            case R.id.stop:
                iSensorAgent.dataReadyExternal(STOP);
                clearThread();
                return true;

            case R.id.reset:
                iSensorAgent.dataReadyExternal(RESET);
                return true;
            case R.id.choose:
                startActivityForResult(new Intent(this, DeviceListActivity.class), 82);
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadDataFromFile() {

        File root = new File(Environment.getExternalStorageDirectory(), ProxyCommPlugin.FOLDER_NAME);
        File logFile = new File(root, "device_config.txt");
        BufferedReader br = null;
        String everything = null;
        try {
            br = new BufferedReader(new FileReader(logFile));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            everything = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            everything = null;
        } finally {
            if (br != null)
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        if (everything == null || everything.length() == 0)
            everything = SET_CONF;

        iSensorAgent.dataReadyExternal(everything);
    }

    private Node getPopulatedSensorAgent(String sensorAgent) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonDeserializer());
        builder.registerTypeAdapter(Channel.class, new NodeGsonDeserializer());
        Gson gsonExt = builder.create();
        Node node = gsonExt.fromJson(sensorAgent, SensorAgent.class);
        return node;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                //Bluetooth not enabled.
                finish();
                return;
            }
        }
        if (resultCode == RESULT_OK && requestCode == 82) {
            connect(data.getStringExtra(Constants.EXTRA_MAC_ADDRESS));
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private synchronized void showMsg(String msg, boolean show) {
        Log.d(TAG, msg);
        if (show) {
            handler.post(new ShowMsgRun(msg));
        }
    }

    @Override
    public void sendConnectionStatusToPlugin(ConnectionStates connectionStates) {
        Log.d(TAG, "!connected..");
    }

    @Override
    public void sendFailMessageToPlugin(String message) {
        Log.d(TAG, "!failed...");
    }

    @Override
    public void sendMessageToPlugin(String message) {
        showMsg(message, true);
    }

    /**
     * Converts json format of data to object node
     *
     * @param data input in json format
     * @return ERR_NO_ERROR if conversion is successful else throws error of type ErrorTypesZs
     */
    private Node objectify(String data) {
        Node node = null;
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Notification.class, new NodeGsonDeserializer());
        Gson gsonExt = builder.create();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(data).getAsJsonObject();
        if (jsonObject.getAsJsonPrimitive("uri") != null) {
            String uri = jsonObject.getAsJsonPrimitive("uri").getAsString();
            if (uri.startsWith(Notification.class.getSimpleName())) {
                node = gsonExt.fromJson(data, Notification.class);
            } else if (uri.startsWith(SensorAgent.class.getSimpleName())) {
                node = gsonExt.fromJson(data, Node.class);
            }
            if (node != null) {
                Log.d(TAG, "!inside objectify, node uri: " + node.getUri());
            }
        }

        return node;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        iSensorAgent = ((ZetaApplication) getApplication()).getSensorAgent(null, null);
        CommBle.BleMsgReceiver.setProxyCommPlugin(null);
    }

    public Node getStateNode() {
        Node node = new Notification();
        node.setName("getState");
        node.setDescription("The functional state of the SensorAgent");
        node.setType(NodeType.METHOD);
        node.setGuid(UUID.randomUUID());
        node.setUri("Notification://" + Constants.META_WEAR_HOSTNAME + ".SensorAgent.getState");
        Data date = new Data();
        date.setDataType(DataType.ENUMERATION);
        date.setTimeStamp(0);
        node.setData(date);
        return node;
    }

    public Node getConnectionStates() {
        Node node = new Notification();
        node.setName("getConnectionStates");
        node.setDescription("The connection state with the external device");
        node.setType(NodeType.METHOD);
        node.setGuid(UUID.randomUUID());
        node.setUri("Notification://" + Constants.META_WEAR_HOSTNAME + ".SensorAgent.getConnectionStates");
        Data date = new Data();
        date.setDataType(DataType.ENUMERATION);
        date.setTimeStamp(0);
        node.setData(date);
        return node;
    }

    /**
     * Converts node object to json format
     *
     * @param node object to be serialized
     * @return ERR_NO_ERROR if conversion is successful else throws error of type ErrorTypesZs
     */
    public String serialize(Node node) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
        if (node instanceof SensorAgent) {
            builder.registerTypeAdapter(SensorAgent.class, new NodeGsonSerializer());
        } else if (node instanceof Device) {
            builder.registerTypeAdapter(Device.class, new NodeGsonSerializer());
        } else if (node instanceof Channel) {
            builder.registerTypeAdapter(Channel.class, new NodeGsonSerializer());
        }
        Gson gsonExt = builder.create();
        String temp = gsonExt.toJson(node, Node.class);
        Log.d(TAG, "!serialized data: " + temp);
        return temp;
    }

    public void checkThread() {
        if (databaseWorker == null)
            databaseWorker = ((ZetaApplication) getApplication()).getDataBase();
        if (!databaseWorker.isEmpty()) {
            if (dbThread == null) {
                dbThread = new DataSenderThread(dbThreadName);
                dbThread.start();
                dbThread.fetchDBContent();
                try {
                    dbThread.currentThread().sleep(Constants.START_DELAY);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                if (dbThread.messageCount == 0) {
                    if (!dbThread.isAlive()) {
                        dbThread.start();
                    }
                    dbThread.fetchDBContent();
                }
            }
        } else {
            clearThread();
        }
    }

    public void clearThread() {
        if (dbThread != null) {
            dbThread.mWorkerHandler.removeCallbacksAndMessages(null);
            dbThread.quit();
            dbThread = null;
            databaseWorker = null;
        }
    }

    class ShowMsgRun implements Runnable {
        /* synthetic */ String msg;

        ShowMsgRun(String str) {
            this.msg = str;
        }

        public void run() {
            Log.d(TAG, msg);
            try {
                JSONObject notification = new JSONObject(msg);
                if (notification.getString("name").equalsIgnoreCase("SensorAgent")
                        || notification.getString("name").equalsIgnoreCase("ConnectionStates")
                        || notification.getString("name").equalsIgnoreCase("ErrorTypesZs")
                        || notification.getString("name").equalsIgnoreCase("getState")) {

                    try {
                        if (notification.getJSONObject("data") != null && notification.getJSONObject("data").getString("values") != null) {
                            byte[] value = Base64.decode(notification.getJSONObject("data").getString("values"), Base64.NO_WRAP);
                            msg = new String(value);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (notification.getString("name").equalsIgnoreCase("ConnectionStates")) {
                        handler.removeCallbacksAndMessages(null);
                    }

                    if (msg != null) {
                        if (msg.equalsIgnoreCase(ConnectionStates.CONNECTED.name())) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    iSensorAgent.dataReadyExternal(serialize(getStateNode()));
                                }
                            });
                        } else if (msg.equalsIgnoreCase(States.WAITING_CONFIGURATION.name())) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    iSensorAgent.dataReadyExternal(GET_DEV_CAPABILITIES);
                                }
                            });
                        } else if (msg.equalsIgnoreCase(States.READY.name())) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (!isGotDeviceConfig)
                                        iSensorAgent.dataReadyExternal(GET_DEV_CAPABILITIES);
                                }
                            });

                        } else if (msg.equalsIgnoreCase(States.OPERATIONAL.name())) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (!isGotDeviceConfig)
                                        iSensorAgent.dataReadyExternal(GET_DEV_CAPABILITIES);
                                }
                            });
                        } else if (notification.getString("name").equalsIgnoreCase("SensorAgent")) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    isGotDeviceConfig = true;
                                    loadDataFromFile();
                                }
                            });
                        }
                    } else if (notification.getString("name").equalsIgnoreCase("SensorAgent")) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                isGotDeviceConfig = true;
                                loadDataFromFile();
                            }
                        });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            stringBuilder.append("\n\n" + msg);
            readValueView.setText(stringBuilder.toString());
        }
    }

    public class DataSenderThread extends HandlerThread {

        private int messageCount = 0;
        private Handler mWorkerHandler;

        public DataSenderThread(String name) {
            super(name);
        }

        @Override
        public synchronized void start() {
            super.start();
            prepareHandler();
        }

        public void queueTask(String data, boolean isFromDB) {
            messageCount++;
            mWorkerHandler.obtainMessage(isFromDB ? 1 : 0, data).sendToTarget();
        }

        private void prepareHandler() {
            mWorkerHandler = new Handler(getLooper(), new Handler.Callback() {

                @Override
                public boolean handleMessage(Message msg) {
                    messageCount--;
                    String data = (String) msg.obj;
                    if (true) {
                        sendMessageToPlugin(data);
                        databaseWorker.deleteFirstRow();
                        checkThread();
                    } else {
                        clearThread();
                    }
                    return true;
                }
            });
        }

        private void fetchDBContent() {
            // TODO : Need to remove delay and create Thread at cordova layer
            if (dbThread != null) {
                queueTask(databaseWorker.getFirstRow(), true);
                try {
                    dbThread.currentThread().sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
