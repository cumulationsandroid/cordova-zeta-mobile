package com.at.zeta.zetamobile;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.at.communication.GenericBle;
import com.at.model.app.ConnectionStates;
import com.at.model.app.IDataReadyExternal;
import com.at.model.app.SensorAgent;
import com.at.model.app.States;
import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;
import com.at.model.meta.NodeType;
import com.at.model.meta.Notification;
import com.at.plugin.ProxyCommPluginCallback;
import com.at.utils.Constants;
import com.at.utils.NodeGsonSerializer;
import com.at.utils.db.DatabaseWorker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

/**
 * Created by uday on 21/12/16.
 */

public class ZetaApplication extends Application {

    private static final String TAG = "JL-ZetaApplication";
    private SharedPreferences mSharedPrefs;
    private SensorAgent iSensorAgent = null;
    private String pre_macAddress = "";
    private DatabaseWorker databaseWorker;

    public static void showToast(String msg, Context context) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    private Node initSensorAgentNode() {
        Node node = new Node();
        node.setName("SensorAgent");
        node.setType(NodeType.OBJECT);
        node.setUri(SensorAgent.class.getSimpleName() + getCommonLiteral());
        node.setDescription("SensorAgent instance");
        node.setGuid(Constants.buildUUID(node.getUri()));
        Data data = new Data();
        node.setData(data);
        node.setNodes(getMethodNodes());
        return node;
    }

    private String getCommonLiteral() {
        return Constants.SCHEME_IDENTIFIER + pre_macAddress.replace(":", "") + "." + Constants.META_WEAR_HOSTNAME + "." + SensorAgent.class.getSimpleName();
    }

    private ArrayList<Node> getMethodNodes() {
        ArrayList<Node> result = new ArrayList<Node>();
//--
        Node setConfigurationNode = new Node();
        setConfigurationNode.setName("setConfiguration");
        setConfigurationNode.setDescription("SensorAgent setConfiguration");
        setConfigurationNode.setUri("Method" + getCommonLiteral() + "." + "setConfiguration");
        setConfigurationNode.setGuid(Constants.buildUUID(setConfigurationNode.getUri()));
        setConfigurationNode.setType(NodeType.METHOD);
        setConfigurationNode.setData(new Data());

        ArrayList<Node> setConfigurationParamNodes = new ArrayList<Node>();
        Node paramSetConfiguration = new Node();
        paramSetConfiguration.setName("Node");
        paramSetConfiguration.setDescription("setConfiguration param Node");
        paramSetConfiguration.setUri("Parameter" + getCommonLiteral() + "." + "setConfiguration." + Node.class.getSimpleName());
        paramSetConfiguration.setGuid(Constants.buildUUID(paramSetConfiguration.getUri()));
        paramSetConfiguration.setType(NodeType.PARAMETER);
        paramSetConfiguration.setData(new Data());
        setConfigurationParamNodes.add(paramSetConfiguration);

        setConfigurationNode.setNodes(setConfigurationParamNodes);
        result.add(setConfigurationNode);
//--
        Node getConfigurationNode = new Node();
        getConfigurationNode.setName("getConfiguration");
        getConfigurationNode.setDescription("SensorAgent getConfiguration");
        getConfigurationNode.setUri("Method" + getCommonLiteral() + "." + "getConfiguration");
        getConfigurationNode.setGuid(Constants.buildUUID(getConfigurationNode.getUri()));
        getConfigurationNode.setData(new Data());
        getConfigurationNode.setType(NodeType.METHOD);
        result.add(getConfigurationNode);
//--
        Node startNode = new Node();
        startNode.setName("start");
        startNode.setDescription("SensorAgent start");
        startNode.setUri("Method" + getCommonLiteral() + "." + "start");
        startNode.setGuid(Constants.buildUUID(startNode.getUri()));
        startNode.setData(new Data());
        startNode.setType(NodeType.METHOD);

        Node paramStart = new Node();
        paramStart.setName("start");
        paramStart.setDescription("start param Node");
        paramStart.setUri("Parameter" + getCommonLiteral() + "." + "start." + Node.class.getSimpleName());
        paramStart.setGuid(Constants.buildUUID(paramStart.getUri()));
        paramStart.setType(NodeType.PARAMETER);
        Data startData = new Data();
        startData.setDataType(DataType.STRING);
        paramStart.setData(startData);

        ArrayList<Node> startParamNodes = new ArrayList<Node>();
        startParamNodes.add(paramStart);

        startNode.setNodes(startParamNodes);
        result.add(startNode);

//--

        Node pingNode = new Node();
        pingNode.setName("ping");
        pingNode.setDescription("SensorAgent ping");
        pingNode.setUri("Method" + getCommonLiteral() + "." + "ping");
        pingNode.setGuid(Constants.buildUUID(pingNode.getUri()));
        pingNode.setData(new Data());
        pingNode.setType(NodeType.METHOD);

        ArrayList<Node> pingParamNodes = new ArrayList<Node>();
        Node paramPing = new Node();
        paramPing.setName("ping");
        paramPing.setDescription("ping param Node");
        paramPing.setUri("Parameter" + getCommonLiteral() + "." + "ping." + Node.class.getSimpleName());
        paramPing.setGuid(Constants.buildUUID(paramPing.getUri()));
        paramPing.setType(NodeType.PARAMETER);

        Data pingData = new Data();
        pingData.setDataType(DataType.INT64);

        paramPing.setData(pingData);
        pingParamNodes.add(paramPing);
        pingNode.setNodes(pingParamNodes);

        result.add(pingNode);

//--

//--
        Node stopNode = new Node();
        stopNode.setName("stop");
        stopNode.setDescription("SensorAgent stop");
        stopNode.setUri("Method" + getCommonLiteral() + "." + "stop");
        stopNode.setGuid(Constants.buildUUID(stopNode.getUri()));
        stopNode.setType(NodeType.METHOD);
        stopNode.setData(new Data());
        result.add(stopNode);
//--
        Node heartBeat = new Node();
        heartBeat.setName("heartBeat");
        heartBeat.setDescription("SensorAgent heartBeat");
        heartBeat.setUri("Method" + getCommonLiteral() + "." + "heartBeat");
        heartBeat.setGuid(Constants.buildUUID(heartBeat.getUri()));
        heartBeat.setType(NodeType.METHOD);
        heartBeat.setData(new Data());

        ArrayList<Node> setHeartBeatParamNodes = new ArrayList<Node>();
        Node paramHeartBeat = new Node();
        paramHeartBeat.setName("Node");
        paramHeartBeat.setDescription("heartBeat param Node");
        paramHeartBeat.setUri("Parameter" + getCommonLiteral() + "." + "heartBeat." + Node.class.getSimpleName());
        paramHeartBeat.setGuid(Constants.buildUUID(paramHeartBeat.getUri()));
        paramHeartBeat.setType(NodeType.PARAMETER);
        paramHeartBeat.setData(new Data());
        setHeartBeatParamNodes.add(paramHeartBeat);

        heartBeat.setNodes(setHeartBeatParamNodes);
        result.add(heartBeat);
//--
        Node getDeviceCapabilities = new Node();
        getDeviceCapabilities.setName("getDeviceCapabilities");
        getDeviceCapabilities.setDescription("SensorAgent getDeviceCapabilities");
        getDeviceCapabilities.setUri("Method" + getCommonLiteral() + "." + "getDeviceCapabilities");
        getDeviceCapabilities.setGuid(Constants.buildUUID(getDeviceCapabilities.getUri()));
        getDeviceCapabilities.setType(NodeType.METHOD);
        getDeviceCapabilities.setData(new Data());
        result.add(getDeviceCapabilities);
//--
        Node getLoggerLevel = new Node();
        getLoggerLevel.setName("getLoggerLevel");
        getLoggerLevel.setDescription("SensorAgent getLoggerLevel");
        getLoggerLevel.setUri("Method" + getCommonLiteral() + "." + "getLoggerLevel");
        getLoggerLevel.setGuid(Constants.buildUUID(getLoggerLevel.getUri()));
        getLoggerLevel.setType(NodeType.METHOD);
        getLoggerLevel.setData(new Data());
        result.add(getLoggerLevel);
//--
        Node setLoggerLevel = new Node();
        setLoggerLevel.setName("setLoggerLevel");
        setLoggerLevel.setDescription("SensorAgent setLoggerLevel");
        setLoggerLevel.setUri("Method" + getCommonLiteral() + "." + "setLoggerLevel");
        setLoggerLevel.setGuid(Constants.buildUUID(setLoggerLevel.getUri()));
        setLoggerLevel.setType(NodeType.METHOD);
        setLoggerLevel.setData(new Data());
        result.add(setLoggerLevel);
//--
        Node notify = new Node();
        notify.setName("notify");
        notify.setDescription("SensorAgent notify");
        notify.setUri("Method" + getCommonLiteral() + "." + "notify");
        notify.setGuid(Constants.buildUUID(notify.getUri()));
        notify.setType(NodeType.METHOD);
        notify.setData(new Data());
        result.add(notify);
//--
        Node getLoggerMessage = new Node();
        getLoggerMessage.setName("getLoggerMessage");
        getLoggerMessage.setDescription("SensorAgent getLoggerMessage");
        getLoggerMessage.setUri("Method" + getCommonLiteral() + "." + "getLoggerMessage");
        getLoggerMessage.setGuid(Constants.buildUUID(getLoggerMessage.getUri()));
        getLoggerMessage.setType(NodeType.METHOD);
        getLoggerMessage.setData(new Data());
        result.add(getLoggerMessage);
//--
        Notification notificationNode = new Notification();
        notificationNode.setName("Notification");
        notificationNode.setDescription("Notification Message");
        notificationNode.setUri("Notification" + getCommonLiteral() + "." + "Notification");
        notificationNode.setGuid(Constants.buildUUID(notificationNode.getUri()));
        notificationNode.setType(NodeType.OBJECT);
        Data data = new Data();
        data.setDataType(DataType.ENUMERATION);
        notificationNode.setData(data);
        result.add(notificationNode);
//--
        Node stateNode = new Node();
        stateNode.setName("getState");
        stateNode.setDescription("SensorAgent states");
        stateNode.setType(NodeType.METHOD);
        stateNode.setUri("Method" + getCommonLiteral() + "." + "getState");
        stateNode.setGuid(Constants.buildUUID(stateNode.getUri()));
        Data date = new Data();
        date.setDataType(DataType.ENUMERATION);
        stateNode.setData(date);
        result.add(stateNode);
//--
        Node nodeReset = new Node();
        nodeReset.setName("reset");
        nodeReset.setDescription("Reset device configurations");
        nodeReset.setType(NodeType.METHOD);
        nodeReset.setUri("Method" + getCommonLiteral() + "." + "reset");
        nodeReset.setGuid(Constants.buildUUID(nodeReset.getUri()));
        nodeReset.setData(new Data());
        result.add(nodeReset);

        Node connectionStateNode = new Notification();
        connectionStateNode.setName("getConnectionStates");
        connectionStateNode.setDescription("The connection state with the external device");
        connectionStateNode.setType(NodeType.METHOD);
        connectionStateNode.setUri("Method://" + getCommonLiteral() + "." + "getConnectionStates");
        connectionStateNode.setGuid(Constants.buildUUID(connectionStateNode.getUri()));
        Data date1 = new Data();
        date1.setDataType(DataType.ENUMERATION);
        connectionStateNode.setData(date1);
        result.add(connectionStateNode);

        return result;
    }

    private Node getPopulatedSensorAgent() {
        Node node = initSensorAgentNode();
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
        Gson gsonExt = builder.create();
        String sensorJson = gsonExt.toJson(node);
        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putString(Constants.SENSOR_AGENT_SERIALIZED_DATA, sensorJson);
        editor.apply();
        return node;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }

    public IDataReadyExternal getSensorAgent(String macAddress, ProxyCommPluginCallback proxyCommPlugin) {

        /**
         * if macAddress is null then just set the new callback(even if it's null) to sensorAgent.
         */
        if (macAddress == null) {
            if (iSensorAgent != null) {
                iSensorAgent.setCallback(proxyCommPlugin);
            }
            return iSensorAgent;
        }

        /**
         * if macAddress is empty then clear the current sensorAgent
         */
        if (macAddress.length() == 0) {
            iSensorAgent = null;
            return null;
        }

        pre_macAddress = macAddress.trim();

        /**
         * if sensorAgent is null or the macAddress is new, reinitialize sensorAgent
         * else update the sensorAgent
         */
        if (iSensorAgent == null || !macAddress.equals(pre_macAddress)) {
            Node sensorAgentNode = getPopulatedSensorAgent();
            if (macAddress != null && macAddress.length() > 0) {
                iSensorAgent = new SensorAgent(this, proxyCommPlugin, sensorAgentNode, macAddress);
                Log.e(TAG, "SensorAgent is created");
            }
        } else {
            Log.e(TAG, "SensorAgent is already initialized and running");
            String connectionState = new String(iSensorAgent.getConnectionStates().getData().getValues());
            /**
             * check the connectionState of the sensorAgent
             * (by checking connectionState will help us to maintain current ticker id)
             *
             * if connectionState is CONNECTED, set the new callback and notify callback about connectionState and sensorAgentState
             * else set the new callback and notify the BLEService to initiate reconnect process
             */
            if (!connectionState.equalsIgnoreCase(ConnectionStates.CONNECTED.toString())) {
                Log.e(TAG, "GenericBle not connected");
                startService(new Intent(this, GenericBle.class).setAction(GenericBle.ACTION_CONNECT).putExtra(Constants.EXTRA_MAC_ADDRESS, pre_macAddress));
                iSensorAgent.setCallback(proxyCommPlugin);
            } else {
                iSensorAgent.setCallback(proxyCommPlugin);
                iSensorAgent.dataReadyInternal(iSensorAgent.getConnectionStates());
                iSensorAgent.dataReadyInternal(iSensorAgent.getStateNode(States.WAITING_CONFIGURATION));
            }
        }
        return iSensorAgent;
    }

    public DatabaseWorker getDataBase() {
        if (databaseWorker == null) {
            databaseWorker = new DatabaseWorker(getApplicationContext());
        }
        return databaseWorker;
    }
}
