package com.at.utils.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteFullException;
import android.database.sqlite.SQLiteOutOfMemoryException;
import android.util.Log;

import java.util.ArrayList;

/**
 * {@code DatabaseWorker} is use to do the io operation in {@link SQLiteDatabase}
 */
public class DatabaseWorker {

    private static final String TAG = "JL-"+DatabaseWorker.class.getSimpleName();
    private DataBaseHelper dbHelper;

    public DatabaseWorker(Context context) {
        dbHelper = new DataBaseHelper(context);
    }

    public long insert(String value) {
        return insert(value, TableChannelStore.CHANNEL_TABLE_NAME);
    }

    public synchronized long insert(String value, String tableName) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TableChannelStore.KEY_CHANNEL, value);
        long rowID = -1;
        try {
            rowID = db.insertOrThrow(tableName, null, values);
        } catch (Exception e) {
            if (e instanceof SQLiteOutOfMemoryException || e instanceof SQLiteFullException) {
                deleteFirstRow(tableName);
                rowID = db.insert(tableName, null, values);
            }
            e.printStackTrace();
        }
        Log.e("DATABASE", tableName + " : Inserted row id is : " + rowID);
        return rowID;
    }


    public String getChannelRowId(int rowID) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT  " +
                TableChannelStore.KEY_ID +
                ", " +
                TableChannelStore.KEY_CHANNEL +
                " FROM " +
                TableChannelStore.CHANNEL_TABLE_NAME +
                " WHERE " +
                TableChannelStore.KEY_ID +
                "=?";

        Cursor cursor = db.rawQuery(selectQuery, new String[]{String.valueOf(rowID)});
        String channelValue = "";
        int retrievedRowID = 0;
        if (cursor.moveToFirst()) {
            do {
                retrievedRowID = cursor.getInt(cursor.getColumnIndex(TableChannelStore.KEY_ID));
                channelValue = cursor.getString(cursor.getColumnIndex(TableChannelStore.KEY_CHANNEL));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return channelValue;
    }

    public ArrayList<String> getAllStoredChannels() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT  " +
                TableChannelStore.KEY_ID +
                ", " +
                TableChannelStore.KEY_CHANNEL +
                " FROM " +
                TableChannelStore.CHANNEL_TABLE_NAME;

        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<String> channels = new ArrayList<String>();
        if (cursor.moveToFirst()) {
            do {
                int retrievedRowID = cursor.getInt(cursor.getColumnIndex(TableChannelStore.KEY_ID));
                Log.e(TAG, "row id is " + retrievedRowID);
                String channelValue = cursor.getString(cursor.getColumnIndex(TableChannelStore.KEY_CHANNEL));
                channels.add(channelValue);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return channels;
    }

    public void deleteAllRow() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("delete from " + TableChannelStore.CHANNEL_TABLE_NAME);
        dbHelper.resetSqlSequence(TableChannelStore.CHANNEL_TABLE_NAME);
    }

    public void deleteRow(int rowID) {
        dbHelper.getWritableDatabase().delete(TableChannelStore.CHANNEL_TABLE_NAME, TableChannelStore.KEY_ID + "=" + rowID, null);
    }

    public String getFirstRow() {
        return getFirstRow(TableChannelStore.CHANNEL_TABLE_NAME);
    }

    public synchronized String getFirstRow(String tableName) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM " +
                tableName + " ORDER BY " +
                TableChannelStore.KEY_ID +
                " ASC LIMIT 1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        String channelValue = "";
        if (cursor.moveToFirst()) {
            do {
                int retrievedRowID;
                retrievedRowID = cursor.getInt(cursor.getColumnIndex(TableChannelStore.KEY_ID));
                Log.d(TAG, "get first row id is - " + retrievedRowID);
                channelValue = cursor.getString(cursor.getColumnIndex(TableChannelStore.KEY_CHANNEL));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return channelValue;
    }

    public String getLastRow() {
        return getLastRow(TableChannelStore.CHANNEL_TABLE_NAME);
    }

    public synchronized String getLastRow(String tableName) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM " +
                tableName + " ORDER BY " +
                TableChannelStore.KEY_ID + " DESC LIMIT 1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        String channelValue = "";
        if (cursor.moveToNext()) {
            int retrievedRowID = cursor.getInt(cursor.getColumnIndex(TableChannelStore.KEY_ID));
            Log.d(TAG, tableName + " : get row id is - " + retrievedRowID);
            channelValue = cursor.getString(cursor.getColumnIndex(TableChannelStore.KEY_CHANNEL));
        }
        cursor.close();
        return channelValue;
    }

    public void deleteLastRow() {
        deleteLastRow(TableChannelStore.CHANNEL_TABLE_NAME);
    }

    public synchronized void deleteLastRow(String tableName) {
        String selectQuery = "DELETE FROM " +
                tableName + " WHERE " +
                TableChannelStore.KEY_ID + " == (select " + TableChannelStore.KEY_ID + " from " + tableName + ""
                + " ORDER BY " + TableChannelStore.KEY_ID + " DESC LIMIT 1" + ")";
        dbHelper.getWritableDatabase().execSQL(selectQuery);
    }

    public void deleteFirstRow() {
        deleteFirstRow(TableChannelStore.CHANNEL_TABLE_NAME);
    }

    public synchronized void deleteFirstRow(String tableName) {
        String selectQuery = "DELETE FROM " +
                tableName + " WHERE " +
                TableChannelStore.KEY_ID + " == (select " + TableChannelStore.KEY_ID + " from " + tableName + ""
                + " ORDER BY " + TableChannelStore.KEY_ID + " ASC LIMIT 1" + ")";
        dbHelper.getWritableDatabase().execSQL(selectQuery);
    }

    public DataBaseHelper getDbHelper() {
        return dbHelper;
    }

    public boolean isEmpty() {
        return isEmpty(TableChannelStore.CHANNEL_TABLE_NAME);
    }

    public synchronized boolean isEmpty(String tableName) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String count = "SELECT COUNT(*) FROM " + tableName;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        mcursor.close();
        boolean result = !(icount > 0);
        if (result) {
            dbHelper.resetSqlSequence(tableName);
        }
        return result;
    }

    public void getItemCount() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String count = "SELECT COUNT(*) FROM " + TableChannelStore.CHANNEL_TABLE_NAME;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        mcursor.close();
        Log.e(TAG, "item count is " + icount);
    }
}
