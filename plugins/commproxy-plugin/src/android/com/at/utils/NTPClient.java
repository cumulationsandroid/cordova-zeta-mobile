package com.at.utils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.text.NumberFormat;
import java.util.*;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.NtpV3Packet;
import org.apache.commons.net.ntp.TimeInfo;
import org.apache.commons.net.ntp.TimeStamp;

public final class NTPClient {

  private static NTPClient ntpClient;
  private static final NumberFormat numberFormat = new java.text.DecimalFormat("0.00");

  private List<String> servers;
  private TimeStamp refNtpTime;
  private TimeStamp origNtpTime;
  private TimeStamp rcvNtpTime;
  private TimeStamp xmitNtpTime;
  private TimeStamp destNtpTime;

  private long offset = -10;
  private long delay = -10;

  NTPClient() {
    servers = new ArrayList<String>();
    //servers.add("time.windows.com");
    servers.add("0.europe.pool.ntp.org");
    servers.add("1.europe.pool.ntp.org");
    servers.add("2.europe.pool.ntp.org");
    fetchNetworkTime();
  }

  public static NTPClient getInstance() {
    if (ntpClient == null) {
      ntpClient = new NTPClient();
    }
    return ntpClient;
  }

  private void fetchNetworkTime(TimeInfo info) {
    NtpV3Packet message = info.getMessage();

    refNtpTime = message.getReferenceTimeStamp();

    // Originate Time is time request sent by client (t1)
    origNtpTime = message.getOriginateTimeStamp();

    long destTime = info.getReturnTime();
    // Receive Time is time request received by server (t2)
    rcvNtpTime = message.getReceiveTimeStamp();

    // Transmit time is time reply sent by server (t3)
    xmitNtpTime = message.getTransmitTimeStamp();

    // Destination time is time reply received by client (t4)
    destNtpTime = TimeStamp.getNtpTime(destTime);

    info.computeDetails(); // compute offset/delay if not already done
    offset = info.getOffset() == null ? 0 : info.getOffset();
    delay = info.getDelay() == null ? 0 : info.getDelay();


  }

  private void fetchNetworkTime() {

    if (servers.size() == 0) {
      System.err.println("Pass the server list");
      System.exit(1);
    }

    NTPUDPClient client = new NTPUDPClient();
    // We want to timeout if a response takes longer than 10 seconds
    client.setDefaultTimeout(10000);
    try {
      client.open();
      for (String host : servers) {
        System.out.println();
        try {
          InetAddress hostAddr = InetAddress.getByName(host);
          //System.out.println("> " + hostAddr.getHostName() + "/" + hostAddr.getHostAddress());
          TimeInfo info = client.getTime(hostAddr);
          fetchNetworkTime(info);
          break;
        } catch (IOException ioe) {
          continue;
        }
      }
    } catch (SocketException e) {
      e.printStackTrace();
    }

    client.close();
  }

  public long getOffset() {
    return offset;
  }

  public long getCorrentTime() {
    return System.currentTimeMillis() + offset;
  }
  public long getCorrentTime(long time) {
    return time + offset;
  }
}
