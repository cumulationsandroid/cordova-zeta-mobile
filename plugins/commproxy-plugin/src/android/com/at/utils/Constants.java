package com.at.utils;

import android.util.Log;

import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;
import com.at.model.meta.NodeType;
import com.at.model.meta.Notification;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.UUID;

/**
 * Created by mathan on 18/1/16.
 */
public class Constants {
    public static final String SCHEME_IDENTIFIER = "://";
    public static final String DEVICE_CONF = "{\"description\":\"This is MetaWear\",\"type\":\"OBJECT\",\"guid\":\"AECD4EB7-FBAA-4CB6-8368-212AA733E6C2\",\"name\":\"MetaWear\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"Used to acquire battery data\",\"type\":\"OBJECT\",\"guid\":\"BFA39631-3EBF-4F1B-A5B3-A9B8DA97B7A0\",\"name\":\"MetaWear Battery\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"Used to acquire battery data\",\"isEnabled\":true,\"samplingDuration\":10,\"serviceUUID\":\"0000180f-0000-1000-8000-00805f9b34fb\",\"characteristicUUID\":\"00002a19-0000-1000-8000-00805f9b34fb\",\"btOpType\":\"READ\",\"guid\":\"14A0B0EC-9E9C-477A-B432-B31AAED76E35\",\"name\":\"Metawear battery\",\"type\":\"OBJECT\",\"data\":{\"dataType\":\"FLOAT\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Channel:\\\\/\\\\/MetaWear.Battery\"}],\"uri\":\"Sensor:\\\\/\\\\/MetaWear.Battery\"}],\"uri\":\"Device:\\\\/\\\\/MetaWear.SensorAgent\"}";
    public static final String SENSOR_AGENT_SERIALIZED_DATA = "metawear_sensor_agent_serialized_data";
    public static final String DEVICE_SERIALIZED_DATA_OPERATIONAL = "metawear_device_serialized_data_operational";
    public static final String META_WEAR_HOSTNAME = "MetaWear";
    public static final String EXTRA_MAC_ADDRESS = "extra_metawear_macaddress";
    public static final String PREF_KEY_MAC_ADDRESS = "zeta_pref_macaddress";
    public static final String PREF_KEY_DEVICE_NAME = "zeta_pref_devicename";

    public static final String STOP_WEAR = "{\"name\":\"stop\",\"description\":\"SensorAgentstop\",\"guid\":\"ff1c614d-c0e8-48a6-aa36-92bf338731ab\",\"uri\":\"Method://d0186d18785598ab.LGEplatina.SensorAgent.stop\",\"type\":\"METHOD\",\"data\":{\"timeStamp\":0},\"nodes\":[]}";
    public static final String DATA = "extra_data";
    public static final String PATH = "extra_path";
    public static final long TRANSMISSION_DELAY = 500;
    public static final long CONNECTION_DELAY = 10 * 1000; // mentioned in milliseconds
    public static final long START_DELAY = 5000;
    private static final float gyroScale = 262.4f;
    private static final float tempScale = 8f;
    public static final String ACTION_FORCE_STOP = "zetaActionForceStop";
    public static final String MESSAGE_WARNING_BATTERY = "BATTERY LOW";
    public static final String MESSAGE_WARNING_STOPPED = "SESSION STOPPED DUE TO LOW BATTERY";
    public static final String MESSAGE_WARNING_STOPPED_U = "SESSION STOPPED DUE TO USER ACTION";
    public static final String ACTION_BATTERY_LEVEL = "zetaActionBatteryLevel";
    public static final String STOP_SESSION = "{ \"name\": \"stop\", \"description\": \"SensorAgent Stop\", \"guid\": \"5c2f4ed5-39f9-4162-b91f-b034f38d9e9d\", \"uri\": \"Method://AndroidWear.SensorAgent.stop\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [] }";
    /**
     * Transmission may takes milliseconds to transfer data. To make sure that we have increased transmission delay to 5 seconds
     */
    public static long TRANSMISSION_DELAY_OFFSET = 2000; // mentioned in milliseconds

    public static void logString(String TAG, String data) {
        if (data.length() > 4000) {
            Log.v(TAG, "sb.length = " + data.length());
            int chunkCount = data.length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= data.length()) {
                    Log.v(TAG, data.substring(4000 * i));
                } else {
                    Log.v(TAG, data.substring(4000 * i, max));
                }
            }
        } else {
            Log.v(TAG, data);
        }
    }

    public static Node buildErrorNode(String state) {
        Node node = new Notification();
        node.setName("ErrorTypesZs");
        node.setDescription("The error type");
        node.setType(NodeType.OBJECT);
        node.setGuid(UUID.randomUUID());
        node.setUri("Notification://" + Constants.META_WEAR_HOSTNAME + ".SensorAgent.ErrorTypesZs");
        Data date = new Data();
        date.setDataType(DataType.ENUMERATION);
        date.setTimeStamp(System.currentTimeMillis());
        date.setValues(state.getBytes());
        node.setData(date);
        return node;
    }


    /**
     * Converts node object to json format
     *
     * @param node object to be serialized
     * @return ERR_NO_ERROR if conversion is successful else throws error of type ErrorTypesZs
     */
    public static String serialize(Node node) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
        Gson gsonExt = builder.create();
        String temp = gsonExt.toJson(node, Node.class);
        Log.d("Constants.serialize", "!serialized data: " + temp);
        return temp;
    }

    public static UUID buildUUID(String uri) {
        return UUID.nameUUIDFromBytes(uri.getBytes());
    }

    /**
     * for more check com.mbientlab.metawear.module.Bmi160Gyro.java
     *
     * @param x
     * @return
     */
    public static float scaleGyro(short x) {
        float value = ((x) / gyroScale);
        return value;
    }

    /**
     * for more check com.mbientlab.metawear.data.TemperatureMessage.java
     *
     * @param x
     * @return
     */
    public static float scaleTemperature(short x) {
        float value =  (x / tempScale);
        return value;
    }
}
