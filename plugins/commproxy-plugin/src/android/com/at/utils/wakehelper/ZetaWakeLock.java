package com.at.utils.wakehelper;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.at.zeta.zetamobile.R;
import com.at.communication.GenericBle;

/**
 * Created by uday on 9/6/16.
 */
public class ZetaWakeLock {

    public static final int NOTIFICATION_ID = 82;
    public static final String TAG = "ZetaWakeLock";
    private static final String NAME = "com.at.utils.wakehelper.ZetaWakeLock";
    private static PowerManager.WakeLock lockStatic;

    /**
     * This method will return the Singleton {@code PowerManager.WakeLock}
     *
     * @param context Context must be ApplicationContext
     * @return {@code PowerManager.WakeLock}
     */
    synchronized public static PowerManager.WakeLock getLock(Context context) {
        if (lockStatic == null) {
            PowerManager mgr = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            lockStatic = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, NAME);
            lockStatic.setReferenceCounted(true);
        }
        return (lockStatic);
    }

    public static Notification createNotification(Context context) {

        Intent viewIntent = new Intent(context, GenericBle.class);
        viewIntent.setAction(GenericBle.ACTION_FORCE_STOP_READING);
        PendingIntent viewPendingIntent = PendingIntent.getService(context, 0, viewIntent, 0);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.icon)
                        .setContentTitle("Metawear")
                        .setContentText("Sensor measuring started")
                        .setOngoing(true)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .addAction(0, "Stop", viewPendingIntent);

        return notificationBuilder.build();
    }


    public static void cancelNotification(Context context) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancel(NOTIFICATION_ID);
    }
}
