package com.at.model.meta;

/**
 * Data collected for each @Channel of the @Sensor device
 */
public class Data {

    private long timeStamp;
    private DataType dataType;
    private byte[] values;


    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public byte[] getValues() {
        return values;
    }

    public void setValues(byte[] values) {
        this.values = values;
    }
}