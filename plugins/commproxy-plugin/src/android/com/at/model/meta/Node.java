package com.at.model.meta;

import java.util.ArrayList;
import java.util.UUID;

public class Node {
    private static final String TAG = Node.class.getSimpleName();
    private String name;
    private String description;
    private UUID guid;
    private String uri;
    private NodeType type;
    private Data data;
    private ArrayList<? extends Node> nodes = new ArrayList<Node>();

    public UUID getGuid() {
        return guid;
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NodeType getType() {
        return type;
    }

    public void setType(NodeType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public ArrayList<? extends Node> getNodes() {
        return nodes;
    }

    public void setNodes(ArrayList<? extends Node> nodes) {
        this.nodes = nodes;
    }
}