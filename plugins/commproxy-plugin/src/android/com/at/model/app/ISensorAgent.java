package com.at.model.app;

import com.at.model.meta.Node;

public interface ISensorAgent {
    public void dataReadyInternal(Node node);
    public void dataReadyExternal(String data);
}