package com.at.communication;

import com.at.model.app.ConnectionRole;
import com.at.model.app.ConnectionStates;

public abstract class Proxy {

    private ConnectionRole connectionRole;
    private ConnectionStates connectionState;

    public abstract void dataReceive(String data);
    public abstract void dataTransmit(String data);

    public ConnectionRole getConnectionRole() {
        return connectionRole;
    }

    public void setConnectionRole(ConnectionRole connectionRole) {
        this.connectionRole = connectionRole;
    }

    public ConnectionStates getConnectionState() {
        return connectionState;
    }

    public void setConnectionState(ConnectionStates connectionState) {
        this.connectionState = connectionState;
    }
}