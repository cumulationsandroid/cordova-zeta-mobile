package com.at.data;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothGattCallback;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import com.at.communication.ProxyComm;
import com.at.model.app.IDataReadyExternal;
import com.at.model.app.INotify;
import com.at.model.domain.Device;
import com.at.model.meta.Node;
import com.at.utils.Constants;
import com.at.utils.NodeGsonDeserializer;
import com.at.utils.NodeGsonSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class DataAcquisition extends BluetoothGattCallback {

    private static final String TAG = "JL-" + DataAcquisition.class.getSimpleName();
    private static final String DEVICE_CAPABILITIES = "{\"description\":\"SensorAgent instance\",\"type\":\"OBJECT\",\"guid\":\"54034A1C-E18B-4002-B0E4-E25F969AF5C6\",\"name\":\"SensorAgent\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"SensorAgent setConfiguration\",\"type\":\"METHOD\",\"guid\":\"D3E5DD86-F8A1-4F82-B6F2-ED8BA9C45C72\",\"name\":\"setConfiguration\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"setConfiguration param Node\",\"type\":\"PARAMETER\",\"guid\":\"03343B36-A4C0-4D59-848C-561AEE1AC58F\",\"name\":\"Node\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Parameter:\\\\/\\\\/BPMonitor.SensorAgent.setConfigurationNode\"}],\"uri\":\"Method:\\\\/\\\\/BPMonitor.SensorAgent.setConfiguration\"},{\"description\":\"SensorAgent getConfiguration\",\"type\":\"METHOD\",\"guid\":\"804E1D1F-2A92-459C-8BE8-56E6F116F2F0\",\"name\":\"getConfigurationNode\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\\\/\\\\/BPMonitor.SensorAgent.getConfiguration\"},{\"description\":\"SensorAgent start\",\"type\":\"METHOD\",\"guid\":\"54899870-D9E8-445D-B015-00E4E68E10FD\",\"name\":\"start\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\\\/\\\\/BPMonitor.SensorAgent.start\"},{\"description\":\"SensorAgent stop\",\"type\":\"METHOD\",\"guid\":\"A526C677-21CC-4167-97E6-52815CFB1C0E\",\"name\":\"stop\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\\\/\\\\/BPMonitor.SensorAgent.stop\"},{\"description\":\"SensorAgent heartBeat\",\"type\":\"METHOD\",\"guid\":\"9EB51B03-AFEB-4751-AC84-CCAA4AA0FDD0\",\"name\":\"heartBeat\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"heartBeat param Node\",\"type\":\"PARAMETER\",\"guid\":\"19B2A939-5201-4B6C-8A2E-BD786BB09E93\",\"name\":\"Node\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Parameter:\\\\/\\\\/BPMonitor.SensorAgent.heartBeat.Node\"}],\"uri\":\"Method:\\\\/\\\\/BPMonitor.SensorAgent.heartBeat\"},{\"description\":\"SensorAgent getDeviceCapabilities\",\"type\":\"METHOD\",\"guid\":\"68C3B8BA-A77C-460B-8986-480AA8A48791\",\"name\":\"getDeviceCapabilities\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\\\/\\\\/BPMonitor.SensorAgent.getDeviceCapabilities\"},{\"description\":\"SensorAgent getLoggerLevel\",\"type\":\"METHOD\",\"guid\":\"1365FFD7-AA25-4EAC-835F-57BDF46D06C6\",\"name\":\"getLoggerLevel\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\\\/\\\\/BPMonitor.SensorAgent.getLoggerLevel\"},{\"description\":\"SensorAgent setLoggerLevel\",\"type\":\"METHOD\",\"guid\":\"D200A879-967D-48F6-914F-D58AA1C31C08\",\"name\":\"setLoggerLevel\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\\\/\\\\/BPMonitor.SensorAgent.setLoggerLevel\"},{\"description\":\"SensorAgent notify\",\"type\":\"METHOD\",\"guid\":\"D0C1824A-83D1-469A-9F4A-C18A4CFBE362\",\"name\":\"notify\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\\\/\\\\/BPMonitor.SensorAgent.notify\"},{\"description\":\"SensorAgent getLoggerMessage\",\"type\":\"METHOD\",\"guid\":\"6F282CC0-69CB-4D57-A39D-12CE704D4734\",\"name\":\"getLoggerMessage\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\\\/\\\\/BPMonitor.SensorAgent.getLoggerMessage\"},{\"description\":\"Notification Message\",\"type\":\"OBJECT\",\"guid\":\"A8952D8B-09F0-44B5-9017-5B83B1F4FC5F\",\"name\":\"Notification\",\"data\":{\"dataType\":\"ENUMERATION\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Notification:\\/\\/BPMonitor.SensorAgent.Notification\"}],\"uri\":\"SensorAgent:\\/\\/BPMonitor.SensorAgent\"}";
    private static final String DEVICE_CONF = "{\"description\":\"This is Digital BP Monitor\",\"type\":\"OBJECT\",\"guid\":\"AECD4EB7-FBAA-4CB6-8368-212AA733E6C2\",\"name\":\"Digital BP Monitor\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"Used to acquire blood pressure data\",\"type\":\"OBJECT\",\"guid\":\"BFA39631-3EBF-4F1B-A5B3-A9B8DA97B7A0\",\"name\":\"BP Monitor\",\"data\":{\"dataType\":\"BASE_DATATYPE\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"Used to acquire systolic data\",\"isEnabled\":true,\"samplingDuration\":10,\"guid\":\"14A0B0EC-9E9C-477A-B432-B31AAED76E35\",\"name\":\"BP Monitor Systolic\",\"type\":\"OBJECT\",\"data\":{\"dataType\":\"FLOAT\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Channel:\\\\/\\\\/BPMonitor.SensorAgent.Systolic\"},{\"description\":\"Used to acquire diastolic data . 2\",\"isEnabled\":true,\"samplingDuration\":10,\"guid\":\"8B4D265C-DA9D-4853-ACAF-C68510BB3B75\",\"name\":\"BP Monitor Diastolic\",\"type\":\"OBJECT\",\"data\":{\"dataType\":\"FLOAT\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Channel:\\\\/\\\\/BPMonitor.SensorAgent.Diastolic\"},{\"description\":\"Used to acquire pulse rate\",\"isEnabled\":true,\"samplingDuration\":10,\"guid\":\"158E5C7A-3834-4E82-B80D-40839DA5DFD8\",\"name\":\"BP Monitor pulse rate\",\"type\":\"OBJECT\",\"data\":{\"dataType\":\"FLOAT\",\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Channel:\\\\/\\\\/BPMonitor.SensorAgent.Pulserate\"}],\"uri\":\"Sensor:\\\\/\\\\/BPMonitor\"}],\"uri\":\"Device:\\\\/\\\\/BPMonitor.SensorAgent\"}";

    private ProxyComm iComm;
    private Device mDevice;
    private IDataReadyExternal iDataReadyInternal;
    private INotify iNotify;
    private SharedPreferences mSharedPrefs;
    private Context mContext;
    private String mDeviceId;

    public DataAcquisition() {

    }

    public DataAcquisition(Context context, INotify notify, IDataReadyExternal dataReadyInternal, String macAddress) {
        mDeviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        mContext = context;
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        iDataReadyInternal = dataReadyInternal;
        iNotify = notify;
        populatedDevice();
    }

    private String getCommonLiteral() {
        return Constants.SCHEME_IDENTIFIER + mDeviceId + "" + "." + Build.MANUFACTURER + Build.PRODUCT;
    }

    private void populatedDevice() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonDeserializer());
        Gson gsonExt = builder.create();
        mDevice = gsonExt.fromJson(Constants.DEVICE_CONF, Device.class);
        String deviceJson = new Gson().toJson(mDevice);
        logString("Converted Device" + deviceJson);
    }

    private void logString(String data) {
        if (data.length() > 4000) {
            Log.v(TAG, "sb.length = " + data.length());
            int chunkCount = data.length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= data.length()) {
                    Log.v(TAG, data.substring(4000 * i));
                } else {
                    Log.v(TAG, data.substring(4000 * i, max));
                }
            }
        } else {
            Log.v(TAG, data);
        }
    }

    public Device getDevice() {
        return mDevice;
    }

    public void setDevice(Device device) {
        mDevice = device;

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
        Gson gsonExt = builder.create();

        String deviceJson = gsonExt.toJson(mDevice);
        logString(deviceJson);
    }

    public void start(Node node) {
        if (mDevice != null) {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
            builder.registerTypeAdapter(Device.class, new NodeGsonSerializer());
            Gson gsonExt = builder.create();
            String temp = gsonExt.toJson(mDevice, Device.class);
            Log.d(TAG, "!serialized data: " + temp);
            iComm.transmitData(temp);
        }
    }

    public void stop() {

    }

    public void resetDevice() {

    }
}
