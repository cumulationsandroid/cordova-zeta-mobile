package com.at.data;

import android.content.Context;

import com.at.model.app.IDataReadyInternal;
import com.at.model.app.INotify;
import com.at.model.domain.Device;
import com.at.model.meta.Node;

/**
 * Created by mathan on 4/12/15.
 */
public class ProxyDataAcquisition implements IDataAcq {
    private DataAcquisition dataAcquisition;

    public ProxyDataAcquisition(Context context, INotify notify, IDataReadyInternal iDataReadyInternal, String macAddress) {
        dataAcquisition = new DataAcquisitionMW2(context, notify, iDataReadyInternal, macAddress);
    }

    @Override
    public Device getDevice() {
        return dataAcquisition.getDevice();
    }

    @Override
    public void setDevice(Device device) {
        dataAcquisition.setDevice(device);
    }

    @Override
    public void start(Node node) {
        dataAcquisition.start(node);
    }

    @Override
    public void stop() {
        dataAcquisition.stop();
    }

    @Override
    public void reset() {
        dataAcquisition.resetDevice();
    }
}
