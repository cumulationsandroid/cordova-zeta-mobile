package com.at.data;

import com.at.model.domain.Device;
import com.at.model.meta.Node;

/**
 * Created by mathan on 4/12/15.
 */
public interface IDataAcq {
    Device getDevice();
    void setDevice(Device device);
    void start(Node node);
    void stop();
    void reset();
}