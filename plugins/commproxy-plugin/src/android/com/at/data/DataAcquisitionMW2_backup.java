package com.at.data;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.at.communication.GenericBle;
import com.at.model.app.ConnectionStates;
import com.at.model.app.IDataReadyInternal;
import com.at.model.app.INotify;
import com.at.model.domain.Channel;
import com.at.model.domain.Device;
import com.at.model.domain.Sensor;
import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class DataAcquisitionMW2_backup extends DataAcquisition {

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static final int SHOW_TOAST = 0;
    private final Context mContext;
    private final IDataReadyInternal iDataReadyInternal;
    private final INotify iNotify;
    private GenericBle genericBle;
    private final BluetoothDevice bluetoothDevice;
    private final BluetoothManager bluetoothManager;
    private final SharedPreferences mSharedPrefs;
    private final String macAddress;
    private String TAG = DataAcquisitionMW2_backup.class.getSimpleName();
    private Device mDevice = new Device();
    private MainHandler mainHandler;
    private LinkedList<ArrayList<Channel>> tickerChannels = new LinkedList<ArrayList<Channel>>();
    private LinkedList<CharacteristicData> characteristicDataList = new LinkedList<CharacteristicData>();
    private HashMap<ArrayList<Channel>, Integer> tickerChannelMap = new HashMap<ArrayList<Channel>, Integer>();
    private Runnable dataWriter = new Runnable() {
        @Override
        public void run() {
            CharacteristicData characteristicData = characteristicDataList.pollFirst();
            if (characteristicData != null) {
                writeCharacteristic(characteristicData.data);
                mainHandler.postDelayed(dataWriter, 1000);
            }
        }
    };
    private Object populatedSensorChannel;

    public DataAcquisitionMW2_backup(Context context, INotify notify, IDataReadyInternal dataReadyInternal, String macAddress) {
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);

        mContext = context;
        iDataReadyInternal = dataReadyInternal;
        iNotify = notify;

        mainHandler = new MainHandler(Looper.getMainLooper());
//        genericBle = new GenericBle(this);

        bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();

        /*macAddress = "EC:62:30:C2:BD:FE";
        macAddress = "D8:40:55:8E:56:77";*/
        this.macAddress = macAddress;
        bluetoothDevice = bluetoothAdapter.getRemoteDevice(macAddress);
        iNotify.notify(ConnectionStates.CONNECTING);
//        bluetoothDevice.connectGatt(context, false, genericBle);
        // startService

    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[0];
        if (bytes != null) {
            hexChars = new char[bytes.length * 2];
            for (int j = 0; j < bytes.length; j++) {
                int v = bytes[j] & 0xFF;
                hexChars[j * 2] = hexArray[v >>> 4];
                hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            }
        }
        return new String(hexChars);
    }

    @Override
    public Device getDevice() {
        return mDevice;
    }

    @Override
    public void setDevice(Device device) {
        mDevice = device;
        tickerChannels.clear();
        processDevice();
        enableNextChannel();
        //service setDevice
    }

    public void start(Node node) {
        //service start
        int connectionState = bluetoothManager.getConnectionState(bluetoothDevice, BluetoothProfile.GATT);
        if (connectionState == BluetoothGatt.STATE_CONNECTED) {
            Object[] tickerIDs = tickerChannelMap.keySet().toArray(new Object[0]);
            if (tickerIDs != null) {
                for (int i = 0; i < tickerIDs.length; i++) {
                    ArrayList<Channel> channels = (ArrayList<Channel>) tickerIDs[i];
                    if (channels.get(0).getUri().matches("(.*)Gyro(.*)")) {

                        CharacteristicData characteristicData = new CharacteristicData();
                        characteristicData.data = new byte[]{0x09, 0x03, 0x01};
                        characteristicDataList.add(characteristicData);

                        characteristicData = new CharacteristicData();
                        characteristicData.data = new byte[]{0x09, 0x07, tickerChannelMap.get(tickerIDs[i]).byteValue(), 0x01};
                        characteristicDataList.add(characteristicData);

                        characteristicData = new CharacteristicData();
                        characteristicData.data = new byte[]{0x13, 0x02, 0x01, 0x00};
                        characteristicDataList.add(characteristicData);

                        characteristicData = new CharacteristicData();
                        characteristicData.data = new byte[]{0x13, 0x01, 0x01};
                        characteristicDataList.add(characteristicData);

                        continue;
                    }

                    CharacteristicData characteristicData = new CharacteristicData();
                    characteristicData.data = new byte[]{0x0C, 0x03, tickerChannelMap.get(tickerIDs[i]).byteValue()};
                    characteristicDataList.add(characteristicData);

                }
            }
            if (characteristicDataList.size() > 0) {
                mainHandler.post(dataWriter);
            }
        } else {
            mainHandler.obtainMessage(SHOW_TOAST, "Device not connected").sendToTarget();
        }
    }

    public void stop() {
        //service stop
        int connectionState = bluetoothManager.getConnectionState(bluetoothDevice, BluetoothProfile.GATT);
        if (connectionState == BluetoothGatt.STATE_CONNECTED) {
            Object[] tickerIDs = tickerChannelMap.keySet().toArray(new Object[0]);
            if (tickerIDs != null) {
                for (int count = 0; count < tickerIDs.length; count++) {
                    ArrayList<Channel> channels = (ArrayList<Channel>) tickerIDs[count];
                    if (channels.get(0).getUri().matches("(.*)Gyro(.*)")) {

                        CharacteristicData characteristicData = new CharacteristicData();
                        characteristicData.data = new byte[]{0x13, 0x1, 0x0};
                        characteristicDataList.add(characteristicData);

                        characteristicData = new CharacteristicData();
                        characteristicData.data = new byte[]{0x13, 0x2, 0x0, 0x1};
                        characteristicDataList.add(characteristicData);

                        characteristicData = new CharacteristicData();
                        characteristicData.data = new byte[]{0x09, 0x07, tickerChannelMap.get(tickerIDs[count]).byteValue(), 0x00};
                        characteristicDataList.add(characteristicData);

                        characteristicData = new CharacteristicData();
                        characteristicData.data = new byte[]{0x09, 0x03, 0x00};
                        characteristicDataList.add(characteristicData);

//                        characteristicData = new CharacteristicData();
//                        characteristicData.data = new byte[]{0x09, 0x06, 0x06};
//                        characteristicDataList.add(characteristicData);

                        continue;
                    }
                    CharacteristicData characteristicData = new CharacteristicData();
                    characteristicData.data = new byte[]{0x0C, 0x04, tickerChannelMap.get(tickerIDs[count]).byteValue()};
                    characteristicDataList.add(characteristicData);
                }
            }

            if (characteristicDataList.size() > 0) {
                mainHandler.post(dataWriter);
            }
        } else {
            mainHandler.obtainMessage(SHOW_TOAST, "Device not connected").sendToTarget();
        }
    }

    public void resetDevice() {
        byte[] data = new byte[]{(byte) 0xFE, 0x01};
        writeCharacteristic(data);
    }

    private void processDevice() {
        if (mDevice != null) {
            ArrayList<Sensor> sensors = (ArrayList<Sensor>) mDevice.getNodes();
            if (sensors != null) {
                for (int i = 0; i < sensors.size(); i++) {
                    processSensor(sensors.get(i));
                }
            }
        }
    }

    private void processSensor(Sensor sensor) {
        if (sensor != null) {
            ArrayList<Channel> channels = (ArrayList<Channel>) sensor.getNodes();
            ArrayList<Channel> channelGroup = new ArrayList<Channel>();
            if (channels != null) {
                for (int i = 0; i < channels.size(); i++) {
                    boolean isEnabled = processChannel(channels.get(i));
                    if (isEnabled) {
                        channelGroup.add(channels.get(i));
                    }
                }

                if (channelGroup.size() > 0) {
                    tickerChannels.addFirst(channelGroup);
                }
            }
        }
    }

    private boolean processChannel(Channel channel) {
        if (channel != null && channel.getUri() != null && channel.isEnabled()) {
            return true;
        }
        return false;
    }

    private void enableNextChannel() {
        if (tickerChannels.peekFirst() == null)
            return;
        Channel channel = tickerChannels.peekFirst().get(0);
        if (channel != null) {
            if (channel.getUri() != null && channel.getUri().trim().endsWith("Heartrate")) {
                togglePulseOptics(true);
            } else if (channel.getUri() != null && channel.getUri().trim().endsWith("Temperature")) {
                enableTimerForChannel(channel);
            } else if (channel.getUri().trim().matches("(.*)Gyro(.*)")) {
                // inti GY(step1)
                enableTimerForGyroChannel(channel);
            }
        }
    }

    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        Log.d(TAG, "!inside onConnectionStateChange, status: " + status +
                " , new state: " + newState);
        if (status == 0) {
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    tickerChannelMap.clear();
                    gatt.discoverServices();
                    iNotify.notify(ConnectionStates.CONNECTED);
                    break;

                case BluetoothProfile.STATE_DISCONNECTED:
                    tickerChannelMap.clear();
                    iNotify.notify(ConnectionStates.DISCONNECTED);
                    break;

                default:
                    iNotify.notify(ConnectionStates.CONNECTIONERROR);
                    break;
            }
        } else {
            iNotify.notify(ConnectionStates.CONNECTIONERROR);
            mainHandler.obtainMessage(SHOW_TOAST, "Connection state change failed with status: " + status).sendToTarget();
        }
    }

    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        List<BluetoothGattService> services = gatt.getServices();
        Log.d(TAG, "!inside onServicesDiscovered, services count: " + services.size() + " , status: " + status);
        enableNotification1();
    }

    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        byte[] characteristicValue = characteristic.getValue();
        Log.d(TAG, "!inside onCharacteristicRead, service uuid: " + characteristic.getService().getUuid() +
                " , characteristic uuid: " + characteristic.getUuid() + " , hex bytes: " +
                bytesToHex(characteristicValue));
    }

    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        byte[] characteristicValue = characteristic.getValue();
        Log.d(TAG, "!inside onCharacteristicWrite, service uuid: " + characteristic.getService().getUuid() +
                " , characteristic uuid: " + characteristic.getUuid() + " , hex bytes: " +
                bytesToHex(characteristicValue));

        if (characteristicValue[0] == 0x0A && characteristicValue[1] == 0x02) {
            // enable (HS step4)
            writeChannelParam();
        } else if (characteristicValue.length == 3 && characteristicValue[0] == 0x05 &&
                characteristicValue[1] == 0x02 && characteristicValue[2] == 0x01) {
            Channel channel = tickerChannels.peekFirst().get(0);
            // enable (HS step2)
            enableTimerForChannel(channel);
        } else if (characteristicValue.length == 4 && characteristicValue[0] == 0X09 &&
                characteristicValue[1] == 0x07 && characteristicValue[2] == 0x00 &&
                characteristicValue[3] == 0x01) {
            Log.d(TAG, "set tickerID GY(step2) done");
//            enableNextChannel();
        }
    }

    // inti GY (step1)
    private void enableTimerForGyroChannel(Channel channel) {

        CharacteristicData characteristicData = new CharacteristicData();
        characteristicData.data = new byte[]{0x13, 0x3, 0x26, 0x0};
        characteristicDataList.add(characteristicData);

        characteristicData = new CharacteristicData();
        characteristicData.data = new byte[]{0x13, 0x3, 0x26, 0x4};
        characteristicDataList.add(characteristicData);

        byte[] duration = ByteBuffer.allocate(2).putShort((short) channel.getSamplingDuration()).array();

        /****** enabling the gyro with ticker id ******/
        characteristicData = new CharacteristicData();
        characteristicData.data = new byte[]{0x09, 0x02, 0x13, 0x05, (byte) 0xFF, (byte) 0xA0, 0x08, 0x05, duration[1], duration[0], 0x00, 0x00};
        characteristicDataList.add(characteristicData);

        mainHandler.post(dataWriter);
    }

    // set tickerID (GY step2)
    private void setTickerToGyroModule(byte param) {
        CharacteristicData characteristicData = new CharacteristicData();
        characteristicData.data = new byte[]{0x09, 0x03, 0x01};
        characteristicDataList.add(characteristicData);

        characteristicData = new CharacteristicData();
        characteristicData.data = new byte[]{0x09, 0x07, param, 0x01};
        characteristicDataList.add(characteristicData);

        mainHandler.post(dataWriter);
    }

    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        byte[] value = characteristic.getValue();
        Log.d(TAG, "!inside onCharacteristicChanged, service uuid: " + characteristic.getService().getUuid() +
                " , characteristic uuid: " + characteristic.getUuid() + " , hex bytes: " +
                bytesToHex(value));

        long curTimeStamp = System.currentTimeMillis();
        if (value.length == 3 && value[0] == 0x09 && value[1] == 0x02) {
            //set tickerID (GY step2)
            tickerChannelMap.put(tickerChannels.pollFirst(), (int) value[2]);
            enableNextChannel();
        } else if (value.length == 3 && value[0] == 0x0C && value[1] == 0x02) {
            // enable (HS step3)
            enableChannel(value[2]);
        } else if (value.length == 3 && value[0] == 0x0A && value[1] == 0x02) {
            // Temperature sensor is enabled with ticker ID
            // Temperature sensor is enabled with ticker ID (HS step5)
            tickerChannelMap.put(tickerChannels.pollFirst(), (int) value[2]);
            enableNextChannel();
        } else if (value.length == 5 && value[0] == 0x04 && value[1] == (byte) 0x81) {
            // Temperature sensor's output
            byte[] data = new byte[2];
            data[0] = value[4];
            data[1] = value[3];
            Channel channel = getChannelForRegister(value[1]).get(0);
            if (channel != null) {
                Channel result = populateChannel(channel, data, curTimeStamp);
                iDataReadyInternal.dataReadyInternal(result);
            }
        } else if (value.length == 5 && value[0] == 0x05 && value[1] == (byte) 0x87) {
            // Pulse sensor's output
            byte[] data = new byte[2];
            data[0] = value[4];
            data[1] = value[3];
            Channel channel = getChannelForRegister(value[1]).get(0);
            if (channel != null) {
                Channel result = populateChannel(channel, data, curTimeStamp);
                iDataReadyInternal.dataReadyInternal(result);
            }
        } else if (value.length == 9 && value[0] == 0x09 && value[1] == 0x03) {
            // Gyro sensor's output

            ArrayList<Channel> channels = getChannelForRegister(value[0]);

            for (int i = 0; i < channels.size(); i++) {
                Channel channel = channels.get(i);
                if (channel != null && channel.isEnabled() && channel.getUri() != null) {
                    if (channel.getUri().endsWith("Gyro.x")) {
                        byte[] data = new byte[2];
                        data[0] = value[4];
                        data[1] = value[3];

                        int samplingDuration = channel.getSamplingDuration();
                        long timeStamp = channel.getData() != null ? channel.getData().getTimeStamp() : 0;
                        long curTime = System.currentTimeMillis();
                        if (curTime - timeStamp > samplingDuration) {
                            Log.d(TAG, "!sampling duration: " + samplingDuration + " , time stamp: " +
                                    timeStamp + " , cur time: " + curTime);
                            Channel result = populateChannel(channel, data, curTimeStamp);
                            iDataReadyInternal.dataReadyInternal(result);
                        }
                    } else if (channel.getUri().endsWith("Gyro.y")) {
                        byte[] data = new byte[2];
                        data[0] = value[6];
                        data[1] = value[5];

                        int samplingDuration = channel.getSamplingDuration();
                        long timeStamp = channel.getData() != null ? channel.getData().getTimeStamp() : 0;
                        long curTime = System.currentTimeMillis();
                        if (curTime - timeStamp > samplingDuration) {
                            Log.d(TAG, "!sampling duration: " + samplingDuration + " , time stamp: " +
                                    timeStamp + " , cur time: " + curTime);
                            Channel result = populateChannel(channel, data, curTimeStamp);
                            iDataReadyInternal.dataReadyInternal(result);
                        }
                    } else if (channel.getUri().endsWith("Gyro.z")) {
                        byte[] data = new byte[2];
                        data[0] = value[8];
                        data[1] = value[7];

                        int samplingDuration = channel.getSamplingDuration();
                        long timeStamp = channel.getData() != null ? channel.getData().getTimeStamp() : 0;
                        long curTime = System.currentTimeMillis();
                        if (curTime - timeStamp > samplingDuration) {
                            Log.d(TAG, "!sampling duration: " + samplingDuration + " , time stamp: " +
                                    timeStamp + " , cur time: " + curTime);
                            Channel result = populateChannel(channel, data, curTimeStamp);
                            iDataReadyInternal.dataReadyInternal(result);
                        }
                    }
                }
            }
        }
    }

    public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        Log.d(TAG, "!inside onDescriptorRead, status: " + status);
    }

    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        BluetoothGattCharacteristic characteristic = descriptor.getCharacteristic();
        BluetoothGattService service = characteristic.getService();
        Log.d(TAG, "!inside onDescriptorWrite, service uuid: " + service.getUuid() +
                " , characteristic uuid: " + characteristic.getUuid() + " , descriptor uuid: " +
                descriptor.getUuid() + " , characteristic hex bytes: " + bytesToHex(characteristic.getValue()) +
                " , descriptor hex bytes: " + bytesToHex(descriptor.getValue()));

        if (service.getUuid().toString().equalsIgnoreCase("0000180f-0000-1000-8000-00805f9b34fb") &&
                characteristic.getUuid().toString().equalsIgnoreCase("00002a19-0000-1000-8000-00805f9b34fb")) {
            enableNotification2();
        } else if (service.getUuid().toString().equalsIgnoreCase("326a9000-85cb-9195-d9dd-464cfbbae75a") &&
                characteristic.getUuid().toString().equalsIgnoreCase("326a9006-85cb-9195-d9dd-464cfbbae75a")) {
            // need to enable timer after this, enable timer is done after setConfiguration is done
        }
    }

    public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
        Log.d(TAG, "!inside onReliableWriteCompleted, status: " + status);
    }

    public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
        Log.d(TAG, "!inside onReadRemoteRssi, status: " + status);
    }

    public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
        Log.d(TAG, "!inside onMtuChanged, status: " + status);
    }

    private void enableNotification1() {
        UUID serviceUuid = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
        BluetoothGattService gattService = genericBle.getService(serviceUuid);
        if (gattService != null) {
            BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(
                    UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb"));
            if (gattCharacteristic != null) {
                boolean status = genericBle.enableCharNotification(gattCharacteristic);
            }
        }
    }

    private void enableNotification2() {
        UUID serviceUuid = UUID.fromString("326a9000-85cb-9195-d9dd-464cfbbae75a");
        BluetoothGattService gattService = genericBle.getService(serviceUuid);
        if (gattService != null) {
            BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(
                    UUID.fromString("326a9006-85cb-9195-d9dd-464cfbbae75a"));
            if (gattCharacteristic != null) {
                boolean status = genericBle.enableCharNotification(gattCharacteristic);
            }
        }
    }

    private void enableTimerForChannel(Channel channel) {
        // enable (HS step2)
        byte[] duration = ByteBuffer.allocate(4).putShort((short) channel.getSamplingDuration()).array();
        byte[] data = new byte[]{0x0C, (byte) 0x02, duration[1], duration[0], 0x00, 0x00, (byte) 0xFF, (byte) 0xFF, 0x01};
        writeCharacteristic(data);
    }

    private void enableChannel(byte param) {
        byte[] data = null;
        if (tickerChannels.peekFirst() == null)
            return;
        Channel channel = tickerChannels.peekFirst().get(0);
        if (channel != null) {
            if (channel.getUri() != null && channel.getUri().trim().endsWith("Temperature")) {
                data = new byte[]{0x0A, 0x02, 0x0C, 0x06, param, 0x04, (byte) 0x81, 0x01};
            } else if (channel.getUri() != null && channel.getUri().trim().endsWith("Heartrate")) {
                // enable (HS step3)
                data = new byte[]{0x0A, 0x02, 0x0C, 0x06, param, 0x05, (byte) 0x87, 0x01};
            }
        }

        if (data != null) {
            writeCharacteristic(data);
        }
    }

    private void writeChannelParam() {
        byte[] data = new byte[]{0x0A, 0x03, 0x00};
        writeCharacteristic(data);
    }

    private void togglePulseOptics(boolean isOn) {
        byte[] data;
        if (isOn) {
            // enable (HS step1)
            data = new byte[]{0x05, 0x02, 0x01};
        } else {
            // disable
            data = new byte[]{0x05, 0x01, 0x01};
        }
        writeCharacteristic(data);
    }

    private boolean writeCharacteristic(byte[] data) {
        boolean status = false;
        BluetoothGattService gattService = genericBle.getService(UUID.fromString("326a9000-85cb-9195-d9dd-464cfbbae75a"));
        if (gattService != null) {
            BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(
                    UUID.fromString("326a9001-85cb-9195-d9dd-464cfbbae75a"));
            if (gattCharacteristic != null) {
                gattCharacteristic.setValue(data);
                gattCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                status = genericBle.writeCharacteristic(gattCharacteristic);

                Log.d(TAG, "!writing characteristic, status: " + status + " , service uuid: " +
                        gattService.getUuid().toString() + " , char uuid: " +
                        gattCharacteristic.getUuid().toString() + " , write type: " +
                        BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE + " , value: " + bytesToHex(data));
            }
        }
        return status;
    }

    private synchronized ArrayList<Channel> getChannelForRegister(byte registerId) {
        ArrayList<Channel> result = null;
        Object[] channels = tickerChannelMap.keySet().toArray(new Object[0]);

        for (int i = 0; i < channels.length; i++) {
            ArrayList<Channel> channelsList = (ArrayList<Channel>) channels[i];
            Channel channel = channelsList.get(0);
            if (registerId == (byte) 0x81 && channel.getUri() != null &&
                    channel.getUri().trim().endsWith("Temperature")) {
                result = channelsList;
                break;
            } else if (registerId == (byte) 0x87 && channel.getUri() != null &&
                    channel.getUri().trim().endsWith("Heartrate")) {
                result = channelsList;
                break;
            } else if (registerId == (byte) 0x09 && channel.getUri() != null &&
                    channel.getUri().trim().matches("(.*)Gyro(.*)")) {
                result = channelsList;
                break;
            }
        }
        return result;
    }

    private Channel populateChannel(Channel temp, byte[] channelData, long timeStamp) {
        Channel channel = new Channel();
        channel.setName(temp.getName());
        channel.setDescription(temp.getDescription());
        channel.setUri(temp.getUri());
        channel.setGuid(temp.getGuid());
        channel.setType(temp.getType());
        channel.setSamplingDuration(temp.getSamplingDuration());
        channel.setIsEnabled(temp.isEnabled());

        Data data = new Data();
        data.setDataType(DataType.FLOAT);
        data.setTimeStamp(timeStamp);
        data.setValues(channelData);
        channel.setData(data);

        temp.setData(data);
        return channel;
    }

    public Object getPopulatedSensorChannel() {
        return populatedSensorChannel;
    }

    public void setPopulatedSensorChannel(Object populatedSensorChannel) {
        this.populatedSensorChannel = populatedSensorChannel;
    }

    private class CharacteristicData {
        private byte[] data;
    }

    private class MainHandler extends Handler {

        MainHandler() {
            super();
        }

        MainHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SHOW_TOAST:
                    Log.d(TAG, "!showing toast: " + msg.obj.toString());
                    Toast.makeText(mContext, msg.obj.toString(), Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }
}
