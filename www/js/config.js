var config = {
    urls: {
        syncGatewayUrl: 'http://52.29.52.88:4984/default',
        syncGatewayAdminUrl: 'http://52.29.52.88:4985/default',
        restApi: 'http://52.58.136.43:8080/rest'
    },
    monitoringOptions: {
        sensors: {
            Accelerometer: {
                frequency: 3000,
            },
            Compass: {
                frequency: 3000,
            },
            Geolocation: {
                timeout: 10000,
                maximumAge: 600000,
                enableHighAccuracy: false
            }
        },

        maxNumberOfMonitoringItems: 10,
        maxNumberOfHistoryItems: 30
    },
    bleOptions: {
        scanDuration: 10000
    },
    logOptions: {
        developmentMode: true,
        maxNumberOfLogItems: 20
    }, units: [
        {
            id: '1',
            label: "μs"
        }, {
            id: '1000',
            label: "ms"
        }, {
            id: '1000000',
            label: "s"
        }, {
            id: '60000000',
            label: "min"
        }, {
            id: '360000000',
            label: "h"
        }
    ]
}