angular.module('controllers.settings', [])

    .controller('SettingsCtrl', function ($scope, $rootScope, $cordovaDevice, $ionicPopup, $timeout, ProfileSrvs, SensorSrvs, MonitorSrvs, WearSrvs, LogSrvs) {
        $scope.deviceInfo = $cordovaDevice.getDevice();

        $scope.wearExists = WearSrvs.doesWearExist();
        $scope.wearConnectionStatus = '';
        $scope.units = config.units;
        $rootScope.disabledProperties = false;



        var tempSelectedUnits = localStorage.getItem('selectedUnits');
        if (tempSelectedUnits) {
            $rootScope.selectedUnits = JSON.parse(tempSelectedUnits);
            console.log($rootScope.selectedUnits);
        } else {
            $rootScope.selectedUnits = {}
        }

        $scope.initTempValue = function (sensor_name, property_name) {
            if (typeof $scope.tempValues == "undefined") {
                $scope.tempValues = {};
            }
            if (typeof $scope.tempValues[sensor_name] == "undefined") {
                $scope.tempValues[sensor_name] = {};
            }
            $scope.tempValues[sensor_name][property_name] = $scope.internalSensorsConfig[sensor_name][property_name] / ($scope.selectedUnits[sensor_name][property_name]/1000)
        };
        $scope.initUnitChooser = function (sensor_name, property_name) {

            if (typeof $rootScope.selectedUnits[sensor_name] == "undefined") {
                $rootScope.selectedUnits[sensor_name] = {};
            }
            if (typeof $rootScope.selectedUnits[sensor_name][property_name] == "undefined") {
                $rootScope.selectedUnits[sensor_name][property_name] = '1000';
            }
        };

        $scope.changeValue = function (sensor_name, property_name) {
            var value = $scope.tempValues[sensor_name][property_name] * ($scope.selectedUnits[sensor_name][property_name] / 1000);
            $rootScope.internalSensorsConfig[sensor_name][property_name] = value;
        };

        $scope.changeUnit = function (sensor_name, property_name) {
            $scope.tempValues[sensor_name][property_name] = $rootScope.internalSensorsConfig[sensor_name][property_name] / ($scope.selectedUnits[sensor_name][property_name] / 1000);
        };

        $rootScope.$watch('debug.transferingData', function () {
            ProfileSrvs.updateDebugStatus("transferingData", $rootScope.debug.transferingData);
        });

        $rootScope.$watch('debug.enableLogs', function () {
            ProfileSrvs.updateDebugStatus("enableLogs", $rootScope.debug.enableLogs);
        });

        $rootScope.$watch('debug', function () {
            console.log("debug");
        });


        $scope.transferingDataDebugChanged = function () {
            console.log($scope.transferingDataDebug);
        };

        WearSrvs.subscribeForConnectionStatus(function (connectionStatus) {
            $timeout(function () {
                $scope.wearConnectionStatus = connectionStatus.charAt(0).toUpperCase() + connectionStatus.slice(1);
            });
        });

        $rootScope.username = null;
        if (ProfileSrvs.checkIsProfileSet()) {
            $rootScope.username = ProfileSrvs.getUsername();
        }

        var profileWorking = false;
        var addNewProfile = function () {
            if (!$scope.username && !profileWorking) {
                $scope.temp = {
                    username: '',
                    password: ''
                };

                var popupVar = $ionicPopup.show({
                    templateUrl: 'templates/popup-addprofile.html',
                    title: 'Set profile',
                    subTitle: 'Please set your username and password',
                    scope: $scope,
                    buttons: [
                        {
                            text: '<b>Save</b>',
                            type: 'button-positive',
                            onTap: function (e) {
                                if (!profileWorking) {
                                    if (!$scope.temp.username || !$scope.temp.password) {
                                        e.preventDefault();
                                    } else {
                                        profileWorking = true;
                                        e.preventDefault();
                                        ProfileSrvs.setProfile($scope.temp.username, $scope.temp.password, function (success) {
                                            profileWorking = false;
                                            if (success) {
                                                $rootScope.username = ProfileSrvs.getUsername();
                                                LogSrvs.log('Profile is set for username ' + $rootScope.username + '.', 'info');
                                                closePopup();
                                            }
                                        });
                                    }
                                } else {
                                    e.preventDefault();
                                }
                            }
                        }
                    ]
                });
                var closePopup = function () {
                    if (popupVar) {
                        popupVar.close();
                    }
                }
            }
        };

        if (!$scope.username) {
            addNewProfile();
        }

        $scope.isMonitoringOn = false;
        MonitorSrvs.isMonitoringOn(function (isMonitoringOnFlag) {
            $scope.isMonitoringOn = isMonitoringOnFlag;
        });

        $scope.internalSensors = [];
        $scope.getInternalSensors = function () {
            $scope.internalSensors.length = 0;
            SensorSrvs.getInternalSensors(function (result) {
                for (var i = 0, len = result.length; i < len; i++) {
                    $scope.internalSensors.push(result[i]);
                }
            });

            // $rootScope.internalSensorsConfig = angular.copy($scope.internalSensors);
        }

        $scope.getInternalSensors();

        var internalWorking = false;
        $scope.availableInternalSensors = [];
        var availableInternalSensorsSelection = [];
        $scope.getAvailableInternalSensors = function () {
            if (!internalWorking) {
                internalWorking = true;
                $scope.availableInternalSensors.length = 0;
                availableInternalSensorsSelection.length = 0;
                SensorSrvs.getAvaliableInternalSensors(function (result) {
                    // Sorting / First already selected go on the top of the avaliable list
                    for (var i = 0, len = $scope.internalSensors.length; i < len; i++) {
                        $scope.availableInternalSensors.push({
                            selected: true,
                            sensor: $scope.internalSensors[i],
                            frequency: 0
                        });
                        availableInternalSensorsSelection.push(true);
                    }
                    for (var i = 0, len = result.length; i < len; i++) {
                        if (!checkIfInternalSensorAlreadySelected(result[i])) {
                            $scope.availableInternalSensors.push({
                                selected: false,
                                sensor: result[i]
                            });
                            availableInternalSensorsSelection.push(false);
                        }
                    }
                    internalWorking = false;
                });
            }
        }

        var checkIfInternalSensorAlreadySelected = function (sensor) {
            for (var i = 0, len = $scope.internalSensors.length; i < len; i++) {
                if ($scope.internalSensors[i].sensorName === sensor.sensorName) {
                    return true;
                }
            }
            return false;
        }

        $scope.addInternalSensor = function () {
            if (!internalWorking && !$scope.isMonitoringOn) {
                $scope.getAvailableInternalSensors();
                $rootScope.internalSensorsConfig = config.monitoringOptions.sensors;

                $ionicPopup.show({
                    templateUrl: 'templates/popup-internalsensor.html',
                    title: 'Add internal sensor',
                    subTitle: 'Please select internal sensor to (dis)connect',
                    scope: $scope,
                    buttons: [
                        {
                            text: 'Cancel',
                            onTap: function (e) {
                                if (internalWorking) {
                                    e.preventDefault();
                                }
                            }
                        },
                        {
                            text: '<b>Save</b>',
                            type: 'button-positive',
                            onTap: function (e) {
                                if (!internalWorking) {
                                    config.monitoringOptions.sensors = $rootScope.internalSensorsConfig;
                                    localStorage.setItem('internalSensors', JSON.stringify(config.monitoringOptions.sensors));
                                    localStorage.setItem('selectedUnits', JSON.stringify($rootScope.selectedUnits));
                                    internalWorking = true;
                                    var sensorsForAdding = [];
                                    var sensorsForRemoving = [];
                                    for (var i = 0, len = $scope.availableInternalSensors.length; i < len; i++) {
                                        if ($scope.availableInternalSensors[i].selected != availableInternalSensorsSelection[i]) {
                                            if (!$scope.availableInternalSensors[i].selected) {
                                                sensorsForRemoving.push($scope.availableInternalSensors[i].sensor);
                                            } else {
                                                sensorsForAdding.push($scope.availableInternalSensors[i].sensor);
                                            }
                                        }
                                    }
                                    SensorSrvs.addOrRemoveMultipleInternalSensors(sensorsForRemoving, sensorsForAdding, function () {
                                        $scope.getInternalSensors();
                                        internalWorking = false;
                                    });
                                } else {
                                    e.preventDefault();
                                }
                            }
                        }
                    ]
                })
            }
        }

        $scope.shareLogs = function () {
            WearSrvs.shareLogFiles();
        };

        $scope.externalSensors = [];
        $scope.getExternalSensors = function () {
            $scope.externalSensors.length = 0;
            SensorSrvs.getExternalSensors(function (result) {
                for (var i = 0, len = result.length; i < len; i++) {
                    $scope.externalSensors.push(result[i]);
                }
            });
        }

        $scope.getExternalSensors();

        var externalWorking = false;
        $scope.availableExternalSensors = [];
        var availableExternalSensorsSelection = [];
        $scope.getAvailableExternalSensors = function () {
            if (!externalWorking) {
                externalWorking = true;
                $scope.availableExternalSensors.length = 0;
                availableExternalSensorsSelection.length = 0;
                SensorSrvs.getAvaliableExternalSensors(function (result) {
                    // Sorting / First already selected go on the top of the avaliable list
                    for (var i = 0, len = $scope.externalSensors.length; i < len; i++) {
                        $scope.availableExternalSensors.push({
                            selected: true,
                            sensor: $scope.externalSensors[i]
                        });
                        availableExternalSensorsSelection.push(true);
                    }
                    for (var i = 0, len = result.length; i < len; i++) {
                        if (!checkIfExternalSensorAlreadySelected(result[i])) {
                            $scope.availableExternalSensors.push({
                                selected: false,
                                sensor: result[i]
                            });
                            availableExternalSensorsSelection.push(false);
                        }
                    }
                    externalWorking = false;
                });
            }
        }

        var checkIfExternalSensorAlreadySelected = function (sensor) {
            for (var i = 0, len = $scope.externalSensors.length; i < len; i++) {
                if ($scope.externalSensors[i].id == sensor.id) {
                    return true;
                }
            }
            return false;
        }

        $scope.addExternalSensor = function () {
            if (!externalWorking && !$scope.isMonitoringOn) {
                $scope.getAvailableExternalSensors();
                $ionicPopup.show({
                    templateUrl: 'templates/popup-externalsensor.html',
                    title: 'Add external sensor',
                    subTitle: 'Please select external sensor to (dis)connect',
                    scope: $scope,
                    buttons: [
                        {
                            text: 'Cancel',
                            onTap: function (e) {
                                if (externalWorking) {
                                    e.preventDefault();
                                }
                            }
                        },
                        {
                            text: '<b>Save</b>',
                            type: 'button-positive',
                            onTap: function (e) {
                                if (!externalWorking) {
                                    externalWorking = true;
                                    var sensorsForAdding = [];
                                    var sensorsForRemoving = [];
                                    for (var i = 0, len = $scope.availableExternalSensors.length; i < len; i++) {
                                        if ($scope.availableExternalSensors[i].selected != availableExternalSensorsSelection[i]) {
                                            if (!$scope.availableExternalSensors[i].selected) {
                                                sensorsForRemoving.push($scope.availableExternalSensors[i].sensor);
                                            } else {
                                                sensorsForAdding.push($scope.availableExternalSensors[i].sensor);
                                            }
                                        }
                                    }
                                    SensorSrvs.addOrRemoveMultipleExternalSensors(sensorsForRemoving, sensorsForAdding, function () {
                                        $scope.getExternalSensors();
                                        externalWorking = false;
                                    });
                                } else {
                                    e.preventDefault();
                                }
                            }
                        }
                    ]
                })
            }
        }

        $scope.devicesConfiguration = function () {
            if (!externalWorking && !$scope.isMonitoringOn) {
                $scope.devicesConfigurationChanged = {};
                $scope.externalDevicesTemp = angular.copy($rootScope.externalDevices);
                for (key in $scope.externalDevicesTemp) {
                    $scope.devicesConfigurationChanged[key] = false;
                }
                $ionicPopup.show({
                    templateUrl: 'templates/popup-externaldevicesensors.html',
                    title: 'Devices Configuration',
                    subTitle: 'Please select external sensor to (dis)connect',
                    scope: $scope,
                    buttons: [
                        {
                            text: 'Cancel',
                            onTap: function (e) {
                            }
                        },
                        {
                            text: '<b>Save</b>',
                            type: 'button-positive',
                            onTap: function (e) {
                                $rootScope.externalDevices = angular.copy($scope.externalDevicesTemp);
                                var updated = 0;
                                for (deviceName in $rootScope.externalDevices) {
                                    if ($scope.devicesConfigurationChanged[deviceName]) {
                                        updated++;
                                        WearSrvs.setNewConfiguration($scope.externalDevicesTemp[deviceName], deviceName);
                                    }
                                }

                            }
                        }
                    ]
                })
            }
        }

        $scope.wearInit = function () {
            WearSrvs.initializeWear(function () {
                LogSrvs.log('Android wear initialization finished.', 'info');
            });
        };

        $scope.getDevicesList = function () {

        };

        $scope.sendMessageToMetaWear = function (method) {
            WearSrvs.sendMessageToMetaWear(method);
        };

        $scope.simulateNativeCrash = function () {
            window.fabric.Crashlytics.addLog("about to send a crash for testing!");
            window.fabric.Crashlytics.sendCrash();
        }

        $scope.getConnectionState = function () {
            WearSrvs.sendMessageToMetaWear("getConnectionState");
        }
        $scope.getDeviceCapabilities = function () {
            WearSrvs.sendMessageToMetaWear("getDeviceCapabilities");
        }
        $scope.initializeWear = function () {
            WearSrvs.initializeWear();
        }
        $scope.checkMetaWearStates = function () {
            WearSrvs.checkMetaWearStates()
        }

        $scope.getExtSensorsTemp = function () {
            $scope.externalSensorsConfig = WearSrvs.getExternalSensors();
        };

        $scope.toggleSensor = function (configurationsObjectName, device_key, sensor_key) {
            $scope.devicesConfigurationChanged[device_key] = true;
            var enabled = $scope[configurationsObjectName][device_key].nodes[sensor_key].nodes[0].isEnabled;
            var samplingDuration = $scope[configurationsObjectName][device_key].nodes[sensor_key].nodes[0].samplingDuration;
            for (channel in $scope[configurationsObjectName][device_key].nodes[sensor_key].nodes) {
                $scope[configurationsObjectName][device_key].nodes[sensor_key].nodes[channel].isEnabled = enabled;
                $scope[configurationsObjectName][device_key].nodes[sensor_key].nodes[channel].samplingDuration = samplingDuration;
            }
        }
    });