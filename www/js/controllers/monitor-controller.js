angular.module('controllers.monitor', [])

    .controller('MonitorCtrl', function ($rootScope, $scope, $ionicPopup, MonitorSrvs, DbSrvs, ChartSrvs, ProfileSrvs, LogSrvs, $ionicScrollDelegate, $location, $q, WearSrvs) {
        $rootScope.started = false;
        $scope.monitoringStatus = '';

        $scope.monitoringChart = [];

        $scope.chartLabels = {};
        $scope.chartSeries = {};
        $scope.chartData = {};
        $scope.chartColours = {};
        $scope.chartOptions = {};
        $rootScope.monitoringLog = [];
        // $rootScope.monitoringLogLimit = config.monitoringOptions.maxNumberOfMonitoringItems;
        $rootScope.monitoringLogFiltered = [];
        $scope.addOnTop = true;
        $rootScope.wearStatus = "disconnected";
        $scope.categoryFilters = {
            info: true,
            warning: true,
            error: true,
        };
        $scope.subCategoryFilters = {};
        $scope.logArrayName = "monitoringLog";
        $scope.logFilteredArrayName = "monitoringLogFiltered";
        $scope.sessionsTempData = {};

        $rootScope.oldSessionData = 0;
        $scope.loading_logs = false;
        date = new Date();
        $rootScope.filters = {
            //from: date.toISOString(),
            // to: new Date(new Date().getTime() + 300000).toISOString(), //+3s
            goto: 0,
            gotoIndex: null,
            gotoBeforeIndex: -1,
            gotoAfterIndex: 0
        };
        $scope.bottomLoading = false;
        $scope.topLoading = false;

        $scope.$watch('filters.goto', function () {
            if ($rootScope.filters.goto != 0) {
                //Go To
                $scope.addOnTop = false;
                now = new Date();
                $rootScope.filters.goto.setDate(now.getDate());
                $rootScope.filters.goto.setMonth(now.getMonth());
                $rootScope.filters.goto.setFullYear(now.getFullYear());
                var goto = $rootScope.filters.goto.getTime();
                $scope.goToTime("monitor-logs", goto, true);

            }
        });

        MonitorSrvs.subscribeForMonitoringStatus(function (currentMonitoringStatus) {
            $scope.monitoringStatus = currentMonitoringStatus;
        });
        $rootScope.dataCounters = {
            startCallbackTriggeredSamplingDuration: 0,
            startCallbackTriggered: 0,
            readSensors: 0,
            readSensorsData: 0,
            createWearSensorValueDocument: 0,
            existingWearSensorsData: 0,
            tempDataFunction: {
                Accelerometer: 0,
                StepCounter: 0,
            },
        };

        $scope.toggleGraph = function (key) {
            if (jQuery(".accordion-tab-content").length < 3 || $scope.monitoringChart[key].visible) {
                $scope.monitoringChart[key].visible = !$scope.monitoringChart[key].visible;
                if ($scope.monitoringChart[key].visible) {
                    $scope.monitoringChart[key].opened = time()
                }
            } else {
                latest = null;
                jQuery(".accordion-tab-content").each(function (index) {
                    eKey = jQuery(this).attr("data-key");
                    if (!latest) {
                        latest = {key: eKey, opened: $scope.monitoringChart[eKey].opened};
                    } else if ($scope.monitoringChart[eKey].opened < latest.opened) {
                        latest = {key: eKey, opened: $scope.monitoringChart[eKey].opened};
                    }
                });
                $scope.monitoringChart[latest.key].visible = false;
                $scope.monitoringChart[key].visible = true;
                $scope.monitoringChart[key].opened =  new Date().getTime();
                //alert("Tri su otvorena / "+jQuery(".tab-content").length);
            }
            //chart.visible = !chart.visible
        };

        var validateSession = function (sessionID) {

            var deferred = $q.defer();

            if (sessionID && (typeof MonitorSrvs.currentSession() == "undefined" || MonitorSrvs.currentSession().getGuid() != sessionID)) {
                if (typeof $scope.sessionsTempData[sessionID] == "undefined") {
                    DbSrvs.getSessionById(sessionID).then(function (document) {
                        $scope.sessionsTempData[sessionID] = document;
                        var session = {
                            guid: sessionID,
                            owner: document.owner,
                            current: false
                        };
                        deferred.resolve(session);
                        $rootScope.oldSessionData++;
                    }, function () {
                        deferred.reject("Unknown session id");
                    });
                } else {
                    var session = {
                        guid: sessionID,
                        owner: $scope.sessionsTempData[sessionID].owner,
                        current: false
                    };
                    deferred.resolve(session);
                }

            } else {
                var session = {
                    guid: MonitorSrvs.currentSession().getGuid(),
                    owner: MonitorSrvs.currentSession().getUser(),
                    current: true
                };
                deferred.resolve(session);
            }

            return deferred.promise;

        };

        MonitorSrvs.subscribeForMonitoringValue(function (currentMonitoringValueIn) {

            // ********* sort labels
            var currentMonitoringValue = {};
            currentMonitoringValue.sensor = currentMonitoringValueIn.sensor;
            currentMonitoringValue.timestamp = currentMonitoringValueIn.timestamp;
            //var sessionID = typeof currentMonitoringValueIn.sessionID != "undefined" ? currentMonitoringValueIn.sessionID : null;
            if (currentMonitoringValueIn.sessionID != "undefined") {
                var sessionID = currentMonitoringValueIn.sessionID;
                delete currentMonitoringValueIn.sessionID;
            } else {
                var sessionID = null;
            }

            //validate session function

            validateSession(sessionID).then(function (session) {
                if (currentMonitoringValue.sensor.split(" - ")[0] != "Wear" && currentMonitoringValue.sensor.split(" - ")[0] != "MetaWear") {
                    //Internal sensor
                    Object.keys(currentMonitoringValueIn).sort().forEach(function (key) {
                        if (key !== 'sensor' && key !== 'timestamp' && key !== 'sessionID') {
                            currentMonitoringValue[key] = currentMonitoringValueIn[key];

                            var sensorData = {
                                timestamp: currentMonitoringValue.timestamp,
                                sensor: currentMonitoringValue.sensor,
                                channel: key,
                                value: currentMonitoringValueIn[key]
                            };

                            DbSrvs.createSensorValueDocument(session, sensorData);
                        }
                    });
                    if (session.current) {
                        ChartSrvs.createOrUpdateChartsByScope(currentMonitoringValue, $scope.chartLabels, $scope.chartSeries,
                            $scope.chartData, $scope.chartColours, $scope.chartOptions, $scope.monitoringChart,
                            config.monitoringOptions.maxNumberOfMonitoringItems, MonitorSrvs.currentSession().getGuid());
                        $scope.addDateToMonitorLog(currentMonitoringValue);
                        addToFiltered(currentMonitoringValue);
                    }

                } else {
                    //external sensor
                    currentMonitoringValue = currentMonitoringValueIn;
                    currentMonitoringValue[currentMonitoringValue.channel] = currentMonitoringValue.value;
                    delete currentMonitoringValue.channel;
                    delete currentMonitoringValue.value;

                    //$scope.doesLogExist(currentMonitoringValue, session).then(function (result) {
                    //currentMonitoringValue = result.currentMonitoringValue;
                    //session = result.session;
                    Object.keys(currentMonitoringValueIn).sort().forEach(function (key) {
                        if (key !== 'sensor' && key !== 'timestamp' && key !== 'sessionID') {
                            currentMonitoringValue[key] = currentMonitoringValueIn[key];

                            var sensorData = {
                                timestamp: currentMonitoringValue.timestamp,
                                sensor: currentMonitoringValue.sensor,
                                channel: key,
                                value: currentMonitoringValueIn[key]
                            };

                            DbSrvs.createSensorValueDocument(session, sensorData);
                        }
                    });

                    $rootScope.dataCounters.createWearSensorValueDocument++;
                    if (session.current) {
                        ChartSrvs.createOrUpdateChartsByScope(currentMonitoringValue, $scope.chartLabels, $scope.chartSeries,
                            $scope.chartData, $scope.chartColours, $scope.chartOptions, $scope.monitoringChart,
                            config.monitoringOptions.maxNumberOfMonitoringItems, MonitorSrvs.currentSession().getGuid());
                        $scope.addDateToMonitorLog(currentMonitoringValue);
                        addToFiltered(currentMonitoringValue);
                    }
                    /*
                     }, function (error) {
                     $rootScope.dataCounters.existingWearSensorsData++;
                     LogSrvs.log(error, "info", MonitorSrvs.currentSession().getGuid());
                     }
                     */
                    //);
                }
            }, function (error) {
                LogSrvs.log(error, "error");
            });

        });

        $scope.startMonitoring = function () {
            if ($rootScope.started || ($rootScope.noSensorsSelected && ( $rootScope.metaWearConnectionStatus == 'DISCONNECTED'  || ($rootScope.metaWearConnectionStatus != 'DISCONNECTED' && ($rootScope.metaWearStatus != 'READY' && $rootScope.metaWearStatus != 'OPERATIONAL'))  ))) {
                console.log("No any sensor attached");
                window.plugins.toast.show("No any sensor attached", 'short', 'bottom');
            } else {

                if (!$rootScope.started) {
                    //$scope.clearMonitoring();
                    $scope.monitoringChart = [];
                    MonitorSrvs.startMonitoring();
                    $rootScope.started = true;
                }
            }
        };
        $scope.stopMonitoring = function () {

            if ($rootScope.started) {

                LogSrvs.log('Monitoring stopped.', 'info', MonitorSrvs.currentSession().getGuid());
                $scope.addMessageToMonitorLog('Monitoring stopped.', 'info');
                MonitorSrvs.stopMonitoring();
                $rootScope.started = false;

                var sessionId = MonitorSrvs.currentSession().getGuid();
                console.info($rootScope.dataCounters);

                if ($rootScope.debug.transferingData) {

                    if (WearSrvs.isWearEnabled()) {
                        WearSrvs.transmitCSVFile();
                    }
                    DbSrvs.getAllSensorValueDocuments(MonitorSrvs.currentSession().getGuid(), function (data) {
                        LogSrvs.saveCSVFile(data, MonitorSrvs.currentSession());
                    });
                    LogSrvs.log('readSensorSuccess: ' + $rootScope.dataCounters.readSensorSuccess + '; createSensorValueDocument: ' + $rootScope.dataCounters.createWearSensorValueDocument
                        , 'info', sessionId);
                    LogSrvs.log(JSON.stringify($rootScope.dataCounters), 'info', sessionId);
                }


            }
        };

        $scope.clearMonitoring = function () {
            if ($rootScope.monitoringLog.length > 0) {
                LogSrvs.log('Monitoring charts and logs cleared.', 'info', MonitorSrvs.currentSession().getGuid());
                $scope.addMessageToMonitorLog('Monitoring charts and logs cleared.', 'info');
                $rootScope.monitoringLog.length = 0;
                ChartSrvs.clearChartsByScope($scope.chartLabels, $scope.chartSeries, $scope.chartData, $scope.monitoringChart);
            }
        };

        var addNewMarkerWorking = false;

        $scope.addNewMarker = function () {
            $scope.temp = {
                markerMessage: ''
            }
            $ionicPopup.show({
                templateUrl: 'templates/popup-addmarker.html',
                title: 'Add new marker',
                subTitle: 'Please enter marker text',
                scope: $scope,
                buttons: [
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            if (!addNewMarkerWorking) {
                                if ($scope.temp.markerMessage) {
                                    addNewMarkerWorking = true;
                                    var currentDate = new Date();
                                    if ($rootScope.monitoringLog.length) {
                                        var previousDate = new Date($rootScope.monitoringLog[$rootScope.monitoringLog.length - 1].timestamp);
                                        if (currentDate.getSeconds() == previousDate.getSeconds() && currentDate.getMinutes() == previousDate.getMinutes()) {
                                            // If time is the same, increase it by 1 second
                                            currentDate.setSeconds(currentDate.getSeconds() + 1);
                                        }
                                    }

                                    var obj = {
                                        sensor: 'Marker',
                                        timestamp: Math.floor(currentDate.getTime()),
                                        text: $scope.temp.markerMessage
                                    }
                                    // save marker data
                                    DbSrvs.createMarkerDocument(MonitorSrvs.currentSession(), obj).then(function (result) {
                                        LogSrvs.log("Marker added", "info", MonitorSrvs.currentSession().getGuid());
                                        $scope.addMessageToMonitorLog("Marker added", "info");
                                    });
                                    ChartSrvs.createOrUpdateChartsByScope(obj, $scope.chartLabels, $scope.chartSeries,
                                        $scope.chartData, $scope.chartColours, $scope.chartOptions, $scope.monitoringChart,
                                        config.monitoringOptions.maxNumberOfMonitoringItems, MonitorSrvs.currentSession().getGuid());
                                    $scope.addDateToMonitorLog(obj);
                                    addToFiltered(obj);
                                    addNewMarkerWorking = false;
                                }
                            }
                        }
                    }
                ]
            })
        }

        $scope.onClick = function (points) {
            if (points && points.length > 0) {
                for (var i = 0, len = points.length; i < len; i++) {
                    if (points[i].datasetLabel === 'marker' && points[i].value != null) {
                        for (var j = 0, leng = $rootScope.monitoringLog.length; j < leng; j++) {
                            if ($rootScope.monitoringLog[j].sensor === 'Marker') {
                                var monitoringDate = new Date($rootScope.monitoringLog[j].timestamp);
                                if (points[i].label === formatTime(monitoringDate)) {
                                    showMarker($rootScope.monitoringLog[j]);
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        };

        var formatTime = function (dateObj) {
            var addLeadingZero = function (number) {
                return number.toString().length == 1 ? '0' + number : number;
            }
            return addLeadingZero(dateObj.getHours()) + ':' + addLeadingZero(dateObj.getMinutes()) + ':' + addLeadingZero(dateObj.getSeconds());
        }

        var showMarker = function (marker) {
            $scope.tempMarker = marker;
            $ionicPopup.show({
                templateUrl: 'templates/popup-showmarker.html',
                title: 'Marker',
                scope: $scope,
                buttons: [
                    {
                        text: '<b>Close</b>',
                        type: 'button-positive',
                        onTap: function (e) {

                        }
                    }
                ]
            })
        }

        $scope.totalLogs = 0;

        $scope.resizeMonitor = function () {
            $ionicScrollDelegate.$getByHandle('monitor-logs').resize();
        };

        var logHeight = $("ion-content.monitor").height() - 200;
        jQuery('.log-section ion-scroll ').css('height', logHeight + 'px').css('    overflow-y', 'scroll');

        var getFilterDate = function (date) {
            d = new Date(date);
            today = new Date();
            month = today.getMonth();

            d.setDate(today.getDate());
            d.setMonth(month);
            d.setFullYear(today.getFullYear());
            return d;
        };

        var addToFiltered = function (log) {
            if ($rootScope.debug.enableLogs) {
                //TOP? Live mode?
                if ($scope.addOnTop) {
                    if (LogSrvs.logFilter(log, $scope)) {
                        $rootScope.monitoringLogFiltered.unshift(log);
                        if ($rootScope.monitoringLogFiltered.length >= config.logOptions.maxNumberOfLogItems) {
                            $rootScope.monitoringLogFiltered.pop();
                        }
                        $scope.filters.gotoAfterIndex = $rootScope.monitoringLog.indexOf(log);
                        $scope.filters.gotoBeforeIndex = $rootScope.monitoringLog.indexOf($rootScope.monitoringLogFiltered[$rootScope.monitoringLogFiltered.length - 1]);
                    }
                }
            }
        };

        $scope.addMessageToMonitorLog = function (message, category) {
            var mlog = {
                message: message,
                timestamp: new Date().getTime(),
                category: category,
            };
            $rootScope.monitoringLog.push(mlog);
            addToFiltered(mlog);
        };

        $scope.addDateToMonitorLog = function (data) {
            if ($rootScope.debug.enableLogs) {
                if (typeof $scope.subCategoryFilters[data.sensor] == "undefined") {
                    $scope.subCategoryFilters[data.sensor] = true;
                }
                data.category = 'data';
                $rootScope.monitoringLog.push(data);
            }
        };

        $scope.doesLogExist = function (currentMonitoringValue, session) {
            var deferred = $q.defer();
            if ($rootScope.monitoringLog.length > 0) {
                for (mKey in $rootScope.monitoringLog) {
                    if (JSON.stringify($rootScope.monitoringLog[mKey]) === JSON.stringify(currentMonitoringValue)) {
                        deferred.reject('Data already exists');
                    }
                    if (parseInt(mKey) + 1 == $rootScope.monitoringLog.length) {
                        deferred.resolve({currentMonitoringValue: currentMonitoringValue, session: session});
                    }
                }
            } else {
                deferred.resolve({currentMonitoringValue: currentMonitoringValue, session: session});
            }
            return deferred.promise;
        };

        //


        $scope.showCategoryFiltersPopup = function () {
            $scope.categoryFiltersPopup = $ionicPopup.show({
                templateUrl: 'templates/popup-filters.html',
                title: 'Set filters',
                subTitle: 'Please select log categories',
                scope: $scope,
                buttons: [
                    {
                        text: '<b>Ok</b>',
                        type: 'button-positive',
                        onTap: function (e) {


                            e.preventDefault();
                            console.log('Log categories are set');
                            $scope.closeCatPopup();
                            $('.filters_settings').hide();
                            var goto = $scope.filters.goto;
                            if (goto == 0) {
                                if ($scope.monitoringLog.length > 0) {
                                    goto = $scope.monitoringLogFiltered[0].timestamp;
                                } else {
                                    goto = $scope.monitoringLog[$scope.monitoringLog.length - 1].timestamp;
                                }
                            }
                            $scope.goToTime("monitor-logs", goto, true);
                        }
                    }
                ]
            });
        };

        $scope.closeCatPopup = function () {
            if ($scope.categoryFiltersPopup) {
                $scope.categoryFiltersPopup.close();
            }
        }

        $scope.gotScrolled = function (dh) {
            var delegate = $ionicScrollDelegate.$getByHandle(dh);
            //if bottom

            if (!$scope.addOnTop && (delegate.getScrollView().__contentHeight - delegate.getScrollView().__clientHeight - 30) <= delegate.getScrollPosition().top && !$scope.bottomLoading) {

                $scope.bottomLoading = true;
                bottomLoadingIndicatorUpdate();
                LogSrvs.loadOlder("add", $scope).then(function () {
                    setTimeout(function () {
                        $scope.bottomLoading = false;
                        bottomLoadingIndicatorUpdate();
                    }, 2000);
                });


            }
            // console.log(delegate.getScrollPosition().top);
            if (delegate.getScrollPosition().top <= 0) {
                topLoadingIndicatorUpdate();
                LogSrvs.loadNewer("add", "monitor-logs", $scope).then(function () {
                    setTimeout(function () {
                        $scope.bottomLoading = false;
                        topLoadingIndicatorUpdate();
                    }, 1500);
                });
                //$scope.addOnTop = true;
            } else {
                $scope.addOnTop = false;
            }
        };


        $scope.goToTime = function (handle, goto, scroll) {
            $scope.loading_logs = true;
            var deferred = $q.defer();
            var prev = $rootScope.monitoringLog[0].timestamp;

            if (handle == "monitor-logs") {
                cName = "monitor";
            } else {
                cName = "logs";
            }
            $rootScope.filters.gotoLoadedAfter = 0;
            $scope.filters.gotoBeforeIndex = -1;
            var scrolled = false;
            for (logIndex in  $rootScope.monitoringLog) {
                var created = $rootScope.monitoringLog[logIndex].timestamp;
                if (prev <= goto && goto <= created && !scrolled) {
                    console.log(created);
                    $rootScope.filters.gotoIndex = logIndex;
                    //Get next and previous 25 logs!
                    $rootScope.monitoringLogFiltered = [];

                    LogSrvs.loadOlder("goto", $scope).then(function () {
                        LogSrvs.loadNewer("goto", null, $scope).then(function () {
                            if (scroll) {
                                $scope.scrollTo($rootScope.monitoringLog[logIndex].timestamp, handle);
                            } else {
                                $ionicScrollDelegate.$getByHandle(handle).scrollTop();
                            }

                        });
                    });


                    scrolled = true;
                    prev = created;
                } else {
                    console.log("prev");
                    prev = created;
                }

                if ($rootScope.monitoringLog.length - 1 == logIndex) {
                    //TODO not completed
                    deferred.resolve();
                    $scope.loading_logs = true;
                }
            }
            ;
            return deferred;
        };


        $scope.scrollLogToTop = function (handle, goto_disable) {
            $rootScope.filters = {
                //from: date.toISOString(),
                // to: new Date(new Date().getTime() + 300000).toISOString(), //+3s
                goto: 0,
                gotoIndex: null,
                gotoBeforeIndex: -1,
                gotoAfterIndex: 0
            };
            var goto = $rootScope.monitoringLog[$rootScope.monitoringLog.length - 1].timestamp;
            if (goto) {
                $scope.addOnTop = true;
                if (!goto_disable) {
                    $scope.goToTime("monitor-logs", goto, false);
                }
            }
            //$ionicScrollDelegate.$getByHandle(handle).scrollTop();
        };

        var bottomLoadingIndicatorUpdate = function () {
            if ($scope.bottomLoading) {
                if ($(".loadding-bottom").length > 0) {
                    $(".loadding-bottom").remove();
                }
                $(".scroll").append("<p class='loadding-bottom'>Loading...</p>");
            } else {
                $(".loadding-bottom").remove();
            }
            $scope.resizeMonitor;
        };

        var topLoadingIndicatorUpdate = function () {
            if ($scope.topLoading) {
                if ($(".loadding-top").length > 0) {
                    $(".loadding-top").remove();
                }
                $(".scroll").prepend("<p class='loadding-top'>Loading...</p>");
            } else {
                $(".loadding-top").remove();
            }
            $scope.resizeMonitor;
        };

        $scope.scrollTo = function (target, handle) {
            $location.hash(target);
            var dHandle = $ionicScrollDelegate.$getByHandle(handle);
            dHandle.anchorScroll(true);
        };

    });
