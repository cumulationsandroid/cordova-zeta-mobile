angular.module('controllers.history', [])

    .controller('HistoryCtrl', function ($scope, $rootScope, $ionicPopup, DbSrvs, ChartSrvs, LogSrvs) {
        var working = false;
        $scope.working = false;
        $scope.monitoringChart = [];

        $scope.chartLabels = {};
        $scope.chartSeries = {};
        $scope.chartData = {};
        $scope.chartColours = {};
        $scope.chartOptions = {};
        $scope.dataArray = {};
        $scope.timestamps = [];
        $scope.markers = {};
        $scope.sessions = [];

        $scope.monitoringLog = [];
        $scope.monitoringLogLimit = config.monitoringOptions.maxNumberOfHistoryItems;

        $scope.selectedSession = "all";

        $scope.filters = {
            todaysResults: true
        };

        $scope.changeTodaysResults = function () {
            clearAllData();
            $scope.refreshData();
        };

        var clearAllData = function () {

            $scope.monitoringChart = [];

            $scope.chartLabels = {};
            $scope.chartSeries = {};
            $scope.chartData = {};
            $scope.chartColours = {};
            $scope.chartOptions = {};
            $scope.dataArray = {};
            $scope.timestamps = [];
            //$scope.markers = {};

        };

        $scope.loadSessions = function () {
            DbSrvs.getSessions($scope.filters.todaysResults).then(function (result) {
                console.log(result.rows.length + "sessions loaded");

                result.rows.sort(function (a, b) {
                        return b.value.sessionStart - a.value.sessionStart
                    }
                );
                $scope.sessions = result.rows;
            }, function (error) {
                console.log("Error receiving sessions");
            });
        };

        $scope.loadSessions();

        $scope.changeSession = function () {
            $scope.refreshData();

        }

        $scope.refreshData = function () {
            DbSrvs.getLastSession();
            $scope.loadSessions();
            if (!$scope.working) {

                LogSrvs.log('History refreshed', 'info');
                $scope.working = true;

                var e = document.getElementById("selectedSession");
                if (typeof e.options[e.selectedIndex] == "undefined") {
                    alert("error");
                }
                var selectedSession = e.options[e.selectedIndex].value;
                console.log("Session selected: " + selectedSession);
                DbSrvs.getGroupedSensorValueDocuments(selectedSession, $scope.markers, function (results) {

                    // ********* sort documents by timestamp
                    results.sort(function (obj1, obj2) {
                        return obj1.value.timestamp - obj2.value.timestamp;
                    });

                    console.log("Number of rows: " + results.length);


                    if ($scope.filters.todaysResults) {
                        // Get only values after midnight
                        var midnight = new Date();
                        midnight.setHours(0, 0, 0, 0);

                        var sensorValueDocuments = results.filter(function (obj) {
                            return obj.value.timestamp >= midnight.getTime();
                        });
                    } else {
                        var sensorValueDocuments = results;
                    }


                    $scope.monitoringLog.length = 0;
                    clearAllData();
                    //ChartSrvs.clearChartsByScope($scope.chartLabels, $scope.chartSeries, $scope.chartData, $scope.monitoringChart);
                    for (var i = 0, len = sensorValueDocuments.length; i < len; i++) {

                        // sort labels
                        var sortedValue = {};
                        sortedValue.sensor = sensorValueDocuments[i].value.sensor;
                        sortedValue.timestamp = sensorValueDocuments[i].value.timestamp;
                        Object.keys(sensorValueDocuments[i].value).sort().forEach(function (key) {
                            if (key !== 'sensor' && key !== 'timestamp') {
                                sortedValue[key] = sensorValueDocuments[i].value[key];
                            }
                        });
                        ChartSrvs.addChartDataByScope($scope.dataArray, $scope.markers, sortedValue, $scope.timestamps);

                        $scope.monitoringLog.unshift(sortedValue);
                    };

                    ChartSrvs.prepareChartByScope($scope.dataArray, $scope.chartLabels, $scope.chartSeries, $scope.chartData,
                        $scope.chartColours, $scope.chartOptions, $scope.monitoringChart, $scope.timestamps, config.monitoringOptions.maxNumberOfHistoryItems);


                    $scope.working = false;
                });
            }
        }

        $scope.onClick = function (points) {
            if (points && points.length > 0) {
                for (var i = 0, len = points.length; i < len; i++) {
                    if (points[i].datasetLabel === 'marker' && points[i].value != null) {
                        for (var ts in $scope.markers) {
                            if (points[i].label === formatTime(new Date(ts*1+1000))) {
                                var marker = {
                                    text: $scope.markers[ts],
                                    timestamp: ts
                                };
                                showMarker(marker);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        };

        var formatTime = function (dateObj) {
            var addLeadingZero = function (number) {
                return number.toString().length == 1 ? '0' + number : number;
            }
            return addLeadingZero(dateObj.getHours()) + ':' + addLeadingZero(dateObj.getMinutes()) + ':' + addLeadingZero(dateObj.getSeconds());
        }
        //$scope.refreshData();
        var showMarker = function (marker) {
            $scope.tempMarker = marker;
            $ionicPopup.show({
                templateUrl: 'templates/popup-showmarker.html',
                title: 'Marker',
                scope: $scope,
                buttons: [
                    {
                        text: '<b>Close</b>',
                        type: 'button-positive',
                        onTap: function (e) {

                        }
                    }
                ]
            })
        }
    });