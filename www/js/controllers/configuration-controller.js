angular.module('controllers.configuration', [])

    .controller('ConfigurationCtrl', function ($scope, $rootScope, $ionicPopup, $timeout, $cordovaDevice, $state, ProfileSrvs, SensorSrvs, MonitorSrvs, WearSrvs, LogSrvs, $ionicHistory, $ionicPlatform) {


        var tempSelectedUnits = localStorage.getItem('selectedUnits');
        if (tempSelectedUnits) {
            $rootScope.selectedUnits = JSON.parse(tempSelectedUnits);
            console.log($rootScope.selectedUnits);
        } else {
            $rootScope.selectedUnits = {}
        }

        $scope.selectedOption = 1;

        $scope.units = config.units;

        $scope.changeValue = function (device_key, sensor_key) {
            $scope.devicesConfigurationChanged[device_key] = true;
            $scope.externalDevicesTemp[device_key].nodes[sensor_key].nodes[0].samplingDuration = $scope.tempValues[device_key][sensor_key] * $scope.selectedUnits[device_key][sensor_key];
            var value = $scope.tempValues[device_key][sensor_key] * $scope.selectedUnits[device_key][sensor_key];
            for (node in $scope.externalDevicesTemp[device_key].nodes[sensor_key].nodes){
                $scope.externalDevicesTemp[device_key].nodes[sensor_key].nodes[node].samplingDuration = value;
            }
        }

        $scope.changeUnit = function (device_key, sensor_key) {
            $scope.tempValues[device_key][sensor_key] = $scope.externalDevicesTemp[device_key].nodes[sensor_key].nodes[0].samplingDuration / $scope.selectedUnits[device_key][sensor_key];
        };

        $scope.initTempValue = function (device_key, sensor_key) {
            if (typeof $scope.tempValues == "undefined") {
                $scope.tempValues = {};
            }
            if (typeof $scope.tempValues[device_key] == "undefined") {
                $scope.tempValues[device_key] = {};
            }
            $scope.tempValues[device_key][sensor_key] = $scope.externalDevicesTemp[device_key].nodes[sensor_key].nodes[0].samplingDuration / $scope.selectedUnits[device_key][sensor_key]
        };

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (toState.name == "sensorsConfig") {
                $scope.devicesConfigurationChanged = {};
                $scope.externalDevicesTemp = angular.copy($rootScope.externalDevices);
                for (device_key in $scope.externalDevicesTemp) {
                    $scope.devicesConfigurationChanged[device_key] = false;
                    if (typeof $rootScope.selectedUnits[device_key] == "undefined") {
                        $rootScope.selectedUnits[device_key] = {};
                    }
                    for (sensor_key in $scope.externalDevicesTemp[device_key].nodes) {
                        if (typeof $rootScope.selectedUnits[device_key][sensor_key] == "undefined") {
                            $rootScope.selectedUnits[device_key][sensor_key] = '1';
                        }
                    }

                }
            }
        });

        $scope.updateUnit = function () {
            console.log("updated");
        };

        $scope.initUnitChooser = function (device_key, sensor_key) {

            if (typeof $rootScope.selectedUnits[device_key] == "undefined") {
                $rootScope.selectedUnits[device_key] = {};
            }
            if (typeof $rootScope.selectedUnits[device_key][sensor_key] == "undefined") {
                $rootScope.selectedUnits[device_key][sensor_key] = '1';
            }
        };

        $scope.setToMs = function (device_key, sensor_key) {
            var val = '1000';
            $rootScope.selectedUnits[device_key][sensor_key] = val;
        };
        /*** old ****/

        $rootScope.$watch('debug.transferingData', function () {
            ProfileSrvs.updateDebugStatus("transferingData", $rootScope.debug.transferingData);
        });

        $rootScope.$watch('debug.enableLogs', function () {
            ProfileSrvs.updateDebugStatus("enableLogs", $rootScope.debug.enableLogs);
        });


        $scope.isMonitoringOn = false;
        MonitorSrvs.isMonitoringOn(function (isMonitoringOnFlag) {
            $scope.isMonitoringOn = isMonitoringOnFlag;
        });


        $scope.toggleSensor = function (configurationsObjectName, device_key, sensor_key) {
            $scope.devicesConfigurationChanged[device_key] = true;
            var enabled = $scope[configurationsObjectName][device_key].nodes[sensor_key].nodes[0].isEnabled;
            var samplingDuration = $scope[configurationsObjectName][device_key].nodes[sensor_key].nodes[0].samplingDuration;
            for (channel in $scope[configurationsObjectName][device_key].nodes[sensor_key].nodes) {
                $scope[configurationsObjectName][device_key].nodes[sensor_key].nodes[channel].isEnabled = enabled;
                $scope[configurationsObjectName][device_key].nodes[sensor_key].nodes[channel].samplingDuration = samplingDuration;
            }
        }

        $scope.saveConfiguration = function () {
            $rootScope.externalDevices = angular.copy($scope.externalDevicesTemp);
            var updated = 0;
            for (deviceName in $rootScope.externalDevices) {
                var a = ProfileSrvs.getProfile();
                if ($scope.devicesConfigurationChanged[deviceName]) {
                    updated++;
                    WearSrvs.saveConfig(deviceName, $scope.externalDevicesTemp[deviceName]);
                    WearSrvs.setNewConfiguration($scope.externalDevicesTemp[deviceName], deviceName);
                    WearSrvs.saveConfig(deviceName, $scope.externalDevicesTemp[deviceName]);
                }
            }
            localStorage.setItem('selectedUnits', JSON.stringify($rootScope.selectedUnits));

            $ionicHistory.goBack();
        }

        $scope.back = function () {

            changed = false;
            for (deviceName in $rootScope.externalDevices) {
                if ($scope.devicesConfigurationChanged[deviceName]) {
                    changed = true;
                }
            }
            if (changed) {
                $ionicPopup.show({
                    title: 'Are you sure?',
                    subTitle: 'All changes will be discarded.',
                    scope: $scope,
                    buttons: [
                        {
                            text: 'Cancel', onTap: function (e) {
                            return true;
                        }
                        },
                        {
                            text: '<b>Ok</b>',
                            type: 'button-positive',
                            onTap: function (e) {
                                $ionicHistory.goBack();
                            }
                        },
                    ]
                });
            } else {
                $ionicHistory.goBack();
            }
        };


        $ionicPlatform.registerBackButtonAction(function () {
            if ($state.current.name == "sensorsConfig") {
                $scope.back();
            } else {
                $rootScope.backBtnCallback();
            }
        }, 100);

    });