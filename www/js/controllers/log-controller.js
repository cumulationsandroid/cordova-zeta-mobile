angular.module('controllers.log', [])

.controller('LogCtrl', function($rootScope, $scope, LogSrvs, $ionicScrollDelegate, $location, $ionicPopup) {
  $rootScope.logArray = [];
  //$scope.LogFiltered = [];
  $rootScope.logArrayLimit = config.logOptions.maxNumberOfLogItems;
  $scope.categoryFilters = {
    info: true,
    warning: true,
    error: true,
  };

  $rootScope.logArrayName = "logArray";
  $scope.logFilteredArrayName = "LogFiltered";

  LogSrvs.getRecentLogMessages(function (recentLogs) {

      $rootScope.logArray = recentLogs;

  });
/*
  LogSrvs.subscribeForLogMessages(function (message) {
    $rootScope.logArray.unshift(message);
  });
*/

  $scope.goToTime = function (handle, goto, scroll) {

    var prev = $rootScope.logArray[0].created;

    if (handle == "monitor-logs") {
      cName = "monitor";
    } else {
      cName = "logs";
    }
    $rootScope.filters.gotoLoadedAfter = 0;
    $scope.filters.gotoBeforeIndex = -1;
    var scrolled = false;
    for (logIndex in  $rootScope.logArray) {
      var created = $rootScope.logArray[logIndex].created;
      if (prev <= goto && goto <= created && !scrolled) {
        console.log(created);
        $rootScope.filters.gotoIndex = logIndex;
        //Get next and previous 25 logs!
        $rootScope.LogFiltered = [];

          LogSrvs.loadOlder("goto", $scope).then(function () {
            LogSrvs.loadNewer("goto", null, $scope).then(function () {
              if (scroll) {
                $scope.scrollTo($rootScope.logArray[logIndex].created, handle);
              } else {
                   $ionicScrollDelegate.$getByHandle(handle).scrollTop();
              }
              
            });
          });
       

        scrolled = true;
        prev = created;
      } else {
        console.log("prev");
        prev = created;
      }
    }
    ;
  };

  $scope.gotScrolled = function (dh) {
    var delegate = $ionicScrollDelegate.$getByHandle(dh);
    //if bottom

    if (!$scope.addOnTop  && (delegate.getScrollView().__contentHeight - delegate.getScrollView().__clientHeight - 30) <= delegate.getScrollPosition().top && !$scope.bottomLoading ){

      $scope.bottomLoading = true;
      bottomLoadingIndicatorUpdate();
      LogSrvs.loadOlder("add", $scope).then(function () {
        setTimeout(function () {
          $scope.bottomLoading = false;
          bottomLoadingIndicatorUpdate();
        }, 2000);
      });


    }
    // console.log(delegate.getScrollPosition().top);
    if (delegate.getScrollPosition().top <= 0) {
      topLoadingIndicatorUpdate();
      LogSrvs.loadNewer("add", "monitor-logs", $scope).then(function () {
        setTimeout(function () {
          $scope.bottomLoading = false;
          topLoadingIndicatorUpdate();
        }, 1500);
      });
      //$scope.addOnTop = true;
    } else {
      $scope.addOnTop = false;
    }
  };;

  $scope.showCategoryFiltersPopup = function () {
    $scope.categoryFiltersPopup = $ionicPopup.show({
      templateUrl: 'templates/popup-filters.html',
      title: 'Set filters',
      subTitle: 'Please select log categories',
      scope: $scope,
      buttons: [
        {
          text: '<b>Ok</b>',
          type: 'button-positive',
          onTap: function (e) {


            e.preventDefault();
            console.log('Log categories are set');
            $scope.closeCatPopup();
            $('.filters_settings').hide();
            var goto = $scope.filters.goto;
            if (goto == 0){
              if ($scope.monitoringLog.length > 0){
                goto = $scope.LogFiltered[0].created;
              } else {
                goto = $scope.monitoringLog[$scope.logArray.length-1].created;
              }
            }            
            $scope.goToTime("logs", goto, true);
          }
        }
      ]
    });
  };

  $scope.closeCatPopup = function () {
    if ($scope.categoryFiltersPopup) {
      $scope.categoryFiltersPopup.close();
    }
  }

  $scope.$watch('logFilters.goto', function() {
    if ($rootScope.logFilters.goto != 0) {
      //Go To
      now = new Date();
      $scope.addOnTop = false;
      $rootScope.logFilters.goto.setDate(now.getDate());
      $rootScope.logFilters.goto.setMonth(now.getMonth());
      $rootScope.logFilters.goto.setFullYear(now.getFullYear());
      var goto = $rootScope.logFilters.goto.getTime();
      $scope.goToTime("logs", goto, true);

    }
  });

  $scope.scrollLogToTop = function (handle, goto_disable) {
    $rootScope.filters = {
      //from: date.toISOString(),
      // to: new Date(new Date().getTime() + 300000).toISOString(), //+3s
      goto: 0,
      gotoIndex: null,
      gotoBeforeIndex: -1,
      gotoAfterIndex: 0
    };
    var goto = $rootScope.logArray[$rootScope.logArray.length-1].created;
    if (goto){
      $scope.addOnTop = true;
      if (!goto_disable) {
        $scope.goToTime("logs", goto, false);
      }
    }
    //$ionicScrollDelegate.$getByHandle(handle).scrollTop();
  };
  
  var bottomLoadingIndicatorUpdate = function (){
    if ($scope.bottomLoading) {
      if ($(".loadding-bottom").length > 0){
        $(".loadding-bottom").remove();
      }
      $(".scroll").append("<p class='loadding-bottom'>Loading...</p>");
    } else {
      $(".loadding-bottom").remove();
    }
    $scope.resizeMonitor;
  };

  var topLoadingIndicatorUpdate = function () {
    if ($scope.topLoading) {
      if ($(".loadding-top").length > 0){
        $(".loadding-top").remove();
      }
      $(".scroll").prepend("<p class='loadding-top'>Loading...</p>");
    } else {
      $(".loadding-top").remove();
    }
    $scope.resizeMonitor;
  };
  
  $scope.scrollTo = function(target, handle){
    $location.hash(target);
    var dHandle = $ionicScrollDelegate.$getByHandle(handle);
    dHandle.anchorScroll(true);
  };

  //$rootScope.LogFiltered
});