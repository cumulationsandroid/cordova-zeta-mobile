// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('zetamobile', ['ionic', 'ion-datetime-picker', 'ngCordova', 'ngIOS9UIWebViewPatch', 'ngCouchbaseLite', 'chart.js',
    'services.monitor', 'services.chart', 'services.http', 'services.db', 'services.profile', 'services.time', 'services.sensor', 'services.wear', 'services.log', 'services.network',
    'controllers.monitor', 'controllers.history', 'controllers.settings', 'controllers.log', 'controllers.configuration', 'angular-uuid'])

    .run(function ($rootScope, $ionicPlatform, $state, $ionicPopup, $ionicBackdrop, $timeout, LogSrvs, NetworkSrvs, DbSrvs, ProfileSrvs, TimeSrvs, SensorSrvs, WearSrvs) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }


            // Initializations
            LogSrvs.initializeLog(function () {
                LogSrvs.log('Log initialization finished.', 'info');
                NetworkSrvs.initializeNetwork(function () {
                    LogSrvs.log('Network initialization finished.', 'info');
                    DbSrvs.initializeDb(function () {
                        LogSrvs.log('Local DB initialization finished.', 'info');
                        ProfileSrvs.initializeProfile(function () {
                            LogSrvs.log('Profile initialization finished.', 'info');
                            SensorSrvs.initializeSensors(function () {
                                LogSrvs.log('Local sensors initialization finished.', 'info');
                                WearSrvs.initializeWear(function () {
                                    LogSrvs.log('Android wear initialization finished.', 'info');
                                    TimeSrvs.initNtp(function () {
                                        if (!NetworkSrvs.isBluetoothEnabled()) {
                                            NetworkSrvs.showBlueToothDialog();
                                        }


                                        if (!ProfileSrvs.checkIsProfileSet()) {
                                            LogSrvs.log('Redirecting to settings tab to set profile.', 'info');
                                            $state.go('tab.settings');
                                        } else {
                                            $state.go('tab.monitor');
                                        }
                                    });

                                });
                            });
                        });
                    });
                });
            });

            $rootScope.backBtnCallback = function () {
                if ($state.current.name == "tab.monitor") {
                    if ($rootScope.started) {
                        $ionicPopup.show({
                            title: 'Monitor is running',
                            subTitle: 'Please stop monitoring before close application.',
                            buttons: [
                                {
                                    text: 'OK', onTap: function (e) {
                                    return true;
                                }
                                }
                            ]
                        });
                    } else {
                        $ionicPopup.show({
                            title: 'Close ZetaMobile',
                            subTitle: 'Are you sure you want to close ZetaMobile?',
                            buttons: [
                                {
                                    text: 'Cancel', onTap: function (e) {
                                    return true;
                                }
                                },
                                {
                                    text: '<b>Ok</b>',
                                    type: 'button-positive',
                                    onTap: function (e) {
                                        navigator.app.exitApp();
                                    }
                                },
                            ]
                        });
                    }
                } else {
                    $ionicHistory.goBack();
                }
            };

            $ionicPlatform.registerBackButtonAction(function () {

                $rootScope.backBtnCallback();

            }, 100);

            $rootScope.closeAllPopups = function () {
                noop = angular.noop;
                elevated = false;
                var popupStack = $ionicPopup._popupStack;
                if (popupStack.length > 0) {
                    popupStack.forEach(function(popup, index) {
                        if (popup.isShown === true) {
                            popup.remove();
                            popupStack.pop();
                        }
                    });


                    $ionicBackdrop.release();
                    //Remove popup-open & backdrop if this is last popup
                    $timeout(function() {
                        // wait to remove this due to a 300ms delay native
                        // click which would trigging whatever was underneath this
                        $ionicBody.removeClass('popup-open');
                        // $ionicPopup._popupStack.pop();
                    }, 400, false);
                    ($ionicPopup._backButtonActionDone || noop)();
                }


            }

            $rootScope.typeof = function (obj) {
                return typeof obj;
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

        // setup an abstract state for the tabs directive
            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html'
            })

            // Each tab has its own nav history stack:

            .state('tab.monitor', {
                url: '/monitor',
                views: {
                    'tab-monitor': {
                        templateUrl: 'templates/tab-monitor.html',
                        controller: 'MonitorCtrl'
                    }
                }
            })

            .state('tab.history', {
                url: '/history',
                views: {
                    'tab-history': {
                        templateUrl: 'templates/tab-history.html',
                        controller: 'HistoryCtrl'
                    }
                }
            })

            .state('tab.settings', {
                url: '/settings',
                views: {
                    'tab-settings': {
                        templateUrl: 'templates/tab-settings.html',
                        controller: 'SettingsCtrl'
                    }
                }
            })

            .state('tab.log', {
                url: '/log',
                views: {
                    'tab-log': {
                        templateUrl: 'templates/tab-log.html',
                        controller: 'LogCtrl'
                    }
                }
            })
            .state('sensorsConfig', {
                url: '/configuration',
                templateUrl: 'templates/configuration.html',
                controller: 'ConfigurationCtrl',
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/tab/monitor');
    });
