angular.module('services.time', [])

    .factory('TimeSrvs', function ($rootScope, LogSrvs, $q) {
            var servers = config.ntp_servers;

            var initialize = function () {
                commproxy.getClockOffset(
                    function (result) {
                        $rootScope.timeOffset = result.offset;
                        LogSrvs.log("Time offset is "+$rootScope.timeOffset);
                    }, function (error) {
                        LogSrvs.log("Error retrieving offset: "+error, "error");
                    });
            };

            var getCurrentTime = function () {
                return Date.now() + $rootScope.timeOffset;
            };
            var getCorrectTime = function (time) {
                return time + $rootScope.timeOffset;
            };

            return {
                initNtp: function (callback) {
                    initialize(callback);
                },
                getCurrentTime: function () {
                    return getCurrentTime();
                },
                getCorrectTime: function (time) {
                    return getCorrectTime(time);
                }
            };
        }
    );