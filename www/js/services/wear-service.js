angular.module('services.wear', [])

    .factory('WearSrvs', function ($rootScope, $cordovaDevice, $timeout, LogSrvs, $q, $injector, $interval, ProfileSrvs, $cordovaLocalNotification) {
        var WearSensorContainer = null;
        var MetaWearSensorContainer = null;
        var statuses = {
            wear: {
                wearExists: false,
                wearEnabled: false,
                started: null,
                macAddress: null
            },
            metaWear: {
                wearExists: false,
                wearEnabled: false,
                macAddress: null
            }
        };
        if (typeof $rootScope.externalDevices == "undefined") {
            $rootScope.externalDevices = {};
        }
        ;

        $rootScope.dataTransferring = {
            'wear': false,
            'metaWear': false
        };
        $rootScope.dataTransferTimeout = {
            'wear': false,
            'metaWear': false
        };
        $rootScope.metaWearConnectionStatus = "DISCONNECTED";

        $rootScope.pingInterval = {};
        //$rootScope.metaWearStatus

        var injectMonitorSrvs = function () {
            return $injector.get('MonitorSrvs');
            //injectMonitorSrvs().readSensor();
        };

        var wearSensorPrefix = 'Wear-';
        var currentConnectionStatus = '';
        var connectionStatusMessagesSubscriptions = [];

        var configurations = {
            wear: {
                getDeviceCapabilities: {
                    "name": "getDeviceCapabilities",
                    "description": "SensorAgent getDeviceCapabilities",
                    "guid": "5c2f4ed5-39f9-4162-b91f-b034f38d9e9d",
                    "uri": "Method://3d0523bd64000f81.Motorolametallica.SensorAgent.getDeviceCapabilities",
                    "type": "METHOD",
                    "data": {"timeStamp": 0},
                    "nodes": []
                },
                getConnectionState: {
                    "name": "getConnectionStates",
                    "description": "The connection state with the external device",
                    "guid": "b0c8d8c8-d086-4393-a2b3-e8133bda0f8c",
                    "uri": "Notification://ZetaMobile.CommBle.getConnectionStates",
                    "type": "METHOD",
                    "data": {"dataType": "ENUMERATION", "timeStamp": 0},
                    "nodes": []
                }
            },
            metaWear: {
                getDeviceCapabilities: {
                    "name": "getDeviceCapabilities",
                    "description": "SensorAgent getDeviceCapabilities",
                    "guid": "68c3b8ba-a77c-460b-8986-480aa8a48791",
                    "uri": "Method:\\/\\/3d0523bd64000f81.MetaWear.SensorAgent.getDeviceCapabilities",
                    "type": "METHOD",
                    "data": {
                        "timeStamp": 0
                    },
                    "nodes": []
                },
                setConfiguration: {
                    "name": "setConfiguration",
                    "description": "SensorAgent setConfiguration",
                    "guid": "5c2f4ed5-39f9-4162-b91f-b034f38d9e9d",
                    "uri": "Method://MAC_ADDRESS.MetaWear.SensorAgent.setConfiguration",
                    "type": "METHOD",
                    "data": {
                        "timeStamp": 0
                    },
                    "nodes": [{
                        "description": "This is MetaWear",
                        "type": "OBJECT",
                        "guid": "AECD4EB7-FBAA-4CB6-8368-212AA733E6C2",
                        "name": "MetaWear",
                        "uri": "Device:\\/\\/MAC_ADDRESS.MetaWear",
                        "data": {
                            "timeStamp": 0
                        },
                        "nodes": [{
                            "description": "metawear.sensor.temperature",
                            "type": "OBJECT",
                            "guid": "BFA39631-3EBF-4F1B-A5B3-A9B8DA97B7A0",
                            "name": "Temperature Sensor",
                            "uri": "Sensor:\\/\\/MAC_ADDRESS.MetaWear.sensor.temperature",
                            "data": {
                                "timeStamp": 0
                            },
                            "nodes": [{
                                "description": "metawear.sensor.temperature.temperature",
                                "isEnabled": true,
                                "samplingDuration": 4000000,
                                "guid": "14A0B0EC-9E9C-477A-B432-B31AAED76E35",
                                "name": "Temperature Sensor temperature",
                                "type": "OBJECT",
                                "data": {
                                    "dataType": "FLOAT",
                                    "timeStamp": 0
                                },
                                "nodes": [],
                                "uri": "Channel:\\/\\/MAC_ADDRESS.MetaWear.sensor.temperature.temperature"
                            }]
                        }, {
                            "description": "metawear.sensor.heartrate",
                            "type": "OBJECT",
                            "guid": "BFA39631-3EBF-4F1B-A5B3-A9B8DA97B7A0",
                            "name": "Heartrate Sensor",
                            "data": {
                                "timeStamp": 0
                            },
                            "uri": "Sensor:\\/\\/MAC_ADDRESS.MetaWear.sensor.heartrate",
                            "nodes": [{
                                "description": "metawear.sensor.heartrate.heartrate",
                                "isEnabled": true,
                                "samplingDuration": 7000000,
                                "guid": "14A0B0EC-9E9C-477A-B432-B31AAED76E35",
                                "name": "Heartrate Sensor heartrate",
                                "type": "OBJECT",
                                "data": {
                                    "dataType": "FLOAT",
                                    "timeStamp": 0
                                },
                                "nodes": [],
                                "uri": "Channel:\\/\\/MAC_ADDRESS.MetaWear.sensor.heartrate.heartrate"
                            }]
                        }, {
                            "description": "metawear.sensor.gyroscope",
                            "type": "OBJECT",
                            "guid": "BFA39631-3EBF-4F1B-A5B3-A9B8DA97B7A0",
                            "name": "Gyroscope Sensor",
                            "data": {
                                "timeStamp": 0
                            },
                            "uri": "Sensor:\\/\\/MAC_ADDRESS.MetaWear.sensor.gyroscope",
                            "nodes": [{
                                "description": "metawear.sensor.gyroscope.x",
                                "isEnabled": true,
                                "samplingDuration": 5000000,
                                "guid": "14A0B0EC-9E9C-477A-B432-B31AAED76E35",
                                "name": "Gyroscope Sensor Channel x",
                                "type": "OBJECT",
                                "data": {
                                    "dataType": "FLOAT",
                                    "timeStamp": 0
                                },
                                "nodes": [],
                                "uri": "Channel:\\/\\/MAC_ADDRESS.MetaWear.sensor.gyroscope.x"
                            }, {
                                "description": "metawear.sensor.gyroscope.y",
                                "isEnabled": true,
                                "samplingDuration": 5000000,
                                "guid": "14A0B0EC-9E9C-477A-B432-B31AAED76E35",
                                "name": "Gyroscope Sensor Channel y",
                                "type": "OBJECT",
                                "data": {
                                    "dataType": "FLOAT",
                                    "timeStamp": 0
                                },
                                "nodes": [],
                                "uri": "Channel:\\/\\/MAC_ADDRESS.MetaWear.sensor.gyroscope.y"
                            }, {
                                "description": "metawear.sensor.gyroscope.z",
                                "isEnabled": true,
                                "samplingDuration": 5000000,
                                "guid": "14A0B0EC-9E9C-477A-B432-B31AAED76E35",
                                "name": "Gyroscope Sensor Channel z",
                                "type": "OBJECT",
                                "data": {
                                    "dataType": "FLOAT",
                                    "timeStamp": 0
                                },
                                "nodes": [],
                                "uri": "Channel:\\/\\/MAC_ADDRESS.MetaWear.sensor.gyroscope.z"
                            }]
                        }]
                    }]
                },
                getDevicesList: {
                    "name": "showDeviceListing",
                    "description": "Show BLE scan UI",
                    "guid": "804e1d1f-2a92-459c-8be8-56e6f116f2f0",
                    "uri": "Method://MetaWear.showDeviceListing",
                    "type": "METHOD",
                    "data": {
                        "timeStamp": 0
                    },
                    "nodes": []
                },

                getConnectionState: {
                    "name": "getConnectionStates",
                    "description": "The connection state with the external device",
                    "guid": "6c6db69f-2e41-4632-b75e-8d6ad67a61b0",
                    "uri": "Method://3d0523bd64000f81.MetaWear.SensorAgent.getConnectionStates",
                    "type": "METHOD",
                    "data": {
                        "dataType": "ENUMERATION"
                    },
                    "nodes": []
                },
                getState: {
                    "name": "getState",
                    "description": "The functional state of the SensorAgent",
                    "guid": "b0c8d8c8-d086-4393-a2b3-e8133bda0f8c",
                    "uri": "Method://3d0523bd64000f81.MetaWear.SensorAgent.getState",
                    "type": "METHOD",
                    "data": {
                        "dataType": "ENUMERATION",
                        "timeStamp": 0
                    },
                    "nodes": []
                },
                start: {
                    "name": "start",
                    "description": "SensorAgent start",
                    "guid": "cd374690-7aa9-4148-8fab-12d24fa9fd04",
                    "uri": "Method://F24F6C8C5185.MetaWear.SensorAgent.start",
                    "type": "METHOD",
                    "data": {
                        "timeStamp": 0
                    },
                    "nodes": []
                },
                stop: {
                    "name": "stop",
                    "description": "SensorAgent stop",
                    "guid": "501abd0b-d1f9-4582-b77f-0863b774f7db",
                    "uri": "Method://F24F6C8C5185.MetaWear.SensorAgent.stop",
                    "type": "METHOD",
                    "data": {
                        "timeStamp": 0
                    },
                    "nodes": []
                },
                reset: {
                    "name": "reset",
                    "description": "Reset device configurations",
                    "guid": "48fcf1bb-e665-4f5a-9f59-2212be07ab88",
                    "uri": "Method://F24F6C8C5185.MetaWear.SensorAgent.reset",
                    "type": "METHOD",
                    "data": {
                        "timeStamp": 0
                    },
                    "nodes": []
                }
            },
        };

        var channelNames = {
            'Wear-Step_counter': {'x': 'steps'},
            'Wear-Temperature': {x: 'temperature'},
            'Wear-Heartrate': {x: 'heartRate'}
        };

        $rootScope.isGetDeviceCapabilitiesSent = false;

        $rootScope.$watch('wearStatus', function (value) {
            if (!$rootScope.isGetDeviceCapabilitiesSent && $rootScope.wearStatus == "CONNECTED") {

                sendMessageToWear(configurations.wear.getDeviceCapabilities, function (success) {
                    if (!success) {
                        $rootScope.wearActive = false;
                        console.log('Wear - getDeviceCapabilities failed');
                        LogSrvs.log('Android wear is not active.', 'warning');
                        WearSensorContainer = null;
                    } else {
                        console.log('Wear - getDeviceCapabilities success');
                        statuses.wear.wearExists = true;
                        enableConnectionListener();
                        // samplingDuration is microseconds
                        // var setConfiguration = {"name":"setConfiguration","description":"SensorAgent setConfiguration","guid":"c5c02dc1-d325-4419-9eb8-5f3916896477","uri":"Method://3d0523bd64000f81.Motorolametallica.SensorAgent.setConfiguration","type":"METHOD","data":{"timeStamp":0},"nodes":[{"data":{"timeStamp":0},"description":"This is Motorola metallica","guid":"9ce047b4-1c8a-4bbb-9a78-5bc77033be46","name":"Motorola metallica","nodes":[{"name":"Accelerometer Sensor","description":"android.sensor.accelerometer","guid":"51773abd-5dda-449f-9478-f40990c03bc3","uri":"Sensor://android.sensor.accelerometer","type":"OBJECT","data":{"timeStamp":0},"nodes":[{"name":"Accelerometer Sensor Channel 1","description":"android.sensor.accelerometer.1","guid":"84aba9f3-55cc-4e36-aa19-060045e49e36","uri":"Channel://android.sensor.accelerometer.channel1","type":"OBJECT","samplingDuration":3000000,"isEnabled":true,"data":{"dataType":"FLOAT","timeStamp":0},"nodes":[]},{"name":"Accelerometer Sensor Channel 2","description":"android.sensor.accelerometer.2","guid":"d1ce3786-6557-4a02-a877-e96895fb4984","uri":"Channel://android.sensor.accelerometer.channel2","type":"OBJECT","samplingDuration":3000000,"isEnabled":true,"data":{"dataType":"FLOAT","timeStamp":0},"nodes":[]},{"name":"Accelerometer Sensor Channel 3","description":"android.sensor.accelerometer.3","guid":"05f88d8b-f03e-4e7d-b7e5-86b93a02af48","uri":"Channel://android.sensor.accelerometer.channel3","type":"OBJECT","samplingDuration":3000000,"isEnabled":true,"data":{"dataType":"FLOAT","timeStamp":0},"nodes":[]}]}],"type":"OBJECT","uri":"Device://f99e4da17af67c85.Motorolametallica"}]};
                    }
                });
                $rootScope.isGetDeviceCapabilitiesSent = true;
            }
        });

        var initialize = function (callback) {
            if (!WearSensorContainer && commproxy) {
                var isAndroid = $cordovaDevice.getPlatform().toLowerCase().indexOf('android') > -1;
                if (isAndroid) {
                    console.log('Wear - Initialization started');
                    WearSensorContainer = new SensorContainer();

                    sendMessageToWear(configurations.wear.getConnectionState);

                } else {
                    $rootScope.wearActive = false;
                    LogSrvs.log('Android wear is not active.', 'warning');
                    console.log('Wear - Initialization stopped, not android platform');
                }
            }
            if (typeof callback == "function") {
                callback();
            }
        }
        var initMetaWear = function () {
            MetaWearSensorContainer = new SensorContainer();

            sendMessageToWear(configurations.metaWear.getDeviceCapabilities, function (success) {
                console.log('MetaWear - getDeviceCapabilities success');
                if (MetaWearSensorContainer.listSensorAgent.length > 0) {
                    statuses.metaWear.wearEnabled = true;
                    $rootScope.noSensorsSelected = false;
                    $rootScope.metaWearActive = true;
                    LogSrvs.log('MetaWear is active.', 'info');
                    console.log('MetaWear - Active, sensor agent successfully set');
                } else {
                    $rootScope.metaWearActive = false;
                    LogSrvs.log('MetaWear is not active.', 'warning');
                    console.log('MetaWear - Not active, sensor agent not set');
                }
                if (typeof callback == "function") {
                    callback();
                }
            });
        };
        var sendMessageToMetaWear = function (method, callback) {

            sendMessageToWear(configurations.metaWear[method], function (data) {
                if (typeof callback == "function") {
                    callback(data);
                }
            });
        };

        // ******* Temp until refactoring the whole app to comply with the new data model

        var tempDataFunction = function (val) {
            var prefix = "";
            var sessionId = null;
            if (val.description.split("||").length == 2) {
                sessionId = val.description.split("||")[0];
                prefix = val.description.split("||")[1].split(".")[0];
            } else {
                sessionId = null;
                prefix = val.description.split("//")[1].split(".")[0];
            }
            prefix = formatPrefix(prefix);
            var channel_letters = ['', 'x', 'y', 'z'];
            var channel_name = getChannelFromUri(val.uri);
            var data = {
                sensor: (prefix ? prefix + " - " : "") + getSensorFromUri(val.uri),
                channel: channel_name,// == 'channel1' ? 'x' : (channel_name == 'channel2' ? 'y' : (channel_name == 'channel3' ? 'y' : channel_name)),
                timestamp: !val.data.timeStamp ? Math.floor(Date.now()) : Math.floor(val.data.timeStamp), // timestamp is milliseconds
                value: getFloatFromBase64ByteArray(val.data.values),
                sessionID: sessionId
            };

            if (typeof channelNames[data.sensor] == "object" && typeof channelNames[data.sensor][data.channel] == "string") {
                data.channel = channelNames[data.sensor][data.channel];
            }
            injectMonitorSrvs().readSensor(data);

        };

        var dataResults = {};
        var aggregate = function (callback, data) {
            console.log(data);
            var uniqueName = data.sensor + '_' + Math.floor(data.timestamp / 10);

            var chName = chNoToChName(data.channel);
            if (!dataResults[uniqueName]) {
                dataResults[uniqueName] = {
                    sensor: data.sensor,
                    timestamp: data.timestamp,
                }
                dataResults[uniqueName][chName] = data.value;
            } else {
                dataResults[uniqueName][chName] = data.value;
                if (Object.keys(dataResults[uniqueName]).length == 5) {
                    callback(dataResults[uniqueName]);
                    delete dataResults[uniqueName];
                }
            }
        }

        var chNoToChName = function (chNumber) {
            if (chNumber === 'channel1') {
                return 'x';
            } else if (chNumber === 'channel2') {
                return 'y';
            } else if (chNumber == 'channel3') {
                return 'z';
            }
        }

        var getFloatFromBase64ByteArray = function (base64str) {
            var a = atob(base64str);
            var c = new DataView(new ArrayBuffer(4));
            for (var i = 0, len = a.length; i < len; i++) {
                c.setInt8(i, a.charCodeAt(i));
            }
            return c.getFloat32(0);
        }

        var getSensorFromUri = function (uri) {
            var a = uri.split('.sensor.')[1];
            /*
             if (uri.indexOf('.sensor.') > -1) {
             var a = uri.split('.sensor.')[1];
             } else if (uri.indexOf('.Channel.') > -1) {
             var a = uri.split('.Channel.')[1];
             } else if (uri.indexOf('.channel.') > -1) {
             var a = uri.split('.channel.')[1];
             }
             */

            var b = a.replace(/\\\//g, "/").split('.')[0];
            return b.charAt(0).toUpperCase() + b.slice(1);
        }

        var getChannelFromUri = function (uri) {
            var a = uri.split('.sensor.')[1];
            return a.split('.')[1];
        }

        // *******

        var commproxySuccessCallback = function (value, callback) {

            //Hardcoded ConnectionListenerSuccessCallback

            var value_obj = JSON.parse(value);
            var action = value_obj.uri.replace(/\\\//g, "/").split("://")[0];
            console.log('WearSrvs received data: Name: ' + value_obj.name + '; ' + value_obj.uri + "; value: " + (typeof value_obj.data.values != "undefined" ? atob(value_obj.data.values) : "") + " | " + (new Date()));
            //console.log(value_obj);
            switch (action) {
                case 'Channel':
                    successStartCallback(value);
                    break;

                case 'Device':
                    callback(true);
                    break;

                case 'SensorAgent':
                    sensorAgentCallback(value, callback);
                    break;
                case 'Notification':
                    if (value_obj.name == "ConnectionStates") {
                        ConnectionListenerSuccessCallback(value_obj, callback);
                    } else if (value_obj.name == "getState") {
                        MetaWearConnectionListenerSuccessCallback(value_obj, callback);
                    } else if (value_obj.name == "Notification") {
                        var value = atob(value_obj.data.values);
                        if (value == "SESSION STOPPED DUE TO USER ACTION" && WearSensorContainer) {
                            //commproxyPing("stop", WearSensorContainer);
                        }

                        var message = "Wear: " + value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();

                        LogSrvs.log("Notification: " + message, 'info');

                        displayNotification(message);
                    }
                    break;
            }

        };

        var successStartCallback = function (value) {
            $rootScope.dataCounters.startCallbackTriggered++;
            if (value.indexOf('samplingDuration') > -1) {

                // callback(WearSensorContainer.objectify(value));

                var uri = JSON.parse(value).uri;
                var sensorContainer = null;

                if (uri.replace(/\\\//g, "/").split('/')[2].split(".").indexOf('MetaWear') > -1) {
                    if (MetaWearSensorContainer) {
                        sensorContainer = MetaWearSensorContainer;
                    } else {
                        sensorContainer = new SensorContainer();
                    }
                    var device = 'metaWear';
                } else {
                    if (WearSensorContainer) {
                        sensorContainer = WearSensorContainer;
                    } else {
                        sensorContainer = new SensorContainer();
                    }

                    var device = 'wear';
                }
                if (typeof $rootScope.dataTransferTimeout[device]) {
                    $timeout.cancel($rootScope.dataTransferTimeout[device]);
                }

                $rootScope.dataTransferring[device] = true;
                tempDataFunction(sensorContainer.objectify(value));
                $rootScope.dataTransferTimeout[device] = $timeout(function () {
                    $rootScope.dataTransferring[device] = false;
                }, 6000);
            }
        };

        var successStopCallback = function (value) {
            console.log('Andorid wear stop success callback');
            console.log(value);
        };

        var failStartCallback = function (value) {
            console.log('Andorid wear start - error callback');
            console.log(value);
        };

        var failStopCallback = function (value) {
            console.log('Andorid wear stop error callback');
            console.log(value);
        };

        var ConnectionListenerSuccessCallback = function (successValue, callback) {
            console.log(successValue);
            currentConnectionStatus = atob(successValue.data.values);//getFloatFromBase64ByteArray(successValue.data.values);
            for (var i = 0, len = connectionStatusMessagesSubscriptions.length; i < len; i++) {
                connectionStatusMessagesSubscriptions[i](currentConnectionStatus);
            }
            if (successValue.uri.split("//")[1].split(".").indexOf("MetaWear") > -1) {
                LogSrvs.log('MetaWear connection status is ' + currentConnectionStatus + '.', currentConnectionStatus == 'connected' ? 'info' : 'warning');
                $rootScope.metaWearConnectionStatus = currentConnectionStatus;
                if (currentConnectionStatus == "DISCONNECTED") {
                    $rootScope.metaWearConnectionStatus = "";
                } else if (currentConnectionStatus != "") {
                    statuses.metaWear.wearEnabled = true;
                }

            } else {
                LogSrvs.log('Android wear connection status is ' + currentConnectionStatus + '.', currentConnectionStatus == 'connected' ? 'info' : 'warning');
                $rootScope.wearStatus = currentConnectionStatus;
            }

            if (typeof callback == "function") {
                callback(successValue);
            }

        };

        var MetaWearConnectionListenerSuccessCallback = function (successValue, callback) {
            console.log("WearSrvs MetaWearConnectionListenerSuccessCallback");
            console.log(successValue);
            currentConnectionStatus = atob(successValue.data.values);//getFloatFromBase64ByteArray(successValue.data.values);
            $rootScope.metaWearStatus = currentConnectionStatus;
            if (currentConnectionStatus == "WAITING_CONFIGURATION") {
                initMetaWear();
            } else if (currentConnectionStatus == "READY") {
                if (!statuses.metaWear.wearEnabled) {
                    /**/
                }
            } else if (currentConnectionStatus == "ERROR") {
//               do nothing: reset is invoked by plugin itself
                /*$timeout(function () {
                    if ($rootScope.wearStatus != "DISCONNECTED") {
                        sendMessageToWear(configurations.metaWear.reset);
                    }
                }, 3000);*/
            }

            for (var i = 0, len = connectionStatusMessagesSubscriptions.length; i < len; i++) {
                connectionStatusMessagesSubscriptions[i](currentConnectionStatus);
            }
            LogSrvs.log('Android wear connection status is ' + currentConnectionStatus + '.', currentConnectionStatus == 'connected' ? 'info' : 'warning');
            $rootScope.metaWearStatus = currentConnectionStatus;

            if (callback) {
                callback();
            }
        };

        var displayNotification = function (message) {

            window.plugins.toast.show(message, 'short', 'bottom');

            $cordovaLocalNotification.schedule({
                id: 1,
                title: 'Zeta Mobile',
                text: message,
                smallIcon: 'img/ic_stat_zm.png',
                icon: 'img/ic_stat_zm.png'
            }).then(function (result) {

            });
        };

        var ConnectionListenerFailCallback = function (errorValue) {
            LogSrvs.log('Wear connection error: ' + errorValue, 'error');
        };

        var commproxySuccessCallbackTransmit = function (res) {
            console.log('Wear transmit CSV success: ' + res);
        };

        var sensorAgentCallback = function (successValue, callback) {
            console.log("WearSrvs sensorAgentCallback");
            var value = JSON.parse(successValue);
            if (value.uri.split(".").indexOf('MetaWear') > -1) {
                if (!MetaWearSensorContainer) {
                    MetaWearSensorContainer = new SensorContainer()
                }
                var sensorAgent = MetaWearSensorContainer.objectify(successValue);
                MetaWearSensorContainer.addSensorAgent(sensorAgent);
                statuses.metaWear.macAddress = value.macAddress;

                var setConfigObject = setConfigurationUUIDs(configurations.metaWear.setConfiguration, statuses.metaWear.macAddress);

                storedConfig = getDeviceConfig("metaWear");
                if (storedConfig) {
                    LogSrvs.log("Meta wear - Set user configuration");
                    setConfigObject.nodes[0] = storedConfig;
                    $rootScope.externalDevices.metaWear = storedConfig;
                } else {
                    LogSrvs.log("Meta wear - Set default configuration");
                    $rootScope.externalDevices.metaWear = setConfigObject.nodes[0]
                }

                sendMessageToWear(setConfigObject);
                $timeout(function () {
                    sendMessageToWear(configurations.metaWear.getState);
                }, 3000);
            } else {
                if (!WearSensorContainer) {
                    WearSensorContainer = new SensorContainer()
                }
                var sensorAgent = WearSensorContainer.objectify(successValue);
                WearSensorContainer.addSensorAgent(sensorAgent);
                statuses.wear.macAddress = value.macAddress;

                var setConfigObject = WearSensorContainer.getSensorAgentNode("setConfiguration");
                var setConfigurationNode = JSON.parse(setConfigObject);

                storedConfig = getDeviceConfig("wear");

                if (storedConfig) {
                    LogSrvs.log("Wear - Set user configuration");
                    setConfigurationNode.nodes[0] = storedConfig;
                    $rootScope.externalDevices.wear = storedConfig;
                } else {
                    LogSrvs.log("Meta wear - Set default configuration");
                    var deviceNodeString = WearSensorContainer.getSensorAgentNode("device");
                    deviceNode = JSON.parse(deviceNodeString);
                    setConfigurationNode.nodes[0] = deviceNode;
                    $rootScope.externalDevices.wear = deviceNode
                }

                if ($rootScope.wearStatus != "DISCONNECTED") {
                    sendMessageToWear(setConfigurationNode, function () {
                    });
                    console.log('Wear - setConfiguration');
                    if (WearSensorContainer.listSensorAgent.length > 0) {
                        statuses.wear.wearEnabled = true;
                        $rootScope.noSensorsSelected = false;
                        $rootScope.wearActive = true;
                        LogSrvs.log('Android wear is active.', 'info');
                        console.log('Wear - Active, sensor agent successfully set');
                    } else {
                        $rootScope.wearActive = false;
                        LogSrvs.log('Android wear is not active.', 'warning');
                        console.log('Wear - Not active, sensor agent not set');
                    }
                } else {
                    LogSrvs.log("Can't set configuration. Wear is disconnected", "error");
                }
            }

            callback(true);
        };

        var sendMessageToWearFailCallback = function (errorValue, callback) {
            console.log('Andorid sendMessageToWear error callback');
            console.log(errorValue);
            callback(false);
        };

        var getSensorAgentNode = function (nodeName, sensorContainer) {
            var deferred = $q.defer();
            var requestedNode = null;
            var sensorAgent = sensorContainer.getSensorAgent(0);
            if (sensorAgent) {
                var nodes = sensorAgent.getNodes();
                for (var i = 0, len = nodes.length; i < len; i++) {
                    var node = nodes[i];
                    if (node.getUri().indexOf(nodeName) > -1) {
                        requestedNode = sensorContainer.serialize(node);
                        deferred.resolve(requestedNode);
                    }

                    if (i == nodes.length - 1 && !requestedNode) {
                        deferred.reject("Node not found");
                    }
                }
            }
            return deferred.promise
        };

        var start = function (sessionId) {
            if (!statuses.wear.started && WearSensorContainer) {
                var sensorAgent = WearSensorContainer.getSensorAgent(0);

                startNode = WearSensorContainer.getSensorAgentNode("start");
                if (startNode) {
                    startNode = addSessionIdToStartNode(startNode, sessionId);
                    console.log("WearSrvs start wear");
                    if ($rootScope.wearStatus == "CONNECTED") {
                        commproxy.sendMessage('transmitData', startNode, function (value) {
                            commproxySuccessCallback(value);
                        }, failStartCallback);
                        statuses.wear.started = true;
                    } else {
                        LogSrvs.log("Wear is disconnected. Can't start monitoring it. ", "error");
                    }
                    //commproxyPing("start", WearSensorContainer);
                } else {
                    LogSrvs.log('Wear failed to start: start node not found', 'error');
                }


            } else {
                console.warn('Monitoring already running');
            }
        };

        var startMetaWear = function (sessionId) {
            if (!statuses.metaWear.started && MetaWearSensorContainer) {
                var sensorAgent = MetaWearSensorContainer.getSensorAgent(0);
                getSensorAgentNode("start", MetaWearSensorContainer).then(function (startNode) {
                    startNode = addSessionIdToStartNode(startNode, sessionId);
                    console.log("WearSrvs start MetaWear");
                    commproxy.sendMessage('transmitData', startNode, function (value) {
                        commproxySuccessCallback(value);
                    }, failStartCallback);
                    statuses.metaWear.started = true;
                    //commproxyPing("start", MetaWearSensorContainer);
                });
            } else {
                console.warn('Monitoring already running');
            }
        };

        var addSessionIdToStartNode = function (node, sessionId) {
            nodeObj = JSON.parse(node);
            nodeObj.nodes[0].data.values = btoa(sessionId);
            nodeObj.nodes[0].data.dataType = "STRING";

            node = JSON.stringify(nodeObj);
            return node;
        };

        var setConfigurationUUIDs = function (object, macAddress) {
            var uri = object.uri.replace("MAC_ADDRESS", macAddress.replace(":", ""));
            console.log(uri);
            object.guid = new UUID(3, "ns:URL", uri).format();

            if (typeof object.nodes != undefined && object.nodes.length > 0) {
                for (node_key in object.nodes) {
                    object.nodes[node_key] = setConfigurationUUIDs(object.nodes[node_key], macAddress);
                }
            }
            return object;
        };

        var stop = function (device) {
            if (device == "wear") {
                sensorContainer = WearSensorContainer;
            } else if (device == "metaWear") {
                sensorContainer = MetaWearSensorContainer;
            } else {
                console.error("Wrong device name");

            }

            if (statuses[device].started && sensorContainer) {
                var sensorAgent = sensorContainer.getSensorAgent(0);
                if (sensorAgent) {
                    var nodes = sensorAgent.getNodes();
                    for (var i = 0, len = nodes.length; i < len; i++) {
                        var node = nodes[i];
                        if (node.getUri().indexOf('stop') > -1) {
                            var stop = sensorContainer.serialize(node);
                            console.log(device + " Stop wear");
                            if ((device == "metaWear" && $rootScope.metaWearStatus != "DISCONNECTED") || (device == "wear" && $rootScope.wearStatus != "DISCONNECTED")) {
                                commproxy.sendMessage('transmitData', stop, commproxySuccessCallback, failStopCallback);
                            } else {
                                LogSrvs.log("Error sending stop message to Wear. Wear is disconnected. ", "error");
                            }
                            //commproxyPing("stop", sensorContainer);
                        }
                    }
                    statuses[device].started = false;
                    $rootScope.dataTransferring[device] = false;
                }
            }
        }

        var sendMessageToWear = function (payload, callback) {
            console.log("WearSrvs transmitData - payload: " + payload.name + " | " + payload.uri + " | " + (new Date()));
            console.log(payload);
            console.log(callback);
            commproxy.sendMessage('transmitData', payload, function (errorValue) {
                commproxySuccessCallback(errorValue, callback);
            }, function (errorValue, callback) {
                sendMessageToWearFailCallback(errorValue, callback);
            });
        };

        var enableConnectionListener = function () {
            console.log("WearSrvs wear enable connection listener");
            commproxy.sendMessage('setOnConnectionChangeListener', {
                "name": "setOnConnectionChangeListener",
                "description": "set device connection change listener"
            }, commproxySuccessCallback, ConnectionListenerFailCallback);
        };

        var transmitCSV = function () {
            console.log("WearSrvs transmitting csv started");
            commproxy.sendMessage('transmitData', {
                    "name": "getLogs",
                    "description": "SensorAgent getLogs",
                    "guid": "11161ca3-ee12-4e92-8ae6-43baa245e7e3",
                    "uri": "Method://42e63d67ad417fda.LGEplatina.SensorAgent.getLogs",
                    "type": "METHOD",
                    "data": {"timeStamp": 0},
                    "nodes": []
                }
                , commproxySuccessCallbackTransmit, function (errorValue) {
                    LogSrvs.log('Wear transmit CSV failed: ' + errorValue, 'error');
                });
        };

        var shareLogs = function () {
            console.log("WearSrvs transmitting csv started");
            commproxy.sendMessage('transmitData', {
                    "name": "shareFiles",
                    "description": "Reads the directory name from file_path and creates email intent with all files uri "
                }
                , commproxySuccessCallbackTransmit, function (errorValue) {
                    LogSrvs.log('Share failed failed: ' + errorValue, 'error');
                });
        };

        var commproxyPing = function (method, sensorAgent) {
            var frequency = 9000;
            if (method == "start") {


                getSensorAgentNode("ping", sensorAgent).then(function (pingNode) {
                    var pingNodeObj = JSON.parse(pingNode);
                    var device = pingNodeObj.uri.split(".")[1];
                    pingNodeObj.nodes[0].data.values = int64ToBase64(frequency + 1000);
                    sendMessageToWear(pingNodeObj);
                    if (!$rootScope.pingInterval[device]) {
                        $rootScope.pingInterval[device] = $interval(function () {
                            sendMessageToWear(pingNodeObj);
                        }, frequency);
                    }
                }, function (error) {
                    console.error("Cant get ping node");
                });

            } else if (method == "stop") {
                getSensorAgentNode("ping", sensorAgent).then(function (pingNode) {
                    var pingNodeObj = JSON.parse(pingNode);
                    var device = pingNodeObj.uri.split(".")[1];

                    if ($rootScope.pingInterval[device]) {
                        $interval.cancel($rootScope.pingInterval[device]);
                        $rootScope.pingInterval[device] = null;
                    }

                }, function (error) {
                    console.error("Cant get ping node");
                });

            } else if (method == "stopAll") {
                for (device in $rootScope.pingInterval) {
                    if ($rootScope.pingInterval[device]) {
                        $interval.cancel($rootScope.pingInterval[device]);
                        $rootScope.pingInterval[device] = null;
                    }
                }
            }

        };

        var int64ToBase64 = function (frequency) {

            longToByteArray = function (long) {
                // we want to represent the input as a 8-bytes array
                var byteArray = [0, 0, 0, 0, 0, 0, 0, 0];

                for (var index = 0; index < byteArray.length; index++) {
                    var byte = long & 0xff;
                    byteArray [index] = byte;
                    long = (long - byte) / 256;
                }
                return new Uint8Array(byteArray.reverse());
            };

            arrayBufferToBase64 = function (buffer) {
                var binary = '';
                var bytes = new Uint8Array(buffer);
                var len = bytes.byteLength;
                for (var i = 0; i < len; i++) {
                    binary += String.fromCharCode(bytes[i]);
                }
                return window.btoa(binary);
            }
            return arrayBufferToBase64(longToByteArray(frequency))
        };

        var getExternalSensors = function () {
            var externalSensorsConfig = {
                wear: configurations.wear.setConfiguration.nodes[0],
                metaWear: configurations.metaWear.setConfiguration.nodes[0]
            }
            return externalSensorsConfig;
        };

        var getDeviceConfig = function (deviceName) {
            var savedConfig = localStorage.getItem('devicesConfig');
            if (savedConfig && typeof savedConfig != "undefined") {
                savedConfig = JSON.parse(savedConfig);

                if (deviceName) {
                    return savedConfig[deviceName];
                } else {
                    return savedConfig;
                }
            } else {
                return false;
            }
        };
        var saveDeviceConfig = function (deviceName, config) {
            var configObj = getDeviceConfig();
            if (!configObj) {
                configObj = {};
            }
            configObj[deviceName] = config;
            localStorage.setItem('devicesConfig', JSON.stringify(configObj));
        };

        var formatPrefix = function (prefix) {

            if (prefix == "metawear") {
                prefix = "MetaWear";
            } else if (prefix == "android") {
                prefix = "Wear";
            }
            ;

            return prefix;
        };

        return {
            initializeWear: function (callback) {
                initialize(callback);
            },
            isWearEnabled: function () {
                return statuses.wear.wearEnabled;
            },
            isMetaWearEnabled: function () {
                return statuses.metaWear.wearEnabled;
            },
            doesWearExist: function () {
                return statuses.wear.wearExists;
            },
            isWearStopped: function (device) {
                return !statuses[device].started;
            },
            startWear: function (sessionId) {
                start(sessionId);
            },
            startMetaWear: function (sessionId) {
                startMetaWear(sessionId);
            },
            stopWear: function (device_name) {
                stop(device_name);
            },

            getWearSensorPrefix: function () {
                return wearSensorPrefix;
            },
            subscribeForConnectionStatus: function (callback) {
                callback(currentConnectionStatus);
                connectionStatusMessagesSubscriptions.push(callback);
            },
            transmitCSVFile: function () {
                transmitCSV();
            },
            shareLogFiles: function () {
                return shareLogs();
            },
            sendMessageToMetaWear: function (method) {
                if (method == "initializeWear") {
                    initialize();
                } else if (method == "initMetaWear") {
                    initMetaWear();
                } else {
                    sendMessageToMetaWear(method);
                }
            },
            checkMetaWearStates: function () {
                checkMetaWearState();
            },
            setNewConfiguration: function (configuration, deviceName) {

                if (deviceName == "wear") {
                    var sensorContainer = WearSensorContainer;
                } else {
                    var sensorContainer = MetaWearSensorContainer;
                }

                setConfigNode = sensorContainer.getSensorAgentNode("setConfiguration");

                if (setConfigNode) {
                    setCofig = JSON.parse(setConfigNode);
                    setCofig.nodes[0] = configuration;
                    if ((deviceName == "wear" && $rootScope.wearStatus != "DISCONNECTED") || deviceName != "wear") {
                        sendMessageToWear(setCofig);
                    }
                } else {
                    LogSrvs.log("No sensor agent");
                }
            },
            saveConfig: function (deviceName, config) {
                return saveDeviceConfig(deviceName, config);
            },
            getConfig: function (deviceName) {
                return getDeviceConfig(deviceName);
            }
        };
    });