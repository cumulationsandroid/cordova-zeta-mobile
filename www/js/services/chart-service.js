angular.module('services.chart', [])

    .factory('ChartSrvs', function (SensorSrvs, WearSrvs, LogSrvs) {
        var firstColor = {
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)"
        }

        var secondColor = {
            fillColor: "rgba(247,70,74,0.2)",
            strokeColor: "rgba(247,70,74,1)",
            pointColor: "rgba(247,70,74,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(247,70,74,1)"
        }

        var thirdColor = {
            fillColor: "rgba(253,180,92,0.2)",
            strokeColor: "rgba(253,180,92,1)",
            pointColor: "rgba(253,180,92,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(253,180,92,1)"
        }

        var fourthColor = {
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)"
        }

        var reservedMarkerColor = {
            fillColor: "rgba(70,191,189,0)",
            strokeColor: "rgba(70,191,189,0)",
            pointColor: "rgba(70,191,189,1)",
            pointStrokeColor: "rgba(70,191,189,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(70,191,189,1)"
        }

        var avaliableColors = [firstColor, secondColor, thirdColor, fourthColor];

        var chartOptions = {
            animation: false,
            showTooltips: false,
            pointDotRadius: 2,
            pointDotStrokeWidth: 1,
            datasetStrokeWidth: 1,
            responsive: true,
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span class=\"<%=name.toLowerCase()%>-legend-icon\" style=\"background-color:<%=datasets[i].pointColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
        };

        var formatChartTime = function (dateObj) {
            var addLeadingZero = function (number) {
                return number.toString().length == 1 ? '0' + number : number;
            }
            return addLeadingZero(dateObj.getHours()) + ':' + addLeadingZero(dateObj.getMinutes()) + ':' + addLeadingZero(dateObj.getSeconds());
        }

        var visibleAtStart = 3;

        var createChart = function (monitoringValue, labelsObj, seriesObj, dataObj, coloursObj, optionsObj, charts, session_id) {
            LogSrvs.log('New chart created for ' + monitoringValue.sensor + '.', 'info', session_id);

            labelsObj[monitoringValue.sensor] = [];
            seriesObj[monitoringValue.sensor] = [];
            dataObj[monitoringValue.sensor] = [];
            coloursObj[monitoringValue.sensor] = [];
            optionsObj[monitoringValue.sensor] = chartOptions;

            var monitoringDate = new Date(monitoringValue.timestamp);
            labelsObj[monitoringValue.sensor].push(formatChartTime(monitoringDate));

            var propIndex = 0;
            coloursObj[monitoringValue.sensor] = avaliableColors;
            monitoringValue.marker = null;
            for (var key in monitoringValue) {
                if (key !== 'sensor' && key !== 'timestamp') {
                    if (monitoringValue.hasOwnProperty(key)) {
                        //coloursObj[monitoringValue.sensor].push(avaliableColors[propIndex]);
                        seriesObj[monitoringValue.sensor].push(key);
                        dataObj[monitoringValue.sensor][propIndex] = [];
                        dataObj[monitoringValue.sensor][propIndex].push(monitoringValue[key]);
                        if (key == "marker") {
                            markerIndex = seriesObj[monitoringValue.sensor].indexOf("marker");
                            //Disabled
                            //coloursObj[monitoringValue.sensor][markerIndex] = reservedMarkerColor;
                        }
                        propIndex++;
                    }
                }
            }

            charts.push({name: monitoringValue.sensor, visible: charts.length < visibleAtStart});
        };

        var updateChart = function (monitoringValue, labelsObj, seriesObj, dataObj, coloursObj, limit) {
            // console.log('********** UPDATE CHART: ' + monitoringValue.sensor + ' **********');

            for (key in monitoringValue) {
                if (['sensor', 'timestamp'].indexOf(key) < 0) {
                    if (seriesObj[monitoringValue.sensor].indexOf(key) < 0) {
                        seriesObj[monitoringValue.sensor].push(key);
                    }

                }
            }

            //Sort sensors
            var sortChannels = function (object, markerIndex) {
                console.log(typeof object[markerIndex]);
                temp = object[markerIndex];
                object[markerIndex] = object[(object.length - 1)];
                object[(object.length - 1)] = temp;


                return object;
            };

            //If marker isn't last in array
            if (seriesObj[monitoringValue.sensor].indexOf("marker") != seriesObj[monitoringValue.sensor].length - 1) {
                markerIndex = seriesObj[monitoringValue.sensor].indexOf("marker");
                if (markerIndex > -1) {
                    seriesObj[monitoringValue.sensor] = sortChannels(seriesObj[monitoringValue.sensor], markerIndex);
                    dataObj[monitoringValue.sensor] = sortChannels(dataObj[monitoringValue.sensor], markerIndex);
                    coloursObj[monitoringValue.sensor] = sortChannels(coloursObj[monitoringValue.sensor], markerIndex);
                }
                for (index in coloursObj[monitoringValue.sensor]) {
                    if (coloursObj[monitoringValue.sensor][index] == reservedMarkerColor && index != markerIndex) {
                        coloursObj[monitoringValue.sensor][index] = avaliableColors[(index <= 4 ? index : (index % 4))];
                    }
                }
                //coloursObj[monitoringValue.sensor][markerIndex] = reservedMarkerColor;                

            }


            var longer = false; // longer then limit
            var created = formatChartTime(new Date(monitoringValue.timestamp));

            //push/shift label
            if (labelsObj[monitoringValue.sensor].indexOf(created) < 0) {
                labelsObj[monitoringValue.sensor].push(created);
                if (limit && labelsObj[monitoringValue.sensor].length > limit) {
                    labelsObj[monitoringValue.sensor].shift();
                    longer = true;
                }
            }

            var labelIndex = labelsObj[monitoringValue.sensor].indexOf(created);

            var markerIndex = seriesObj[monitoringValue.sensor].indexOf('marker');

            //If monitoring value is not marker
            if (!monitoringValue.marker) {
                for (var key in monitoringValue) {
                    if (key !== 'sensor' && key !== 'timestamp') {
                        var seriesIndex = seriesObj[monitoringValue.sensor].indexOf(key);


                        //If has more data then limit

                        if (longer) {
                            if (monitoringValue.sensor.indexOf('Wear - ') < 0) {
                                dataObj[monitoringValue.sensor][seriesIndex].shift();
                            } else {
                                for (serieKey in dataObj[monitoringValue.sensor]) {
                                    dataObj[monitoringValue.sensor][serieKey].shift();
                                }
                            }
                        }

                        //Create array for series if not exists
                        if (typeof dataObj[monitoringValue.sensor][seriesIndex] == "undefined") {
                            dataObj[monitoringValue.sensor][seriesIndex] = [];
                        }

                        //dataObj[monitoringValue.sensor][seriesIndex].push(monitoringValue[key]);
                        dataObj[monitoringValue.sensor][seriesIndex][labelIndex] = monitoringValue[key];


                        // Add null for series that doesn't have value
                        for (i = 0; i <= limit; i++) {
                            if (typeof dataObj[monitoringValue.sensor][seriesIndex][i] == "undefined") {
                                dataObj[monitoringValue.sensor][seriesIndex][i] = null;
                            }
                        }

                    }
                }

                //Add marker array to dataObj if doesn't exist
                if (markerIndex > -1) {
                    if (typeof dataObj[monitoringValue.sensor][markerIndex] == "undefined") {
                        dataObj[monitoringValue.sensor][markerIndex] = [];
                    }
                    dataObj[monitoringValue.sensor][markerIndex].push(null);
                    if (longer) {
                        dataObj[monitoringValue.sensor][markerIndex].shift();
                    }
                }
            }
            //If monitoring value is marker
            else {
                //Add marker series and marker array to dataObj if doesn't exist
                if (markerIndex == -1) {
                    seriesObj[monitoringValue.sensor].push('marker');
                    markerIndex = seriesObj[monitoringValue.sensor].length - 1;
                    //coloursObj[monitoringValue.sensor].push(reservedMarkerColor);
                    dataObj[monitoringValue.sensor][markerIndex] = [];
                    for (var i = 0, len = dataObj[monitoringValue.sensor][markerIndex - 1].length; i < len; i++) {
                        dataObj[monitoringValue.sensor][markerIndex].push(null);
                    }
                }

                // Find max value from exisitng ones to put a marker point above them
                var arrayOfMax = [];
                for (var i = 0, len = dataObj[monitoringValue.sensor].length; i < len; i++) {
                    if (i != markerIndex) {
                        dataObj[monitoringValue.sensor][i].push(null);
                        if (longer) {
                            dataObj[monitoringValue.sensor][i].shift();
                        }
                        arrayOfMax.push(Math.max.apply(null, dataObj[monitoringValue.sensor][i]));
                    }
                }
                var maxFromArrayOfMax = Math.floor(Math.max.apply(null, arrayOfMax));
                maxFromArrayOfMax = Math.pow(10, maxFromArrayOfMax.toString().length);

                if (typeof dataObj[monitoringValue.sensor][markerIndex] == "undefined") {
                    dataObj[monitoringValue.sensor][markerIndex] = [];
                }
                dataObj[monitoringValue.sensor][markerIndex].push(maxFromArrayOfMax);
                if (longer) {
                    dataObj[monitoringValue.sensor][markerIndex].shift();
                }
            }
        }

        var removeChart = function (chartForRemoval, labelsObj, seriesObj, dataObj, coloursObj, optionsObj, chartsArr, session_id) {
            LogSrvs.log('Removing chart for ' + chartForRemoval + '.', 'info', session_id);
            for (var i = 0, len = chartsArr.length; i < len; i++) {
                if (chartsArr[i].name === chartForRemoval) {
                    chartsArr.name.splice(i, 1);
                    break;
                }
            }
            delete labelsObj[chartForRemoval];
            delete seriesObj[chartForRemoval];
            delete dataObj[chartForRemoval];
            delete coloursObj[chartForRemoval];
            delete optionsObj[chartForRemoval];
        }

        var createOrUpdateCharts = function (sensorValue, labelsObj, seriesObj, dataObj, coloursObj, optionsObj, chartsArr, limit, createAllChartsFlag, session_id) {
            if (sensorValue.sensor !== 'Marker') {

                if ($.grep(chartsArr, function (e) {
                        return e.name == sensorValue.sensor;
                    }).length == 0) {
                    createChart(sensorValue, labelsObj, seriesObj, dataObj, coloursObj, optionsObj, chartsArr, session_id);
                } else {
                    updateChart(sensorValue, labelsObj, seriesObj, dataObj, coloursObj, limit);
                }

                if (!createAllChartsFlag) {
                    // Remove charts for sensors that are not selected anymore
                    SensorSrvs.getAllSelectedSensors(function (allSensors) {
                        if (chartsArr.length > allSensors.length) {
                            for (var i = 0, len = chartsArr.length; i < len; i++) {
                                var duplicate = false;
                                for (var j = 0, leng = allSensors.length; j < leng; j++) {
                                    if (chartsArr[i].name === allSensors[j].sensorName) {
                                        duplicate = true;
                                        break;
                                    }
                                }

                                if (!duplicate && WearSrvs.isWearEnabled()) {
                                    // ******* Temp until refactoring the whole app to comply with the new data model
                                    if (!chartsArr[i].name.startsWith("Wear") && !chartsArr[i].name.startsWith("MetaWear")) {
                                        removeChart(chartsArr[i], labelsObj, seriesObj, dataObj, coloursObj, optionsObj, chartsArr, session_id);
                                    }
                                }

                            }
                        }
                    });
                }
            } else {
                var obj = {
                    timestamp: sensorValue.timestamp,
                    marker: sensorValue.text
                }
                for (var i = 0, len = chartsArr.length; i < len; i++) {
                    obj.sensor = chartsArr[i].name;
                    updateChart(obj, labelsObj, seriesObj, dataObj, coloursObj, limit);
                }
            }
        };

        var removeMiliseconds = function (timestamp) {
            return Math.floor(timestamp / 1000) * 1000;
        };

        var addChartData = function (chartDataArray, markers, readValue, timestamps) {

            for (var key in readValue) {

                var timestamp = removeMiliseconds(readValue.timestamp);
                if (timestamps.indexOf(timestamp) == -1) {
                    timestamps.push(timestamp);
                }

                if (key !== 'sensor' && key !== 'timestamp') {
                    if (typeof chartDataArray[readValue.sensor] == "undefined") {
                        chartDataArray[readValue.sensor] = {values: {}, series: []}
                    }
                    if (chartDataArray[readValue.sensor].series.indexOf(key) == -1) {
                        chartDataArray[readValue.sensor].series.push(key);
                    }

                    if (typeof chartDataArray[readValue.sensor].values[timestamp] == "undefined") {
                        chartDataArray[readValue.sensor].values[timestamp] = {};
                    }
                    chartDataArray[readValue.sensor].values[timestamp][key] = readValue[key];
                }
            }
        };

        var prepareChartDate = function (dataArray, labelsObj, seriesObj, dataObj, coloursObj, optionsObj, charts, timestamps, limit) {

            timestamps.sort();
            for (var sensor_name in dataArray) {

                sensor_data = dataArray[sensor_name];

                if (typeof labelsObj[sensor_name] == "undefined") {
                    labelsObj[sensor_name] = [];
                    seriesObj[sensor_name] = sensor_data.series;
                    dataObj[sensor_name] = [];
                    coloursObj[sensor_name] = [];
                    optionsObj[sensor_name] = chartOptions;

                    if (seriesObj[sensor_name].indexOf("marker") == -1) {
                        seriesObj[sensor_name].push("marker");
                    }
                    for (var i = 0; i < sensor_data.series.length - 1; i++) {
                        coloursObj[sensor_name].push(avaliableColors[i]);
                    }
                    coloursObj[sensor_name][sensor_data.series.length - 1] = reservedMarkerColor;
                    //coloursObj[sensor_name].push(reservedMarkerColor);

                }

                //ok
                for (var timestamp_key in timestamps) {
                    timestamp = timestamps[timestamp_key];
                    var label = formatChartTime(new Date(parseInt(timestamp)));

                    if (labelsObj[sensor_name].indexOf(label) == -1) {
                        labelsObj[sensor_name].push(label);


                        for (seriesKey in seriesObj[sensor_name]) {
                            seriesName = seriesObj[sensor_name][seriesKey];

                            if (typeof dataObj[sensor_name][seriesKey] == "undefined") {
                                dataObj[sensor_name][seriesKey] = [];
                            }
                            if (typeof sensor_data.values[timestamp] != "undefined") {
                                if (typeof sensor_data.values[timestamp][seriesName] != "undefined") {

                                    dataObj[sensor_name][seriesKey].push(sensor_data.values[timestamp][seriesName]);
                                } else {
                                    dataObj[sensor_name][seriesKey].push(null);
                                }
                            } else {
                                dataObj[sensor_name][seriesKey].push(null);
                            }
                        }
                    }

                    if (limit && labelsObj[sensor_name].length > limit) {
                        labelsObj[sensor_name].shift();

                        for (sensor_serie in dataObj[sensor_name]) {
                            dataObj[sensor_name][sensor_serie].shift();
                        }
                    }
                }
                if ($.grep(charts, function (e) {
                        return e.name == sensor_name;
                    }).length == 0) {
                    charts.push({
                        name: sensor_name,
                        visible: charts.length < visibleAtStart,
                        opened: charts.length < visibleAtStart ? new Date().getTime() : null
                    });
                }
                //if (charts.name.indexOf(sensor_name) == -1) {
                //   charts.push({name: sensor_name, visible: charts.length < visibleAtStart + 1});
                //}
            }
            ;
        }

        var clearCharts = function (labelsObj, seriesObj, dataObj, charts) {
            for (var i = 0, len = charts.length; i < len; i++) {
                for (var key in seriesObj[charts[i]]) {
                    dataObj[charts[i]][key].length = 0;
                    if (key === 'marker') {
                        dataObj[charts[i]][key].push(null);
                    } else {
                        dataObj[charts[i]][key].push(0);
                    }
                }
                labelsObj[charts[i]].length = 0;
                labelsObj[charts[i]].push(0);
            }
        }

        return {
            createOrUpdateChartsByScope: function (sensorValue, labelsObj, seriesObj, dataObj, coloursObj, optionsObj, chartsArr, limit, session_id) {
                createOrUpdateCharts(sensorValue, labelsObj, seriesObj, dataObj, coloursObj, optionsObj, chartsArr, limit, false, session_id);
            },
            clearChartsByScope: function (labelsObj, seriesObj, dataObj, charts) {
                clearCharts(labelsObj, seriesObj, dataObj, charts);
            },
            createOrUpdateAllChartsByScope: function (sensorValue, labelsObj, seriesObj, dataObj, coloursObj, optionsObj, chartsArr, limit, session_id) {
                createOrUpdateCharts(sensorValue, labelsObj, seriesObj, dataObj, coloursObj, optionsObj, chartsArr, limit, true, session_id);
            },
            addChartDataByScope: function (chartDataArray, markers, sensorValue, timestamps) {
                addChartData(chartDataArray, markers, sensorValue, timestamps);
            },
            prepareChartByScope: function (dataArray, labelsObj, seriesObj, dataObj, coloursObj, optionsObj, charts, timestamps, limit) {
                session_id = "";
                prepareChartDate(dataArray, labelsObj, seriesObj, dataObj, coloursObj, optionsObj, charts, timestamps, limit);
            }
        };
    });