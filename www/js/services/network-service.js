angular.module('services.network', [])

    .factory('NetworkSrvs', function ($rootScope, $cordovaNetwork, $ionicPopup, LogSrvs) {
        var isDeviceOnline = null;
        var funcsForExecWhenDeviceGoesOnline = [];
        var BLenabled = null;

        var initialize = function (callback) {


            if (isDeviceOnline == null) {
                isDeviceOnline = $cordovaNetwork.isOnline();

                $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
                    if (!isDeviceOnline) {
                        isDeviceOnline = true;
                        LogSrvs.log('Device became online.', 'info');

                        for (var i = 0, len = funcsForExecWhenDeviceGoesOnline.length; i < len; i++) {
                            funcsForExecWhenDeviceGoesOnline[i]();
                        }
                    }
                });

                $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
                    if (isDeviceOnline) {
                        isDeviceOnline = false;
                        LogSrvs.log('Device became offline.', 'warning');
                    }
                });

                LogSrvs.log('Device is ' + (isDeviceOnline ? 'online on ' + $cordovaNetwork.getNetwork() + ' network.' : 'offline.'), isDeviceOnline ? 'info' : 'warning');
                callback();
            }


            networking.bluetooth.getAdapterState(function (adapterInfo) {
                BLenabled = adapterInfo.enabled;
                if (BLenabled) {
                    LogSrvs.log('Adapter is enabled', 'info');
                } else {
                    LogSrvs.log('Adapter is disabled', 'info');
                    showBTDialog();
                }
            });

            networking.bluetooth.onAdapterStateChanged.addListener(function (adapterInfo) {
                // The adapterInfo object has the same properties as getAdapterState
                if (adapterInfo.enabled !== BLenabled) {
                    BLenabled = adapterInfo.enabled;
                    if (BLenabled) {
                        LogSrvs.log('Adapter is enabled', 'info');
                    } else {
                        LogSrvs.log('Adapter is disabled', 'info');
                        showBTDialog();
                    }
                }
            })
        };

        var blueToothEnabled = function () {
            return BLenabled;
        };

        var enableBlueTooth = function () {
            networking.bluetooth.enable(function () {
                // The adapter is now enabled
            }, function () {
                // The user has cancelled the operation
            });
        };

        var showBTDialog = function () {
            $rootScope.closeAllPopups();
            $ionicPopup.show({
                title: 'Bluetooth is disabled',
                subTitle: 'In order to connect and operate to external devices you need to enable bluetooth adapter. Do you want to enable it?',
                //scope: $scope,
                buttons: [
                    {
                        text: 'Cancel',
                        onTap: function (e) {
                        }
                    },
                    {
                        text: '<b>Enable Bluetooth</b>',
                        type: 'button-positive',
                        onTap: function (e) {
                            enableBlueTooth();

                        }
                    }
                ]
            })
        };
        return {
            initializeNetwork: function (callback) {
                initialize(callback);
            },
            isDeviceOnline: function () {
                return isDeviceOnline;
            },
            insertFunctionForOnlineExecution: function (callback) {
                funcsForExecWhenDeviceGoesOnline.push(callback);
            },
            isBluetoothEnabled: function () {
                return BLenabled;
            },
            showBlueToothDialog: function () {
                showBTDialog();
            }
        }
    });