angular.module('services.db', [])

    .factory('DbSrvs', function ($couchbase, HttpSrvs, LogSrvs, $cordovaDevice, uuid, $q) {
        var zetamobileDb = null;

        var zetamobileDbName = 'zetadb';
        var zetamobileDesignName = '_design/zetadesign';

        var zetamobileDbViews = {};

        var sensorvaluesViewName = 'sensorvalues';
        var sessionViewName = 'session';
        var undefinedLogsViewName = 'undefinedLogs';
        var sensorvaluesDocType = 'data';

        zetamobileDbViews[sensorvaluesViewName] = {
            map: function (doc) {
                if (doc.type == 'marker') {
                    emit(doc.sessionId,
                        {
                            markerValue: doc.markerValue,
                            sessionId: doc.sessionId,
                            created: doc.created,
                            type: doc.type,
                            completeDocument: doc
                        });
                } else if (doc.type == 'data') {
                    emit(doc.sessionId,
                        {
                            sensorName: doc.sensorName,
                            channelName: doc.channelName,
                            channelDataValue: doc.channelDataValue,
                            created: doc.created,
                            sessionId: doc.sessionId,
                            completeDocument: doc,
                            type: doc.type,
                            markerValue: doc.markerValue
                        });
                }
            }.toString()
        };

        zetamobileDbViews[sessionViewName] = {
            map: function (doc) {
                if (doc.type == 'session') {
                    emit(doc.sessionStart,
                        {
                            sessionStart: doc.sessionStart,
                            sessionEnd: doc.sessionEnd,
                            owner: doc.owner
                        });
                }
            }.toString()
        };

        zetamobileDbViews[undefinedLogsViewName] = {
            map: function (doc) {
                if (doc.type == 'log' && doc.owner == 'undefined') {
                    emit(doc._id, {
                        message: doc.message,
                        category: doc.category,
                        owner: doc.owner,
                        created: doc.created,
                        rev: doc._rev
                    });
                }
            }.toString()
        };


        var profileDocName = 'zetaaccount';

        var sensorsDocName = 'sensors';

        var initialize = function (callback) {

            if (!zetamobileDb) {
                if (!window.cblite) {
                    console.log('Error Couchbase lite plugin is missing.');
                } else {
                    console.log('Couchbase lite plugin is installed.');
                    cblite.getURL(function (err, url) {
                        if (err) {
                            LogSrvs.log('Error getting couchbase url.', 'error');
                            console.log(err);
                        } else {
                            zetamobileDb = new $couchbase(url, zetamobileDbName);
                            zetamobileDb.createDatabase().then(function (result) {
                                zetamobileDb.createDesignDocument(zetamobileDesignName, zetamobileDbViews);
                                LogSrvs.log('Success creating local database.', 'info');
                                callback();
                            }, function (error) {
                                if (error.status == 412) {
                                    LogSrvs.log('Local database already exists.', 'warning');
                                    callback();
                                } else {
                                    LogSrvs.log('Error creating database.', 'error');
                                    console.log(error);
                                }
                            });
                        }
                    });
                }
            }
        };

        var enableReplication = function (profileObj) {
            console.log("enableReplication");
            if (zetamobileDb) {
                // TODO should be authorized on sync-gateway from our rest api after introducing server side security
                //      and just do the start bi-directional replication with http basic auth
                HttpSrvs.getRequest(config.urls.syncGatewayAdminUrl + '/_user/' + profileObj.id, function (response) {
                    if (!response) {
                        console.log('User does not exist on sync-gateway');
                        var sgUserData = {
                            name: profileObj.id,
                            password: profileObj.password,
                            admin_channels: [profileObj.id]
                        };
                        HttpSrvs.putRequest(config.urls.syncGatewayAdminUrl + '/_user/' + profileObj.id, sgUserData, function () {
                            console.log('User created and authorized on sync-gateway');
                            startReplication();
                        });
                    } else {
                        console.log('User exists on sync-gateway');
                        startReplication();
                    }
                }, function (error) {
                    console.log("sync-gateway - server error");
                    LogSrvs.log("sync-gateway - server error", "error");
                    console.log(error);
                });

                var startReplication = function () {
                    var syncGwReplObj = {
                        url: config.urls.syncGatewayUrl,
                        headers: {Authorization: 'Basic ' + btoa(profileObj.id + ':' + profileObj.password)}
                    };
                    zetamobileDb.replicate(zetamobileDbName, syncGwReplObj, true).then(function () {
                        console.log('Started replicating to the Sync Gateway (PUSH).');
                        zetamobileDb.replicate(syncGwReplObj, zetamobileDbName, true).then(function () {
                            console.log('Started replicating to the local db (PULL).');
                            LogSrvs.log('Sync-gateway enabled.', 'info');
                        }, function (err) {
                            console.log('There was an error replicating to the local db (PULL)');
                            console.log(err);
                        });
                    }, function (error) {
                        console.log('There was an error replicating to the Sync Gateway (PUSH)');
                        console.log(error);
                    });
                };
            }
        }

        var createLogDoc = function (log_obj) {
            if (zetamobileDb) {
                createDoc(log_obj);
            }
        }

        var createSessionDoc = function createSession(session) {
            var deferred = $q.defer();
            if (zetamobileDb) {
                var doc = {
                    'type': 'session',
                    //'id': uuid.v4(),
                    'sessionEnd': 0,
                    'sessionStart': session.getStart(),
                    'owner': session.getUser()
                };

                createDocRe(doc).then(function (result) {
                        console.log('Success creating session document.');
                        deferred.resolve(result);
                    },
                    function (result) {
                        deferred.reject("Error creating document");
                    });

            } else {
                deferred.reject("DB Error");
            }
            return deferred.promise;

        }

        var createSensorValueDoc = function (session, data) {
            if (zetamobileDb) {
                var doc = {
                    type: 'data',
                    id: uuid.v4(),
                    created: data.timestamp,
                    owner: session.owner,
                    sessionId: session.guid,
                    sensorName: data.sensor,
                    channelName: data.channel,
                    channelDataValue: data.value
                };
                createDoc(doc);
            }
        }

        var getSensorValueDocs = function (selectedSession, markers, group_sensor_channels, callback) {

            var params = {descending: true, limit: 5000};
            if (typeof selectedSession != "undefined" && selectedSession != "all") {
                params.key = "\"" + selectedSession + "\"";
            }
            if (zetamobileDb) {

                getDocs(sensorvaluesViewName, params, function (data) {
                    if (!data || data.length == 0) {
                        console.log("No data");
                        callback([]);
                        return;
                    }

                    var modelData = [];
                    var sensors = [];

                    if (group_sensor_channels) {
                        var temp_data = {};

                        for (var i = 0; i < data.length; i++) {
                            var data_value = data[i].value;
                            if (typeof data_value.sensorName != "undefined" && typeof temp_data[data_value.sensorName] == "undefined") {
                                temp_data[data_value.sensorName] = {};
                            }
                            if (typeof data_value.sensorName != "undefined" && sensors.indexOf(data_value.sensorName) == -1) {
                                sensors.push(data_value.sensorName);
                            }
                            if (data_value.type == "data") {
                                if (typeof temp_data[data_value.sensorName][data_value.created] == "undefined") {
                                    temp_data[data_value.sensorName][data_value.created] = {};
                                    temp_data[data_value.sensorName][data_value.created].value = {};
                                }

                                temp_data[data_value.sensorName][data_value.created].value[data_value.channelName] = data_value.channelDataValue;
                                temp_data[data_value.sensorName][data_value.created].value.sensor = data_value.sensorName;
                                temp_data[data_value.sensorName][data_value.created].value.timestamp = data_value.created;

                            } else if (data_value.type == "marker") {

                                for (sensor_key in sensors) {
                                    sensor = sensors[sensor_key];
                                    if (typeof temp_data[sensor][data_value.created] == "undefined") {
                                        temp_data[sensor][data_value.created] = {
                                            value: {}
                                        };
                                    }
                                    //temp_data[sensor][data_value.created].value.text = data_value.markerValue;
                                    temp_data[sensor][data_value.created].value.marker = 0;
                                    temp_data[sensor][data_value.created].value.sensor = sensor;
                                    temp_data[sensor][data_value.created].value.timestamp = data_value.created;
                                    markers[data_value.created] = data_value.markerValue;
                                }
                            }

                        }
                        for (sensor in temp_data) {
                            for (timestamp in temp_data[sensor]) {
                                modelData.push(temp_data[sensor][timestamp]);
                            }
                        }
                    } else {
                        for (var i = 0; i < data.length; i++) {
                            modelData.push(data[i].value);
                        }
                    }
                    callback(modelData);
                }, function () {
                    console.log("Failed to get documents");
                });
            }
        };

        var getSessionDocs = function (todayResults) {
            var params = {};

            var midnight = new Date();
            midnight.setHours(0, 0, 0, 0);

            if (todayResults) {
                params = {
                    endkey: midnight.getTime(),
                   // endkey: midnight.getTime() + 24 * 60 * 60 * 1000
                };
            }
            params.descending = true;

            if (zetamobileDb) {
                return zetamobileDb.queryView(zetamobileDesignName, sessionViewName, params);
            } else {
                //handle no zetamobileDb
                console.log("getSessions error: no zetamobileDb");
            }
        };

        var getLogsUndefinedOwner = function () {
            if (zetamobileDb) {
                return zetamobileDb.queryView(zetamobileDesignName, undefinedLogsViewName);
            } else {
                //handle no zetamobileDb
                console.log("getUpdateLogsOwner error: no zetamobileDb");
            }
        };

        var UpdateLogsOwner = function (id, rev, document) {
            zetamobileDb.deleteDocument(id, rev).then(function (data) {
                zetamobileDb.createDocument(document).then(function () {
                    console.log('Unknown log updated');
                }, function (error) {
                    console.log('Error: update unknown log');
                })
            }, function (error) {
                console.log('Error: delete unknown log');
            });
            //updateLocalDoc = function (docName, rev, doc)
        };

        var getLastSessionData = function () {
            getSessionDocs().then(function (sessions) {
                sessions.rows.sort(function (a, b) {
                        return b.value.sessionStart - a.value.sessionStart
                    }
                );
                lastSession = sessions.rows[0];
                new Date(lastSession.value.sessionStart);
                return lastSession;
            }, function () {
                return null;
            });

        };

        var updateSessionEndTime = function (id) {
            zetamobileDb.getDocument(id).then(function (document) {
                var rev = document._rev;
                document.sessionEnd = new Date().getTime();


                //delete document._id;
                delete document._rev;
                // zetamobileDb.updateDocument(id, rev, document);

                zetamobileDb.deleteDocument(id, rev).then(function (data) {
                    zetamobileDb.createDocument(document).then(function () {
                        console.log('Session end time updated');
                    }, function (error) {
                        console.error('Error: update session end time');
                    })
                }, function (error) {
                    console.error('Error: delete updated session');
                });

            });


        };

        var getSessionDoc = function (sessionID) {
            return zetamobileDb.getDocument(sessionID);
        }

        var createProfileDoc = function (profileObj, callback) {
            if (zetamobileDb) {
                var wrapper = {};
                wrapper[profileDocName] = profileObj;
                createLocalDoc(profileDocName, wrapper, callback);
            }
        }

        var getProfileDoc = function (callback) {
            if (zetamobileDb) {
                getLocalDoc(profileDocName, function (result) {
                    if (!result) {
                        callback(result);
                    } else {
                        callback(result.data[profileDocName]);
                    }
                });
            }
        }

        var createSensorsDoc = function (sensorsDoc, callback) {
            if (zetamobileDb) {
                var wrapper = {};
                wrapper[sensorsDocName] = sensorsDoc;
                createLocalDoc(sensorsDocName, wrapper, callback);
            }
        }

        var getSensorsDoc = function (callback) {
            if (zetamobileDb) {
                getLocalDoc(sensorsDocName, function (result) {
                    if (!result) {
                        callback(result);
                    } else {
                        callback(result.data[sensorsDocName]);
                    }
                });
            }
        }

        var updateSensorsDoc = function (sensorsDoc) {
            if (zetamobileDb) {
                getLocalDoc(sensorsDocName, function (result) {
                    var wrapper = {};
                    wrapper[sensorsDocName] = sensorsDoc;
                    updateLocalDoc(sensorsDocName, result.data._rev, wrapper);
                });
            }
        };

        var updateProfileDebug = function (name, debug) {
            if (zetamobileDb) {
                getLocalDoc(profileDocName, function (result) {
                    if (typeof result.data.zetaaccount.debug == "undefined") {
                        result.data.zetaaccount.debug = {};
                    }
                    result.data.zetaaccount.debug[name] = debug;
                    updateLocalDoc(profileDocName, result.data._rev, result.data);

                });
            }
        };

        var updateProfileConfig = function (device, config) {
            if (zetamobileDb) {
                getLocalDoc(profileDocName, function (result) {

                    if (typeof result.data.zetaaccount.config == "undefined") {
                        result.data.zetaaccount.config = {};
                    }
                    result.data.zetaaccount.config[device] = config;
                    updateLocalDoc(profileDocName, result.data._rev, result.data);

                });
            }
        }

        var createDoc = function (doc) {
            zetamobileDb.createDocument(doc).then(function (result) {
                console.log('Success creating document.');
            }, function (error) {
                console.log('Error creating document.');
                console.log(error);
            });
        }

        ///createDoc refactoring
        var createDocRe = function (doc) {
            return zetamobileDb.createDocument(doc);
        }

        var getDocs = function (viewName, params, callback) {
            zetamobileDb.queryView(zetamobileDesignName, viewName, params).then(function (result) {
                console.log('Success retrieving documents.');
                callback(result.rows);
            }, function (error) {
                console.log('Error retrieving documents.');
                console.log(error);
                callback([]);
            });
        }

        var createLocalDoc = function (docName, doc, callback) {
            console.log('Creating local document ' + docName);
            var docUrl = zetamobileDb.databaseUrl + zetamobileDb.databaseName + '/_local/' + docName;
            HttpSrvs.putRequest(docUrl, doc, callback);
        }

        var updateLocalDoc = function (docName, rev, doc) {
            console.log('Updating local document ' + docName);
            var docUrl = zetamobileDb.databaseUrl + zetamobileDb.databaseName + '/_local/' + docName + '?rev=' + rev;
            HttpSrvs.putRequest(docUrl, doc);
        }

        var getLocalDoc = function (docName, callback) {
            console.log('Retrieving local document ' + docName);
            var docUrl = zetamobileDb.databaseUrl + zetamobileDb.databaseName + '/_local/' + docName;
            HttpSrvs.getRequest(docUrl, callback);
        };
        var getLocalDocument = function (docName) {
            var docUrl = zetamobileDb.databaseUrl + zetamobileDb.databaseName + '/_local/' + docName;
            return HttpSrvs.getRequest(docUrl);
        };

        var createMarkerDoc = function (session, data) {
            var deferred = $q.defer();
            if (zetamobileDb) {
                var doc = {
                    id: uuid.v4(),
                    type: 'marker',
                    created: data.timestamp,
                    owner: session.getUser(),
                    sessionId: session.getGuid(),
                    markerValue: data.text,
                };
                createDocRe(doc).then(function (res) {
                    deferred.resolve(res);
                }, function (error) {
                    deferred.reject(error);
                });
            } else {
                deferred.reject("DB error");
            }
            return deferred.promise;
        }

        return {
            initializeDb: function (callback) {
                initialize(callback);
            },
            enableSyncGatewayReplicationForUser: function (profileObj) {
                enableReplication(profileObj);
            },
            createSessionDocument: function (session) {
                return createSessionDoc(session);
            },
            createSensorValueDocument: function (session, sensorValue) {
                createSensorValueDoc(session, sensorValue)
            },
            createProfileDocument: function (profileObj, callback) {
                createProfileDoc(profileObj, callback);
            },
            createSensorsDocument: function (sensorsDoc, callback) {
                createSensorsDoc(sensorsDoc, callback);
            },
            createLogDocument: function (log) {
                createLogDoc(log);
            },
            getGroupedSensorValueDocuments: function (selectedSession, markers, callback) {
                getSensorValueDocs(selectedSession, markers, true, callback);
            },
            getAllSensorValueDocuments: function (selectedSession, callback) {
                getSensorValueDocs(selectedSession, null, false, callback);
            },
            getProfileDocument: function (callback) {
                getProfileDoc(callback);
            },
            getSensorsDocument: function (callback) {
                getSensorsDoc(callback);
            },
            updateSensorsDocument: function (sensorsDoc) {
                updateSensorsDoc(sensorsDoc);
            },
            updateProfileDocument: function (name, debug) {
                return updateProfileDebug(name, debug);
            },
            updateProfileConfig: function (device, config) {
                return updateProfileConfig(device, config);
            },
            createMarkerDocument: function (session, marker_obj) {
                return createMarkerDoc(session, marker_obj)
            },
            getSessions: function (todayResults) {
                return getSessionDocs(todayResults);
            },
            getLogsUndefinedOwner: function () {
                return getLogsUndefinedOwner();
            },
            updateLogsOwner: function (id, rev, document) {
                return UpdateLogsOwner(id, rev, document);
            },
            updateSessionEnd: function (id) {
                return updateSessionEndTime(id);
            },
            getLastSession: function () {
                return getLastSessionData();
            },
            getSessionById: function (sessionID) {
                return getSessionDoc(sessionID);
            }
        }
    });