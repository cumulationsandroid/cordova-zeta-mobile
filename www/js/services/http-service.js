angular.module('services.http', [])

    .factory('HttpSrvs', function ($http) {
        var httpBasicAuthObj = {};

        var httpRequest = function (reqMethod, reqUrl, reqData, reqCallback) {
            var settings = {
                method: reqMethod,
                url: reqUrl
            }
            if (reqData) {
                settings.data = reqData;
            }
            if (checkIfRestAPIUrl(reqUrl)) {
                settings.headers = httpBasicAuthObj;
            }

            if (typeof reqCallback != "undefined") {
                $http(settings).then(function successCallback(response) {
                    console.log('Http ' + reqMethod + ' success.');
                    if (reqCallback) {
                        reqCallback(response);
                    }
                }, function errorCallback(error) {
                    console.log('Http ' + reqMethod + ' failed.');
                    console.log(error);
                    if (reqCallback && error.status == 404) {
                        reqCallback(null);
                    }
                });
            } else {
                return $http(settings);
            }

        };

        var setHttpBasicAuthObj = function (user, pass) {
            if (Object.keys(httpBasicAuthObj).length == 0) {
                httpBasicAuthObj.Authorization = 'Basic ' + btoa(user + ':' + pass);
            }
        }

        var checkIfRestAPIUrl = function (completeUrl) {
            return completeUrl.slice(0, config.urls.restApi.length) == config.urls.restApi;
        }

        return {
            getRequest: function (url, callback) {
                return httpRequest('GET', url, null, callback);
            },
            postRequest: function (url, data, callback) {
                return httpRequest('POST', url, data, callback);
            },
            putRequest: function (url, data, callback) {
                return httpRequest('PUT', url, data, callback);
            },
            deleteRequest: function (url) {
                return httpRequest('DELETE', url, null, null);
            },
            setAuthHeader: function (user, pass) {
                return setHttpBasicAuthObj(user, pass);
            }
        }
    });