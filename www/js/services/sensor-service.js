angular.module('services.sensor', [])

    .factory('SensorSrvs', function ($rootScope, $cordovaDeviceMotion, $cordovaDeviceOrientation, $cordovaGeolocation, $timeout, DbSrvs, LogSrvs, WearSrvs, TimeSrvs, $q) {
        var bleOp = null;
        var sensors = null;

        var internalSensorsConfig = localStorage.getItem('internalSensors');
        if (internalSensorsConfig && JSON.parse(internalSensorsConfig)) {
            config.monitoringOptions.sensors = JSON.parse(internalSensorsConfig);
        }

        var createInternalSensor = function (sensorNameArg, sensorFunctions) {
            console.log('Initializing internal sensor: ' + sensorNameArg);
            return {
                initialize: sensorFunctions().initialize,
                getCurrentData: sensorFunctions().getCurrentData,
                watchVar: null,
                sensorName: sensorNameArg,
                startWatch: function (sensorNameIn, callback) {
                    if (!this.watchVar) {
                        this.watchVar = this.initialize();
                        this.watchVar.then(null, function (error) {
                            console.log('Internal sensor ' + sensorNameIn + ' error');
                            console.log(error);
                        }, function (resultIn) {
                            console.log('Internal sensor read success: ' + sensorNameIn);

                            // Fix to unwrap and flatten object values (example Geolacation coords)
                            var transformResult = function (result) {
                                var getNumbersAndFlatten = function (object, finalObject) {
                                    for (var key in object) {
                                        if (!object[key]) {
                                            finalObject[key] = 0;
                                        } else if (typeof object[key] === 'number' && !isNaN(object[key])) {
                                            finalObject[key] = object[key];
                                        } else if (object[key] != null && typeof object[key] === 'object') {
                                            getNumbersAndFlatten(object[key], finalObject);
                                        }
                                    }
                                    return finalObject;
                                }
                                return getNumbersAndFlatten(result, {});
                            }

                            var newResult = transformResult(resultIn);
                            if (!newResult.timestamp) {
                                newResult.timestamp = Math.floor(TimeSrvs.getCurrentTime());
                            } else {
                                newResult.timestamp = Math.floor(TimeSrvs.getCorrectTime(newResult.timestamp));
                            }
                            newResult.sensor = sensorNameIn;
                            callback(newResult);
                        });
                    }
                },
                stopWatch: function () {
                    if (this.watchVar) {
                        this.watchVar.clearWatch();
                        this.watchVar = null;
                    }
                }
            }
        }

        var scanForInternalSensors = function (callback) {
            // Check avaliable internal sensors
            // TODO Test if accelerometer, compass, geolocation etc give responses on single getValue call
            // For now just return accelerometer, compass and geolocation since
            // every mobile device today should be equipped with those sensors
            var result = [];
            result.push(createInternalSensor('Accelerometer', function () {
                return {
                    initialize: function () {
                        return $cordovaDeviceMotion.watchAcceleration({frequency: config.monitoringOptions.sensors.Accelerometer.frequency});
                    },
                    getCurrentData: function () {
                        return $cordovaDeviceMotion.getCurrentAcceleration()
                    }
                };
            }));
            result.push(createInternalSensor('Compass', function () {
                return {
                    initialize: function () {
                        return $cordovaDeviceOrientation.watchHeading({frequency: config.monitoringOptions.sensors.Compass.frequency});
                    },
                    getCurrentData: function () {
                        return $cordovaDeviceOrientation.getCurrentHeading();
                    }
                };
            }));
            result.push(createInternalSensor('Geolocation', function () {
                return {
                    initialize: function () {
                        return $cordovaGeolocation.watchPosition(config.monitoringOptions.sensors.Geolocation);
                    },
                    getCurrentData: function () {
                        return $cordovaGeolocation.getCurrentPosition()
                    }
                };

            }));
            callback(result);
        }

        var createSensorsInMemory = function (callback) {
            sensors = {};
            sensors.avaliableInternal = [];
            sensors.internal = [];
            sensors.external = [];

            scanForInternalSensors(function (results) {
                for (var i = 0, len = results.length; i < len; i++) {
                    sensors.avaliableInternal.push(results[i]);
                }
                callback();
            });
        }

        var createSensorsInDB = function (callback) {
            var sensorsForDB = angular.copy(sensors);
            // Serialize start and stop watch functions
            for (var i = 0, len = sensorsForDB.avaliableInternal.length; i < len; i++) {
                sensorsForDB.avaliableInternal[i].initialize = sensorsForDB.avaliableInternal[i].initialize.toString();
                sensorsForDB.avaliableInternal[i].getCurrentData = sensorsForDB.avaliableInternal[i].getCurrentData.toString();
                sensorsForDB.avaliableInternal[i].startWatch = sensorsForDB.avaliableInternal[i].startWatch.toString();
                sensorsForDB.avaliableInternal[i].stopWatch = sensorsForDB.avaliableInternal[i].stopWatch.toString();
            }

            DbSrvs.createSensorsDocument(sensorsForDB, function () {
                callback();
            });
        }

        var updateSensorsInMemeory = function (sensorsFromDB, callback) {
            sensors = angular.copy(sensorsFromDB);
            // Deserialize start and stop watch functions
            for (var i = 0, len = sensors.avaliableInternal.length; i < len; i++) {
                eval('sensors.avaliableInternal[i].initialize = ' + sensors.avaliableInternal[i].initialize);
                eval('sensors.avaliableInternal[i].getCurrentData = ' + sensors.avaliableInternal[i].getCurrentData);
                eval('sensors.avaliableInternal[i].startWatch = ' + sensors.avaliableInternal[i].startWatch);
                eval('sensors.avaliableInternal[i].stopWatch = ' + sensors.avaliableInternal[i].stopWatch);
            }
            for (var i = 0, len = sensors.internal.length; i < len; i++) {
                eval('sensors.internal[i].initialize = ' + sensors.internal[i].initialize);
                eval('sensors.internal[i].getCurrentData = ' + sensors.internal[i].getCurrentData);
                eval('sensors.internal[i].startWatch = ' + sensors.internal[i].startWatch);
                eval('sensors.internal[i].stopWatch = ' + sensors.internal[i].stopWatch);
            }
            callback();
        }

        var initialize = function (callback) {
            if (!sensors && !bleOp) {
                bleOp = ble;
                DbSrvs.getSensorsDocument(function (result) {
                    if (!result) {
                        console.log('Sensors local document does not exist');
                        createSensorsInMemory(function () {
                            createSensorsInDB(function () {
                                LogSrvs.log('Local document for sensors created.', 'info');
                                updateFlagForNoSensorsSelected();
                                callback();
                            });
                        });
                    } else {
                        LogSrvs.log('Local document for sensors retrieved.', 'info');
                        updateSensorsInMemeory(result, function () {
                            // Reestablish ble connections
                            if (sensors.external.length > 0) {
                                LogSrvs.log('Establishing ble connections with avaliable devices.', 'info');
                                avaliableExternalSensors(function (results) {
                                    var sensorsForRemoval = [];
                                    for (var i = 0, len = sensors.external.length; i < len; i++) {
                                        sensorsForRemoval.push(sensors.external[i]);
                                        for (var j = 0, leng = results.length; j < leng; j++) {
                                            if (results[j].id === sensors.external[i].id) {
                                                connectToExternalSensor(sensors.external[i]);
                                                sensorsForRemoval.pop();
                                                break;
                                            }
                                        }
                                    }
                                    addOrRemoveMultipleExtSensors(sensorsForRemoval, [], function () {
                                        updateFlagForNoSensorsSelected();
                                        callback();
                                    });
                                });
                            }
                            updateFlagForNoSensorsSelected();
                            callback();
                        });
                    }
                });
            }
        }

        var avaliableInternalSensors = function (callback) {
            if (sensors) {
                callback(sensors.avaliableInternal);
            }
        }

        var getIntSensors = function (callback) {
            if (sensors) {
                callback(sensors.internal);
            }
        }

        var addOrRemoveMultipleIntSensors = function (sensorsForRemovingArray, sensorsForAddingArray, callback) {
            if (sensors) {
                if (sensorsForRemovingArray.length > 0 || sensorsForAddingArray.length > 0) {
                    DbSrvs.getSensorsDocument(function (result) {
                        // Remove sensors
                        for (var i = 0, len = sensorsForRemovingArray.length; i < len; i++) {
                            for (var ii = 0, lenn = sensors.internal.length; ii < lenn; ii++) {
                                if (sensors.internal[ii].sensorName === sensorsForRemovingArray[i].sensorName) {
                                    LogSrvs.log('Removing internal sensor ' + sensorsForRemovingArray[i].sensorName + '.', 'info');
                                    sensors.internal.splice(ii, 1);
                                    result.internal.splice(ii, 1);
                                    break;
                                }
                            }
                        }
                        // Add sensors
                        for (var j = 0, leng = sensorsForAddingArray.length; j < leng; j++) {
                            var duplicate = false;
                            for (var jj = 0, lengg = sensors.internal.length; jj < lengg; jj++) {
                                if (sensors.internal[jj].sensorName === sensorsForAddingArray[j].sensorName) {
                                    duplicate = true;
                                    break;
                                }
                            }
                            if (!duplicate) {
                                LogSrvs.log('Adding internal sensor ' + sensorsForAddingArray[j].sensorName + '.', 'info');
                                sensors.internal.push(sensorsForAddingArray[j]);
                                var sensorForDB = angular.copy(sensorsForAddingArray[j]);
                                // Serialize functions
                                sensorForDB.initialize = sensorForDB.initialize.toString();
                                sensorForDB.startWatch = sensorForDB.startWatch.toString();
                                sensorForDB.stopWatch = sensorForDB.stopWatch.toString();
                                result.internal.push(sensorForDB);
                            }
                        }
                        // Update sensors in DB
                        DbSrvs.updateSensorsDocument(result);
                        updateFlagForNoSensorsSelected();
                        callback();
                    });
                } else {
                    callback();
                }
            }
        }

        var avaliableExternalSensors = function (callback) {
            if (bleOp) {
                var results = [];
                LogSrvs.log('BLE scan.', 'info');
                bleOp.startScan([], function (result) {
                    result.sensorName = !result.name ? result.id : result.name;
                    LogSrvs.log('Found ble device with id: ' + result.id, 'info');
                    results.push(result);
                }, function (error) {
                    console.log(error);
                });

                $timeout(function () {
                    LogSrvs.log('********** BLE SCAN STOPPED ************');
                    bleOp.stopScan(function () {
                    }, function (error) {
                        LogSrvs.log('BLE error: ' + error, 'error');
                    });
                    callback(results);
                }, config.bleOptions.scanDuration);
            }
        }

        var getExtSensors = function (callback) {
            if (sensors) {
                callback(sensors.external);
            }
        }

        var addOrRemoveMultipleExtSensors = function (sensorsForRemovingArray, sensorsForAddingArray, callback) {
            if (sensors) {
                if (sensorsForRemovingArray.length > 0 || sensorsForAddingArray.length > 0) {
                    DbSrvs.getSensorsDocument(function (result) {
                        // Remove sensors
                        for (var i = 0, len = sensorsForRemovingArray.length; i < len; i++) {
                            for (var ii = 0, lenn = sensors.external.length; ii < lenn; ii++) {
                                if (sensors.external[ii].id === sensorsForRemovingArray[i].id) {
                                    LogSrvs.log('Removing external sensor ' + sensorsForRemovingArray[i].id + '.', 'info');
                                    sensors.external.splice(ii, 1);
                                    result.external.splice(ii, 1);
                                    disconnectFromExternalSensor(sensorsForRemovingArray[i]);
                                    break;
                                }
                            }
                        }
                        // Add sensors
                        for (var j = 0, leng = sensorsForAddingArray.length; j < leng; j++) {
                            var duplicate = false;
                            for (var jj = 0, lengg = sensors.external.length; jj < lengg; jj++) {
                                if (sensors.external[jj].id === sensorsForAddingArray[j].id) {
                                    duplicate = true;
                                    break;
                                }
                            }
                            if (!duplicate) {
                                LogSrvs.log('Adding external sensor ' + sensorsForAddingArray[j].id + '.', 'info');
                                sensors.external.push(sensorsForAddingArray[j]);
                                connectToExternalSensor(sensorsForAddingArray[j]);
                                var sensorForDB = angular.copy(sensorsForAddingArray[j]);
                                result.external.push(sensorForDB);
                            }
                        }
                        // Update sensors in DB
                        DbSrvs.updateSensorsDocument(result);
                        updateFlagForNoSensorsSelected();
                        callback();
                    });
                } else {
                    callback();
                }
            }
        }

        var connectToExternalSensor = function (sensor) {
            bleOp.connect(sensor.id, function (result) {
                LogSrvs.log('Connected to external sensor: ' + sensor.id + '.', 'info');
                console.log(result);
            }, function (error) {
                console.log(error);
            });
        }

        var disconnectFromExternalSensor = function (sensor) {
            bleOp.disconnect(sensor.id, function () {
                LogSrvs.log('Disconnected from external sensor: ' + sensor.id, 'info');
            }, function (error) {
                console.log(error);
            });
        }

        var getAllSensors = function (callback) {
            callback(sensors.internal.concat(sensors.external));
        }

        var updateFlagForNoSensorsSelected = function () {
            if (sensors.internal.length == 0 && sensors.external.length == 0 && !WearSrvs.isWearEnabled()) {
                $rootScope.noSensorsSelected = true;
            } else {
                $rootScope.noSensorsSelected = false;
            }
        }

        getSensorsDetails = function () {

            sensors.internal;
            sensors_data = {
                internal: {},
                external: {}
            }
            if (sensors.internal.length > 0) {
                for (key in sensors.internal) {
                    sensorName = sensors.internal[key].sensorName;
                }
            }

            sensors.external;
        }

        getInternalSensorDetails = function () {
            var sensorsDetails = [];
            var deferred = $q.defer();
            if (sensors.internal > 0)
                for (key in sensors.internal) {
                    var obj = {
                        channels: [],
                        name: sensors.internal[key].sensorName
                    };
                    getChannels(key).then(function (result) {
                        console.error("getChannels then");
                        for (channel_name in result) {
                            if ("timestamp" != channel_name) {
                                if (channel_name != "coords") {
                                    obj.channels.push(channel_name);
                                } else {
                                    for (cord_name in result[channel_name]) {
                                        obj.channels.push(cord_name);
                                    }
                                }

                            }
                        }
                        sensorsDetails.push(obj);
                        if (sensorsDetails.length == sensors.internal.length) {
                            deferred.resolve(sensorsDetails);
                        }
                    }, function () {
                        deferred.reject("Unable to getChannels");
                    });
                } else {
                deferred.resolve([]);
            }
            return deferred.promise;
        }

        getChannels = function (key) {
            console.log("get channels");
            return sensors.internal[key].getCurrentData();
        }


        return {
            initializeSensors: function (callback) {
                initialize(callback);
            },
            getAvaliableExternalSensors: function (callback) {
                avaliableExternalSensors(callback);
            },
            getExternalSensors: function (callback) {
                getExtSensors(callback);
            },
            addOrRemoveMultipleExternalSensors: function (sensorsForRemovingArray, sensorsForAddingArray, callback) {
                addOrRemoveMultipleExtSensors(sensorsForRemovingArray, sensorsForAddingArray, callback);
            },
            getAvaliableInternalSensors: function (callback) {
                avaliableInternalSensors(callback);
            },
            getInternalSensors: function (callback) {
                getIntSensors(callback);
            },
            addOrRemoveMultipleInternalSensors: function (sensorsForRemovingArray, sensorsForAddingArray, callback) {
                addOrRemoveMultipleIntSensors(sensorsForRemovingArray, sensorsForAddingArray, callback);
            },
            getAllSelectedSensors: function (callback) {
                getAllSensors(callback);
            },
            getIntSensorDetails: function () {
                return getInternalSensorDetails();
            }
        };
    });