angular.module('services.monitor', [])

    .factory('MonitorSrvs', function ($rootScope, SensorSrvs, WearSrvs, ProfileSrvs, DbSrvs, uuid, $cordovaDevice, LogSrvs) {
        var monitoringStatuses = {
            STARTED: 'Started',
            STOPPED: 'Stopped'
        };
        var currentMonitoringStatus = monitoringStatuses.STOPPED;

        var monitoringStatusSubscriptions = [];
        var monitoringValueSubscriptions = [];
        var isMonitoringOnSubscriptions = [];
        var currentSession;

        var updateCurrentMonitoringStatus = function (status) {
            console.log('* STATUS : ' + status + ' *');
            currentMonitoringStatus = status;
            for (var i = 0, len = monitoringStatusSubscriptions.length; i < len; i++) {
                monitoringStatusSubscriptions[i](status);
            }
        }

        var updateCurrentMonitoringValue = function (sensorData) {
            for (var i = 0, len = monitoringValueSubscriptions.length; i < len; i++) {
                monitoringValueSubscriptions[i](sensorData);
            }
        };

        var updateIsMonitoringOn = function () {
            for (var i = 0, len = isMonitoringOnSubscriptions.length; i < len; i++) {
                isMonitoringOnSubscriptions[i](currentMonitoringStatus === monitoringStatuses.STARTED);
            }
        }

        function startSession(callback) {

            configuration = new Configuration();
            configuration.setDevice($cordovaDevice.getDevice());

            currentSession = new Session();
            currentSession.setStart((new Date()).getTime());
            currentSession.setUser(ProfileSrvs.getUsername());

            SensorSrvs.getIntSensorDetails().then(function (result) {
                configuration.setSensors(result);
                currentSession.setConfiguration(configuration);
                callback(currentSession);
            }, function (error) {
                console.error(error);
            });

        }

        var startMonitoringSensors = function () {
            if (currentMonitoringStatus === monitoringStatuses.STOPPED) {
                startSession(function (currentSession) {
                    DbSrvs.createSessionDocument(currentSession).then(function (result) {
                        console.log('* START *');
                        LogSrvs.log('Monitoring started.', 'info', currentSession.getGuid());
                        console.log("Session document created");
                        var sessionId = result.id;
                        currentSession.setGuid(result.id);
                        SensorSrvs.getInternalSensors(function (results) {
                            LogSrvs.createSessionDirectory(currentSession);
                            for (var i = 0, len = results.length; i < len; i++) {
                                results[i].startWatch(results[i].sensorName, function (sensorData) {
                                    readSensorSuccess(sensorData);
                                }, function (error) {
                                    LogSrvs.log('Failed to read sensor.', 'error', currentSession.getGuid());
                                });
                            }
                        });

                        // Start android wear
                        if (WearSrvs.isWearEnabled()) {
                            WearSrvs.startWear(sessionId);
                        }
                        if (WearSrvs.isMetaWearEnabled()) {
                            WearSrvs.startMetaWear(sessionId);
                        }
                        updateCurrentMonitoringStatus(monitoringStatuses.STARTED);
                        updateIsMonitoringOn();

                    }, function (error) {
                        // TODO Handle this error
                        console.log(error);
                    });
                });

            }
        }

        var stopMonitoringSensors = function () {
            if (currentMonitoringStatus === monitoringStatuses.STARTED) {
                console.log('********** STOP **********');
                var sessionId = currentSession.getGuid();

                SensorSrvs.getInternalSensors(function (results) {
                    for (var i = 0, len = results.length; i < len; i++) {
                        results[i].stopWatch();
                    }
                });

                // Stop android wear
                if (WearSrvs.isWearEnabled()) {
                    WearSrvs.stopWear('wear');
                }

                if (WearSrvs.isMetaWearEnabled()) {
                    WearSrvs.stopWear('metaWear');
                }
                DbSrvs.updateSessionEnd(sessionId);
                updateCurrentMonitoringStatus(monitoringStatuses.STOPPED);
                updateIsMonitoringOn();
            }
        }

        var readSensorSuccess = function (sensorData) {
            updateCurrentMonitoringValue(sensorData);
        };

        return {
            subscribeForMonitoringStatus: function (callback) {
                monitoringStatusSubscriptions.push(callback);
            },
            subscribeForMonitoringValue: function (callback) {
                monitoringValueSubscriptions.push(callback);
            },
            startMonitoring: function () {
                startMonitoringSensors();
            },
            stopMonitoring: function () {
                stopMonitoringSensors();
            },
            isMonitoringOn: function (callback) {
                isMonitoringOnSubscriptions.push(callback);
            },
            currentSession: function () {
                return currentSession;
            },
            readSensor: function (sensorData) {
                return readSensorSuccess(sensorData);
            }
        };
    });