angular.module('services.profile', [])

    .factory('ProfileSrvs', function ($rootScope, DbSrvs, HttpSrvs, LogSrvs, NetworkSrvs) {
        var profile = null;

        $rootScope.debug = {
            transferingData: false,
            enableLogs: false
        };

        var initialize = function (callback) {
            if (!profile) {
                DbSrvs.getProfileDocument(function (result) {
                    if (!result) {
                        LogSrvs.log('Profile is not set.', 'warning');
                        callback();
                    } else {
                        if (NetworkSrvs.isDeviceOnline()) {
                            HttpSrvs.setAuthHeader(result.id, result.password);
                            HttpSrvs.getRequest(config.urls.restApi + '/zetaaccount/getcurrentaccount', function (account) {
                                if (!account.data) {
                                    LogSrvs.log('User can not be authenticated remotely.', 'warning');
                                } else {
                                    profile = result;
                                    if (typeof profile.debug != "undefined" && profile.debug) {
                                        $rootScope.debug = profile.debug;
                                    }
                                    DbSrvs.enableSyncGatewayReplicationForUser(profile);
                                    LogSrvs.log('Profile is set for username ' + profile.id + '.', 'info');
                                    LogSrvs.updateLogsOwner();
                                    callback();
                                }
                            });
                        } else {
                            profile = result;
                            LogSrvs.log('Profile is set only loaclly for username ' + profile.id + '.', 'info');
                            LogSrvs.log('No remote sync, data stored only locally.', 'warning');
                            NetworkSrvs.insertFunctionForOnlineExecution(function () {
                                HttpSrvs.setAuthHeader(profile.id, profile.password);
                                HttpSrvs.getRequest(config.urls.restApi + '/zetaaccount/getcurrentaccount', function (account) {
                                    if (!account.data) {
                                        LogSrvs.log('User can not be authenticated remotely.', 'warning');
                                    } else {
                                        DbSrvs.enableSyncGatewayReplicationForUser(profile);
                                        LogSrvs.updateLogsOwner();
                                    }
                                });
                            });
                            callback();
                        }
                    }
                });
            }
        }

        var setProfile = function (username, pass, callback) {
            if (!profile) {
                if (checkPasswordComplexity(pass)) {
                    LogSrvs.log('Password is strong.', 'info');
                    HttpSrvs.postRequest(config.urls.restApi + '/public/checkaccountbyid', username, function (exists) {
                        if (!exists.data) {
                            console.log('User id ' + username + ' does not exist', 'warning');
                            var profileObj = {
                                id: username,
                                password: pass,
                                role: 'ROLE_USER',
                                type: 'zetaaccount',
                                created: Date.now(),
                                debug: $rootScope.debug,
                            };
                            DbSrvs.createProfileDocument(profileObj, function () {
                                console.log('Profile was successfully created locally', 'info');
                                DbSrvs.getProfileDocument(function (result) {
                                    HttpSrvs.postRequest(config.urls.restApi + '/public/createaccount', result, function (success) {
                                        if (!success.data) {
                                            console.log('Profile was not created remotely', 'warning');
                                            callback(false);
                                        } else {
                                            console.log('Profile was successfully created remotely', 'info');
                                            profile = result;
                                            HttpSrvs.setAuthHeader(profile.id, profile.password);
                                            DbSrvs.enableSyncGatewayReplicationForUser(profile);
                                            callback(true);
                                        }
                                    });
                                });
                            });
                        } else {
                            console.log('User id ' + username + ' exists', 'info');
                            HttpSrvs.setAuthHeader(username, pass);
                            HttpSrvs.getRequest(config.urls.restApi + '/zetaaccount/getcurrentaccount', function (account) {
                                if (!account.data) {
                                    console.log('User can not be authenticated', 'warning');
                                } else {
                                    console.log('User is authenticated', 'info');
                                    account.data.password = pass;
                                    DbSrvs.createProfileDocument(account.data, function () {
                                        DbSrvs.getProfileDocument(function (result) {
                                            console.log('Profile was successfully created locally');
                                            profile = result;
                                            DbSrvs.enableSyncGatewayReplicationForUser(profile);
                                            callback(true);
                                        });
                                    });
                                }
                            });
                        }
                    });
                } else {
                    LogSrvs.log('Password is not strong enough.', 'warning');
                    callback(false);
                }
            }
        }

        var checkPasswordComplexity = function (password) {
            var isPasswordStrong = false;

            var minPasswordLenght = 8;
            var anUpperCase = /[A-Z]/;
            var aLowerCase = /[a-z]/;
            var aNumber = /[0-9]/;
            var aSpecial = /[!|@|#|$|%|^|&|*|(|)|-|_|.]/;

            if (password.length > minPasswordLenght - 1) {
                var hasUpperCase = false;
                var hasLowercase = false;
                var hasNumber = false;
                var hasSpecial = false;
                for (var i = 0, len = password.length; i < len; i++) {
                    if (anUpperCase.test(password[i])) {
                        hasUpperCase = true;
                    }
                    if (aLowerCase.test(password[i])) {
                        hasLowercase = true;
                    }
                    if (aNumber.test(password[i])) {
                        hasNumber = true;
                    }
                    if (aSpecial.test(password[i])) {
                        hasSpecial = true;
                    }
                    if (hasUpperCase && hasLowercase && hasNumber && hasSpecial) {
                        isPasswordStrong = true;
                        break;
                    }
                }
            }

            return isPasswordStrong;
        };

        var updateDebug = function (name, debug) {
            DbSrvs.updateProfileDocument(name, debug);
        };

        return {
            initializeProfile: function (callback) {
                initialize(callback);
            },
            checkIsProfileSet: function () {
                return profile != null;
            },
            getProfile: function () {
                return profile;
            },
            setProfile: function (username, password, callback) {
                setProfile(username, password, callback);
            },
            getUsername: function () {
                return ( profile && profile.id) ? profile.id : 'undefined';
            },
            updateDebugStatus: function (name, debug) {
                updateDebug(name, debug);
            }
        };
    });