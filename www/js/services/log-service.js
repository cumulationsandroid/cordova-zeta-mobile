angular.module('services.log', [])


    .factory('LogSrvs', ['$injector', '$rootScope', '$ionicScrollDelegate', '$location', '$q', '$cordovaFile', '$filter', function ($injector, $rootScope, $ionicScrollDelegate, $location, $q, $cordovaFile, $filter) {

        date = new Date();

        $rootScope.LogFiltered = [];
        $rootScope.addOnTopLogs = true;
        $rootScope.logFilters = {
            from: date.toISOString(),
            to: new Date(new Date().getTime() + 300000).toISOString(), //+3s
            goto: 0,
        };

        var injectDbSrvs = function () {
            return $injector.get('DbSrvs');
        };
        var injectProfileSrvs = function () {
            return $injector.get('ProfileSrvs');
        };

        var isDevelopmentMode = null;

        var initialize = function (callback) {
            if (isDevelopmentMode == null) {
                $rootScope.isDevelopmentMode = config.logOptions.developmentMode;
                isDevelopmentMode = $rootScope.isDevelopmentMode;
                enterLogMessage('Logging ' + (isDevelopmentMode ? 'is ' : 'is not ') + 'in development mode.');
                callback();
            }
        };

        var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var logMessagesSubscriptions = [];
        var logMessagesFiltered = [];
        var logMessages = [];

        var formatTime = function (dateObj) {
            var getMonthName = function (monthNumber) {
                return monthNames[monthNumber];
            };

            var addLeadingZero = function (number) {
                return number.toString().length == 1 ? '0' + number : number;
            };

            return addLeadingZero(dateObj.getDate()) + '-' + getMonthName(dateObj.getMonth()) + '-' + dateObj.getFullYear()
                + ' ' + addLeadingZero(dateObj.getHours()) + ':' + addLeadingZero(dateObj.getMinutes())
                + ':' + addLeadingZero(dateObj.getSeconds());
        };

        var enterLogMessage = function (msg, category, session_id) {
            var formattedMessage = formatTime(new Date()) + ' ' + msg;
            if (isDevelopmentMode) {
                var log_obj = {
                    message: msg,
                    created: new Date().getTime(),
                    category: category,
                    type: 'log',
                    owner: injectProfileSrvs().getUsername()
                };

                logMessagesSubscriptions.push(log_obj);
                addToFiltered(log_obj);

                session_id = session_id ? session_id : "";
                log_obj.sessionId = session_id;

                injectDbSrvs().createLogDocument(log_obj);


            }
            console.log(formattedMessage);
        };

        var addToFiltered = function (log) {
            if ($rootScope.addOnTopLogs) {
                $rootScope.LogFiltered.unshift(log);
            }
        };

        var formatFilterDate = function (date) {
            d = new Date(date);
            today = new Date();
            month = today.getMonth();

            d.setDate(today.getDate());
            d.setMonth(month);
            d.setFullYear(today.getFullYear());
            return d;
        };
        var applyFilters = function () {
            $('.filters_settings').hide();
            $rootScope.LogFiltered = [];
            for (key in logMessagesSubscriptions) {
                addToFiltered(logMessagesSubscriptions[key]);
            }
        };
        updateLogsOwner = function () {
            injectDbSrvs().getLogsUndefinedOwner().then(function (result) {
                    if (result && result.rows) {
                        console.log("Documents without owner: " + result.rows.length);
                        for (key in result.rows) {
                            var log_doc = result.rows[key].value;
                            var rev = log_doc.rev;
                            var id = result.rows[key].id;
                            log_doc.owner = injectProfileSrvs().getUsername();
                            injectDbSrvs().updateLogsOwner(id, rev, log_doc);
                        }
                    } else {
                        console.log("Documents without owner: 0!");
                    }
                },
                function (error) {
                    console.log("error");

                });
        };


        /*** I ***/

        var loadNewerLogs = function (mode, handle, scope, rootScope) {
            if (!rootScope) {
                rootScope = scope;
            }
            var deferred = $q.defer();
            if (!scope.addOnTop) {
                scope.topLoading = true;


                if (mode && mode == "goto") {
                    logIndex = scope.filters.gotoIndex * 1;
                } else {
                    logIndex = scope.filters.gotoAfterIndex > -1 ? scope.filters.gotoAfterIndex * 1 : 1;
                    logIndex = scope.filters.gotoAfterIndex * 1;
                }

                var counter = 0;
                for (i = logIndex; i <= $rootScope[scope.logArrayName].length + 1; i++) {
                    if (typeof $rootScope[scope.logArrayName][i] != "undefined" && counter <= config.logOptions.maxNumberOfLogItems / 2) {
                        if (logFilter($rootScope[scope.logArrayName][i], scope)) {
                            if ($rootScope[scope.logFilteredArrayName].indexOf($rootScope[scope.logArrayName][i]) == -1) {
                                $rootScope[scope.logFilteredArrayName].unshift($rootScope[scope.logArrayName][i]);
                                scope.filters.gotoAfterIndex = i;
                                counter++;
                            }
                        }
                        if (i >= $rootScope[scope.logArrayName].length - 1) {
                            scope.addOnTop = true;
                            scope.scrollLogToTop('monitor-logs', true);
                        }
                    } else {
                        // scroll to
                        if (typeof handle != "undefined" && !scope.addOnTop) {
                            scope.scrollTo($rootScope[scope.logArrayName][logIndex].timestamp, handle);
                        }
                        deferred.resolve();
                        scope.topLoading = false;
                        break;
                    }
                }
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        };

        var loadOlderLogs = function (mode, scope) {
            scope.bottomLoading = true;
            var deferred = $q.defer();

            if (mode && mode == "goto") {
                logIndex = scope.filters.gotoIndex * 1;
            } else {
                logIndex = scope.filters.gotoBeforeIndex > -1 ? scope.filters.gotoBeforeIndex * 1 : 1;
            }

            var counter = 0;
            for (i = logIndex - 1; i >= -1; i--) {
                if (typeof $rootScope[scope.logArrayName][i] != "undefined" && counter <= config.logOptions.maxNumberOfLogItems / 2) {
                    if (logFilter($rootScope[scope.logArrayName][i], scope)) {
                        $rootScope[scope.logFilteredArrayName].push($rootScope[scope.logArrayName][i]);
                        scope.filters.gotoBeforeIndex = i;
                        counter++;
                    }
                } else {
                    deferred.resolve();
                    scope.bottomLoading = false;
                    break;
                }
            }
            return deferred.promise;
        };

        var logFilter = function (log, scope) {

            if (typeof scope.categoryFilters[log.category] != "undefined") {
                if (scope.categoryFilters[log.category]) {
                    //filter categories
                    return log;
                }
            } else if (typeof log.sensor != "undefined") {
                //if sup category doesn't exist add it
                if (typeof scope.subCategoryFilters[log.sensor] == "undefined") {
                    scope.subCategoryFilters[log.sensor] = true;
                }
                if (scope.subCategoryFilters[log.sensor]) {
                    //filter sub categories
                    return log;
                }
            }
            return false;
        };

        /***/
        $rootScope.ConvertToCSV = function (objArray) {
            var deferred = $q.defer();
            var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
            var str = '';
            var header = ['created', 'owner', 'type', 'sensor', 'steps', 'x', 'y', 'z'];
            var empty_row = ['', '', '', '', '', '', '', '', ''];
            str = header.join(",")+"\r\n";
            for (var i = 0; i < array.length; i++) {

                for (var index in array[i]) {

                        if (index != 'timestamp' && index != 'sensor') {
                            var row = empty_row.slice(0);
                            var line = '';
                            row[header.indexOf('created')] = array[i].created;
                            row[header.indexOf('sensor')] = array[i].sensorName;
                            row[header.indexOf(array[i].channelName)] = array[i].channelDataValue;
                            line += row.join(",");
                        }
                }

                str += line + '\r\n';
                if (i * 1 + 1 == array.length) {
                    deferred.resolve(str);
                }
            }

            return deferred.promise;
        };
        var saveCSV = function (data, session) {

            var session_date = $filter('date')(session.start, 'dd_MMM_yyyy-HH_mm_ss');
            var storage_path = cordova.file.externalRootDirectory;
            var file_dir = storage_path + "Zeta_Mobile/"+session_date+"/";


            if (data.length > 0) {
                $rootScope.ConvertToCSV(data).then(function (csv_data) {
                    var file_name = "ZMClogs.csv";

                    $cordovaFile.writeFile(file_dir, file_name, csv_data, true)
                        .then(function (success) {
                            // success
                            enterLogMessage("CSV file created", "info", session.guid);
                            console.log(success);

                        }, function (error) {
                            enterLogMessage("CSV file not created: "+JSON.stringify(error), "error", session.guid);
                        });
                                      
                });
            }
            
        };


        var createSessionDir = function (session) {

            var session_date = $filter('date')(session.start, 'dd_MMM_yyyy-HH_mm_ss');
            var storage_path = cordova.file.externalRootDirectory;
            var file_dir = storage_path + "Zeta_Mobile/"+session_date+"/";


                    // CREATE
                    $cordovaFile.createDir(storage_path, "Zeta_Mobile", false)
                        .then(function (success) {
                            // success
                            console.log("dir created");
                            console.log(success);
                        }, function (error) {
                            console.log("dir not created");
                            console.log(error);
                            // error
                        });


                    $cordovaFile.createDir(storage_path+"/Zeta_Mobile/", session_date, false)
                        .then(function (success) {
                            // success
                            console.log("dir created");
                            console.log(success);

                        }, function (error) {
                            LogSrvs.log("Session dir not created: "+JSON.stringify(error), "error", session.guid);
                        });

                    $cordovaFile.writeFile(storage_path+"/Zeta_Mobile/", "file_path.txt", session_date, true)
                        .then(function (success) {
                            console.log("Path file updated");
                        }, function (error) {
                            console.log("Path file not updated");
                        });

        };

        function listDir(path){
            window.resolveLocalFileSystemURL(path,
                function (fileSystem) {
                    var reader = fileSystem.createReader();
                    reader.readEntries(
                        function (entries) {
                            console.log("entries");
                            console.log(entries);
                        },
                        function (err) {
                            console.log(err);
                        }
                    );
                }, function (err) {
                    console.log(err);
                }
            );
        }


        return {
            initializeLog: function (callback) {
                initialize(callback);
            },
            log: function (msg, category, session_id) {
                enterLogMessage(msg, category, session_id);
            },
            getRecentLogMessages: function (callback) {
                callback(logMessagesSubscriptions);
            },
            subscribeForLogMessages: function (callback) {
                logMessagesSubscriptions.unshift(callback);
            },
            updateLogsOwner: function () {
                return updateLogsOwner();
            },
            getFilterDate: function (date) {
                return formatFilterDate(date);
            },
            applyFilters: function () {
                applyFilters();
            },
            loadNewer: function (mode, handle, scope) {
                return loadNewerLogs(mode, handle, scope);
            },
            loadOlder: function (mode, scope) {
                return loadOlderLogs(mode, scope);
            },
            logFilter: function (log, scope) {
                return logFilter(log, scope);
            },
            saveCSVFile: function (data, session) {
                return saveCSV(data, session);
            },
            createSessionDirectory: function (session) {
                return createSessionDir(session);
            }

        }
    }]);