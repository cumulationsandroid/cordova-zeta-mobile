var SensorContainer = function() {
    this.listSensorAgent = [];
};

SensorContainer.prototype.addSensorAgent = function(sensorAgent) {
    this.listSensorAgent.push(sensorAgent);
}

SensorContainer.prototype.removeSensorAgent = function(sensorAgent) {
    var index = listSensorAgent.indexOf(sensorAgent);
    if(index > -1) {
        this.listSensorAgent.splice(index,1);
    }
}

SensorContainer.prototype.clearSensorAgent = function() {
    this.listSensorAgent = [];
}

SensorContainer.prototype.getSensorAgent = function(index) {
    return this.listSensorAgent[index];
}

function stringStartsWith (string, prefix) {
    return string.slice(0, prefix.length) == prefix;
}

SensorContainer.prototype.objectify = function(value) {
    var a = JSON.parse(value);
    return nodeRecursiveObjectify(a);
};

var nodeRecursiveObjectify = function (jsonObj) {
    var recurObjectify = function (jsonO, acc) {
        var node = createNodeForObjectify(jsonO);
        for (var i = 0, len = jsonO.nodes.length; i < len; i++) {
            acc.push(recurObjectify(jsonO.nodes[i], []));
        }
        node.setNodes(acc);
        return node;
    }

return recurObjectify(jsonObj, []);
}

var createNodeForObjectify = function (jsonObj) {
    var node;

    if(stringStartsWith(jsonObj.uri,Constants.Channel)) {
        node = new Channel();
        node.setSamplingDuration(jsonObj.samplingDuration);
        node.setIsEnabled(jsonObj.isEnabled);
    } else if(stringStartsWith(jsonObj.uri,Constants.SensorAgent)){
        node = new Node();
    } else if(stringStartsWith(jsonObj.uri,Constants.Sensor)){
        node = new Sensor();
    } else if(stringStartsWith(jsonObj.uri,Constants.Device)){
        node = new Device();
    } else {
        node = new Node();
    }

    node.setName(jsonObj.name);
    node.setDescription(jsonObj.description);
    node.setGuid(jsonObj.guid);
    node.setUri(jsonObj.uri);
    node.setType(jsonObj.type);
    var data = jsonObj.data;
    if(data != undefined && data != null) {
        var dataObj = new Data();
        dataObj.setTimeStamp(data.timeStamp);
        dataObj.setDataType(data.dataType);
        dataObj.setValues(data.values);
        node.setData(dataObj);
    }

    return node;
}

SensorContainer.prototype.serialize = function (node) {
  var nodeJson = nodeRecursiveSerialize(node);
  return JSON.stringify(nodeJson);
}

var nodeRecursiveSerialize = function (node) {
  var recurSerialize = function (n, acc) {
    var nodeJson = createNodeForSerialize(n);
    for (var i = 0, len = n.getNodes().length; i < len; i++) {
        acc.push(recurSerialize(n.getNodes()[i], []));
    }
    nodeJson.nodes = acc;
    return nodeJson
  }

  return recurSerialize(node, []);
}

var createNodeForSerialize = function (node) {
    var nodeJson = {};

    if(stringStartsWith(node.getUri(),Constants.Channel)) {
        nodeJson.samplingDuration = node.getSamplingDuration();
        nodeJson.isEnabled = node.getIsEnabled();
    } else if(stringStartsWith(node.getUri(),Constants.SensorAgent)){
    } else if(stringStartsWith(node.getUri(),Constants.Sensor)){
    } else if(stringStartsWith(node.getUri(),Constants.Device)){
    } else {
    }

    nodeJson.name = node.getName();
    nodeJson.description = node.getDescription();
    nodeJson.guid = node.getGuid();
    nodeJson.uri = node.getUri();
    nodeJson.type = node.getType();
    var dataObj = node.getData();
    var data = {};
    if(dataObj != undefined && dataObj != null) {
        data.timeStamp = dataObj.getTimeStamp();
        data.dataType = dataObj.getDataType();
        data.values = dataObj.getValues();
        nodeJson.data = data;
    }

    return nodeJson;
}

SensorContainer.prototype.getSensorAgentNode = function (nodeName) {
    if (nodeName == "device"){
        nodeName = "Device:";
    }
    var requestedNode = null;
    var sensorAgent = this.listSensorAgent[0];
    if (sensorAgent) {
        var nodes = sensorAgent.getNodes();
        for (var i = 0, len = nodes.length; i < len; i++) {
            var node = nodes[i];
            if (node.getUri().indexOf(nodeName) > -1) {
                requestedNode = this.serialize(node);
                return requestedNode;
            }

            if (i == nodes.length - 1 && !requestedNode) {
                return false;
            }
        }
    }

};

