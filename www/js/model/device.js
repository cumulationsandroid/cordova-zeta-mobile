var Device = function() {
	this.sensors;
};

Device.prototype = Object.create(Node.prototype);

Device.prototype.getSensors = function() {
    return this.sensors;
}

Device.prototype.setSensors = function(value) {
    this.sensors = value;
}