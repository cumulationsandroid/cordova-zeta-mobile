var Data = function() {
    var timeStamp;
    var dataType;
    var values;
};

Data.prototype.getTimeStamp = function() {
    return this.timeStamp;
}
Data.prototype.setTimeStamp = function(value) {
    this.timeStamp = value;
}

Data.prototype.getDataType = function() {
    return this.dataType;
}
Data.prototype.setDataType = function(value) {
    this.dataType = value;
}

Data.prototype.getValues = function() {
    return this.values;
}
Data.prototype.setValues = function(value) {
    this.values = value;
}
