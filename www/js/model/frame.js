var Frame = function() {
    this.guid;
    this.user;
    this.description;
    this.nodes;
};

Frame.prototype = Object.create(Node.prototype);

Frame.prototype.getGuid = function() {
    return this.guid;
}

Frame.prototype.setGuid = function(value) {
    this.guid = value;
}

Frame.prototype.getUser = function() {
    return this.user;
}

Frame.prototype.setUser = function(value) {
    this.user = value;
}

Frame.prototype.getDescription = function() {
    return this.description;
}

Frame.prototype.setDescription = function(value) {
    this.description = value;
}

Frame.prototype.getNodes = function() {
    return this.nodes;
}

Frame.prototype.set = function(value) {
    this.nodes = value;
}
