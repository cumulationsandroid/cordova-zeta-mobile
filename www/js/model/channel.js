var Channel = function() {
    this.samplingDuration;
    this.isEnabled;
};

Channel.prototype = Object.create(Node.prototype);

Channel.prototype.getSamplingDuration = function() {
    return this.samplingDuration;
}

Channel.prototype.setSamplingDuration = function(value) {
    this.samplingDuration = value;
}

Channel.prototype.getIsEnabled = function() {
    return this.isEnabled;
}

Channel.prototype.setIsEnabled = function(value) {
    this.isEnabled = value;
}
