var Node = function() {
    this.name;
    this.description;
    this.guid;
    this.uri;
    this.type;
    this.data;
    this.nodes;
};

Node.prototype.getName = function() {
    return this.name;
}
Node.prototype.setName = function(value) {
    this.name = value;
}

Node.prototype.getDescription = function() {
    return this.description;
}
Node.prototype.setDescription = function(value) {
    this.description = value;
}

Node.prototype.getGuid = function() {
    return this.guid;
}
Node.prototype.setGuid = function(value) {
    this.guid = value;
}

Node.prototype.getUri = function() {
    return this.uri;
}
Node.prototype.setUri = function(value) {
    this.uri = value;
}

Node.prototype.getType = function() {
    return this.type;
}
Node.prototype.setType = function(value) {
    this.type = value;
}

Node.prototype.getData = function() {
  return this.data;
}
Node.prototype.setData = function(value) {
  this.data = value;
}

Node.prototype.getNodes = function() {
  return this.nodes;
}
Node.prototype.setNodes = function(value) {
  this.nodes = value;
}