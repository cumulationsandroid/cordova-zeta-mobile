var Configuration = function() {
    var device;
    var sensors;
};

Configuration.prototype = Object.create(Session.prototype);


Configuration.prototype.setDevice = function(value) {
    this.device = value;
}

Configuration.prototype.setSensors = function(value) {
    this.sensors = value;
}