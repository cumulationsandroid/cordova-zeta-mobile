var Session = function() {
	this.start;
	this.configuration;
    this.guid;
    this.user;
    

};

Session.prototype = Object.create(Frame.prototype);

Session.prototype.setStart = function(value) {
    this.start = value;
}

Session.prototype.getStart = function() {
    return this.start;
}

Session.prototype.setConfiguration = function(value) {
    this.configuration = value;
}

Session.prototype.getConfiguration = function() {
    return this.configuration;
}

Session.prototype.setUser = function(value) {
    this.user = value;
}

Session.prototype.getUser = function() {
    return this.user;
}

Session.prototype.setGuid = function(value) {
    this.guid = value;
}

Session.prototype.getGuid = function(value) {
    return this.guid;
}

