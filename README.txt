The following are instructions on using the ZetaCloud and ZetaMobile apps, but only for Android.
The only way to use the ios app is to set up the dev environment on your apple computer, and deploy it from there onto your iphone; or through the app store. The instructions are a bit complicated so we'll keep it simple with just the android version. If anybody wants the ios version we can have a step-by-step through skype.

pre: Make sure your phone has an internet connection, enabled location services for wifi/gps, bluetooth etc.

1. Install the apk on your android contained within /platforms/android/build/outputs/apk/ folder.
2. The first-time run will take you to the account setup dialog.
- You can set up a new account by inputing username and password (password has to have at least: 8 characters, 1 digit, 1 special (.,#$@;][{} etc.))
- Or you can use one of the test accounts, for example user/pass are petar/Petar.84
(existing account should automatically sync previous data from that account onto your phone which you can see later by invoking refresh on the History tab)
3. Select local sensors for monitoring from the list of available internal sensors by clicking the plus sign button on the settings tab
4. on the monitoring tab click start/stop to star/stop monitoring the sensors and view the latest data graphs and logs.
5. on history tab see all previous data stored on graphs and logs

6. Go to ZetaCloud on the http://54.93.158.120:8080 address
7. input your previously selected username and password
8. the home screen will show some user data
9. the history page will visualise all your sensor data in graph and txt format
10. the monitoring page, will display data you monitor on your mobile phone once you click the start button (with slight time delay between the two because of advanced indexing, backups and node replication checks within the nosql data store)

11. The data store (Couchbase) itself has a web client exposed on http://52.59.247.197:8091
12. Access it with user/pass Administrator/marko12345
13. You can see various info regarding the cluster and nodes, data(documents) and sync metadata within data buckets, development and production views etc. 