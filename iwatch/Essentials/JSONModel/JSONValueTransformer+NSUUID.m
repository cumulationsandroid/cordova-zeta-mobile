//
//  JSONValueTransformer+NSUUID.m
//  zetasense
//
//  Created by Madhu V Swamy on 31/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "JSONValueTransformer+NSUUID.h"

@implementation JSONValueTransformer (NSUUID)

- (id)NSUUIDFromNSString:(NSString*)string{
    return [[NSUUID alloc] initWithUUIDString:string];
}

- (id)JSONObjectFromNSUUID:(NSUUID*)uuid{
    return [uuid UUIDString];
}
@end
