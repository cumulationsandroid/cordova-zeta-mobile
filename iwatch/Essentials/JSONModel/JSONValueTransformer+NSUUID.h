//
//  JSONValueTransformer+NSUUID.h
//  zetasense
//
//  Created by Madhu V Swamy on 31/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "JSONValueTransformer.h"

@interface JSONValueTransformer (NSUUID)
- (id)NSUUIDFromNSString:(NSString*)string;
- (id)JSONObjectFromNSUUID:(NSUUID*)uuid;
@end

