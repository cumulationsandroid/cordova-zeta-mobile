//
//  JSONValueTransformer+NSMutableData.m
//  zetasense
//
//  Created by Madhu V Swamy on 05/04/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "JSONValueTransformer+NSMutableData.h"

@implementation JSONValueTransformer (NSMutableData)
- (id)NSMutableDataFromNSString:(NSString*)string{
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}

- (id)JSONObjectFromNSMutableData:(NSMutableData*)data{
    return [NSString stringWithFormat:@"%@",data];
}

@end
