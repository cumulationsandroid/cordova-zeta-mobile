//
//  JSONValueTransformer+NSMutableData.h
//  zetasense
//
//  Created by Madhu V Swamy on 05/04/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "JSONValueTransformer.h"

@interface JSONValueTransformer (NSMutableData)
- (id)NSMutableDataFromNSString:(NSString*)string;
- (id)JSONObjectFromNSMutableData:(NSMutableData*)data;
@end
