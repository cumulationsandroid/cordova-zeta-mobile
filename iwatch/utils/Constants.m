//
//  Constants.m
//  zetasense
//
//  Created by Madhu V Swamy on 16/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "Constants.h"

@implementation Constants

NSInteger const DEFAULT_SAMPLING_DURATION = 10;
Boolean const DEFAULT_ENABLED_STATE = YES;

@end
