//
//  Constants.h
//  zetasense
//
//  Created by Madhu V Swamy on 16/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  This class maintains all the constants in the project
 */

@interface Constants : NSObject

#define kSchemeIdentifier @"://"
#define kManufacturer @"Apple"
#define kProduct @"iWatch"
#define kAccelerometer @"Accelerometer"
#define kAccelerometerDescription @"Used to acquire device's accelerometer data"
#define kPedometer @"Pedometer"
#define kPedometerDescription @"Used to acquire device's pedometer data"
#define kHeartRate @"Heart Rate"
#define kHeartRateDescription @"Used to acquire device's heart rate data"
#define kActiveEnergyBurned @"Active Energy Burned"
#define kActiveEnergyBurnedDescription @"Used to acquire device's Active Energy Burned data"

extern NSInteger const DEFAULT_SAMPLING_DURATION;
extern Boolean const DEFAULT_ENABLED_STATE;

@end



