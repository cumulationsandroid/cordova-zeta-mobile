//
//  SensorConfig.m
//  zetasense
//
//  Created by Madhu V Swamy on 09/05/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "SensorConfig.h"

@interface SensorConfig()

@property(strong,nonatomic)NSString *name;
@property(strong,nonatomic)NSString *description;
@property(nonatomic,assign)int noofchannels;

@end

@implementation SensorConfig
@synthesize name,description,noofchannels;

- (void)setName:(NSString *)_name{
    name = _name;
}

- (NSString *)getName{
    return name;
}

- (void)setDescription:(NSString *)_description{
    description = _description;
}

- (NSString *)getDescription{
    return description;
}

- (void)setNoofchannels:(int)_noofchannels{
    noofchannels = _noofchannels;
}

- (int)getNoofchannels{
    return noofchannels;
}

@end
