//
//  ZetaHealthKitManager.h
//  zetasense
//
//  Created by Madhu V Swamy on 14/05/16.
//  Copyright © 2016 Tuts+. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../model/meta/Node.h"
#import "../model/meta/DataType.h"
#import <HealthKit/HealthKit.h>
@import WatchConnectivity;

@interface ZetaHealthKitManager : NSObject

+ (ZetaHealthKitManager *)sharedManager;
typedef void (^authorisationStatus)(BOOL authorised,NSError *error);
typedef void (^authorisationStatus)(BOOL authorised,NSError *error);
- (void)requestAuthorization:(authorisationStatus)status;
- (void)readHeartRate;
- (void)readActiveEnergy;
-(BOOL)getAuthorisationStatus;
-(HKHealthStore *)getHealthKitStore;

@end
