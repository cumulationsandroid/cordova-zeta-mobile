//
//  Utils.h
//  zetasense
//
//  Created by Madhu V Swamy on 02/05/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataType.h"
#import "NodeType.h"
/**
 *  This class has all the generic utilty functions that are required in the project
 */

@interface Utils : NSObject

+ (NSMutableArray *)getNodesFromDictionary:(NSArray *)array;
+ (NSMutableArray *)getSensorConfiguration;
+ (NSString *)stringFromDataType:(DataType)_dataType;
+ (DataType)dataTypeFromString:(NSString *)string;
+ (NSString *)stringFromNodeType:(NodeType)_nodeType;
+ (NodeType)nodeTypeFromString:(NSString *)string;

@end
