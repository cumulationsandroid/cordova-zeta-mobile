//
//  Utils.m
//  zetasense
//
//  Created by Madhu V Swamy on 02/05/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "Utils.h"
#import "Node.h"
#import "Channel.h"
#import "SensorConfig.h"

@implementation Utils

/**
 *  This is a function used to return nodes array based on the type of the node. Due to the limitation of JSON model class being able to differentiate between channel and node, we are checking the node object's URI. Based on whether it is a channel or any other type we are creating channel object or node object. The samr is added to the array.
     This function is called from setNodesWithNSArray which is a custom setter function from the Node class.
 *
 *  @param array array of nodes which is has NSDictionary objects.
 *
 *  @return array of Channel or Node objects.
 */
+ (NSMutableArray *)getNodesFromDictionary:(NSArray *)array{
    NSMutableArray *nodes = [[NSMutableArray alloc] init];
    for(int i =0;i<array.count;i++){
        NSDictionary *dict = [array objectAtIndex:i];
        //based on the uri we are deciding if its a channel or a normal node.
        if([[dict valueForKey:@"uri"] hasPrefix:@"Channel"]){
            Channel *channel = [[Channel alloc] initWithDictionary:dict error:nil];
            [nodes addObject:channel];
        }else{
            Node *node = [[Node alloc] initWithDictionary:dict error:nil];
            [nodes addObject:node];
        }
    }
    if(nodes.count==0)
        return nil;
    return nodes;
}

/**
 *  This is a function which objectifies SensorConfig.json file under Support files group into Array of SensorConfig Model class.
 *
 *  @return Array of SensorConfig Model class.
 */

+ (NSMutableArray *)getSensorConfiguration{
    NSMutableArray *sensorsList = [[NSMutableArray alloc] init];
    
    NSError *error;
    NSString *strFileContent = [NSString stringWithContentsOfFile:[[NSBundle mainBundle]
                                                                   pathForResource: @"SensorConfig" ofType: @"json"] encoding:NSUTF8StringEncoding error:&error];
    if(!error) {
        sensorsList = [SensorConfig arrayOfModelsFromString:strFileContent error:nil];
    }
    return sensorsList;
}

/**
 *  This function is used to convert DataType enum to String. This is used during serialising JSONs.
 *
 *  @param _dataType
 *
 *  @return NSString version of enum
 */

+(NSString *)stringFromDataType:(DataType)_dataType{
    NSArray *dataTypeArray = [[NSArray alloc] initWithObjects:kDataTypeArray];
    return [dataTypeArray objectAtIndex:_dataType];
}


+(DataType)dataTypeFromString:(NSString *)string{
    NSArray *dataTypeArray = [[NSArray alloc] initWithObjects:kDataTypeArray];
    NSUInteger dataType = [dataTypeArray indexOfObject:string];
    return (DataType)dataType;
}

+(NSString *)stringFromNodeType:(NodeType)_nodeType{
    NSArray *nodeTypeArray = [[NSArray alloc] initWithObjects:kNodeTypeArray];
    return [nodeTypeArray objectAtIndex:_nodeType];
}


+(NodeType)nodeTypeFromString:(NSString *)string{
    NSArray *nodeTypeArray = [[NSArray alloc] initWithObjects:kNodeTypeArray];
    NSUInteger nodeType = [nodeTypeArray indexOfObject:string];
    return (NodeType)nodeType;
}



@end
