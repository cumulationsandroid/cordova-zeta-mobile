//
//  SensorConfig.h
//  zetasense
//
//  Created by Madhu V Swamy on 09/05/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

/**
 *  This is a file used to get sensor configuration in object format.
 */

@interface SensorConfig : JSONModel{
    NSString *name;
    NSString *description;
    int noofchannels;
}
- (void)setName:(NSString *)_name;
- (NSString *)getName;
- (void)setDescription:(NSString *)_description;
- (NSString *)getDescription;
- (void)setNoofchannels:(int)_noofchannels;
- (int)getNoofchannels;

@end
