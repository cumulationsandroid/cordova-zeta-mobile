//
//  ZetaHealthKitManager.m
//  zetasense
//
//  Created by Madhu V Swamy on 14/05/16.
//  Copyright © 2016 Tuts+. All rights reserved.
//

#import "ZetaHealthKitManager.h"

@interface ZetaHealthKitManager()

@property (nonatomic, retain) HKHealthStore *healthStore;
@property(nonatomic,assign) BOOL authorised;

@end

void(^authoStatus)(BOOL authorised,NSError *error);

@implementation ZetaHealthKitManager

+ (ZetaHealthKitManager *)sharedManager {
    static dispatch_once_t pred = 0;
    static ZetaHealthKitManager *instance = nil;
    dispatch_once(&pred, ^{
        instance = [[ZetaHealthKitManager alloc] init];
        instance.healthStore = [[HKHealthStore alloc] init];
        authoStatus=NO;
    });
    return instance;
}

- (void)requestAuthorization:(authorisationStatus)status {

//    @synchronized(self) {
    
    authoStatus = status;
    if ([HKHealthStore isHealthDataAvailable] == NO) {
        // If our device doesn't support HealthKit -> return.
        return;
    }
    
    NSArray *readTypes = @[[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate],[HKObjectType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned]];
    
    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
    [debugDictionary setValue:[NSString stringWithFormat:@"HEALTHKIT Request Autho %@",[self.healthStore earliestPermittedSampleDate]] forKey:@"DEBUG"];
    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    
    [self.healthStore requestAuthorizationToShareTypes:nil
                                             readTypes:[NSSet setWithArray:readTypes] completion:^(BOOL success, NSError * _Nullable error){
                                                 
                                                 if(error!=nil){
                                                     NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
                                                     [debugDictionary setValue:@"HEALTHKIT" forKey:@"DEBUG"];
                                                     [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
                                                    self.authorised=NO;
                                                    authoStatus(NO,error);
                                                 }else{
//                                                     [self readHeartRate];
//                                                     [self readActiveEnergy];
                                                     self.authorised=YES;
                                                      authoStatus(YES,nil);
                                                 }
                                             }];
    
//    if(error!=nil){
//        Node *node = [[Node alloc] init];
//        [node setName:@"requestHealthKit"];
//        [node setDescription:@"This is used to authorize healthkit"];
//        [node setUri:[[NSUUID UUID] UUIDString]];
//        [node setType:METHOD];
//    }else{
//        [self readHeartRate];
//        [self readActiveEnergy];
//    }
//}
}

-(BOOL)getAuthorisationStatus{
    return self.authorised;
}

-(HKHealthStore *)getHealthKitStore{
    return self.healthStore;
}

- (void)readHeartRate{
    
    HKSampleType *sampleType = [HKSampleType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
    NSSortDescriptor *lastDateDescriptor = [[NSSortDescriptor alloc]
                                            initWithKey:HKSampleSortIdentifierEndDate ascending:NO selector:@selector(localizedStandardCompare:)];
    NSArray *sortDescriptors = @[lastDateDescriptor];
    
    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:sampleType predicate:nil limit:1 sortDescriptors:sortDescriptors resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error){
        if (!results) {
            NSLog(@"An error occured fetching the user's heartrate. The error was: %@.", error);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            for (HKQuantitySample *sample in results) {
                HKQuantity *quantity =  sample.quantity;
                HKUnit *bpm = [HKUnit unitFromString:@"count/min"];
//                NSLog(@"Heart rate is %f at %@",[quantity doubleValueForUnit:bpm],sample.endDate);

                NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
                [debugDictionary setValue:[NSString stringWithFormat:[NSString stringWithFormat:[NSString stringWithFormat:@"Heart rate is %f at %@",[quantity doubleValueForUnit:bpm],sample.endDate],[sample.quantity doubleValueForUnit:bpm],sample.endDate],[error description]] forKey:@"DEBUG"];
                [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];

            }
        });
    }];
    [self.healthStore executeQuery:query];
}

- (void)readActiveEnergy{
    HKSampleType *sampleType = [HKSampleType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
    NSSortDescriptor *lastDateDescriptor = [[NSSortDescriptor alloc]
                                            initWithKey:HKSampleSortIdentifierEndDate ascending:NO selector:@selector(localizedStandardCompare:)];
    NSArray *sortDescriptors = @[lastDateDescriptor];
    
    HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:sampleType predicate:nil limit:1 sortDescriptors:sortDescriptors resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error){
        if (!results) {
            NSLog(@"An error occured fetching the user's active Energy. The error was: %@.", error);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            for (HKQuantitySample *sample in results) {
                HKUnit *kCal = [HKUnit unitFromString:@"kcal"];
//                NSLog(@"Active Energy is %f kCal at %@",[sample.quantity doubleValueForUnit:kCal],sample.endDate);
                
                NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
                [debugDictionary setValue:[NSString stringWithFormat:[NSString stringWithFormat:@"Active Energy is %f kCal at %@",[sample.quantity doubleValueForUnit:kCal],sample.endDate],[error description]] forKey:@"DEBUG"];
                [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
            }
        });
    }];
    [self.healthStore executeQuery:query];
}

@end
