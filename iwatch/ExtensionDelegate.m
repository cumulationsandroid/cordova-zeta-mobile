//
//  ExtensionDelegate.m
//  zetasensewear Extension
//
//  Created by Madhu V Swamy on 29/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "ExtensionDelegate.h"

//#import "TestCases.h"

@implementation ExtensionDelegate

- (void)applicationDidFinishLaunching {
    // Perform any final initialization of your application.
    NSLog(@"Launched");
    
    self.motionManager = [[CMMotionManager alloc] init];
    
    Node *sensorAgent = [self getPopulatedSensorAgent];
    iSensorAgent = [[SensorAgent alloc] initWithDetails:sensorAgent];
    
    
    
//    if ([WCSession isSupported]) {
//        self.session = [WCSession defaultSession];
//        self.session.delegate = self;
//        [self.session activateSession];
//        NSLog(@"Got watch session...");
//        
//        NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
//        [debugDictionary setValue:@"GOT SESSION" forKey:@"DEBUG"];
//        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
//    }
    //    @try {
    //        Node *sensorAgent = [self getPopulatedSensorAgent];
    ////        [self testSerialise];
    ////        [self testObjectify];
    //        SensorAgent *sensorAgent1 = [[SensorAgent alloc] initWithDetails:sensorAgent];
    //        [sensorAgent1 start];
    //
    //    }
    //    @catch (NSException *exception) {
    //        NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
    //        [debugDictionary setValue:[exception description] forKey:@"EXCEPTION"];
    //        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    //    }
    //    @finally {
    //
    //    }
}

- (void)applicationDidBecomeActive {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillResignActive {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, etc.
    [self.motionManager stopAccelerometerUpdates];
}

- (Node *)getPopulatedSensorAgent{
    Node *node = [self initialiseSensorAgentNode];
    ExtensionDelegate *watchDelegate = (ExtensionDelegate *)[WKExtension sharedExtension].delegate;
    [watchDelegate debuLog:@"SENSORAGENT" value:[node toJSONString]];
    return node;
}

- (Node *)initialiseSensorAgentNode{
    @try {
        Node *node = [[Node alloc] init];
        [node setName:@"SensorAgent"];
        [node setType:OBJECT];
        [node setUri:[NSString stringWithFormat:@"%@%@",NSStringFromClass([SensorAgent class]),[self getCommonLiteral]]];
        [node setDescription:@"SensorAgent instance"];
        [node setGuid:[NSUUID UUID]];
        Data *data = [[Data alloc] init];
        [node setData:data];
        [node setNodes:[self getMethodNodes]];
        return node;
    }
    @catch (NSException *exception) {
        NSDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
        [debugDictionary setValue:[exception description] forKey:@"DEBUG"];
        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    }
    @finally {
        
    }
    return nil;
}

- (NSString *)getCommonLiteral{
    NSString *literal = [NSString stringWithFormat:@"%@%@.%@%@.%@",kSchemeIdentifier,mDeviceId,kManufacturer,kProduct,NSStringFromClass([SensorAgent class])];
    return literal;
}

- (NSMutableArray *)getMethodNodes{
    
    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
    [debugDictionary setValue:@"getMethodNodes" forKey:@"DEBUG"];
    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    Node *setConfigurationNode = [[Node alloc] init];
    [setConfigurationNode setName:@"setConfiguration"];
    [setConfigurationNode setDescription:@"SensorAgent setConfiguration"];
    [setConfigurationNode setGuid:[NSUUID UUID]];
    [setConfigurationNode setUri:[NSString stringWithFormat:@"Method%@.setConfiguration",[self getCommonLiteral]]];
    [setConfigurationNode setType:METHOD];
    [setConfigurationNode setData:[[Data alloc] init]];
    
    NSMutableArray *setConfigurationParamNodes = [[NSMutableArray alloc] init];
    Node *paramSetConfiguration = [[Node alloc] init];
    [paramSetConfiguration setName:@"Node"];
    [paramSetConfiguration setDescription:@"setConfiguration param Node"];
    [paramSetConfiguration setGuid:[NSUUID UUID]];
    [paramSetConfiguration setUri:[NSString stringWithFormat:@"Parameter%@.setConfiguration%@",[self getCommonLiteral],NSStringFromClass([Node class])]];
    [paramSetConfiguration setType:PARAMETER];
    [paramSetConfiguration setData:[[Data alloc] init]];
    [setConfigurationParamNodes addObject:paramSetConfiguration];
    [setConfigurationNode setNodes:setConfigurationParamNodes];
    [result addObject:setConfigurationNode];
    
    Node *getConfigurationNode = [[Node alloc] init];
    [getConfigurationNode setName:@"getConfigurationNode"];
    [getConfigurationNode setDescription:@"SensorAgent getConfiguration"];
    [getConfigurationNode setGuid:[NSUUID UUID]];
    [getConfigurationNode setUri:[NSString stringWithFormat:@"Method%@.getConfiguration",[self getCommonLiteral]]];
    [getConfigurationNode setType:METHOD];
    [getConfigurationNode setData:[[Data alloc] init]];
    [result addObject:getConfigurationNode];
    
    Node *startNode = [[Node alloc] init];
    [startNode setName:@"start"];
    [startNode setDescription:@"SensorAgent start"];
    [startNode setGuid:[NSUUID UUID]];
    [startNode setUri:[NSString stringWithFormat:@"Method%@.start",[self getCommonLiteral]]];
    [startNode setType:METHOD];
    [startNode setData:[[Data alloc] init]];
    [result addObject:startNode];
    
    Node *stopNode = [[Node alloc] init];
    [stopNode setName:@"stop"];
    [stopNode setDescription:@"SensorAgent stop"];
    [stopNode setGuid:[NSUUID UUID]];
    [stopNode setUri:[NSString stringWithFormat:@"Method%@.stop",[self getCommonLiteral]]];
    [stopNode setType:METHOD];
    [stopNode setData:[[Data alloc] init]];
    [result addObject:stopNode];
    
    Node *heartBeat = [[Node alloc] init];
    [heartBeat setName:@"heartBeat"];
    [heartBeat setDescription:@"SensorAgent heartBeat"];
    [heartBeat setGuid:[NSUUID UUID]];
    [heartBeat setUri:[NSString stringWithFormat:@"Method%@.heartBeat",[self getCommonLiteral]]];
    [heartBeat setType:METHOD];
    [heartBeat setData:[[Data alloc] init]];
    
    NSMutableArray *setHeartBeatParamNodes = [[NSMutableArray alloc] init];
    Node *paramHeartBeat = [[Node alloc] init];
    [paramHeartBeat setName:@"Node"];
    [paramHeartBeat setDescription:@"heartBeat param Node"];
    [paramHeartBeat setGuid:[NSUUID UUID]];
    [paramHeartBeat setUri:[NSString stringWithFormat:@"Parameter%@.heartBeat.%@",[self getCommonLiteral],NSStringFromClass([Node class])]];
    [paramHeartBeat setType:PARAMETER];
    [paramHeartBeat setData:[[Data alloc] init]];
    [setHeartBeatParamNodes addObject:paramHeartBeat];
    [heartBeat setNodes:setHeartBeatParamNodes];
    [result addObject:heartBeat];
    
    Node *getDeviceCapabilities = [[Node alloc] init];
    [getDeviceCapabilities setName:@"getDeviceCapabilities"];
    [getDeviceCapabilities setDescription:@"SensorAgent getDeviceCapabilities"];
    [getDeviceCapabilities setGuid:[NSUUID UUID]];
    [getDeviceCapabilities setUri:[NSString stringWithFormat:@"Method%@.getDeviceCapabilities",[self getCommonLiteral]]];
    [getDeviceCapabilities setType:METHOD];
    [getDeviceCapabilities setData:[[Data alloc] init]];
    [result addObject:getDeviceCapabilities];
    
    Node *getLoggerLevel = [[Node alloc] init];
    [getLoggerLevel setName:@"getLoggerLevel"];
    [getLoggerLevel setDescription:@"SensorAgent getLoggerLevel"];
    [getLoggerLevel setGuid:[NSUUID UUID]];
    [getLoggerLevel setUri:[NSString stringWithFormat:@"Method%@.getLoggerLevel",[self getCommonLiteral]]];
    [getLoggerLevel setType:METHOD];
    [getLoggerLevel setData:[[Data alloc] init]];
    [result addObject:getLoggerLevel];
    
    Node *setLoggerLevel = [[Node alloc] init];
    [setLoggerLevel setName:@"setLoggerLevel"];
    [setLoggerLevel setDescription:@"SensorAgent setLoggerLevel"];
    [setLoggerLevel setGuid:[NSUUID UUID]];
    [setLoggerLevel setUri:[NSString stringWithFormat:@"Method%@.setLoggerLevel",[self getCommonLiteral]]];
    [setLoggerLevel setType:METHOD];
    [setLoggerLevel setData:[[Data alloc] init]];
    [result addObject:setLoggerLevel];
    
    Node *notify = [[Node alloc] init];
    [notify setName:@"notify"];
    [notify setDescription:@"SensorAgent notify"];
    [notify setGuid:[NSUUID UUID]];
    [notify setUri:[NSString stringWithFormat:@"Method%@.notify",[self getCommonLiteral]]];
    [notify setType:METHOD];
    [notify setData:[[Data alloc] init]];
    [result addObject:notify];
    
    Node *getLoggerMessage = [[Node alloc] init];
    [getLoggerMessage setName:@"getLoggerMessage"];
    [getLoggerMessage setDescription:@"SensorAgent getLoggerMessage"];
    [getLoggerMessage setGuid:[NSUUID UUID]];
    [getLoggerMessage setUri:[NSString stringWithFormat:@"Method%@.getLoggerMessage",[self getCommonLiteral]]];
    [getLoggerMessage setType:METHOD];
    [getLoggerMessage setData:[[Data alloc] init]];
    [result addObject:getLoggerMessage];
    
    Node *notificationNode = [[Node alloc] init];
    [notificationNode setName:@"Notification"];
    [notificationNode setDescription:@"Notification Message"];
    [notificationNode setGuid:[NSUUID UUID]];
    [notificationNode setUri:[NSString stringWithFormat:@"Notification%@.Notification",[self getCommonLiteral]]];
    [notificationNode setType:OBJECT];
    Data *data = [[Data alloc] init];
    [data setDataType:ENUMERATION];
    [notificationNode setData:data];
    [result addObject:notificationNode];
    
    return result;
}

//- (void)session:(WCSession *)session
//didReceiveApplicationContext:(NSDictionary<NSString *,
//                              id> *)applicationContext
//{
//    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
//    [debugDictionary setValue:[applicationContext valueForKey:@"COMMAND"] forKey:@"FROM PHONE DELEGATE"];
//    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
//    //    [self debuLog:@"FROM PHONE DELEGATE" value:[applicationContext valueForKey:@"COMMAND"]];
//    @try {
//        Node *sensorAgent = [self getPopulatedSensorAgent];
//        //        [self testSerialise];
//        //        [self testObjectify];
//        SensorAgent *sensorAgent1 = [[SensorAgent alloc] initWithDetails:sensorAgent];
//        [sensorAgent1 dataReadyExternal:[applicationContext valueForKey:@"COMMAND"]];
//        
//    }
//    @catch (NSException *exception) {
//        NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
//        [debugDictionary setValue:[exception description] forKey:@"EXCEPTION"];
//        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
//    }
//    @finally {
//        
//    }
//}

- (void)debuLog:(NSString *)key value:(NSString *)value{
    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
    [debugDictionary setValue:value forKey:key];
    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
}



- (void)testObjectify{
    [self debuLog:@"OBJECTIFY" value:@"testObjectify"];
    //NSString *jsonString = @"{\"description\":\"SensorAgent instance\",\"type\":0,\"guid\":\"A3A6A164-CF4D-42C9-BC37-4F46BB4710E0\",\"name\":\"SensorAgent\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"SensorAgent setConfiguration\",\"type\":2,\"guid\":\"3CAD0E19-09C3-483E-A684-FDE261C7D9D1\",\"name\":\"setConfiguration\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"setConfiguration param Node\",\"type\":3,\"guid\":\"E37B8F2F-AEC2-46E3-B7D1-E4D3533B8435\",\"name\":\"Node\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Parameter:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.setConfigurationNode\"}],\"uri\":\"Method:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.setConfiguration\"},{\"description\":\"SensorAgent getConfiguration\",\"type\":2,\"guid\":\"64753306-B688-4102-A6A2-9396E5D0F551\",\"name\":\"getConfigurationNode\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.getConfiguration\"},{\"description\":\"SensorAgent start\",\"type\":2,\"guid\":\"B74FACF3-88E3-43C7-9A61-2EAC95EB7114\",\"name\":\"start\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.start\"},{\"description\":\"SensorAgent stop\",\"type\":2,\"guid\":\"6F2E865E-59E7-47CA-BA21-3A1076B34912\",\"name\":\"stop\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.stop\"},{\"description\":\"SensorAgent heartBeat\",\"type\":2,\"guid\":\"ADDDF603-91A3-4BDE-930E-E64C4C38829A\",\"name\":\"heartBeat\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"heartBeat param Node\",\"type\":3,\"guid\":\"CF01E48F-DD8D-42D2-B292-CBAC6E86D0BE\",\"name\":\"Node\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Parameter:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.heartBeat.Node\"}],\"uri\":\"Method:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.heartBeat\"},{\"description\":\"SensorAgent getDeviceCapabilities\",\"type\":2,\"guid\":\"6B7D3440-EB86-42C8-8095-BDB23D1A549A\",\"name\":\"getDeviceCapabilities\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.getDeviceCapabilities\"},{\"description\":\"SensorAgent getLoggerLevel\",\"type\":2,\"guid\":\"7ACFFA4E-B42E-49E8-A255-69D9176F6237\",\"name\":\"getLoggerLevel\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.getLoggerLevel\"},{\"description\":\"SensorAgent setLoggerLevel\",\"type\":2,\"guid\":\"3165B173-F292-4E86-9F3A-67AC6E717017\",\"name\":\"setLoggerLevel\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.setLoggerLevel\"},{\"description\":\"SensorAgent notify\",\"type\":2,\"guid\":\"CC5099D5-018E-452E-9BB1-768FA209FFEA\",\"name\":\"notify\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.notify\"},{\"description\":\"SensorAgent getLoggerMessage\",\"type\":2,\"guid\":\"F150501C-F856-46F5-B33F-4B4C077E9DF8\",\"name\":\"getLoggerMessage\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Method:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.getLoggerMessage\"},{\"description\":\"Notification Message\",\"type\":0,\"guid\":\"A79DA015-3688-4A48-8B3C-9A1A7533F75A\",\"name\":\"Notification\",\"data\":{\"dataType\":6,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[],\"uri\":\"Notification:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent.Notification\"}],\"uri\":\"SensorAgent:\\/\\/TEMPORARYPENDING.AppleiWatch.SensorAgent\"}";
    
    //    NSString *jsonString = @"{\"description\":\"This is Apple iWatch\",\"type\":0,\"guid\":\"85654D63-2375-4033-99D8-8E1F2126E786\",\"name\":\"Apple iWatch\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"Used to acquire device's accelerometer data\",\"type\":0,\"guid\":\"25F65EC8-9028-49A3-8173-3F9D5B1EFB91\",\"name\":\"Accelerometer\",\"data\":{\"dataType\":0,\"value\":\"<>\",\"timeStamp\":0},\"nodes\":[{\"description\":\"Used to acquire device's accelerometer data . 1\",\"isEnabled\":true,\"samplingDuration_ms\":5,\"guid\":\"27BC83C3-6C5B-47AC-B56C-30D380D5892E\",\"name\":\"Accelerometer Channel 1\",\"type\":0,\"data\":{\"dataType\":7,\"value\":\"<2d302e30 30353636 31>\",\"timeStamp\":1460474112},\"nodes\":[],\"uri\":\"Channel:\\/\\/TEMPORARY.AppleiWatch.SensorAgent\"},{\"description\":\"Used to acquire device's accelerometer data . 2\",\"isEnabled\":true,\"samplingDuration_ms\":5,\"guid\":\"6E9F365A-7A7D-4A5B-BE73-A57F0F1416FB\",\"name\":\"Accelerometer Channel 2\",\"type\":0,\"data\":{\"dataType\":7,\"value\":\"<302e3031 30303235>\",\"timeStamp\":1460474112},\"nodes\":[],\"uri\":\"Channel:\\/\\/TEMPORARY.AppleiWatch.SensorAgent\"},{\"description\":\"Used to acquire device's accelerometer data . 3\",\"isEnabled\":true,\"samplingDuration_ms\":5,\"guid\":\"B8FCE93D-CEC6-4CDD-ADA6-EAC3B0DD1BFA\",\"name\":\"Accelerometer Channel 3\",\"type\":0,\"data\":{\"dataType\":0,\"value\":\"<2d312e30 30363337 38>\",\"timeStamp\":1460474113},\"nodes\":[],\"uri\":\"Channel:\\/\\/TEMPORARY.AppleiWatch.SensorAgent\"}],\"uri\":\"Sensor:\\/\\/Accelerometer\"}],\"uri\":\"Device:\\/\\/TEMPORARY.AppleiWatch.SensorAgent\"}";
    //    SensorAgent *data = [[SensorAgent alloc] initWithString:jsonString error:nil];
    //    Data *dataDetails = [data getData];
    //    [self debuLog:@"OBJECTIFY" value:[NSString stringWithFormat:@"%i %ld",[[data getNodes] count],(long)[dataDetails getDataType]]];
    
    //    Device *device = [[Device alloc] initWithString:jsonString error:nil];
    //    Node *tempNode = [[device getNodes] objectAtIndex:0];
    //    [self debuLog:@"OBJECTIFY" value:[NSString stringWithFormat:@"%@ %@",[device getDescription],[tempNode getGuid]]];
    
    
    NSString *jsonString = @"{\"description\":\"Used to acquire device's accelerometer data . 3\",\"isEnabled\":true,\"samplingDuration_ms\":5,\"guid\":\"2E3712D1-E468-4A2D-B9FD-487A8F6E6D08\",\"name\":\"Accelerometer Channel 3\",\"type\":0,\"data\":{\"dataType\":0,\"value\":\"<2d312e30 30373930 34>\",\"timeStamp\":1460474589},\"nodes\":[],\"uri\":\"Channel:\\/\\/TEMPORARY.AppleiWatch.SensorAgent\"}";
    Channel *channel = [[Channel alloc] initWithString:jsonString error:nil];
    [self debuLog:@"OBJECTIFY" value:[NSString stringWithFormat:@"%@ %@",[channel getDescription],[channel getUri]]];
}

@end
