//
//  DataAcquisition.m
//  zetasense
//
//  Created by Madhu V Swamy on 03/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "DataAcquisition.h"
@import WatchConnectivity;


@implementation DataAcquisition
@synthesize energyChannelTimer;
-(id)initWithDetails:(INotify *)iNotify sensorAgent:(IDataReadyInternal *)sensorAgent{
    self = [super init];
    if (self) {
        mDeviceId = @"TEMPORARY";
        iDataReadyInternal = sensorAgent;
    }
    [self populatedDevice_without_sampling];
    return self;
}

- (NSString *)getCommonLiteral{
    NSString *literal = [NSString stringWithFormat:@"%@%@.%@%@.%@",kSchemeIdentifier,mDeviceId,kManufacturer,kProduct,NSStringFromClass([SensorAgent class])];
    return literal;
}

- (Device *)initialiseDevice{
    Device *device = [[Device alloc] init];
    [device setDescription:[NSString stringWithFormat:@"This is %@ %@",kManufacturer,kProduct]];
    [device setData:[[Data alloc] init]];
    [device setUri:[NSString stringWithFormat:@"%@%@",NSStringFromClass([Device class]),[self getCommonLiteral]]];
    [device setType:OBJECT];
    [device setGuid:[NSUUID UUID]];
    [device setName:[NSString stringWithFormat:@"%@ %@",kManufacturer,kProduct]];
    return device;
}

- (void)populatedDevice_without_sampling{
    mDevice = [self initialiseDevice];
    NSMutableArray *sensorConfigList = [[NSMutableArray alloc] init];
    NSMutableArray *sensorList = [[NSMutableArray alloc] init];
    sensorConfigList = [Utils getSensorConfiguration];
    for (int i=0; i<sensorConfigList.count; i++) {
        SensorConfig *sensorConfig = [sensorConfigList objectAtIndex:i];
        Sensor *mySensor = [[Sensor alloc] init];
        [mySensor setName:[sensorConfig getName]];
        [mySensor setDescription:[sensorConfig getDescription]];
        [mySensor setData:[[Data alloc] init]];
        [mySensor setGuid:[NSUUID UUID]];
        [mySensor setType:OBJECT];
        [mySensor setUri:[NSString stringWithFormat:@"%@%@%@",NSStringFromClass([Sensor class]),kSchemeIdentifier,[sensorConfig getName]]];
        
        NSMutableArray *channelList = [[NSMutableArray alloc] init];
        for (int j=0; j<[sensorConfig getNoofchannels]; j++) {
            Channel *xChannel = [[Channel alloc] init];
            [xChannel setName:[NSString stringWithFormat:@"%@ Channel %i",[mySensor getName],[channelList count]+1]];
            [xChannel setDescription:[NSString stringWithFormat:@"%@ . %i",[mySensor getDescription],[channelList count]+1]];
            [xChannel setUri:[NSString stringWithFormat:@"%@%@",NSStringFromClass([Channel class]),[self getCommonLiteral]]];
            [xChannel setGuid:[NSUUID UUID]];
            [xChannel setType:OBJECT];
            [xChannel setSamplingDuration_ms:DEFAULT_SAMPLING_DURATION];
            [xChannel setIsEnabled:DEFAULT_ENABLED_STATE];
            Data *xData = [[Data alloc] init];
            [xData setDataType:FLOAT];
            [xChannel setData:xData];
            [channelList addObject:xChannel];
        }
        [mySensor setNodes:channelList];
        [sensorList addObject:mySensor];
    }
    [mDevice setNodes:sensorList];
}

//- (Sensor *)populateAcceleromterSensor{
//    Sensor *mySensor = [[Sensor alloc] init];
//    [mySensor setName:kAccelerometer];
//    [mySensor setDescription:kAccelerometerDescription];
//    [mySensor setData:[[Data alloc] init]];
//    [mySensor setGuid:[NSUUID UUID]];
//    [mySensor setType:OBJECT];
//    [mySensor setUri:[NSString stringWithFormat:@"%@%@%@",NSStringFromClass([Sensor class]),kSchemeIdentifier,kAccelerometer]];
//    
//    NSMutableArray *channelList = [[NSMutableArray alloc] init];
//    Channel *xChannel = [[Channel alloc] init];
//    [xChannel setName:[NSString stringWithFormat:@"%@ Channel %i",[mySensor getName],[channelList count]+1]];
//    [xChannel setDescription:[NSString stringWithFormat:@"%@ . %i",[mySensor getDescription],[channelList count]+1]];
//    [xChannel setUri:[NSString stringWithFormat:@"%@%@",NSStringFromClass([Channel class]),[self getCommonLiteral]]];
//    [xChannel setGuid:[NSUUID UUID]];
//    [xChannel setType:OBJECT];
//    [xChannel setSamplingDuration_ms:DEFAULT_SAMPLING_DURATION];
//    [xChannel setIsEnabled:DEFAULT_ENABLED_STATE];
//    Data *xData = [[Data alloc] init];
//    [xData setDataType:FLOAT];
//    [xChannel setData:xData];
//    [channelList addObject:xChannel];
//    
//    Channel *yChannel = [[Channel alloc] init];
//    [yChannel setName:[NSString stringWithFormat:@"%@ Channel %i",[mySensor getName],[channelList count]+1]];
//    [yChannel setDescription:[NSString stringWithFormat:@"%@ . %i",[mySensor getDescription],[channelList count]+1]];
//    [yChannel setUri:[NSString stringWithFormat:@"%@%@",NSStringFromClass([Channel class]),[self getCommonLiteral]]];
//    [yChannel setGuid:[NSUUID UUID]];
//    [yChannel setType:OBJECT];
//    [yChannel setSamplingDuration_ms:DEFAULT_SAMPLING_DURATION];
//    [yChannel setIsEnabled:DEFAULT_ENABLED_STATE];
//    Data *yData = [[Data alloc] init];
//    [yData setDataType:FLOAT];
//    [yChannel setData:yData];
//    [channelList addObject:yChannel];
//    
//    Channel *zChannel = [[Channel alloc] init];
//    [zChannel setName:[NSString stringWithFormat:@"%@ Channel %i",[mySensor getName],[channelList count]+1]];
//    [zChannel setDescription:[NSString stringWithFormat:@"%@ . %i",[mySensor getDescription],[channelList count]+1]];
//    [zChannel setUri:[NSString stringWithFormat:@"%@%@",NSStringFromClass([Channel class]),[self getCommonLiteral]]];
//    [zChannel setGuid:[NSUUID UUID]];
//    [zChannel setType:OBJECT];
//    [zChannel setSamplingDuration_ms:DEFAULT_SAMPLING_DURATION];
//    [zChannel setIsEnabled:DEFAULT_ENABLED_STATE];
//    Data *zData = [[Data alloc] init];
//    [yData setDataType:FLOAT];
//    [zChannel setData:zData];
//    [channelList addObject:zChannel];
//    
//    [mySensor setNodes:channelList];
//    return mySensor;
//}

//- (Sensor *)populatePedometerSensor{
//    Sensor *mySensor = [[Sensor alloc] init];
//    [mySensor setName:kPedometer];
//    [mySensor setDescription:kPedometerDescription];
//    [mySensor setData:[[Data alloc] init]];
//    [mySensor setGuid:[NSUUID UUID]];
//    [mySensor setType:OBJECT];
//    [mySensor setUri:[NSString stringWithFormat:@"%@%@%@",NSStringFromClass([Sensor class]),kSchemeIdentifier,kPedometer]];
//    
//    NSMutableArray *channelList = [[NSMutableArray alloc] init];
//    Channel *xChannel = [[Channel alloc] init];
//    [xChannel setName:[NSString stringWithFormat:@"%@ Channel %i",[mySensor getName],[channelList count]+1]];
//    [xChannel setDescription:[NSString stringWithFormat:@"%@ . %i",[mySensor getDescription],[channelList count]+1]];
//    [xChannel setUri:[NSString stringWithFormat:@"%@%@",NSStringFromClass([Channel class]),[self getCommonLiteral]]];
//    [xChannel setGuid:[NSUUID UUID]];
//    [xChannel setType:OBJECT];
//    [xChannel setSamplingDuration_ms:DEFAULT_SAMPLING_DURATION];
//    [xChannel setIsEnabled:DEFAULT_ENABLED_STATE];
//    Data *xData = [[Data alloc] init];
//    [xData setDataType:FLOAT];
//    [xChannel setData:xData];
//    [channelList addObject:xChannel];
//    
//    [mySensor setNodes:channelList];
//    return mySensor;
//}

- (void)session:(WCSession *)session
didReceiveApplicationContext:(NSDictionary<NSString *,
                              id> *)applicationContext
{
    NSLog(@"Watch %@",applicationContext);
}

-(void)saveLocally{
    
    if ([WCSession isSupported]) {
        ExtensionDelegate *delegate =  (ExtensionDelegate *)[WKExtension sharedExtension].delegate;
        delegate.session = [WCSession defaultSession];
        delegate.session.delegate = self;
        [delegate.session activateSession];
    }
    
    NSDictionary *sensorAgentDictonary = [[Node arrayOfDictionariesFromModels:[NSMutableArray arrayWithObjects:mDevice, nil]] objectAtIndex:0];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sensorAgentDictonary
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
    [debugDictionary setValue:jsonString forKey:@"JSON"];
    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
}


-(void)start
{
    int sensorCount = [[[[mDevice getNodes] objectAtIndex:0] getNodes] count];
    ExtensionDelegate *watchDelegate = (ExtensionDelegate *)[WKExtension sharedExtension].delegate;
    
    [watchDelegate debuLog:[NSString stringWithFormat:@"DATA ACQ START %i %@",sensorCount,watchDelegate]  value:@"NEW_METHOD"];
    
    for(int i=0;i<[mDevice getNodes].count;i++){
        
        Sensor *sensor = [[mDevice getNodes] objectAtIndex:i];
        NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
        [debugDictionary setValue:[NSString stringWithFormat:@"SENSOR NAME %@ %@",[sensor getName],watchDelegate.motionManager] forKey:@"DEBUG"];
        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
        
        if([[sensor getName] isEqualToString:kAccelerometer]){
            NSMutableArray *channelsList = [sensor getNodes];
            watchDelegate.motionManager.accelerometerUpdateInterval = 1;
            
            dispatch_async(dispatch_get_main_queue(), ^{
            [watchDelegate.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
                
                @try {
                    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
                    [debugDictionary setValue:[NSString stringWithFormat:@"Data Acquistion accelerometer updates %i %i",channelsList.count,[[channelsList objectAtIndex:0] getSamplingDuration_ms]] forKey:@"DEBUG"];
                    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
                    
                    Channel *xChannel = [channelsList objectAtIndex:0];
                    if([[NSDate date] timeIntervalSince1970] >= [[xChannel getData] getTimeStamp] + [xChannel getSamplingDuration_ms]){
                        double xValue = accelerometerData.acceleration.x;
                        [xChannel setData:[self getRefreshedData:[xChannel getData] timeStamp:[[NSDate date] timeIntervalSince1970] values:[[NSString stringWithFormat:@"%f",xValue] dataUsingEncoding:NSUTF8StringEncoding]]];
                        [iDataReadyInternal dataReadyInternal:xChannel];
                    }
                    
//                    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
                    
                    Channel *yChannel = [channelsList objectAtIndex:1];
                    if([[NSDate date] timeIntervalSince1970] >= [[yChannel getData] getTimeStamp] + [yChannel getSamplingDuration_ms]){
                        double yValue = accelerometerData.acceleration.y;
                        [yChannel setData:[self getRefreshedData:[yChannel getData] timeStamp:[[NSDate date] timeIntervalSince1970] values:[[NSString stringWithFormat:@"%f",yValue] dataUsingEncoding:NSUTF8StringEncoding]]];
                        [iDataReadyInternal dataReadyInternal:yChannel];
                    }
                    
//                    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
                    
                    Channel *zChannel = [channelsList objectAtIndex:2];
                    if([[NSDate date] timeIntervalSince1970] >= [[zChannel getData] getTimeStamp] + [zChannel getSamplingDuration_ms]){
                        double zValue = accelerometerData.acceleration.z;
                        [zChannel setData:[self getRefreshedData:[zChannel getData] timeStamp:[[NSDate date] timeIntervalSince1970] values:[[NSString stringWithFormat:@"%f",zValue] dataUsingEncoding:NSUTF8StringEncoding]]];
                        [iDataReadyInternal dataReadyInternal:zChannel];
                    }
                    
//                    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
                    
//                    NSDictionary *applicationDict = [[NSMutableDictionary alloc] init];
//                    Channel *channel = [[[[mDevice getNodes] objectAtIndex:0] getNodes] objectAtIndex:0];
//                    [applicationDict setValue:[NSString stringWithFormat:@"%@",[channel getData]] forKey:@"X"];
//                    channel = [[[[mDevice getNodes] objectAtIndex:0] getNodes] objectAtIndex:1];
//                    [applicationDict setValue:[NSString stringWithFormat:@"%@",[channel getData]] forKey:@"Y"];
//                    channel = [[[[mDevice getNodes] objectAtIndex:0] getNodes] objectAtIndex:2];
//                    [applicationDict setValue:[NSString stringWithFormat:@"%@ %@",[channel getData],                    [mDevice toJSONString]] forKey:@"Y"];
                    
//                    [watchDelegate debuLog:@"JSON-channel-X" value:[xChannel toJSONString]];
                    
//                    [watchDelegate debuLog:@"JSON-channel-Y" value:[yChannel toJSONString]];
                    
//                    [watchDelegate debuLog:@"JSON-channel-Z" value:[zChannel toJSONString]];
                    
//                    [watchDelegate debuLog:@"DEVICE" value:[mDevice toJSONString]];
                }
                @catch (NSException *exception) {
                    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
                    [debugDictionary setValue:[exception description] forKey:@"EXCEPTION"];
                    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
                }
                @finally {
                    
                }
                
            }];
            });
        }
        
        if([[sensor getName] isEqualToString:kPedometer]){
            NSMutableArray *channelsList = [sensor getNodes];
            
            watchDelegate.pedometer = [[CMPedometer alloc]init];
            [watchDelegate.pedometer startPedometerUpdatesFromDate:[NSDate date] withHandler:^(CMPedometerData *  pedometerData, NSError * error) {
                
                Channel *stepsChannel = [channelsList objectAtIndex:0];
                
                if([[NSDate date] timeIntervalSince1970] >= [[stepsChannel getData] getTimeStamp] + ([stepsChannel getSamplingDuration_ms]*1000)){
                    double stepsValue = [pedometerData.numberOfSteps doubleValue];
                    [stepsChannel setData:[self getRefreshedData:[stepsChannel getData] timeStamp:[[NSDate date] timeIntervalSince1970] values:[[NSString stringWithFormat:@"%f",stepsValue] dataUsingEncoding:NSUTF8StringEncoding]]];
                    [iDataReadyInternal dataReadyInternal:stepsChannel];
                }
    
                Channel *distanceChannel = [channelsList objectAtIndex:1];
                if([[NSDate date] timeIntervalSince1970] >= [[distanceChannel getData] getTimeStamp] + [distanceChannel getSamplingDuration_ms]){
                    double distanceValue = [pedometerData.distance doubleValue];
                    [distanceChannel setData:[self getRefreshedData:[distanceChannel getData] timeStamp:[[NSDate date] timeIntervalSince1970] values:[[NSString stringWithFormat:@"%f",distanceValue] dataUsingEncoding:NSUTF8StringEncoding]]];
                    [iDataReadyInternal dataReadyInternal:distanceChannel];
                }
                
                Channel *paceChannel = [channelsList objectAtIndex:2];
                if([[NSDate date] timeIntervalSince1970] >= [[paceChannel getData] getTimeStamp] + ([paceChannel getSamplingDuration_ms])){
                    double paceValue = [pedometerData.currentPace doubleValue];
                    [paceChannel setData:[self getRefreshedData:[paceChannel getData] timeStamp:[[NSDate date] timeIntervalSince1970] values:[[NSString stringWithFormat:@"%f",paceValue] dataUsingEncoding:NSUTF8StringEncoding]]];
                    [iDataReadyInternal dataReadyInternal:paceChannel];
                }
                
                Channel *cadenceChannel = [channelsList objectAtIndex:3];
                if([[NSDate date] timeIntervalSince1970] >= [[cadenceChannel getData] getTimeStamp] + ([cadenceChannel getSamplingDuration_ms])){
                    double cadenceValue = [pedometerData.currentCadence doubleValue];
                    [cadenceChannel setData:[self getRefreshedData:[cadenceChannel getData] timeStamp:[[NSDate date] timeIntervalSince1970] values:[[NSString stringWithFormat:@"%f",cadenceValue] dataUsingEncoding:NSUTF8StringEncoding]]];
                    [iDataReadyInternal dataReadyInternal:cadenceChannel];
                }
                
                NSString *debugTime = [NSString stringWithFormat:@"%f %ld",[[NSDate date] timeIntervalSince1970],[[stepsChannel getData] getTimeStamp] + ([stepsChannel getSamplingDuration_ms])];
                
                NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
                [debugDictionary setValue:debugTime forKey:@"JSON"];
                [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
                
            }];
        }
        
        
        if([[sensor getName] isEqualToString:kActiveEnergyBurned]){
            
            @try {
                NSMutableArray *channelsList = [sensor getNodes];
                Channel *caloriesChannel = [channelsList objectAtIndex:0];
                NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
                [userInfo setValue:sensor forKey:@"sensor"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    energyChannelTimer = [NSTimer scheduledTimerWithTimeInterval: [caloriesChannel getSamplingDuration_ms]*1.0
                                                               target:self
                                                             selector:@selector(executeHealthKitQuery:)
                                                             userInfo:userInfo
                                                              repeats:YES];
                });
                
                NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
                [debugDictionary setValue:[NSString stringWithFormat:@"Name %@ %i",[sensor getName],[caloriesChannel getSamplingDuration_ms]] forKey:@"DEBUG"];
                [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
            }
            @catch (NSException *exception) {
                NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
                [debugDictionary setValue:[exception description] forKey:@"EXCEPTION"];
                [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
            }
        }
        
        if([[sensor getName] isEqualToString:kHeartRate]){
            @try {
                NSMutableArray *channelsList = [sensor getNodes];
                Channel *caloriesChannel = [channelsList objectAtIndex:0];
                NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
                [userInfo setValue:sensor forKey:@"sensor"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    energyChannelTimer = [NSTimer scheduledTimerWithTimeInterval: [caloriesChannel getSamplingDuration_ms]
                                                                      target:self
                                                                    selector:@selector(executeHealthKitQuery:)
                                                                    userInfo:userInfo
                                                                     repeats:YES];
                });

            }
            @catch (NSException *exception) {
                NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
                [debugDictionary setValue:[NSString stringWithFormat:@"Exception Heart %@",[exception description]] forKey:@"DEBUG"];
                [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
            }
        }
        
    }
}

- (void)stop{
    ExtensionDelegate *watchDelegate = (ExtensionDelegate *)[WKExtension sharedExtension].delegate;
    [watchDelegate.motionManager stopAccelerometerUpdates];
    [watchDelegate.pedometer stopPedometerUpdates];
    if([energyChannelTimer isValid])
        [energyChannelTimer invalidate];
}


- (void)setDevice:(Device *)device{
    mDevice = device;
}

- (Device *)getDevice{
    return mDevice;
}


-(Device *)getPopulatedDevice{
    return nil;
}

-(void)notifyError:(ErrorTypesZs)errorType{
    
}

-(void)notifyConnectionState:(ConnectionStates)connectionStates{
    
}

-(void)dataReadyInternal:(Node *)node{
    
}

-(Data *)getRefreshedData:(Data *)actualData timeStamp:(long)timeStamp values:(NSData *)values{
    Data *data = [[Data alloc] init];
    [data setDataType:[actualData getDataType]];
    [data setTimeStamp:timeStamp];
    [data setValue:[NSMutableData dataWithData:values]];
    return data;
}

-(void)executeHealthKitQuery:(id)sender{
    
    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
    [debugDictionary setValue:[NSString stringWithFormat:@"executeHealthKitQuery"] forKey:@"DEBUG"];
    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    
    @try {
        Sensor *sensor = [[sender userInfo] valueForKey:@"sensor"];
        ExtensionDelegate *watchDelegate = (ExtensionDelegate *)[WKExtension sharedExtension].delegate;
        watchDelegate.healthKitManager = [ZetaHealthKitManager sharedManager];

        if([[sensor getName] isEqualToString:kActiveEnergyBurned]){
            NSMutableArray *channelsList = [sensor getNodes];
            Channel *caloriesChannel = [channelsList objectAtIndex:0];
            HKSampleType *sampleType = [HKSampleType quantityTypeForIdentifier:HKQuantityTypeIdentifierActiveEnergyBurned];
            NSSortDescriptor *lastDateDescriptor = [[NSSortDescriptor alloc]
                                                    initWithKey:HKSampleSortIdentifierEndDate ascending:NO selector:@selector(localizedStandardCompare:)];
            NSArray *sortDescriptors = @[lastDateDescriptor];
            
            HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:sampleType predicate:nil limit:1 sortDescriptors:sortDescriptors resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error){
                if (!error) {
                    NSLog(@"An error occured fetching the user's active Energy. The error was: %@.", error);
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (HKQuantitySample *sample in results) {
                        HKUnit *kCal = [HKUnit unitFromString:@"kcal"];
                        double calories = [sample.quantity doubleValueForUnit:kCal];
                        
                        [caloriesChannel setData:[self getRefreshedData:[caloriesChannel getData] timeStamp:[sample.endDate timeIntervalSince1970] values:[[NSString stringWithFormat:@"%f",calories] dataUsingEncoding:NSUTF8StringEncoding]]];
                        [iDataReadyInternal dataReadyInternal:caloriesChannel];
                    }
                });
            }];
            [[watchDelegate.healthKitManager getHealthKitStore] executeQuery:query];
        }else if([[sensor getName] isEqualToString:kHeartRate]){
            HKSampleType *sampleType = [HKSampleType quantityTypeForIdentifier:HKQuantityTypeIdentifierHeartRate];
            NSSortDescriptor *lastDateDescriptor = [[NSSortDescriptor alloc]
                                                    initWithKey:HKSampleSortIdentifierEndDate ascending:NO selector:@selector(localizedStandardCompare:)];
            NSArray *sortDescriptors = @[lastDateDescriptor];
            
            HKSampleQuery *query = [[HKSampleQuery alloc] initWithSampleType:sampleType predicate:nil limit:1 sortDescriptors:sortDescriptors resultsHandler:^(HKSampleQuery *query, NSArray *results, NSError *error){
                if (!error) {
                    NSLog(@"An error occured fetching the user's heartrate. The error was: %@.", error);
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (HKQuantitySample *sample in results) {
                        
                        HKUnit *bpm = [HKUnit unitFromString:@"count/min"];
                        NSMutableArray *channelsList = [sensor getNodes];
                        Channel *heartBeatChannel = [channelsList objectAtIndex:0];
                        double heartBeat = [sample.quantity doubleValueForUnit:bpm];
                        
                        [heartBeatChannel setData:[self getRefreshedData:[heartBeatChannel getData] timeStamp:[sample.endDate timeIntervalSince1970] values:[[NSString stringWithFormat:@"%f",heartBeat] dataUsingEncoding:NSUTF8StringEncoding]]];
                        [iDataReadyInternal dataReadyInternal:heartBeatChannel];
                    }
                });
            }];
            [[watchDelegate.healthKitManager getHealthKitStore] executeQuery:query];
        }
    }
    @catch (NSException *exception) {
        NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
        [debugDictionary setValue:[NSString stringWithFormat:@"Exception in timer %@",[exception description]] forKey:@"DEBUG"];
        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    }
    
}




@end
