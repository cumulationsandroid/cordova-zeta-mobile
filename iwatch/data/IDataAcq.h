//
//  IDataAcq.h
//  zetasense
//
//  Created by Madhu V Swamy on 23/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Device.h"

@interface IDataAcq : NSObject

- (id) init __unavailable;

@end

@protocol IDataAcq <NSObject>

@required
-(Device *)getDevice;
-(void)setDevice:(Device *)_device;
-(void)start;
-(void)stop;

@end