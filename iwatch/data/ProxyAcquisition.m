//
//  ProxyDataAcquisition.m
//  zetasense
//
//  Created by Madhu V Swamy on 03/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "ProxyAcquisition.h"
@import WatchConnectivity;

@implementation ProxyAcquisition

-(id)initWithDetails:(INotify *)iNotify sensorAgent:(IDataReadyInternal *)sensorAgent{
    self = [super init];
    if (self) {
//        mDataAcquistion = [[DataAcquisition alloc] initWithDetails:iNotify sensorAgent:sensorAgent];
    }
    return self;
}

- (void)setDevice:(Device *)device{
//    [mDataAcquistion setDevice:device];
}

- (Device *)getDevice{
//    return [mDataAcquistion getDevice];
    return nil;
}

- (void)stop{
//    [mDataAcquistion stop];
}

- (void)start{
//    [mDataAcquistion start];
}

@end
