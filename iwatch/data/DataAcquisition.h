//
//  DataAcquisition.h
//  zetasense
//
//  Created by Madhu V Swamy on 03/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>
#import "ExtensionDelegate.h"
#import "INotify.h"
#import "Sensor.h"
#import "Channel.h"
#import "Constants.h"
#import "IDataAcq.h"
#import "Device.h"
#import "IDataReadyInternal.h"
#import "SensorConfig.h"

@interface DataAcquisition : NSObject<IDataAcq,INotify,IDataReadyInternal>{
    NSString *mDeviceId;
    IDataReadyInternal<IDataReadyInternal> *iDataReadyInternal;
    Device *mDevice;
    INotify *notify;
//    NSTimer *energyChannelTimer;
}
@property (strong, nonatomic) NSTimer *energyChannelTimer;
- (id) init __unavailable;
-(id)initWithDetails:(INotify *)iNotify  sensorAgent:(IDataReadyInternal *)sensorAgent;
-(void)setDevice:(Device *)device;
-(Device *)getDevice;
-(void)start;
-(void)stop;
-(Device *)getPopulatedDevice;

@end
