//
//  ProxyDataAcquisition.h
//  zetasense
//
//  Created by Madhu V Swamy on 03/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDataAcq.h"
#import "IDataReadyInternal.h"
#import "DataAcquisition.h"
#import "INotify.h"

@interface ProxyAcquisition : NSObject<IDataAcq>{
}



- (id) init __unavailable;
-(id)initWithDetails:(INotify *)iNotify  sensorAgent:(IDataReadyInternal *)sensorAgent;
-(void)setDevice:(Device *)device;
-(Device *)getDevice;
-(void)start;
-(void)stop;

@end


