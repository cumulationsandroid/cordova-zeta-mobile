//
//  Channel.h
//  zetasense
//
//  Created by Madhu V Swamy on 23/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "Node.h"

@interface Channel : Node{
    BOOL isEnabled;
    int samplingDuration_ms;
}

- (BOOL)getIsEnabled;
- (void)setIsEnabled:(BOOL)_isEnabled;
- (int)getSamplingDuration_ms;
- (void)setSamplingDuration_ms:(int)_samplingDuration_ms;

//public ErrorTypesZs sampleData()  - is not implemented as its not used.


@end
