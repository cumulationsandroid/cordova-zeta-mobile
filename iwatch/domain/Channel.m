//
//  Channel.m
//  zetasense
//
//  Created by Madhu V Swamy on 23/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "Channel.h"

@interface Channel()

@property(nonatomic,assign)BOOL isEnabled;
@property(nonatomic,assign)int samplingDuration_ms;

@end

@implementation Channel
@synthesize isEnabled,samplingDuration_ms;

- (BOOL)getIsEnabled{
    return isEnabled;
}

- (void)setIsEnabled:(BOOL)_isEnabled{
    isEnabled = _isEnabled;
}

- (int)getSamplingDuration_ms{
    return samplingDuration_ms;
}

- (void)setSamplingDuration_ms:(int)_samplingDuration_ms{
    samplingDuration_ms = _samplingDuration_ms;
}

@end
