//
//  ErrorTypesZs.h
//  zetasense
//
//  Created by Madhu V Swamy on 23/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#ifndef ErrorTypesZs_h
#define ErrorTypesZs_h

typedef NS_ENUM(NSInteger, ErrorTypesZs) {
    ERR_NO_ERROR, ERR_GENERIC, ERR_OBJECTIFY_FAILED, ERR_SERIALIZE_FAILED
};

#endif /* ErrorTypesZs_h */
