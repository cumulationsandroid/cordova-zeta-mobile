//
//  INotify.h
//  zetasense
//
//  Created by Madhu V Swamy on 22/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnectionStates.h"
#import "ErrorTypesZs.h"

@interface INotify : NSObject

- (id) init __unavailable;

@end

@protocol INotify <NSObject>
@required
- (void)notifyError:(ErrorTypesZs)errorType;
- (void)notifyConnectionState:(ConnectionStates)connectionStates;
@end


