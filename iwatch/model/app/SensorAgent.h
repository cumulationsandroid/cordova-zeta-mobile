//
//  SensorAgent.h
//  zetasense
//
//  Created by Madhu V Swamy on 25/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "Node.h"
#import "IDataReadyExternal.h"
#import "IDataReadyInternal.h"
#import "INotify.h"
#import "IComm.h"
#import "States.h"
#import "Notification.h"
#import <objc/runtime.h>
#include <objc/message.h>
#include <objc/objc.h>
#import "ProxyAcquisition.h"
#import "DataAcquisition.h"
#import "ProxyComm.h"


@interface SensorAgent : Node<IDataReadyExternal,IDataReadyInternal,INotify> {
    States mState;
    IComm<IComm> *iComm;
    Notification *notification;
    IDataAcq <IDataAcq> *mIDataAcq;
}

- (id) init __unavailable;
-(id)initWithDetails:(Node *)sensorAgentNode;
-(NSString *)getDeviceCapabilities;
-(NSString *)getConfiguration;
-(void)setConfiguration:(Node *)node;
-(void)start;
-(void)stop;
-(void)heartBeat:(Node *)node;
-(Node *)objectify:(NSString *)data;
-(NSString *)serialize:(Node *)node;
-(ProxyComm *)getProxy;
-(void)setProxy:(ProxyComm *)_proxy;
-(States)getState;
//-(void)setState:(States)_state;

@end
