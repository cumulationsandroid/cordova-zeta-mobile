//
//  ConnectionRole.h
//  zetasense
//
//  Created by Madhu V Swamy on 23/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#ifndef ConnectionRole_h
#define ConnectionRole_h

typedef NS_ENUM(NSInteger, ConnectionRole) {
    MASTER, SLAVE, CLIENT, SERVER
};

#endif /* ConnectionRole_h */
