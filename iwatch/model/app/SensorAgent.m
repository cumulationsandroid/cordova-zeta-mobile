//
//  SensorAgent.m
//  zetasense
//
//  Created by Madhu V Swamy on 25/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "SensorAgent.h"
@import WatchConnectivity;

@implementation SensorAgent

-(id)initWithDetails:(Node *)sensorAgentNode{
    self = [super init];
    if (self) {
        NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
        [debugDictionary setValue:@"Sensor Agent Initi" forKey:@"DEBUG"];
        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
        
        iComm = [[ProxyComm alloc] initWithDetails:self notify:self];
        mState=STARTUP;
        [self populateSensorAgentNodes:sensorAgentNode];
        mIDataAcq  = [[DataAcquisition alloc] initWithDetails:self sensorAgent:self];
    }
    return self;
}

-(void)populateSensorAgentNodes:(Node *)node{
    [self setName:[node getName]];
    [self setDescription:[node getDescription]];
    [self setGuid:[node getGuid]];
    [self setUri:[node getUri]];
    [self setType:[node getType]];
    [self setData:[node getData]];
    NSMutableArray *tempNodes = [node getNodes];
//    [nodes addObject:[mIDataAcq getDevice]];
    [self setNodes:tempNodes];
    mState = READY;
}

-(NSString *)getDeviceCapabilities{
    
    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
    [debugDictionary setValue:@"getDeviceCapabilities" forKey:@"DEBUG"];
    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    
    return [self serialize:self];
}

-(NSString *)getConfiguration{
    return [self serialize:[mIDataAcq getDevice]];
}


-(void)setConfiguration:(Node *)node{
    if([node isKindOfClass:[node class]]){
        mState = CONFIGURATION;
        [self stop];
        mState = READY;
        [mIDataAcq setDevice:(Device *)node];
        [iComm transmitData:[self getConfiguration]];
    }
}

-(void)start{
    @try {
        NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
        [debugDictionary setValue:[NSString stringWithFormat:@"STARTING SENSOR AGENT %@",mIDataAcq] forKey:@"DEBUG"];
        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
        mState=OPERATIONAL;
        [mIDataAcq start];
    }
    @catch (NSException *exception) {
        NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
        [debugDictionary setValue:[NSString stringWithFormat:@"Error %@",[exception description]] forKey:@"DEBUG"];
        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    }
    
}

-(void)stop{
    mState=READY;
    [mIDataAcq stop];
}

-(void)heartBeat:(Node *)node{
    
}

-(NSString *)serialize:(Node *)node{
    if([node isKindOfClass:[Node class]]){
        return [node toJSONString];
    }else if ([node isKindOfClass:[Node class]]){
        Device *device = (Device *)node;
        return [device toJSONString];
    }else if ([node isKindOfClass:[SensorAgent class]]){
        SensorAgent *sensorAgent = (SensorAgent *)node;
        return [sensorAgent toJSONString];
    }
    return nil;
}

-(Node *)objectify:(NSString *)jsonString{
    Node* node = [[Node alloc] initWithString:jsonString error:nil];
    
    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
    [debugDictionary setValue:[NSString stringWithFormat:@"SENSOR NODE class %@ %@",[node getUri],NSStringFromClass([Node class])] forKey:@"DEBUG"];
    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    
    if(node!=nil && [node getUri]!=nil){
        NSString *tempUri = [node getUri];
        if([tempUri containsString:NSStringFromClass([Node class])]){
            return node;
        }else if([tempUri containsString:NSStringFromClass([Device class])]){
            Device *device = [[Device alloc] initWithString:jsonString error:nil];
            return device;
        }else if([tempUri containsString:NSStringFromClass([SensorAgent class])]){
            SensorAgent *sensorAgent = [[SensorAgent alloc] initWithString:jsonString error:nil];
            return sensorAgent;
        }
    }
    return nil;
}

-(ProxyComm *)getProxy{
    return nil;
}

-(void)setProxy:(ProxyComm *)_proxy{
    
}

-(States)getState{
    return mState;
}


-(void)dataReadyInternal:(Node *)node{
    NSString *strData = [[NSString alloc] init];
    strData=[self serialize:node];
    [iComm transmitData:strData];
}

-(void)dataReadyExternal:(NSString *)jsonString{
    @try {
        Node *node = [self objectify:jsonString];
        
        Device *device = [[node getNodes] objectAtIndex:0];
        Sensor *sensor = [[device getNodes] objectAtIndex:0];
        Channel *xChannel = (Channel *)[[sensor getNodes] objectAtIndex:0];
        
        NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
        [debugDictionary setValue:[NSString stringWithFormat:@"TEMP DEXT %i %@",[sensor getNodes].count,[xChannel getName]] forKey:@"DEBUG"];
        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
        
        if([node isKindOfClass:[Node class]]){
            NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
            [debugDictionary setValue:[NSString stringWithFormat:@"AFTER OBJ %@ %i",node,[node getNodes].count] forKey:@"DEBUG"];
            [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
        
            NSString *returnValue = nil;
            if([node getNodes]==nil || [node getNodes].count==0){
                NSString *methodName = [node getName];
                SEL selector = NSSelectorFromString(methodName);
                //Fix to handle void return type and avoid BAD_ACCESS error.
                Method m = class_getInstanceMethod([SensorAgent class],selector);
                char type[128];
                method_getReturnType(m, type, sizeof(type));
                if(type[0]=='v')
                    ((void *(*)(id, SEL, ...))objc_msgSend)(self,selector);
                else
                    returnValue = ((NSString *(*)(id, SEL, ...))objc_msgSend)(self,selector);
            }else{
                NSString *methodName = [NSString stringWithFormat:@"%@:",[node getName]];
                SEL selector = NSSelectorFromString(methodName);
                returnValue = ((NSString *(*)(id, SEL, ...))objc_msgSend)(self,selector,[node getNodes][0]);
            }
            
            [iComm transmitData:returnValue];
        }
    }
    @catch (NSException *exception) {
        NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
        [debugDictionary setValue:[NSString stringWithFormat:@"%@",[exception description]] forKey:@"DEBUG"];
        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    }
   }

-(void)notify:(ErrorTypesZs)errorTypes{
    
}







@end
