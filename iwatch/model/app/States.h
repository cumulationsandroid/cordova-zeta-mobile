//
//  States.h
//  zetasense
//
//  Created by Madhu V Swamy on 23/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#ifndef States_h
#define States_h

typedef NS_ENUM(NSInteger, States) {
    STARTUP, CONFIGURATION, READY, OPERATIONAL, ERROR
};

#endif /* States_h */
