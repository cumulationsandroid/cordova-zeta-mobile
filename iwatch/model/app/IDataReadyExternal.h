//
//  IDataReadyExternal.h
//  zetasense
//
//  Created by Madhu V Swamy on 16/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Node.h"

@interface IDataReadyExternal : NSObject

- (id) init __unavailable;

@end

@protocol IDataReadyExternal <NSObject>
@required
- (void)dataReadyExternal:(NSString *)data;
@end