//
//  IDataReadyInternal.h
//  zetasense
//
//  Created by Madhu V Swamy on 16/03/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Node.h"

@interface IDataReadyInternal : NSObject

- (id) init __unavailable;

@end

@protocol IDataReadyInternal <NSObject>
@required
- (void)dataReadyInternal:(Node *)node;
@end
