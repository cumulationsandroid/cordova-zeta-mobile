//
//  ConnectionStates.h
//  zetasense
//
//  Created by Madhu V Swamy on 23/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#ifndef ConnectionStates_h
#define ConnectionStates_h

typedef NS_ENUM(NSInteger, ConnectionStates) {
    CONNECTED, CONNECTING, DISCONNECTED, CONNECTIONERROR
};

#endif /* ConnectionStates_h */
