//
//  NodeType.h
//  zetasense
//
//  Created by Madhu V Swamy on 22/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#ifndef NodeType_h
#define NodeType_h

typedef NS_ENUM(NSInteger, NodeType) {
    OBJECT, DATA, METHOD, PARAMETER, ACTIVITY, EVENT, CONTEXT
};

#define kNodeTypeArray @"OBJECT", @"DATA", @"METHOD", @"PARAMETER", @"ACTIVITY", @"EVENT", @"CONTEXT",nil

#endif /* NodeType_h */
