//
//  Node.m
//  zetasense
//
//  Created by Madhu V Swamy on 19/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "Node.h"

@interface Node()

@property(strong,nonatomic)NSString *name;
@property(strong,nonatomic)NSString *description;
@property(strong,nonatomic)NSUUID *guid;
@property(strong,nonatomic)NSString *uri;
@property(strong,nonatomic)NSMutableArray<Node> *nodes;
@property(nonatomic,assign)NodeType type;
@property(strong,nonatomic)Data *data;

@end

@implementation Node
@synthesize name,description,guid,uri,nodes,type,data;
- (instancetype)init
{
    self = [super init];
    if (self) {
        nodes=[[NSMutableArray<Node> alloc] init];
    }
    return self;
}

-(NSString *)getDescription{
    return description;
}

-(void)setDescription:(NSString *)_description{
    description=_description;
}

-(NSUUID *)getGuid{
    return guid;
}

-(void)setGuid:(NSUUID *)_guid{
    guid=_guid;
}

-(NSString *)getName{
    return name;
}

-(void)setName:(NSString *)_name{
    name=_name;
}

-(NSString *)getUri{
    return uri;
}

-(void)setUri:(NSString *)_uri{
    uri=_uri;
}

-(NSMutableArray<Node> *)getNodes{
    return nodes;
}

-(void)setNodes:(NSMutableArray<Node> *)_nodes{
//    if(nodes==nil)
//        nodes = [[NSMutableArray<Node> alloc] init];
    nodes=_nodes;
}

-(NodeType)getType{
    return type;
}

-(void)setType:(NodeType)_type{
    type=_type;
}

-(Data*)getData{
    return data;
}

-(void)setData:(Data *)_data{
    data=_data;
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    
    if ([propertyName isEqualToString: @"nodes"] || [propertyName isEqualToString: @"data"])
        return YES;
    return NO;
}

-(void)setNodesWithNSArray:(NSArray*)array{
    nodes = [Utils getNodesFromDictionary:array];
}

-(NodeType)setTypeWithNSString:(NSString *)_nodeType{
    return [Utils nodeTypeFromString:_nodeType];
}

-(NSString *)JSONObjectForType{
    return [Utils stringFromNodeType:self.type];
}



@end
