//
//  DataType.h
//  zetasense
//
//  Created by Madhu V Swamy on 22/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#ifndef DataType_h
#define DataType_h

typedef NS_ENUM(NSInteger, DataType) {
    BASE_DATATYPE, BOOLEAN, BYTE, BYTE_ARRAY, DATE_TIME, DOUBLE, ENUMERATION, FLOAT, GUID, IMAGE,
    IMAGE_BMP, IMAGE_GIF, IMAGE_PNG, IMAGE_JPG, INT16, INT32, INT64, BASE64, INTEGER, NUMERIC_RANGE,
    STRING, UINT16, UINT32, UINT64, UINTEGER, UTC_TIME, VARIANT, TIMESTAMP_MILLISEC, ELEMENT
};
//BOOL changed to BOOLEAN as BOOL is a keyword.

#define kDataTypeArray @"BASE_DATATYPE", @"BOOLEAN", @"BYTE", @"BYTE_ARRAY", @"DATE_TIME", @"DOUBLE", @"ENUMERATION", @"FLOAT", @"GUID", @"IMAGE",@"IMAGE_BMP", @"IMAGE_GIF",@"IMAGE_PNG", @"IMAGE_JPG", @"INT16", @"INT32", @"INT64", @"BASE64", @"INTEGER", @"NUMERIC_RANGE",@"STRING", @"UINT16", @"UINT32", @"UINT64", @"UINTEGER", @"UTC_TIME", @"VARIANT", @"TIMESTAMP_MILLISEC", @"ELEMENT",nil


#endif /* DataType_h */
