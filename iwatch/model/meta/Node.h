//
//  Node.h
//  zetasense
//
//  Created by Madhu V Swamy on 19/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataType.h"
#import "NodeType.h"
#import "Data.h"
#import <objc/runtime.h>
#import "JSONModel.h"
#import "Utils.h"
@import WatchConnectivity;

@protocol Node

@end

@interface Node : JSONModel
{
    NSString *name;
    NSString *description;
    NSUUID *guid;
    NSString *uri;
    NSMutableArray<Node> *nodes;
    NodeType type;
    Data *data;
}


-(NSString *)getName;
-(void)setName:(NSString *)_name;
-(NSString *)getDescription;
-(void)setDescription:(NSString *)_description;
-(NSUUID *)getGuid;
-(void)setGuid:(NSUUID *)_guid;
-(NSString *)getUri;
-(void)setUri:(NSString *)_uri;
-(NSMutableArray *)getNodes;
-(void)setNodes:(NSMutableArray<Node> *)_nodes;
-(NodeType)getType;
-(void)setType:(NodeType)_type;
-(Data*)getData;
-(void)setData:(Data *)_data;
//-(void)encodeWithCoder:(NSCoder *)encoder;
//- (id)initWithCoder:(NSCoder *)decoder;
//- (NSString *)JSONString;

@end
