//
//  Data.m
//  zetasense
//
//  Created by Madhu V Swamy on 22/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "Data.h"

@interface Data()

@property(nonatomic,assign)DataType dataType;
@property(nonatomic,assign)long timeStamp;
@property(strong,nonatomic)NSMutableData *value;

@end

@implementation Data
@synthesize dataType,timeStamp,value;

- (instancetype)init
{
    self = [super init];
    if (self) {
        value = [[NSMutableData alloc] init];
    }
    return self;
}

-(DataType)getDataType{
    return dataType;
}

-(void)setDataType:(DataType)_dataType{
    dataType=_dataType;
}

-(long)getTimeStamp{
    return timeStamp;
}

-(void)setTimeStamp:(long)_timeStamp{
    timeStamp=_timeStamp;
}

-(NSMutableData *)getValue{
    return value;
}

-(void)setValue:(NSMutableData *)_value{
    if(value==nil)
        value = [[NSMutableData alloc] init];
    value=_value;
}

-(DataType)setDataTypeWithNSString:(NSString *)_dataType{
    return [Utils dataTypeFromString:_dataType];
}

-(NSString *)JSONObjectForDataType{
    return [Utils stringFromDataType:self.dataType];
}

@end
