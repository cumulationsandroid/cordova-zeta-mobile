//
//  Data.h
//  zetasense
//
//  Created by Madhu V Swamy on 22/02/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataType.h"
#import "JSONModel.h"
#import "Utils.h"

@interface Data : JSONModel{
    DataType dataType;
    long timeStamp;
    NSMutableData *value;
}

- (DataType)getDataType;
- (void)setDataType:(DataType)_dataType;
- (long)getTimeStamp;
- (void)setTimeStamp:(long)_timeStamp;
- (NSMutableData *)getValue;
- (void)setValue:(NSMutableData *)_value;

@end
