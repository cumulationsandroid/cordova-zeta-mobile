#import <WatchKit/WatchKit.h>
#import <CoreMotion/CoreMotion.h>
#import "model/app/SensorAgent.h"
#import "model/app/IDataReadyInternal.h"
#import "model/meta/DataType.h"
#import "model/meta/NodeType.h"
#import "utils/Constants.h"
#import "model/meta/Data.h"
#import "Essentials/JSONModel/JSONModelLib.h"
#import "Essentials/JSONModel/JSONModel/JSONModel.h"
#import "utils/Utils.h"
#import "utils/ZetaHealthKitManager.h"
@import WatchConnectivity;
@import HealthKit;


@interface ExtensionDelegate : NSObject <WKExtensionDelegate>
{
    NSString *mDeviceId;
    IDataReadyInternal<IDataReadyInternal> *iSensorAgent;
}

@property (strong, nonatomic) WCSession *session;
@property (nonatomic, strong) CMMotionManager *motionManager;
@property (nonatomic, strong) CMPedometer *pedometer;
@property (nonatomic, strong) ZetaHealthKitManager *healthKitManager;
- (void)debuLog:(NSString *)key value:(NSString *)value;
@end