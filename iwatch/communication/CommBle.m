//
//  CommBle.m
//  zetasense
//
//  Created by Madhu V Swamy on 04/04/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "CommBle.h"

@implementation CommBle

-(id)initWithDetails:(IDataReadyExternal *)sensorAgent notify:(INotify *)notify{
    self = [super init];
    if (self) {
        iSensorAgent = sensorAgent;
        iNotify = notify;
        
        if ([WCSession isSupported]) {
            session = [WCSession defaultSession];
            session.delegate = self;
            [session activateSession];
            
            NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
            [debugDictionary setValue:@"GOT SESSION" forKey:@"DEBUG"];
            [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
        }
        
        NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
        [debugDictionary setValue:@"CommBle INIT" forKey:@"DEBUG"];
        [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    }
    return self;
}

- (void)transmitData:(NSString *)data{
    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
    [debugDictionary setValue:data forKey:@"COMM_BLE"];
    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
}

- (ConnectionStates)getConnectionStatus{
    return CONNECTED;
}

- (ConnectionRole)getConnectionRole{
    return SERVER;
}

- (void)session:(WCSession *)session
didReceiveApplicationContext:(NSDictionary<NSString *,
                              id> *)applicationContext
{
    NSMutableDictionary *debugDictionary = [[NSMutableDictionary alloc] init];
    [debugDictionary setValue:[NSString stringWithFormat:@"%@",applicationContext] forKey:@"DEBUG_COMMBLE"];
    [[WCSession defaultSession] updateApplicationContext:debugDictionary error:nil];
    NSString *input = [applicationContext valueForKey:@"COMMAND"];
    if(input!=nil)
        [iSensorAgent dataReadyExternal:input];
}

@end
