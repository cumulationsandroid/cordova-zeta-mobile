//
//  CommBle.h
//  zetasense
//
//  Created by Madhu V Swamy on 04/04/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INotify.h"
#import "IDataReadyExternal.h"
#import "ConnectionStates.h"
#import "ConnectionRole.h"
#import <WatchKit/WatchKit.h>

@interface CommBle : NSObject<WCSessionDelegate>{
    INotify *iNotify;
    IDataReadyExternal<IDataReadyExternal> *iSensorAgent;
    WCSession *session;
}

- (id) init __unavailable;
- (id)initWithDetails:(IDataReadyExternal *)sensorAgent notify:(INotify *)notify;
- (void)transmitData:(NSString *)data;
- (ConnectionStates)getConnectionStatus;
- (ConnectionRole)getConnectionRole;

@end
