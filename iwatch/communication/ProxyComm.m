//
//  ProxyComm.m
//  zetasense
//
//  Created by Madhu V Swamy on 04/04/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "ProxyComm.h"

@implementation ProxyComm

-(id)initWithDetails:(IDataReadyExternal *)sensorAgent notify:(INotify *)notify{
    self = [super init];
    if (self) {
        mCommBle = [[CommBle alloc] initWithDetails:sensorAgent notify:notify];
    }
    return self;
}

-(void)transmitData:(NSString *)data{
    [mCommBle transmitData:data];
}

-(ConnectionStates)getConnectionStatus{
    return connectionState;
}

-(ConnectionRole)getConnectionRole{
    return connectionRole;
}

@end
