//
//  ProxyComm.h
//  zetasense
//
//  Created by Madhu V Swamy on 04/04/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import "IComm.h"
#import "INotify.h"
#import "IDataReadyExternal.h"
#import "ConnectionStates.h"
#import "ConnectionRole.h"
#import "CommBle.h"

@interface ProxyComm : NSObject<IComm>{
    ConnectionStates connectionState;
    ConnectionRole connectionRole;
    CommBle *mCommBle;
}
- (id) init __unavailable;
-(id)initWithDetails:(IDataReadyExternal *)sensorAgent notify:(INotify *)notify;
- (void)transmitData:(NSString *)data;
- (ConnectionStates)getConnectionStatus;
- (ConnectionRole)getConnectionRole;

@end
