//
//  IComm.h
//  zetasense
//
//  Created by Madhu V Swamy on 04/04/16.
//  Copyright © 2016 Cumulations Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnectionStates.h"
#import "ConnectionRole.h"

@protocol IComm <NSObject>

- (void)transmitData:(NSString *)data;
- (ConnectionStates)getConnectionStatus;
- (ConnectionRole)getConnectionRole;

@end

@interface IComm : NSObject

- (id) init __unavailable;

@end
