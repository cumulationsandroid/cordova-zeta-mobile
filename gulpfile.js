var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');

var paths = {
  sass: ['./scss/**/*.scss']
};

gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('commproxy', function(done) {
    gulp.src('./plugins/commproxy-plugin/src/android/com/at/communication/**')
        .pipe( gulp.dest('./platforms/android/src/com/at/communication') );
    gulp.src('./plugins/commproxy-plugin/src/android/com/at/data/**')
        .pipe( gulp.dest('./platforms/android/src/com/at/data') );
    gulp.src('./plugins/commproxy-plugin/src/android/com/at/model/**')
        .pipe( gulp.dest('./platforms/android/src/com/at/model') );
    gulp.src('./plugins/commproxy-plugin/src/android/com/at/plugin/**')
        .pipe( gulp.dest('./platforms/android/src/com/at/plugin') );
    gulp.src('./plugins/commproxy-plugin/src/android/com/at/utils/**')
        .pipe( gulp.dest('./platforms/android/src/com/at/utils') );
    gulp.src('./plugins/commproxy-plugin/src/android/com/at/*.java')
        .pipe( gulp.dest('./platforms/android/src/com/at/zeta/zetamobile/') );

    gulp.src('./plugins/commproxy-plugin/src/android/com/at/layout/**')
        .pipe( gulp.dest('./platforms/android/res/layout') );
    gulp.src('./plugins/commproxy-plugin/src/android/com/at/menu/**')
        .pipe( gulp.dest('./platforms/android/res/menu/') );

    gulp.src('./plugins/commproxy-plugin/www/**')
        .pipe( gulp.dest('./platforms/android/assets/www/plugins/commproxy-plugin/www') );
    gulp.src('./plugins/commproxy-plugin/www/**')
        .pipe( gulp.dest('./platforms/android/platform_www/plugins/commproxy-plugin/www') );

});

gulp.task('wear', function(done) {
    gulp.src('./wear/**')
        .pipe( gulp.dest('./platforms/android/wear') );

});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
