package com.at.model.meta;

public enum NodeType {
    OBJECT, DATA, METHOD, PARAMETER, ACTIVITY, EVENT, CONTEXT
}