package com.at.model.app;

/**
 * Defines the connection role of the device
 */
public enum ConnectionRole {
    MASTER, SLAVE, CLIENT, SERVER
}