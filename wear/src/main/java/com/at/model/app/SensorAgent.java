package com.at.model.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.text.format.Formatter;
import android.util.Log;

import com.at.communication.IComm;
import com.at.communication.Proxy;
import com.at.communication.ProxyComm;
import com.at.data.IDataAcq;
import com.at.data.ProxyDataAcquisition;
import com.at.model.domain.Channel;
import com.at.model.domain.Device;
import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;
import com.at.model.meta.NodeType;
import com.at.model.meta.Notification;
import com.at.utils.Constants;
import com.at.utils.LoggingService;
import com.at.utils.NodeGsonDeserializer;
import com.at.utils.NodeGsonSerializer;
import com.at.utils.db.DataBaseHelper;
import com.at.utils.wakehelper.ZetaWakeLock;
import com.at.zeta.zetamobile.MessageSender;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.UUID;

/**
 * @SensorAgent is a service that is running on the wear which
 * acts as communication gateway between wear and phone
 */
public class SensorAgent extends Node implements IDataReadyExternal, IDataReadyInternal, INotify, ISensorState {

    private static final String TAG = SensorAgent.class.getSimpleName();
    private final IComm iComm;
    private final IDataAcq mIDataAcq;
    private final Context mContext;
    public Node stopperMsg = null;
    Handler handler = new Handler(Looper.getMainLooper());
    private States mState;
    private transient Notification mNotification;
    private BroadcastReceiver receiver;
    private ConnectionStates connectionStates = ConnectionStates.DISCONNECTED;
    private long interval = -1;
    private Runnable stopper = new Runnable() {
        @Override
        public void run() {
            if (mState == States.OPERATIONAL && connectionStates == ConnectionStates.CONNECTED) {
//                stop();
                Log.e(TAG, "ping - stopped session");
            } else {
                // to make sure that the messageQueue has only one stopper runnable
                handler.removeCallbacks(stopper);
                if (mState == States.OPERATIONAL) {
                    Log.e(TAG, "ping - posponded ping");
                    handler.postDelayed(stopper, interval);
                }
            }
        }
    };

    public SensorAgent(Object applicationContext, Node sensorAgentNode) {
        mContext = (Context) applicationContext;
        this.iComm = new ProxyComm(applicationContext, this, this);
        mState = States.STARTUP;
        mIDataAcq = new ProxyDataAcquisition(applicationContext, this, this);
        populateSensorAgentNodes(sensorAgentNode);
        logString(serialize(this));
    }

    private void populateSensorAgentNodes(Node node) {
        setName(node.getName());
        setDescription(node.getDescription());
        setGuid(node.getGuid());
        setUri(node.getUri());
        setType(node.getType());
        setData(node.getData());
        ArrayList<Node> nodes = (ArrayList<Node>) node.getNodes();
        nodes.add(mIDataAcq.getDevice());
        setNodes(nodes);
        mState = States.READY;
    }

    /**
     * Returns the list of sensors available
     */
    public String getDeviceCapabilities() {
        return serialize(this);
    }

    /**
     * Gets the device node
     */
    public String getConfiguration() {
        return serialize(mIDataAcq.getDevice());
    }

    /**
     * Sets the configuration for a given node
     *
     * @param node
     */
    public void setConfiguration(Node node) {
        if (node instanceof Device) {
            mState = States.CONFIGURATION;
            stop();
            mState = States.READY;
            mIDataAcq.setDevice((Device) node);
            // TODO : If we wish to start, we have to do it here.
            //start();
//            dataReadyInternal(getStateNode(mState));
        }
    }

    /**
     * Starts the data collection for a given node
     */
    public void start(Node node) {
        stopperMsg = null;
        calculateAvailableStorage();
        PowerManager.WakeLock lock = ZetaWakeLock.getLock(mContext);
        if (!lock.isHeld())
            lock.acquire();
        mState = States.OPERATIONAL;
        mIDataAcq.start(node);
        registerBattery();
        LoggingService.reset(mContext);
    }

    private void calculateAvailableStorage() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize;
        long availableBlocks;
        long totalSize;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            blockSize = stat.getBlockSizeLong();
            availableBlocks = stat.getAvailableBlocksLong();
            totalSize = stat.getBlockCountLong();
        } else {
            //noinspection deprecation
            blockSize = stat.getBlockSize();
            //noinspection deprecation
            availableBlocks = stat.getAvailableBlocks();
            //noinspection deprecation
            totalSize = stat.getBlockCount();
        }


        Log.e(TAG, "raw Total Storage size is " + (totalSize * blockSize));
        Log.e(TAG, "raw Free Storage size is " + (availableBlocks * blockSize));
        Log.e(TAG, "Total Storage size is " + Formatter.formatFileSize(mContext, totalSize * blockSize));
        Log.e(TAG, "Free Storage size is " + Formatter.formatFileSize(mContext, availableBlocks * blockSize));

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        long max_range = ((totalSize * blockSize) / 10);
        max_range = max_range / DataBaseHelper.AVERAGE_CHANNEL_SIZE;
        Log.e(TAG, "calculated max rage is : " + max_range);

        preferences.edit().putLong(DataBaseHelper.MAX_VALUE, max_range).commit();

        DataBaseHelper dbHelper = new DataBaseHelper(mContext);
        dbHelper.dropTrigger();
        dbHelper.setTrigger();
    }

    /**
     * Stops the data collection for a given node
     */
    public void stop() {
        unregisterBattery();
        handler.removeCallbacks(stopper);
        interval = -1;
        mState = States.READY;
        mIDataAcq.stop();
        PowerManager.WakeLock lock = ZetaWakeLock.getLock(mContext);
        if (lock.isHeld())
            lock.release();
    }

    /**
     * Sends heart-beat message for a given node to check the status of node
     *
     * @param node
     */
    public void heartBeat(Node node) {

    }

    /**
     * Converts json format of data to object node
     *
     * @param data input in json format
     * @return ERR_NO_ERROR if conversion is successful else throws error of type ErrorTypesZs
     */
    public Node objectify(String data) {
        Node node = null;
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonDeserializer());
        builder.registerTypeAdapter(Channel.class, new NodeGsonDeserializer());
        Gson gsonExt = builder.create();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(data).getAsJsonObject();
        if (jsonObject.getAsJsonPrimitive("uri") != null) {
            String uri = jsonObject.getAsJsonPrimitive("uri").getAsString();
            if (uri.startsWith(Device.class.getSimpleName())) {
                node = gsonExt.fromJson(data, Device.class);
            } else if (uri.startsWith(SensorAgent.class.getSimpleName())) {
                node = gsonExt.fromJson(data, SensorAgent.class);
            } else if (uri.contains(SensorAgent.class.getSimpleName())) {
                node = gsonExt.fromJson(data, Node.class);
            }
            if (node != null) {
                Log.d(TAG, "!inside objectify, node uri: " + node.getUri());
            }
        }

        return node;
    }

    /**
     * Converts node object to json format
     *
     * @param node object to be serialized
     * @return ERR_NO_ERROR if conversion is successful else throws error of type ErrorTypesZs
     */
    public String serialize(Node node) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
        if (node instanceof SensorAgent) {
            builder.registerTypeAdapter(SensorAgent.class, new NodeGsonSerializer());
        } else if (node instanceof Device) {
            builder.registerTypeAdapter(Device.class, new NodeGsonSerializer());
        } else if (node instanceof Channel) {
            builder.registerTypeAdapter(Channel.class, new NodeGsonSerializer());
        }
        Gson gsonExt = builder.create();
        String temp = gsonExt.toJson(node, Node.class);
        Log.d(TAG, "!serialized data: " + temp);
        return temp;
    }

    /**
     * @return proxy of the SensorAgent
     */
    public Proxy getProxy() {
        return null;
    }

    /**
     * @param proxy set proxy for SensorAgent
     */
    public void setProxy(Proxy proxy) {

    }

    /**
     * Get current connection state of the wear
     *
     * @return
     */
    @Override
    public States getState() {
        return mState;
    }

    /**
     * Set current connection state of the wear
     *
     * @param state
     */
    @Override
    public void setState(States state) {
        this.mState = state;
    }

    @Override
    public void dataReadyInternal(final Node node) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                /*Log.d(TAG, "!inside run");*/
                String data = new String();
                long timePrev = System.currentTimeMillis();
                /*String logMsg = "!time before serializing " + node.getUri() + " : " + timePrev;
                Log.d(TAG, logMsg);*/
                data = serialize(node);
//                if (node.getDescription().startsWith(android.hardware.Sensor.STRING_TYPE_STEP_COUNTER)) {
//                    LoggingService.d(mContext, TAG, "serialized data --> " + data);
//                }
                logString(data);
                long timePost = System.currentTimeMillis();
                /*logMsg = "!time after serializing " + node.getUri() + " : " + timePost +
                        " , time taken to serialize: " + (timePost - timePrev);
                Log.d(TAG, logMsg);*/
                iComm.transmitData(data);
            }
        }).start();
    }

    @Override
    public void notify(ErrorTypesZs errorTypes) {

    }

    @Override
    public void notify(ConnectionStates connectionStates) {
        this.connectionStates = connectionStates;
        Log.e(TAG, "ConnectionStates - " + connectionStates);
        if (connectionStates == ConnectionStates.DISCONNECTED && mState == States.OPERATIONAL && interval != -1) {
            handler.removeCallbacks(stopper);
            Log.e(TAG, "ping - posponded ping");
            handler.postDelayed(stopper, interval);
        } else if (connectionStates == ConnectionStates.CONNECTED && mState == States.OPERATIONAL && interval != -1) {
            handler.removeCallbacks(stopper);
            Log.e(TAG, "ping - created ping");
            handler.postDelayed(stopper, interval);

        }
    }

    private void logString(String data) {
        if (data.length() > 4000) {
            Log.v(TAG, "sb.length = " + data.length());
            int chunkCount = data.length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= data.length()) {
                    Log.v(TAG, data.substring(4000 * i));
                } else {
                    Log.v(TAG, data.substring(4000 * i, max));
                }
            }
        } else {
            Log.v(TAG, data);
        }
    }

    @Override
    public void dataReadyExternal(String data) {
        Node node = objectify(data);
        if (node instanceof Node) {
            try {
                Class params[] = new Class[node.getNodes().size()];
                if (node.getNodes().size() > 0) {
                    params[0] = Node.class;
                }
                Object[] objects = null;
                if (node.getNodes().size() > 0) {
                    ArrayList<? extends Node> nodeList = node.getNodes();
                    objects = new Object[nodeList.size()];
                    for (int i = 0; i < nodeList.size(); i++) {
                        Node objectNode = nodeList.get(i);
                        objects[i] = objectNode;
                    }
                }
                Method method = this.getClass().getDeclaredMethod(node.getName(), params);
                if (method != null) {
                    if (method.getReturnType().equals(Void.TYPE)) {
                        method.invoke(this, objects);
                    } else if (method.getReturnType().equals(String.class)) {
                        String name = (String) method.invoke(this, objects);
                        iComm.transmitData(name);
                    } else if (method.getReturnType().equals(States.class)) {
                        States curState = getState();
                        Node stateNode = getStateNode(curState);
                        String serializedState = serialize(stateNode);
                        iComm.transmitData(serializedState);
                    }

                    Log.d(TAG, "Invoked " + node.getName());

                } else {
                    Log.d(TAG, "Method not available " + node.getName());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void ping(Node node) {
        handler.removeCallbacks(stopper);
        interval = ByteBuffer.wrap(node.getData().getValues()).getLong();
        if (interval <= 0)
            throw new IllegalArgumentException("Interval should be greater then zero");
        interval += (Constants.CONNECTION_DELAY + Constants.TRANSMISSION_DELAY_OFFSET);
        if (mState == States.OPERATIONAL) {
            handler.postDelayed(stopper, interval);
            Log.e(TAG, "ping - ping set for " + interval);
        } else {
            if (stopperMsg != null) {
                Log.e(TAG, "ping - session not running, notification sent");
                dataReadyInternal(stopperMsg);
                stopperMsg = null;
            }
            Log.e(TAG, "ping - session not running, notification already sent");
        }
    }


    private void getLogs() {
        Intent intent = new Intent(mContext, MessageSender.class);
        intent.putExtra(Constants.DATA, "");
        intent.putExtra(Constants.PATH, "get_logs");
        intent.putExtra(Constants.SOURCE, "get_logs");
        mContext.startService(intent);
    }

    /**
     * This returns the {@link #mState } in {@link Node}
     *
     * @return The value of {@link #mState } in {@link Node}
     */
    private Node getStateNode(States state) {
        Node node = null;
        ArrayList<Node> nodes = (ArrayList<Node>) getNodes();
        for (int i = nodes.size() - 1; i >= 0; i--) {
            if (nodes.get(i).getName().equalsIgnoreCase("getState")) {
                node = nodes.get(i);
                node.setUri(node.getUri().replace("Method:", "Notification:"));
                node.setType(NodeType.OBJECT);
                break;
            }
        }
        if (node == null) {
            node = new Notification();
            node.setName("getState");
            node.setDescription("The functional state of the SensorAgent");
            node.setType(NodeType.OBJECT);
            node.setGuid(UUID.randomUUID());
            node.setUri(getUri().replace("SensorAgent:", "Notification:") + ".getState");
            nodes.add(node);
        }
        Data data = node.getData();
        if (data == null) {
            data = new Data();
            data.setDataType(DataType.ENUMERATION);
        }
        data.setTimeStamp(System.currentTimeMillis());
        data.setValues(state.name().getBytes());
        node.setData(data);
        return node;
    }

    private void registerBattery() {
        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction(Intent.ACTION_BATTERY_LOW);
//        intentFilter.addAction(Intent.ACTION_BATTERY_OKAY);
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        intentFilter.addAction(Constants.ACTION_FORCE_STOP);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e("onReceive","onReceive");
                if (intent.getAction() != Constants.ACTION_FORCE_STOP) {
                    validateBattery(intent);
                } else {
                    stopperMsg = buildBattery(Constants.MESSAGE_WARNING_STOPPED_U);
                    dataReadyInternal(stopperMsg);
                }
            }
        };

        Intent batteryStatus = mContext.registerReceiver(receiver, intentFilter);
        validateBattery(batteryStatus,true);
    }

    private void unregisterBattery() {
        if (receiver != null) {
            mContext.unregisterReceiver(receiver);
            receiver = null;
        }
    }

    private void validateBattery(Intent batteryStatus) {
        validateBattery(batteryStatus, false);
    }

    private void validateBattery(Intent batteryStatus, boolean needDelay) {

        int plugged = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean isPlugged = plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB;

        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);

        if (!isCharging && !isPlugged) {
            if (level > Constants.BATTERY_MIN && level <= Constants.BATTERY_MAX) {
                stopperMsg = buildBattery(Constants.MESSAGE_WARNING_BATTERY);
                dataReadyInternal(stopperMsg);
            } else if (level <= Constants.BATTERY_MIN) {
                stopperMsg = buildBattery(Constants.MESSAGE_WARNING_STOPPED);
                dataReadyInternal(stopperMsg);
                int delay = 0;
                if (needDelay) {
                    delay = 1000;
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        stop();
                    }
                }, delay);
            }
        }
    }

    private Node buildBattery(String msg) {
        Node node = null;
        ArrayList<Node> nodes = (ArrayList<Node>) getNodes();
        for (int i = nodes.size() - 1; i >= 0; i--) {
            if (nodes.get(i).getName().equalsIgnoreCase("Notification")) {
                node = nodes.get(i);
                break;
            }
        }
        if (node == null) {
            node = new Notification();
            node.setName("Notification");
            node.setDescription("Notification message");
            node.setType(NodeType.OBJECT);
            node.setGuid(UUID.randomUUID());
            node.setUri(getUri().replace("SensorAgent:", "Notification:") + ".Notification");
            nodes.add(node);
        }

        Data data = new Data();
        data.setDataType(DataType.STRING);
        data.setTimeStamp(System.currentTimeMillis());
        data.setValues(msg.getBytes());

        node.setData(data);

        return node;
    }
}
