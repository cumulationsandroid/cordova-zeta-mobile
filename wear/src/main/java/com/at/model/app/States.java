package com.at.model.app;

/**
 * Defines current state of Wear
 */
public enum States {
    STARTUP, CONFIGURATION, READY, OPERATIONAL, ERROR
}