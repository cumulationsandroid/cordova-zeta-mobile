package com.at.zeta.zetamobile;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.at.model.app.IDataReadyInternal;
import com.at.model.app.SensorAgent;
import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;
import com.at.model.meta.NodeType;
import com.at.model.meta.Notification;
import com.at.utils.Constants;
import com.at.utils.NTPClient;
import com.at.utils.NodeGsonDeserializer;
import com.at.utils.NodeGsonSerializer;
import com.at.utils.wakehelper.ZetaWakeLock;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.UUID;

public class WearApplication extends Application {

  private static final String TAG = WearApplication.class.getSimpleName();
  private SensorManager mSensorManager;
  private SharedPreferences mSharedPrefs;
  private IDataReadyInternal iSensorAgent;
  private String mDeviceId;
  private int activityCount = 0;

  private GoogleApiClient mGoogleApiClient;

  private Long timeOffset = null;
  private boolean ntpAvailable;
  private NTPClient ntp;

  @Override
  public void onCreate() {
    super.onCreate();

    mDeviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

    Node sensorAgentNode = getPopulatedSensorAgent();
    iSensorAgent = new SensorAgent(this, sensorAgentNode);

    ZetaWakeLock.setUpAlarm(this);

    getTime();
  }

  private String getCommonLiteral() {
    return Constants.SCHEME_IDENTIFIER + mDeviceId +
      "" + "." + Build.MANUFACTURER + Build.PRODUCT + "." + SensorAgent.class
      .getSimpleName();
  }

  private Node getPopulatedSensorAgent() {
    if (TextUtils.isEmpty(mSharedPrefs.getString(Constants.SENSOR_AGENT_SERIALIZED_DATA, ""))) {
      Node node = initSensorAgentNode();
      GsonBuilder builder = new GsonBuilder();
      builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
      Gson gsonExt = builder.create();
      String sensorJson = gsonExt.toJson(node);
      logString("SensorAgent - New " + sensorJson);

      SharedPreferences.Editor editor = mSharedPrefs.edit();
      editor.putString(Constants.SENSOR_AGENT_SERIALIZED_DATA, sensorJson);
      editor.apply();

      return node;
    } else {
      String serializedDeviceData = mSharedPrefs.getString(Constants
        .SENSOR_AGENT_SERIALIZED_DATA, "");
      logString("Existing sensor agent" + serializedDeviceData);

      GsonBuilder builder = new GsonBuilder();
      builder.registerTypeAdapter(Node.class, new NodeGsonDeserializer());
      builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
      Gson gsonExt = builder.create();
      Node node = gsonExt.fromJson(serializedDeviceData, Node.class);

      //Debugging
            /*String sensorJson = gsonExt.toJson(node);
            Log.d(TAG, "Converted SensorAgent " + sensorJson);*/
      return node;
    }
  }

  private Node initSensorAgentNode() {
    Node node = new Node();
    node.setName("SensorAgent");
    node.setType(NodeType.OBJECT);
    node.setUri(SensorAgent.class.getSimpleName() + getCommonLiteral());
    node.setDescription("SensorAgent instance");
    node.setGuid(UUID.randomUUID());
    Data data = new Data();
    node.setData(data);
    node.setNodes(getMethodNodes());
    return node;
  }


  private ArrayList<Node> getMethodNodes() {
    ArrayList<Node> result = new ArrayList<>();
////
    Node setConfigurationNode = new Node();
    setConfigurationNode.setName("setConfiguration");
    setConfigurationNode.setDescription("SensorAgent setConfiguration");
    setConfigurationNode.setGuid(UUID.randomUUID());
    setConfigurationNode.setUri("Method" + getCommonLiteral() + "." + "setConfiguration");
    setConfigurationNode.setType(NodeType.METHOD);
    setConfigurationNode.setData(new Data());

    ArrayList<Node> setConfigurationParamNodes = new ArrayList<>();
    Node paramSetConfiguration = new Node();
    paramSetConfiguration.setName("Node");
    paramSetConfiguration.setDescription("setConfiguration param Node");
    paramSetConfiguration.setGuid(UUID.randomUUID());
    paramSetConfiguration.setUri("Parameter" + getCommonLiteral() + "." + "setConfiguration." + Node.class.getSimpleName());
    paramSetConfiguration.setType(NodeType.PARAMETER);
    paramSetConfiguration.setData(new Data());
    setConfigurationParamNodes.add(paramSetConfiguration);
    setConfigurationNode.setNodes(setConfigurationParamNodes);
    result.add(setConfigurationNode);
////
    Node getConfigurationNode = new Node();
    getConfigurationNode.setName("getConfiguration");
    getConfigurationNode.setDescription("SensorAgent getConfiguration");
    getConfigurationNode.setGuid(UUID.randomUUID());
    getConfigurationNode.setUri("Method" + getCommonLiteral() + "." + "getConfiguration");
    getConfigurationNode.setData(new Data());
    getConfigurationNode.setType(NodeType.METHOD);
    result.add(getConfigurationNode);

    Node startNode = new Node();
    startNode.setName("start");
    startNode.setDescription("SensorAgent start");
    startNode.setGuid(UUID.randomUUID());
    startNode.setUri("Method" + getCommonLiteral() + "." + "start");
    startNode.setData(new Data());
    startNode.setType(NodeType.METHOD);

    ArrayList<Node> startParamNodes = new ArrayList<>();
    Node paramStart = new Node();
    paramStart.setName("start");
    paramStart.setDescription("start param Node");
    paramStart.setGuid(UUID.randomUUID());
    paramStart.setUri("Parameter" + getCommonLiteral() + "." + "start." + Node.class.getSimpleName());
    paramStart.setType(NodeType.PARAMETER);

    Data startData = new Data();
    startData.setDataType(DataType.STRING);

    paramStart.setData(startData);
    startParamNodes.add(paramStart);
    startNode.setNodes(startParamNodes);

    result.add(startNode);


    Node pingNode = new Node();
    pingNode.setName("ping");
    pingNode.setDescription("SensorAgent ping");
    pingNode.setGuid(UUID.randomUUID());
    pingNode.setUri("Method" + getCommonLiteral() + "." + "ping");
    pingNode.setData(new Data());
    pingNode.setType(NodeType.METHOD);

    ArrayList<Node> pingParamNodes = new ArrayList<>();
    Node paramPing = new Node();
    paramPing.setName("ping");
    paramPing.setDescription("ping param Node");
    paramPing.setGuid(UUID.randomUUID());
    paramPing.setUri("Parameter" + getCommonLiteral() + "." + "ping." + Node.class.getSimpleName());
    paramPing.setType(NodeType.PARAMETER);

    Data pingData = new Data();
    pingData.setDataType(DataType.INT64);

    paramPing.setData(pingData);
    pingParamNodes.add(paramPing);
    pingNode.setNodes(pingParamNodes);

    result.add(pingNode);

    Node stopNode = new Node();
    stopNode.setName("stop");
    stopNode.setDescription("SensorAgent stop");
    stopNode.setGuid(UUID.randomUUID());
    stopNode.setUri("Method" + getCommonLiteral() + "." + "stop");
    stopNode.setType(NodeType.METHOD);
    stopNode.setData(new Data());
    result.add(stopNode);
//
    Node heartBeat = new Node();
    heartBeat.setName("heartBeat");
    heartBeat.setDescription("SensorAgent heartBeat");
    heartBeat.setGuid(UUID.randomUUID());
    heartBeat.setUri("Method" + getCommonLiteral() + "." + "heartBeat");
    heartBeat.setType(NodeType.METHOD);
    heartBeat.setData(new Data());

    ArrayList<Node> setHeartBeatParamNodes = new ArrayList<>();
    Node paramHeartBeat = new Node();
    paramHeartBeat.setName("Node");
    paramHeartBeat.setDescription("heartBeat param Node");
    paramHeartBeat.setGuid(UUID.randomUUID());
    paramHeartBeat.setUri("Parameter" + getCommonLiteral() + "." + "heartBeat." + Node.class.getSimpleName());
    paramHeartBeat.setType(NodeType.PARAMETER);
    paramHeartBeat.setData(new Data());
    setHeartBeatParamNodes.add(paramHeartBeat);
    heartBeat.setNodes(setHeartBeatParamNodes);
    result.add(heartBeat);
//
    Node getDeviceCapabilities = new Node();
    getDeviceCapabilities.setName("getDeviceCapabilities");
    getDeviceCapabilities.setDescription("SensorAgent getDeviceCapabilities");
    getDeviceCapabilities.setGuid(UUID.randomUUID());
    getDeviceCapabilities.setUri("Method" + getCommonLiteral() + "." + "getDeviceCapabilities");
    getDeviceCapabilities.setType(NodeType.METHOD);
    getDeviceCapabilities.setData(new Data());
    result.add(getDeviceCapabilities);

    Node getLoggerLevel = new Node();
    getLoggerLevel.setName("getLoggerLevel");
    getLoggerLevel.setDescription("SensorAgent getLoggerLevel");
    getLoggerLevel.setGuid(UUID.randomUUID());
    getLoggerLevel.setUri("Method" + getCommonLiteral() + "." + "getLoggerLevel");
    getLoggerLevel.setType(NodeType.METHOD);
    getLoggerLevel.setData(new Data());
    result.add(getLoggerLevel);

    Node setLoggerLevel = new Node();
    setLoggerLevel.setName("setLoggerLevel");
    setLoggerLevel.setDescription("SensorAgent setLoggerLevel");
    setLoggerLevel.setGuid(UUID.randomUUID());
    setLoggerLevel.setUri("Method" + getCommonLiteral() + "." + "setLoggerLevel");
    setLoggerLevel.setType(NodeType.METHOD);
    setLoggerLevel.setData(new Data());
    result.add(setLoggerLevel);

    Node notify = new Node();
    notify.setName("notify");
    notify.setDescription("SensorAgent notify");
    notify.setGuid(UUID.randomUUID());
    notify.setUri("Method" + getCommonLiteral() + "." + "notify");
    notify.setType(NodeType.METHOD);
    notify.setData(new Data());
    result.add(notify);

    Node getLoggerMessage = new Node();
    getLoggerMessage.setName("getLoggerMessage");
    getLoggerMessage.setDescription("SensorAgent getLoggerMessage");
    getLoggerMessage.setGuid(UUID.randomUUID());
    getLoggerMessage.setUri("Method" + getCommonLiteral() + "." + "getLoggerMessage");
    getLoggerMessage.setType(NodeType.METHOD);
    getLoggerMessage.setData(new Data());
    result.add(getLoggerMessage);

    Notification notificationNode = new Notification();
    notificationNode.setName("Notification");
    notificationNode.setGuid(UUID.randomUUID());
    notificationNode.setDescription("Notification Message");
    notificationNode.setUri("Notification" + getCommonLiteral() + "." + "Notification");
    notificationNode.setType(NodeType.OBJECT);
    Data data = new Data();
    data.setDataType(DataType.ENUMERATION);
    notificationNode.setData(data);
    result.add(notificationNode);

    Node getStateNode = new Node();
    getStateNode.setName("getState");
    getStateNode.setDescription("SensorAgent states");
    getStateNode.setType(NodeType.METHOD);
    getStateNode.setGuid(UUID.randomUUID());
    getStateNode.setUri("Method" + getCommonLiteral() + "." + "getState");
    Data date = new Data();
    date.setDataType(DataType.ENUMERATION);
    getStateNode.setData(date);
    result.add(getStateNode);

    return result;
  }

  private void logString(String data) {
    if (data.length() > 4000) {
      Log.v(TAG, "sb.length = " + data.length());
      int chunkCount = data.length() / 4000;     // integer division
      for (int i = 0; i <= chunkCount; i++) {
        int max = 4000 * (i + 1);
        if (max >= data.length()) {
          Log.v(TAG, data.substring(4000 * i));
        } else {
          Log.v(TAG, data.substring(4000 * i, max));
        }
      }
    } else {
      Log.v(TAG, data);
    }
  }

  private void getTime() {

    //Try to get ntp time
    try {

      new Thread(new Runnable() {
        @Override
        public void run() {
          ntp = NTPClient.getInstance();
          Log.i(TAG, "ntp.getCorrentTime: " + ntp.getCorrentTime());
          ntpAvailable = true;

        }
      }).start();
    } catch (Exception e){
      //If fail, try to get time from phone
      ntpAvailable = false;
      new Thread(new Runnable() {
        @Override
        public void run() {
          mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
            .addApi(Wearable.API)
            // .addConnectionCallbacks( this )
            .build();

          if (mGoogleApiClient != null && !(mGoogleApiClient.isConnected() || mGoogleApiClient.isConnecting()))
            mGoogleApiClient.connect();

          String msg = "get";
          MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleApiClient,
            "time_sync", "get_time", msg.getBytes()).await();
          Status status = result.getStatus();

        }
      }).start();
    }

  }

  public void setTime(Long time) {
    timeOffset = time - System.currentTimeMillis();
  }

  public Long gettimeOffset() {
    Long currentTime;
    if (ntpAvailable){
      ntp = NTPClient.getInstance();
      currentTime = ntp.getCorrentTime();
    } else {
      currentTime = System.currentTimeMillis() + timeOffset ;
    }
    return currentTime;
  }

  public Long getCurrentTime() {
    Long currentTime;
    if (ntpAvailable){
      ntp = NTPClient.getInstance();
      currentTime = ntp.getCorrentTime();
    } else {
      currentTime = System.currentTimeMillis() + timeOffset ;
    }
    return currentTime;
  }

}
