package com.at.zeta.zetamobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.wearable.view.WatchViewStub;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.at.utils.Constants;
import com.at.utils.LoggingService;
import com.at.utils.db.DatabaseWorker;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";
    private TextView mTextView;
    private Button button;
    private DatabaseWorker databaseWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
                button = (Button) stub.findViewById(R.id.button);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(MainActivity.this, MessageSender.class);
                        intent.putExtra(Constants.DATA, "");
                        intent.putExtra(Constants.PATH, "get_logs");
                        intent.putExtra(Constants.SOURCE, "get_logs");
                        startService(intent);

                        button.setEnabled(false);

//                        storeData();

                    }
                });

                File logFile = new File(Environment.getExternalStorageDirectory(),
                        "ZetaSense/" + LoggingService.FILE_NAME);
                if (logFile.exists()) {
                    button.setEnabled(true);
                } else {
//                    try {
//                        if (logFile.createNewFile()) {
//                            button.setEnabled(true);
//                        } else {
                            button.setEnabled(false);
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                        button.setEnabled(false);
//                    }
                }
            }
        });

        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        long totalSize = stat.getBlockCount();
        Log.e(TAG, "raw Total Storage size is " + (totalSize * blockSize));
        Log.e(TAG, "raw Free Storage size is " + (availableBlocks * blockSize));
        Log.e(TAG, "Total Storage size is " + Formatter.formatFileSize(this, totalSize * blockSize));
        Log.e(TAG, "Free Storage size is " + Formatter.formatFileSize(this, availableBlocks * blockSize));
//        databaseWorker = new DatabaseWorker(this);
    }

    public void storeData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                databaseWorker.getDbHelper().dropTrigger();
                databaseWorker.getDbHelper().setTrigger();
                String c1 = "{\"name\":\"Accelerometer Sensor Channel 1\",\"description\":\"android.sensor.accelerometer.1\",\"guid\":\"05f88d8b-f03e-4e7d-b7e5-86b93a02af48\",\"uri\":\"Channel://android.sensor.accelerometer.channel1\",\"type\":\"OBJECT\",\"samplingDuration\":3000000,\"isEnabled\":true,\"data\":{\"dataType\":\"FLOAT\",\"timeStamp\":1480924550412,\"values\":\"QRsAKQ\\u003d\\u003d\"},\"nodes\":[]}";
                String c2 = "{\"name\":\"Accelerometer Sensor Channel 2\",\"description\":\"android.sensor.accelerometer.2\",\"guid\":\"05f88d8b-f03e-4e7d-b7e5-86b93a02af48\",\"uri\":\"Channel://android.sensor.accelerometer.channel2\",\"type\":\"OBJECT\",\"samplingDuration\":3000000,\"isEnabled\":true,\"data\":{\"dataType\":\"FLOAT\",\"timeStamp\":1480924550412,\"values\":\"QRsAKQ\\u003d\\u003d\"},\"nodes\":[]}";
                String c3 = "{\"name\":\"Accelerometer Sensor Channel 3\",\"description\":\"android.sensor.accelerometer.3\",\"guid\":\"05f88d8b-f03e-4e7d-b7e5-86b93a02af48\",\"uri\":\"Channel://android.sensor.accelerometer.channel3\",\"type\":\"OBJECT\",\"samplingDuration\":3000000,\"isEnabled\":true,\"data\":{\"dataType\":\"FLOAT\",\"timeStamp\":1480924550412,\"values\":\"QRsAKQ\\u003d\\u003d\"},\"nodes\":[]}";
                for (int i = 0; i < 30; i++) {
                    databaseWorker.insert(c1);
                    databaseWorker.insert(c2);
                    databaseWorker.insert(c3);
                }
                databaseWorker.getAllStoredChannels();
                databaseWorker.getItemCount();
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (button != null) {
            button.setEnabled(true);
        }
    }

    private void showData() {
        try {
            File logFile = new File(Environment.getExternalStorageDirectory(),
                    "ZetaSense/" + LoggingService.FILE_NAME);
            byte[] assetData = IOUtils.toByteArray(new FileInputStream(logFile));
            String text = new String(assetData);
            int start = text.length() - 10000;
            if (start < 0) {
                start = 0;
            }
            mTextView.setText(text.substring(start));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
