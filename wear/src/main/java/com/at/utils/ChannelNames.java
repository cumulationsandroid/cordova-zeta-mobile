package com.at.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kale on 13.7.2017.
 */
public class ChannelNames {

  protected Map<String, List> Channels = new HashMap<>();

  public ChannelNames() {
    Channels.put("android.sensor.accelerometer", Arrays.asList("x", "y", "z"));
    Channels.put("android.sensor.magnetic_field", Arrays.asList("x", "y", "z"));
    Channels.put("android.sensor.magnetic_field_uncalibrated", Arrays.asList("x_uncalib", "y_uncalib", "z_uncalib", "x_bias", "y_bias", "z_bias"));
    Channels.put("android.sensor.gyroscope", Arrays.asList("x", "y", "z"));
    Channels.put("android.sensor.gyroscope_uncalibrated", Arrays.asList("angular_speed_x", "angular_speed_y", "angular_speed_z", "drift_x", "drift_y", "drift_z"));
    Channels.put("android.sensor.pressure", Arrays.asList("pressure "));
    Channels.put("android.sensor.gravity", Arrays.asList("x", "y", "z"));
    Channels.put("android.sensor.linear_acceleration", Arrays.asList("x", "y", "z"));
    Channels.put("android.sensor.rotation_vector", Arrays.asList("x", "y", "z", "Angle", "Accuracy"));
    Channels.put("android.sensor.step_detector", Arrays.asList("x", "y", "z"));
    Channels.put("android.sensor.step_counter", Arrays.asList("Steps"));
    Channels.put("android.sensor.significant_motion", Arrays.asList("x", "y", "z"));
    Channels.put("android.sensor.game_rotation_vector", Arrays.asList("x", "y", "z", "Angle", "Accuracy"));
    Channels.put("android.sensor.geomagnetic_rotation_vector", Arrays.asList("x", "y", "z", "Angle", "Accuracy"));
    Channels.put("android.sensor.orientation", Arrays.asList("Azimuth", "Pitch", "Roll"));
    Channels.put("android.sensor.tilt_detector", Arrays.asList("x", "y", "z"));
    Channels.put("android.permission.BODY_SENSORS", Arrays.asList("x", "y", "z"));
    Channels.put("android.sensor.heart_rate", Arrays.asList("Heart_beat"));
    Channels.put("android.sensor.wrist_tilt_gesture", Arrays.asList("x", "y", "z"));
    Channels.put("android.sensor.proximity", Arrays.asList("proximity"));
    Channels.put("android.sensor.humidity", Arrays.asList("humidity"));
    Channels.put("android.sensor.ambient_temperature", Arrays.asList("temperature"));
    Channels.put("android.sensor.stationary_detect", Arrays.asList("stationary"));
    Channels.put("android.sensor.motion_detect", Arrays.asList("motion"));
    Channels.put("android.sensor.ambient_temperature", Arrays.asList("temperature"));
    Channels.put("android.sensor.heart_beat", Arrays.asList("heart_beat"));

  }

  public String getChannelName(String name, int key) {
    String result;
    try {
      result = Channels.get(name).get(key).toString();
    } catch (Exception e) {
      result = "Channel" + key;
    }
    return result;
  }


}
