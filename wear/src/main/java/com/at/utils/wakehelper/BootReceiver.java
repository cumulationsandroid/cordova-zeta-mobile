package com.at.utils.wakehelper;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.util.Log;

import com.at.utils.Constants;

import java.util.Calendar;


public class BootReceiver extends BroadcastReceiver {

    public static final String PREF_UPDATED_ON = "bootCompleteReceiver_lastTime";
    public static final String PREF_OFFSET = "bootCompleteReceiver_offset";
    public static final String PREF_STEP = "bootCompleteReceiver_lastData";
    public static final String PREF_NAME = "com.at.utils.wakehelper";
    public static final String ACTION_ALARM_MANAGER = "com.at.utils.wakehelper.alarm";
    public static final String ACTION_ALARM_MANAGER_END = "com.at.utils.wakehelper.alarm.end";
    private static final String TAG = "BootReceiver";
    private SharedPreferences sharedpreferences;
    private Handler handler;
    private String PREF_LAST_SHUTDOWN = "bootCompleteReceiver_last_shutdown";

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e("BootReceiver", "Receiver " + intent.getAction());

        if (!Constants.isHaveStepCounter(context)) {
            Log.e("BootReceiver", "Device doesn't have step counter");
            return;
        }

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED) ||
                intent.getAction().equals("android.intent.action.QUICKBOOT_POWERON")) {
            ZetaWakeLock.setUpAlarm(context);
            checkForSteps();
            return;
        }

        if (intent.getAction().equals(Intent.ACTION_DATE_CHANGED) ||
                intent.getAction().equals(Intent.ACTION_TIME_CHANGED) ||
                intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
            ZetaWakeLock.setUpAlarm(context);
            return;
        }

        sharedpreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        handler = new Handler();

        if (intent.getAction().equals(ACTION_ALARM_MANAGER) ||
                intent.getAction().equals(ACTION_ALARM_MANAGER_END)) {
            ZetaWakeLock.setUpAlarm(context);
            return;
        }

        if (intent.getAction().equals(Intent.ACTION_SHUTDOWN) ||
                intent.getAction().equals("android.intent.action.QUICKBOOT_POWEROFF")) {
            onShutDown(context);
            return;
        }

        Log.e(TAG, "NOT RESPONDED FOR ACTION : " + intent.getAction());
    }

    private void checkForSteps() {

    }

    private void onShutDown(final Context context) {

        sharedpreferences.edit().putLong(PREF_LAST_SHUTDOWN, System.currentTimeMillis()).commit();

        context.sendBroadcast(new Intent(context, SensorReadService.class).setAction(SensorReadService.ACTION_STOP_READING));

        final long lastTime = sharedpreferences.getLong(PREF_UPDATED_ON, 0);
        int offset = sharedpreferences.getInt(PREF_OFFSET, 0);
        final int lastValue = sharedpreferences.getInt(PREF_STEP, 0);

        Calendar lastDay = Calendar.getInstance();
        lastDay.setTimeInMillis(lastTime);

        Calendar today = Calendar.getInstance();
        int preDay = lastDay.get(Calendar.DAY_OF_MONTH);
        int currentDay = today.get(Calendar.DAY_OF_MONTH);

        Log.e(TAG, lastValue + " " + preDay + " " + offset);
//        LoggingService.d(context, TAG, "onShutdown --> " + lastValue + " " + preDay + " " + offset);


        if (currentDay == preDay && ZetaWakeLock.isWithin24Hour(lastTime, today.getTimeInMillis())) {
            // shutdown happening in same day
            offset = offset - lastValue;
            sharedpreferences.edit()
                    .putInt(PREF_OFFSET, offset)
                    .putLong(PREF_UPDATED_ON, System.currentTimeMillis())
                    .putInt(PREF_STEP, 0).apply();

            Log.e(TAG, "shutdown --->(of = of - pd)");
            Log.e(TAG, lastValue + " " + currentDay + " " + offset);
//            LoggingService.d(context, TAG, "pre calculation --> " + lastValue + " " + currentDay + " " + offset);

        } else {
            // shutdown happening in other day
            sharedpreferences.edit()
                    .putInt(PREF_OFFSET, 0)
                    .putLong(PREF_UPDATED_ON, 0)
                    .putInt(PREF_STEP, 0).apply();

            Log.e(TAG, "shutdown --->(reset all)");
            Log.e(TAG, 0 + " " + 0 + " " + 0);
//            LoggingService.d(context, TAG, "pre calculation --> " + 0 + " " + 0 + " " + 0);
        }

        final SensorManager sensorManager = (SensorManager) context.getSystemService(Activity.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        final SensorEventListener mSensorEvent = new SensorEventListener() {

            @Override
            public void onSensorChanged(SensorEvent event) {
                Log.e(TAG, "Step count : " + event.values[0]);
                sensorManager.unregisterListener(this);
                float value = event.values[0];

                int offset = sharedpreferences.getInt(PREF_OFFSET, 0);

                Calendar lastDay = Calendar.getInstance();
                lastDay.setTimeInMillis(lastTime);

                Calendar today = Calendar.getInstance();

                int preDay = lastDay.get(Calendar.DAY_OF_MONTH);
                int currentDay = today.get(Calendar.DAY_OF_MONTH);

                Log.e(TAG, lastValue + " " + preDay + " " + offset);

                offset = (int) (offset - (value - lastValue));

                sharedpreferences.edit()
                        .putInt(PREF_OFFSET, offset)
                        .putLong(PREF_UPDATED_ON, System.currentTimeMillis())
                        .putInt(PREF_STEP, 0).apply();

                Log.e(TAG, "update ----->(of = of - (nd - pd))");
                Log.e(TAG, 0 + " " + currentDay + " " + offset);
//                LoggingService.d(context, TAG, "post calculation --> " + 0 + " " + currentDay + " " + offset);
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        sensorManager.registerListener(mSensorEvent, sensor, SensorManager.SENSOR_DELAY_FASTEST, 0);
    }
}
