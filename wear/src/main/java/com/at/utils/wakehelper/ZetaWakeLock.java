package com.at.utils.wakehelper;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.at.utils.Constants;
import com.at.zeta.zetamobile.R;

import java.util.Calendar;

/**
 * Created by uday on 9/6/16.
 */
public class ZetaWakeLock {

    public static final int NOTIFICATION_ID = 82;
    public static final int ALARM_HOUR = 23;
    public static final int ALARM_MINUTE = 55;
    public static final int ALARM_MINUTE2 = 59;
    public static final long ALARM_INTERVAL = 30 * 60 * 1000;
    public static final String TAG = "ZetaWakeLock";
    private static final String NAME = "com.at.utils.wakehelper.ZetaWakeLock";
    private static PowerManager.WakeLock lockStatic;

    /**
     * This method will return the Singleton {@code PowerManager.WakeLock}
     *
     * @param context Context must be ApplicationContext
     * @return {@code PowerManager.WakeLock}
     */
    synchronized public static PowerManager.WakeLock getLock(Context context) {
        if (lockStatic == null) {
            PowerManager mgr = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            lockStatic = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, NAME);
            lockStatic.setReferenceCounted(true);
        }
        return (lockStatic);
    }

    public static Notification createNotification(Context context) {

        Intent viewIntent = new Intent(context, SensorReadService.class);
        viewIntent.setAction(SensorReadService.ACTION_FORCE_STOP_READING);
        viewIntent.putExtra(Constants.DATA,"notification");
        PendingIntent viewPendingIntent = PendingIntent.getService(context, 0, viewIntent, 0);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Zeta Sense")
                        .setContentText("Sensor measuring started")
                        .setOngoing(true)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .addAction(R.mipmap.ic_launcher, "Stop", viewPendingIntent);

        return notificationBuilder.build();
    }

    public static Notification createNotificationForStepCounter(Context context) {

        Intent viewIntent = new Intent(context, SensorReadService.class);
        viewIntent.setAction(BootReceiver.ACTION_ALARM_MANAGER_END);
        PendingIntent viewPendingIntent = PendingIntent.getService(context, 0, viewIntent, 0);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Zeta Sense")
                        .setContentText("Resetting StepCounter")
                        .setOngoing(true)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .addAction(R.mipmap.ic_launcher,
                                "Stop", viewPendingIntent);

        return notificationBuilder.build();
    }

    public static void cancelNotification(Context context) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    /**
     * Alarm is used to collect the step counter offset at the end of the day.<br>
     * Alarm is initiated from {@link BootReceiver} and {@link com.at.zeta.zetamobile.WearApplication}
     *
     * @param context Context of the Application
     */
    public static void setUpAlarm(Context context) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, ALARM_HOUR);
        calendar.set(Calendar.MINUTE, ALARM_MINUTE);
        calendar.set(Calendar.SECOND, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        PendingIntent pendingIntent = PendingIntent.getService(context
                , 1
                , new Intent(context, SensorReadService.class).setAction(BootReceiver.ACTION_ALARM_MANAGER)
                , PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP
                    , calendar.getTimeInMillis()
                    , pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP
                    , calendar.getTimeInMillis()
                    , pendingIntent);
        }

        // setUpAlarm2(context);

    }

    public static void setUpAlarm2(Context context) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, ALARM_HOUR);
        calendar.set(Calendar.MINUTE, ALARM_MINUTE2);
        calendar.set(Calendar.SECOND, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        PendingIntent pendingIntent = PendingIntent.getService(context
                , 2
                , new Intent(context, SensorReadService.class).setAction(BootReceiver.ACTION_ALARM_MANAGER_END)
                , PendingIntent.FLAG_UPDATE_CURRENT);

        if (System.currentTimeMillis() >= calendar.getTimeInMillis()) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP
                    , calendar.getTimeInMillis()
                    , pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP
                    , calendar.getTimeInMillis()
                    , pendingIntent);
        }

    }

    public static boolean isWithin24Hour(long date1, long date2) {
        try {
            long difference = Math.abs(date1 - date2);
            long dayDifference = difference / (24 * 60 * 60 * 1000);
            Log.e(TAG, "Day difference : " + dayDifference);
            if (dayDifference == 0) {
                return true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return false;
    }


}
