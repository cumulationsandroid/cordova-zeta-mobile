package com.at.utils.wakehelper;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.at.model.domain.Channel;
import com.at.model.domain.Sensor;
import com.at.model.meta.Data;
import com.at.model.meta.Node;
import com.at.utils.Constants;
import com.at.zeta.zetamobile.WearApplication;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.at.utils.wakehelper.BootReceiver.PREF_NAME;
import static com.at.utils.wakehelper.BootReceiver.PREF_OFFSET;
import static com.at.utils.wakehelper.BootReceiver.PREF_STEP;
import static com.at.utils.wakehelper.BootReceiver.PREF_UPDATED_ON;

/**
 * {@code SensorReadService} will register/unregister the sensor listeners and the sensor data will send to the Communication module
 */
public class SensorReadService extends Service {

  public WearApplication wearApplication;
  public static final String ACTION_START_READING = "com.at.utils.wakehelper.start";
  public static final String ACTION_STOP_READING = "com.at.utils.wakehelper.stop";
  private static final String TAG = "SensorReadService";
  public static final String ACTION_FORCE_STOP_READING = "com.at.utils.wakehelper.forecestop";

  private ArrayList<SensorListener> mSensorListenersList = new ArrayList<>();
  private IBinder mBinder = new LocalBinder();
  private SensorManager mSensorManager;
  private Node mDevice;
  private EventCallback eventCallback;
  private SharedPreferences sharedpreferences;
  private int offset;
  private int skipCount = 0;
  private String sessionID = "";

  @Override
  public IBinder onBind(Intent intent) {
    return mBinder;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    sharedpreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    wearApplication = (WearApplication) getApplicationContext();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    if (intent == null || intent.getAction() == null)
      return START_NOT_STICKY;
    if (intent.getAction().equals(ACTION_START_READING)) {
      sessionID = intent.getStringExtra(Constants.EXTRA_SESSIONID);
      startReadingData(sessionID);
    } else if (intent.getAction().equals(ACTION_STOP_READING)) {
      stopReadingData();
    } else if (intent.getAction().equals(ACTION_FORCE_STOP_READING)) {
      sendBroadcast(new Intent(Constants.ACTION_FORCE_STOP));
      stopReadingData();
    } else if (intent.getAction() != null && intent.getAction().equals(BootReceiver.ACTION_ALARM_MANAGER)) {
      Log.e(TAG, intent.getAction());
//            LoggingService.d(getApplicationContext(), TAG, "onAlarm wake call");
      checkStepCountSensor();
    } else if (intent.getAction() != null && intent.getAction().equals(BootReceiver.ACTION_ALARM_MANAGER_END)) {
      Log.e(TAG, intent.getAction());
      checkStepCountSensor2();
    }
        /*else if (intent.getStringExtra(EXTRA_ACTION).equals(ACTION_UPDATE_STEP_COUNT)) {
            isStepCounterRunning();
        }*/
    return START_NOT_STICKY;
  }

  private void checkStepCountSensor2() {
    if (!isStepCounterRunning()) {
      // foreground service is not running
      if (mSensorListenersList == null || mSensorListenersList.size() == 0) {
        onAlarmTriggered(this);
        stopReadingData();
      }
      sendBroadcast(new Intent(this, BootReceiver.class).setAction(BootReceiver.ACTION_ALARM_MANAGER_END));
    }
  }

  private void checkStepCountSensor() {
    // step counter is not enabled
    if (!isStepCounterRunning()) {
//             foreground service is not running
//            if (mSensorListenersList == null || mSensorListenersList.size() == 0) {
//                startForeground(ZetaWakeLock.NOTIFICATION_ID, ZetaWakeLock.createNotificationForStepCounter(this));
//            }
      onAlarmTriggered(this);
//            ZetaWakeLock.setUpAlarm2(this);
    }
  }

  /**
   * This will unregister the sensor listeners and stops the foreground service
   */
//    @SuppressLint("NewApi")
  public void stopReadingData() {
    if (eventCallback != null)
      eventCallback.onStop();
    for (int j = 0, size = mSensorListenersList.size(); j < size; j++) {
      SensorListener sensorListener = mSensorListenersList.get(j);
      int reportingMode;
      try {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
          reportingMode = sensorListener.hardSensor.getReportingMode();
        } else {
          reportingMode = 0;
        }
      } catch (Exception e) {
        e.printStackTrace();
        reportingMode = 0;
      }
      if (reportingMode == android.hardware.Sensor.REPORTING_MODE_ONE_SHOT) {
        mSensorManager.cancelTriggerSensor(sensorListener, sensorListener.hardSensor);
      } else {
        mSensorManager.unregisterListener(sensorListener);
      }
    }
    mSensorListenersList.clear();
    stopForeground(true);
    stopSelf();
  }

  /**
   * This will register the sensor listeners and starts the foreground service
   *
   * @param sessionID
   */
  public void startReadingData(String sessionID) {
    this.sessionID = sessionID;
    if (eventCallback != null)
      eventCallback.onStart();
    startForeground(ZetaWakeLock.NOTIFICATION_ID, ZetaWakeLock.createNotification(this));
    List<android.hardware.Sensor> hardSensorList = mSensorManager.getSensorList(android.hardware.Sensor.TYPE_ALL);
    if (hardSensorList != null && hardSensorList.size() > 0) {
      ArrayList<Sensor> mSensors = new ArrayList<>((ArrayList<Sensor>) mDevice.getNodes());
      for (int i = 0, count = hardSensorList.size(); i < count; i++) {
        android.hardware.Sensor hardSensor = hardSensorList.get(i);
        for (int j = 0; j < mSensors.size(); j++) {
          Sensor sensor = mSensors.get(j);
          if (sensor.getDescription().equalsIgnoreCase(hardSensor.getStringType())) {
            mSensors.set(j, registerForSensor(sensor, hardSensor));
            mDevice.setNodes(mSensors);
          }
        }
      }
    }
  }

  private Data getRefreshedData(Data actualData, long timestamp, byte[] values) {
    Data data = new Data();
    data.setDataType(actualData.getDataType());
    data.setTimeStamp(timestamp);
    data.setValues(values);
    if (values == null) {
      Log.d(TAG, "values is null");
    } else {
      Log.d(TAG, "Values is not null");
    }
    return data;
  }

  private Sensor registerForSensor(Sensor sensor, android.hardware.Sensor hardSensor) {
    int samplingTime = 0;
    int size = sensor.getNodes().size();
    for (int j = 0; j < size; j++) {
      Channel channel = (Channel) sensor.getNodes().get(j);
      if (samplingTime != 0) {
        if (channel.getSamplingDuration() < samplingTime) {
          samplingTime = channel.getSamplingDuration();
        }
      } else {
        samplingTime = channel.getSamplingDuration();
      }
    }
    ArrayList<Node> mChannelsList = (ArrayList<Node>) sensor.getNodes();
    for (int j = 0; j < size; j++) {
      Channel channel = (Channel) mChannelsList.get(j);
      if (channel.isEnabled()) {
        SensorListener sensorListener = new SensorListener(channel, samplingTime, hardSensor);
        int reportingMode;
        try {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            reportingMode = sensorListener.hardSensor.getReportingMode();
          } else {
            reportingMode = 0;
          }
        } catch (Exception e) {
          e.printStackTrace();
          reportingMode = 0;
        }
        if (reportingMode == android.hardware.Sensor.REPORTING_MODE_ONE_SHOT) {
          mSensorManager.requestTriggerSensor(sensorListener, sensorListener.hardSensor);
        } else {
          mSensorManager.registerListener(sensorListener, hardSensor, samplingTime, 0);
        }
        mSensorListenersList.add(sensorListener);
        Log.d(TAG, "!! Registering " + channel.getUri());
      }
    }
    return sensor;
  }

  public void setDevice(Node mDevice) {
    this.mDevice = mDevice;
  }

  public void setEventCallback(EventCallback eventCallback) {
    this.eventCallback = eventCallback;
  }

  private float updateStepCount(float value) {

    long lastTime = sharedpreferences.getLong(PREF_UPDATED_ON, 0);
    int offset = sharedpreferences.getInt(PREF_OFFSET, 0);
    int lastValue = sharedpreferences.getInt(PREF_STEP, 0);
    WearApplication wearApplication = ((WearApplication) this.getApplication());


    Calendar lastDay = Calendar.getInstance();
    lastDay.setTimeInMillis(lastTime);

    Calendar today = Calendar.getInstance();

    int preDay = lastDay.get(Calendar.DAY_OF_MONTH);
    int currentDay = today.get(Calendar.DAY_OF_MONTH);

    boolean isStepCountOK;
    try {
      isStepCountOK = Constants.isStepCountOK(lastDay.getTimeInMillis(), today.getTimeInMillis(), lastValue, (int) value);
    } catch (Exception e) {
      e.printStackTrace();
      isStepCountOK = true;
    }

    if (!isStepCountOK) {
      if (skipCount <= Constants.MAX_SKIP) {
        skipCount++;
        return 0;
      }
    }

    skipCount = 0;

    if (value < offset) {
      sharedpreferences.edit()
        .putInt(PREF_OFFSET, 0)
        .putLong(PREF_UPDATED_ON, wearApplication.getCurrentTime())
        .putInt(PREF_STEP, (int) value).apply();
    }

    Log.e(TAG, "Step count accuracy " + isStepCountOK);

    Log.e(TAG, lastValue + " " + preDay + " " + offset);

    // current day data
    if (currentDay == preDay && ZetaWakeLock.isWithin24Hour(lastTime, today.getTimeInMillis())) {

      float newData = Math.abs(value - offset);

      sharedpreferences.edit()
        .putInt(PREF_OFFSET, offset)
        .putLong(PREF_UPDATED_ON, wearApplication.getCurrentTime())
        .putInt(PREF_STEP, (int) value).apply();

      Log.e(TAG, ((int) value) + " " + currentDay + " " + offset);
      Log.e(TAG, "-----> " + newData);
//            LoggingService.d(getApplicationContext(), TAG, "calculated --> " + ((int) value) + " " + currentDay + " " + offset);
      return newData;
    } else if (today.getTimeInMillis() > lastTime) {

      offset = lastValue;
      float newData = Math.abs(value - offset);

      sharedpreferences.edit()
        .putInt(PREF_OFFSET, offset)
        .putLong(PREF_UPDATED_ON, wearApplication.getCurrentTime())
        .putInt(PREF_STEP, (int) value).apply();

      Log.e(TAG, ((int) value) + " " + currentDay + " " + offset);
      Log.e(TAG, "-----> " + newData);
//            LoggingService.d(getApplicationContext(), TAG, "calculated --> " + ((int) value) + " " + currentDay + " " + offset);
      return newData;
    } else {

      offset = lastValue;
      float newData = Math.abs(value - offset);

      sharedpreferences.edit()
        .putInt(PREF_OFFSET, offset)
        .putLong(PREF_UPDATED_ON, wearApplication.getCurrentTime())
        .putInt(PREF_STEP, (int) value).apply();

      Log.e(TAG, ((int) value) + " " + currentDay + " " + offset);
      Log.e(TAG, "-----> " + newData);
//            LoggingService.d(getApplicationContext(), TAG, "calculated --> " + ((int) value) + " " + currentDay + " " + offset);
      return newData;
    }
  }

  private boolean isStepCounterRunning() {
    if (mSensorListenersList != null && mSensorListenersList.size() > 0) {
      for (int i = 0; i < mSensorListenersList.size(); i++) {
        if (mSensorListenersList.get(i).hardSensor.getStringType().equals(android.hardware.Sensor.STRING_TYPE_STEP_COUNTER)) {
          // Step sensor is already running
          return true;
        }
      }
    }
    return false;
  }

  private void onAlarmTriggered(Context context) {
    final WearApplication wearApplication = ((WearApplication) this.getApplication());
    final SensorManager sensorManager = (SensorManager) context.getSystemService(Activity.SENSOR_SERVICE);
    android.hardware.Sensor sensor = sensorManager.getDefaultSensor(android.hardware.Sensor.TYPE_STEP_COUNTER);
    final EventListener mSensorEvent = new EventListener() {

      public Runnable runnable;
      public Handler handler;

      @Override
      public void onSensorChanged(SensorEvent event) {
        onEvent(event.values[0]);
      }

      @Override
      public void setRemover() {
        final EventListener listener = this;
        handler = new Handler();
        runnable = new Runnable() {
          @Override
          public void run() {
            listener.onEvent(0f);
          }
        };
        handler.postDelayed(runnable, 500);
      }

      @Override
      public void cancelRemover() {
        if (runnable != null && handler != null)
          handler.removeCallbacks(runnable);
      }

      @Override
      public void onEvent(float value) {

        cancelRemover();
//                LoggingService.d(getApplicationContext(), TAG, "rawData --> " + value);

        Log.e(TAG, "step count " + value);
        sensorManager.unregisterListener(this);

        long lastTime = sharedpreferences.getLong(PREF_UPDATED_ON, 0);
        int offset = sharedpreferences.getInt(PREF_OFFSET, 0);
        int lastValue = sharedpreferences.getInt(PREF_STEP, 0);

        Calendar lastDay = Calendar.getInstance();
        lastDay.setTimeInMillis(lastTime);

        Calendar today = Calendar.getInstance();

        int preDay = lastDay.get(Calendar.DAY_OF_MONTH);
        int currentDay = today.get(Calendar.DAY_OF_MONTH);

        Log.e(TAG, lastValue + " " + preDay + " " + offset);
//                LoggingService.d(getApplicationContext(), TAG, "before calculate --> " + lastValue + " " + preDay + " " + offset);

        if (currentDay == preDay && ZetaWakeLock.isWithin24Hour(lastTime, today.getTimeInMillis())) {

          offset = (int) value;
          sharedpreferences.edit()
            .putInt(PREF_OFFSET, offset)
            .putLong(PREF_UPDATED_ON, wearApplication.getCurrentTime())
            .putInt(PREF_STEP, (int) value).apply();
          Log.e(TAG, ((int) value) + " " + currentDay + " " + offset);
//                    LoggingService.d(getApplicationContext(), TAG, "after calculate --> " + ((int) value) + " " + currentDay + " " + offset);

        } else if (today.getTimeInMillis() > lastTime) {

          // went to feature date
          offset = lastValue;
          sharedpreferences.edit()
            .putInt(PREF_OFFSET, offset)
            .putLong(PREF_UPDATED_ON, wearApplication.getCurrentTime())
            .putInt(PREF_STEP, (int) value).apply();
          Log.e(TAG, ((int) value) + " " + currentDay + " " + offset);
//                    LoggingService.d(getApplicationContext(), TAG, "after calculate --> " + ((int) value) + " " + currentDay + " " + offset);

        } else {

          // went to past date
          offset = lastValue;
          sharedpreferences.edit()
            .putInt(PREF_OFFSET, offset)
            .putLong(PREF_UPDATED_ON, wearApplication.getCurrentTime())
            .putInt(PREF_STEP, (int) value).apply();

          Log.e(TAG, ((int) value) + " " + currentDay + " " + offset);
//                    LoggingService.d(getApplicationContext(), TAG, "after calculate --> " + ((int) value) + " " + currentDay + " " + offset);

        }
      }

      @Override
      public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {

      }
    };
    sensorManager.registerListener(mSensorEvent, sensor, SensorManager.SENSOR_DELAY_FASTEST, 0);
    mSensorEvent.setRemover();

  }

  public interface EventCallback {
    void onStart();

    void onData(Node channel);

    void onStop();
  }

  /**
   * {@code LocalBinder} is used to bind the service from outside
   */
  public class LocalBinder extends Binder {
    public SensorReadService getService() {
      return SensorReadService.this;
    }
  }

  public class SensorListener extends TriggerEventListener implements SensorEventListener {

    private final android.hardware.Sensor hardSensor;
    private Channel mChannel;
    private long originalSamplingTime;
    private int sampledTimes;

    public SensorListener(Channel channel, long originalSamplingTime, android.hardware.Sensor hardSensor) {
      mChannel = channel;
      this.originalSamplingTime = originalSamplingTime;
      this.hardSensor = hardSensor;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

      if (mChannel.getDescription().startsWith(android.hardware.Sensor.STRING_TYPE_STEP_COUNTER)) {
        float[] channelValues = event.values.clone();
        String msg = "rawData --> " + channelValues[0] + "\naccuracy - " + event.accuracy + "\ntimestamp - " + event.timestamp;
//                LoggingService.d(getApplicationContext(), TAG, msg);
        Log.e(TAG, "Step count - " + event.values[0]);
        event.values[0] = updateStepCount(event.values[0]);
        if (event.values[0] < 1) {
          Log.e(TAG, "calculated step count is - " + event.values[0]);
          return;
        }
      }

      String msg = mChannel.getDescription() + "\n" + wearApplication.getCurrentTime();
//            LoggingService.d(getApplicationContext(), TAG, msg);

      sampledTimes++;
      //if (sampledTimes % (mChannel.getSamplingDuration() / originalSamplingTime) == 0) {
      if ((mChannel.getData().getTimeStamp() + (mChannel.getSamplingDuration() / 1000)) <= wearApplication.getCurrentTime()) {
        float[] channelValues = event.values.clone();

//                if (mChannel.getDescription().startsWith(android.hardware.Sensor.STRING_TYPE_STEP_COUNTER)) {
//                    LoggingService.d(getApplicationContext(), TAG, "sending --> " + channelValues[0]);
//                }

        //Log.d(TAG, event.sensor.getStringType());
        String channelDescription = mChannel.getDescription();
        if (channelDescription.contains("||")) {
          channelDescription = channelDescription.split("\\|\\|")[1];
        }
        if (sessionID != null && sessionID.length() > 0) {
          channelDescription = sessionID + "||" + channelDescription;
        }
        mChannel.setDescription(channelDescription);

        int channelLevel = Integer.parseInt("" + channelDescription.charAt(channelDescription.length() - 1));
        mChannel.setData(getRefreshedData(mChannel.getData(), wearApplication.getCurrentTime(), ByteBuffer.allocate(4).putFloat(channelValues[channelLevel - 1]).array()));
        //Log.d(TAG, "!! SAMPLED " + mChannel.getUri() + " - Timestamp " + event.timestamp);
        //iSensorAgent.dataReadyInternal(mChannel);
        if (eventCallback != null)
          eventCallback.onData(mChannel);
      } else {
//                LoggingService.d(getApplicationContext(), "ignored", nptClient.getCorrentTime() + "&" + mChannel.getName());
      }
    }

    @Override
    public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {

    }

    @Override
    public void onTrigger(TriggerEvent event) {
      sampledTimes++;
      //if (sampledTimes % (mChannel.getSamplingDuration() / originalSamplingTime) == 0) {
      if ((mChannel.getData().getTimeStamp() + (mChannel.getSamplingDuration() / 1000)) <= wearApplication.getCurrentTime()) {
        float[] channelValues = event.values.clone();
        //Log.d(TAG, event.sensor.getStringType());
        String channelDescription = mChannel.getDescription();

        if (channelDescription.contains("||")) {
          channelDescription = channelDescription.split("\\|\\|")[1];
        }
        if (sessionID != null && sessionID.length() > 0) {
          channelDescription = sessionID + "||" + channelDescription;
        }
        mChannel.setDescription(channelDescription);

        int channelLevel = Integer.parseInt("" + channelDescription.charAt(channelDescription.length() - 1));
        mChannel.setData(getRefreshedData(mChannel.getData(), wearApplication.getCurrentTime(), ByteBuffer.allocate(4).putFloat(channelValues[channelLevel - 1]).array()));
        //Log.d(TAG, "!! SAMPLED " + mChannel.getUri() + " - Timestamp " + event.timestamp);
        if (eventCallback != null)
          eventCallback.onData(mChannel);      }
      mSensorManager.requestTriggerSensor(this, hardSensor);
    }
  }

  public class EventListener implements SensorEventListener {

    @Override
    public void onSensorChanged(android.hardware.SensorEvent event) {

    }

    public void onEvent(float e) {

    }

    public void setRemover() {

    }

    public void cancelRemover() {

    }

    @Override
    public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {

    }
  }
}
