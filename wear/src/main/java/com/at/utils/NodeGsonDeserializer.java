package com.at.utils;

import android.net.Uri;
import android.util.Base64;

import com.at.model.app.SensorAgent;
import com.at.model.domain.Channel;
import com.at.model.domain.Device;
import com.at.model.domain.Sensor;
import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;
import com.at.model.meta.NodeType;
import com.at.model.meta.Notification;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.UUID;

public class NodeGsonDeserializer implements JsonDeserializer {

    private static final String TAG = NodeGsonDeserializer.class.getSimpleName();

    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        String className = jsonObject.getAsJsonPrimitive("uri").getAsString();
        Uri uri = Uri.parse(className);
        //Log.d(TAG, "" + uri);
        JsonArray jsonArray = jsonObject.getAsJsonArray("nodes");
        if (uri.getScheme().equalsIgnoreCase(Channel.class.getSimpleName())) {
            Channel channel = new Channel();
            channel = (Channel) getNodeData(channel, jsonObject);
            return channel;
        } else if (uri.getScheme().equalsIgnoreCase(SensorAgent.class.getSimpleName())) {
            Node sensorAgent = new Node();
            sensorAgent = getNodeData(sensorAgent, jsonObject);
            ArrayList<Node> nodes = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                Node node = context.deserialize(jsonArray.get(i), Node.class);
                nodes.add(node);
            }
            sensorAgent.setNodes(nodes);
            return sensorAgent;
        } else if (uri.getScheme().equalsIgnoreCase(Sensor.class.getSimpleName())) {
            Sensor sensor = new Sensor();
            sensor = (Sensor) getNodeData(sensor, jsonObject);
            ArrayList<Channel> channels = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                Channel channel = context.deserialize(jsonArray.get(i), Channel.class);
                channels.add(channel);
            }
            sensor.setNodes(channels);
            return sensor;
        } else if (uri.getScheme().equalsIgnoreCase(Device.class.getSimpleName())) {
            Device device = new Device();
            device = (Device) getNodeData(device, jsonObject);
            ArrayList<Node> nodes = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                Node node = context.deserialize(jsonArray.get(i), Node.class);
                nodes.add(node);
            }
            device.setNodes(nodes);
            return device;
        } else if (uri.getScheme().equalsIgnoreCase(Notification.class.getSimpleName())) {
            Notification notification = new Notification();
            notification = (Notification) getNodeData(notification, jsonObject);
            return notification;
        } else if (uri.getScheme().equalsIgnoreCase("Method")) {
            Node nodeMethod = new Node();
            nodeMethod = getNodeData(nodeMethod, jsonObject);
            ArrayList<Node> nodes = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                Node node = context.deserialize(jsonArray.get(i), Node.class);
                nodes.add(node);
            }
            nodeMethod.setNodes(nodes);
            return nodeMethod;
        } else if (uri.getScheme().equalsIgnoreCase("Parameter")) {
            Node nodeParameter = new Node();
            nodeParameter = getNodeData(nodeParameter, jsonObject);
            return nodeParameter;
        }
        return context.deserialize(json, typeOfT);
    }


    private Node getNodeData(Node node, JsonObject jsonObject) {
        node.setName(jsonObject.getAsJsonPrimitive("name").getAsString());
        node.setDescription(jsonObject.getAsJsonPrimitive("description").getAsString());
        node.setGuid(UUID.fromString(jsonObject.getAsJsonPrimitive("guid").getAsString()));
        node.setUri(jsonObject.getAsJsonPrimitive("uri").getAsString());
        node.setType(NodeType.valueOf(jsonObject.getAsJsonPrimitive("type").getAsString()));
        if (jsonObject.getAsJsonPrimitive("samplingDuration") != null) {
            Channel channel = (Channel) node;
            channel.setSamplingDuration(jsonObject.getAsJsonPrimitive("samplingDuration").getAsInt());
            channel.setIsEnabled(jsonObject.getAsJsonPrimitive("isEnabled").getAsBoolean());
            node = channel;
        }
        JsonObject dataObject = jsonObject.getAsJsonObject("data");
        if (dataObject != null) {
            Data data = new Data();
            if (dataObject.getAsJsonPrimitive("dataType") != null) {
                data.setDataType(DataType.valueOf(dataObject.getAsJsonPrimitive("dataType").getAsString()));
            }
            if (dataObject.getAsJsonPrimitive("timeStamp") != null) {
                data.setTimeStamp(dataObject.getAsJsonPrimitive("timeStamp").getAsLong());
            }
            if (dataObject.getAsJsonPrimitive("values") != null) {
                data.setValues(Base64.decode(dataObject.get("values").getAsString(), Base64.NO_WRAP));
            }
            node.setData(data);
        }
        return node;
    }

}
