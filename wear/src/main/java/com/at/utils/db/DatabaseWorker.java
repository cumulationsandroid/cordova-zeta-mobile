package com.at.utils.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteFullException;
import android.database.sqlite.SQLiteOutOfMemoryException;
import android.util.Log;

import java.util.ArrayList;

/**
 * {@code DatabaseWorker} is use to do the io operation in {@link SQLiteDatabase}
 */
public class DatabaseWorker {

    private static final String TAG = DatabaseWorker.class.getSimpleName();
    private DataBaseHelper dbHelper;

    public DatabaseWorker(Context context) {
        dbHelper = new DataBaseHelper(context);
    }

    public long insert(String value) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TableChannelStore.KEY_CHANNEL, value);
        long rowID = -1;
        try {
            rowID = db.insertOrThrow(TableChannelStore.CHANNEL_TABLE_NAME, null, values);
        } catch (Exception e) {
            if (e instanceof SQLiteOutOfMemoryException || e instanceof SQLiteFullException) {
                deleteFirstRow();
                rowID = db.insert(TableChannelStore.CHANNEL_TABLE_NAME, null, values);
            }
            e.printStackTrace();
        }
        Log.e("DATABASE", "Inserted row id is : " + rowID);
        return rowID;
    }

    public String getChannelRowId(int rowID) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT  " +
                TableChannelStore.KEY_ID +
                ", " +
                TableChannelStore.KEY_CHANNEL +
                " FROM " +
                TableChannelStore.CHANNEL_TABLE_NAME +
                " WHERE " +
                TableChannelStore.KEY_ID +
                "=?";

        Cursor cursor = db.rawQuery(selectQuery, new String[]{String.valueOf(rowID)});
        String channelValue = "";
        int retrievedRowID = 0;
        if (cursor.moveToFirst()) {
            do {
                retrievedRowID = cursor.getInt(cursor.getColumnIndex(TableChannelStore.KEY_ID));
                channelValue = cursor.getString(cursor.getColumnIndex(TableChannelStore.KEY_CHANNEL));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return channelValue;
    }

    public ArrayList<String> getAllStoredChannels() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT  " +
                TableChannelStore.KEY_ID +
                ", " +
                TableChannelStore.KEY_CHANNEL +
                " FROM " +
                TableChannelStore.CHANNEL_TABLE_NAME;

        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<String> channels = new ArrayList<String>();
        if (cursor.moveToFirst()) {
            do {
                int retrievedRowID = cursor.getInt(cursor.getColumnIndex(TableChannelStore.KEY_ID));
                Log.e(TAG, "row id is " + retrievedRowID);
                String channelValue = cursor.getString(cursor.getColumnIndex(TableChannelStore.KEY_CHANNEL));
                channels.add(channelValue);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return channels;
    }

    public void deleteAllRow() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("delete from " + TableChannelStore.CHANNEL_TABLE_NAME);
        dbHelper.resetSqlSequence();
    }

    public void deleteRow(int rowID) {
        dbHelper.getWritableDatabase().delete(TableChannelStore.CHANNEL_TABLE_NAME, TableChannelStore.KEY_ID + "=" + rowID, null);
    }

    public String getFirstRow() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM " +
                TableChannelStore.CHANNEL_TABLE_NAME + " ORDER BY " +
                TableChannelStore.KEY_ID + " ASC LIMIT 1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        String channelValue = "";
        if (cursor.moveToFirst()) {
            do {
                int retrievedRowID = cursor.getInt(cursor.getColumnIndex(TableChannelStore.KEY_ID));
                Log.d(TAG, "get first row id is - " + retrievedRowID);
                channelValue = cursor.getString(cursor.getColumnIndex(TableChannelStore.KEY_CHANNEL));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return channelValue;
    }

    public String getLastRow() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM " +
                TableChannelStore.CHANNEL_TABLE_NAME + " ORDER BY " +
                TableChannelStore.KEY_ID + " DESC LIMIT 1";
        Cursor cursor = db.rawQuery(selectQuery, null);
        String channelValue = "";
        if (cursor.moveToNext()) {
            int retrievedRowID = cursor.getInt(cursor.getColumnIndex(TableChannelStore.KEY_ID));
            Log.d(TAG, "get row id is - " + retrievedRowID);
            channelValue = cursor.getString(cursor.getColumnIndex(TableChannelStore.KEY_CHANNEL));
        }
        cursor.close();
        return channelValue;
    }

    public void deleteFirstRow() {
        String selectQuery = "DELETE FROM " +
                TableChannelStore.CHANNEL_TABLE_NAME + " WHERE " +
                TableChannelStore.KEY_ID + " == (select " + TableChannelStore.KEY_ID + " from " + TableChannelStore.CHANNEL_TABLE_NAME + ""
                + " ORDER BY " + TableChannelStore.KEY_ID + " ASC LIMIT 1" + ")";
        dbHelper.getWritableDatabase().execSQL(selectQuery);
    }

    public void deleteLastRow() {
        String selectQuery = "DELETE FROM " +
                TableChannelStore.CHANNEL_TABLE_NAME + " WHERE " +
                TableChannelStore.KEY_ID + " == (select " + TableChannelStore.KEY_ID + " from " + TableChannelStore.CHANNEL_TABLE_NAME + ""
                + " ORDER BY " + TableChannelStore.KEY_ID + " DESC LIMIT 1" + ")";
        dbHelper.getWritableDatabase().execSQL(selectQuery);
    }

    public DataBaseHelper getDbHelper() {
        return dbHelper;
    }

    public boolean isEmpty() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String count = "SELECT COUNT(*) FROM " + TableChannelStore.CHANNEL_TABLE_NAME;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        mcursor.close();
        boolean result = !(icount > 0);
        if (result) {
            dbHelper.resetSqlSequence();
        }
        return result;
    }

    public void getItemCount() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String count = "SELECT COUNT(*) FROM " + TableChannelStore.CHANNEL_TABLE_NAME;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        mcursor.close();
        Log.e(TAG, "item count is " + icount);
    }
}
