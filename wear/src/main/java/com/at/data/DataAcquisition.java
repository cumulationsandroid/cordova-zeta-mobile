package com.at.data;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.at.model.app.IDataReadyInternal;
import com.at.model.app.INotify;
import com.at.model.app.ISensorState;
import com.at.model.app.States;
import com.at.model.domain.Channel;
import com.at.model.domain.Device;
import com.at.model.domain.Sensor;
import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;
import com.at.model.meta.NodeType;
import com.at.utils.Constants;
import com.at.utils.NodeGsonDeserializer;
import com.at.utils.NodeGsonSerializer;
import com.at.utils.wakehelper.SensorReadService;
import com.at.utils.ChannelNames;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DataAcquisition {

    private static final String TAG = DataAcquisition.class.getSimpleName();
    private final SensorManager mSensorManager;
    private final ISensorState iSensorStates;
    private Device mDevice;
    private IDataReadyInternal iSensorAgent;
    private INotify mINotify;
    private SharedPreferences mSharedPrefs;
    private Context mContext;
    private String mDeviceId;
    private int mTotalRegistered = 0;
    private SensorReadService mService;
    private String sessionID = "";
    private SensorReadService.EventCallback eventCallback = new SensorReadService.EventCallback() {
        @Override
        public void onStart() {

        }

        @Override
        public void onData(Node channel) {
            iSensorAgent.dataReadyInternal(channel);
        }

        @Override
        public void onStop() {
            try {
                if (mService != null)
                    mContext.unbindService(myConnection);
            } catch (Exception e) {
                e.printStackTrace();
            }
            iSensorStates.setState(States.READY);
        }
    };
    private ServiceConnection myConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            SensorReadService.LocalBinder binder = (SensorReadService.LocalBinder) service;
            mService = binder.getService();
            mService.setEventCallback(eventCallback);
            mService.setDevice(mDevice);
            mService.startReadingData(sessionID);
        }

        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
        }

    };

    public DataAcquisition(Context context, INotify iNotify, IDataReadyInternal sensorAgent) {
        mDeviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mContext = context;
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        iSensorAgent = sensorAgent;
        iSensorStates = (ISensorState) sensorAgent;
        mINotify = iNotify;
        populatedDevice();
    }

    private String getCommonLiteral() {
        return Constants.SCHEME_IDENTIFIER + mDeviceId + "" + "." + Build.MANUFACTURER + Build.PRODUCT;
    }

    private Device initDevice() {
        Device device = new Device();
        device.setDescription("This is " + Build.MANUFACTURER + " " + Build.PRODUCT);
        device.setData(new Data());
        device.setUri(Device.class.getSimpleName() + getCommonLiteral());
        device.setType(NodeType.OBJECT);
        device.setGuid(UUID.randomUUID());
        device.setName(Build.MANUFACTURER + " " + Build.PRODUCT);
        return device;
    }

    private void populatedDevice() {
        if (TextUtils.isEmpty(mSharedPrefs.getString(Constants.DEVICE_SERIALIZED_DATA_OPERATIONAL, ""))) {
            mDevice = initDevice();
            List<android.hardware.Sensor> hardSensorList = mSensorManager.getSensorList(android.hardware.Sensor.TYPE_ALL);
            Log.d(TAG, "Sensor size " + hardSensorList.size());

            if (hardSensorList != null && hardSensorList.size() > 0) {

                // doing some reflection to get maximum output value length of the Sensor
                Method[] methodList = android.hardware.Sensor.class.getDeclaredMethods();
                int m_count = methodList.length;
                for (int j = 0; j < m_count; j++) {
                    Method method = methodList[j];
                    method.setAccessible(true);

                    //got that method
                    if (method.getName().equals("getMaxLengthValuesArray")) {

                        List<Sensor> sensorList = (List<Sensor>) mDevice.getNodes();

                        for (int i = 0, count = hardSensorList.size(); i < count; i++) {
                            android.hardware.Sensor hardSensor = hardSensorList.get(i);
                            Sensor mSensor = getMySensor(hardSensor);
                            if (!TextUtils.isEmpty(hardSensor.getStringType())) {
                                try {
                                    int values_length = (Integer) method.invoke(hardSensor, hardSensor, Build.VERSION.SDK_INT);
                                    mSensor.setNodes((ArrayList<? extends Node>) populateChannel(values_length, mSensor, hardSensor.getStringType() ));
                                    sensorList.add(mSensor);
                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                } catch (InvocationTargetException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        Log.d(TAG, "SensorList size " + sensorList.size());
                        mDevice.setNodes((ArrayList<? extends Node>) sensorList);
                        saveLocally();
                        break;
                    }
                }
            }
        } else {
            String serializedDeviceData = mSharedPrefs.getString(Constants.DEVICE_SERIALIZED_DATA_OPERATIONAL, "");
            logString(serializedDeviceData);
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Node.class, new NodeGsonDeserializer());
            Gson gsonExt = builder.create();
            mDevice = gsonExt.fromJson(serializedDeviceData, Device.class);
            String deviceJson = new Gson().toJson(mDevice);
            logString("Converted Device" + deviceJson);
        }

    }

    private List<Channel> populateChannel(int values_length, Sensor sensor, String type) {
        List<Channel> channelList = new ArrayList<>();
        ChannelNames channelNames = new ChannelNames();
        for (int i = 0, size = values_length; i < size; i++) {
            Channel channel = new Channel();
            //channel.setName(sensor.getName() + " Channel " + (i + 1));
            channel.setName(sensor.getName() + "||"+ channelNames.getChannelName(type, i));
            channel.setDescription(sensor.getDescription() + "." + (i + 1));
            //channel.setUri(Channel.class.getSimpleName() + Constants.SCHEME_IDENTIFIER + sensor.getDescription() + ".channel" + (i + 1));
            channel.setUri(Channel.class.getSimpleName() + Constants.SCHEME_IDENTIFIER + sensor.getDescription() + "." + channelNames.getChannelName(type, i));
            channel.setGuid(UUID.randomUUID());
            channel.setType(NodeType.OBJECT);
            channel.setSamplingDuration(Constants.DEFAULT_SAMPLING_DURATION);
            channel.setIsEnabled(Constants.DEFAULT_ENABLED_STATE);
            Data data = new Data();
            data.setDataType(DataType.FLOAT);
            channel.setData(data);
            channelList.add(channel);
        }
        Log.e("Sensor added", "senor " + sensor.getDescription());
        Log.d(TAG, "Sampled for " + sensor.getDescription() + " size " + values_length);
        return channelList;
    }

    synchronized private void saveLocally() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
        Gson gsonExt = builder.create();
        String deviceJson = gsonExt.toJson(mDevice);
        logString(deviceJson);
        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putString(Constants.DEVICE_SERIALIZED_DATA_DEFAULT, deviceJson);
        editor.putString(Constants.DEVICE_SERIALIZED_DATA_OPERATIONAL, deviceJson);
        editor.apply();
        Log.d(TAG, "Saved");
    }

    private void logString(String data) {
        if (data.length() > 4000) {
            Log.v(TAG, "sb.length = " + data.length());
            int chunkCount = data.length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= data.length()) {
                    Log.v(TAG, data.substring(4000 * i));
                } else {
                    Log.v(TAG, data.substring(4000 * i, max));
                }
            }
        } else {
            Log.v(TAG, data);
        }
    }

    public Device getDevice() {
        return mDevice;
    }

    public void setDevice(Device device) {
        mDevice = roundOffSampling(device);

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
        Gson gsonExt = builder.create();

        String deviceJson = gsonExt.toJson(mDevice);
        logString(deviceJson);
        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putString(Constants.DEVICE_SERIALIZED_DATA_OPERATIONAL, deviceJson);
        editor.apply();
        logString("!! Set the device " + deviceJson);
    }

    public IDataReadyInternal getSensorAgent() {
        return iSensorAgent;
    }

    public void setSensorAgent(IDataReadyInternal iSensorAgent) {
        this.iSensorAgent = iSensorAgent;
    }

    public void start(Node node) {
        try {
            byte[] bytes = node.getData().getValues();
            sessionID = new String(bytes);
        } catch (Exception e) {
            e.printStackTrace();
            sessionID = null;
        }
        Intent intent = new Intent(mContext, SensorReadService.class);
        mContext.bindService(intent, myConnection, Context.BIND_AUTO_CREATE | Context.BIND_WAIVE_PRIORITY);
    }

    public Device roundOffSampling(Device device) {
        ArrayList<Sensor> mSensors = (ArrayList<Sensor>) device.getNodes();
        for (int i = 0; i < mSensors.size(); i++) {
            Sensor sensor = mSensors.get(i);
            int samplingTime = 0;
            int size = sensor.getNodes().size();
            for (int j = 0; j < size; j++) {
                Channel channel = (Channel) sensor.getNodes().get(j);
                if (samplingTime != 0) {
                    if (channel.getSamplingDuration() < samplingTime) {
                        samplingTime = channel.getSamplingDuration();
                    }
                } else {
                    samplingTime = channel.getSamplingDuration();
                }
            }
            Log.d(TAG, "!! Least time " + samplingTime);
            ArrayList<Node> mChannelsList = (ArrayList<Node>) sensor.getNodes();
            for (int j = 0; j < size; j++) {
                Channel channel = (Channel) mChannelsList.get(j);
                int channelSamplingDuration = channel.getSamplingDuration();
                int changedSamplingTime = channelSamplingDuration - (channelSamplingDuration % samplingTime);
                channel.setSamplingDuration(changedSamplingTime);
                Log.d(TAG, "!! Original Time " + channelSamplingDuration + " Changed time " + changedSamplingTime + " URI " + channel.getUri());
                mChannelsList.set(j, channel);
            }
            sensor.setNodes(mChannelsList);
            mSensors.set(i, sensor);
        }
        device.setNodes(mSensors);
        return device;
    }

    public void stop() {
        Intent intent = new Intent(mContext, SensorReadService.class);
        intent.setAction(SensorReadService.ACTION_STOP_READING);
        mContext.startService(intent);
    }

    private Sensor getMySensorFromHardware(String type) {
        if (mDevice != null) {
            ArrayList<Sensor> mySensorList = (ArrayList<Sensor>) ((ArrayList<?>) mDevice.getNodes());
            if (mySensorList != null) {
                for (int i = 0, count = mySensorList.size(); i < count; i++) {
                    Sensor sensor = (mySensorList.get(i));
                    if (sensor != null && type.equalsIgnoreCase(sensor.getDescription())) {
                        return sensor;
                    }
                }
            }
        }
        return null;
    }

    private Sensor getMySensor(android.hardware.Sensor hardSensor) {
        Sensor mySensor = new Sensor();
        mySensor.setName(hardSensor.getName());
        mySensor.setDescription(hardSensor.getStringType());
        mySensor.setData(new Data());
        mySensor.setGuid(UUID.randomUUID());
        mySensor.setType(NodeType.OBJECT);
        mySensor.setUri(Sensor.class.getSimpleName() + Constants.SCHEME_IDENTIFIER + hardSensor.getStringType());
        return mySensor;
    }

}
