package com.at.data;

import android.content.Context;

import com.at.model.app.IDataReadyInternal;
import com.at.model.app.INotify;
import com.at.model.domain.Device;
import com.at.model.meta.Node;

/**
 * Created by mathan on 4/12/15.
 */
public class ProxyDataAcquisition implements IDataAcq{
    private DataAcquisition mDataAcquisition;

    public ProxyDataAcquisition(Object context, INotify notify, IDataReadyInternal iDataReadyInternal) {
        mDataAcquisition = new DataAcquisition((Context)context,notify,iDataReadyInternal);
    }

    @Override
    public Device getDevice() {
        return mDataAcquisition.getDevice();
    }

    @Override
    public void setDevice(Device device) {
        mDataAcquisition.setDevice(device);
    }

    @Override
    public void start(Node node) {
        mDataAcquisition.start(node);
    }

    @Override
    public void stop() {
        mDataAcquisition.stop();
    }

}
