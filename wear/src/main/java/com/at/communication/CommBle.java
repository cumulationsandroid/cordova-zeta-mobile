package com.at.communication;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import com.at.model.app.ConnectionRole;
import com.at.model.app.ConnectionStates;
import com.at.model.app.IDataReadyExternal;
import com.at.model.app.INotify;
import com.at.model.app.ISensorState;
import com.at.model.app.States;
import com.at.utils.Constants;
import com.at.utils.LoggingService;
import com.at.utils.db.DatabaseWorker;
import com.at.zeta.zetamobile.MessageSender;
import com.at.zeta.zetamobile.WearApplication;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CommBle implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

  private static final String TAG = CommBle.class.getSimpleName();
  private static final String currentThreadName = "ZetaCommunicationMainThread";
  private static final String dbThreadName = "ZetaCommunicationSecondaryThread";
  private final Context mContext;
  private final GoogleApiClient mGoogleClient;
  private final IDataReadyExternal iSensorAgent;
  private final INotify iNotify;
  private final ISensorState iSensorState;
  public ConnectionStates connectionState = ConnectionStates.DISCONNECTED;
  DataSenderThread currentThread;
  DataSenderThread dbThread = null;
  /**
   * buffer contains only transmitted data, which has the transmission success
   */
  ArrayList<BufferData> buffer = new ArrayList<>();
  private NodeApi.NodeListener peerListener;
  private DatabaseWorker databaseWorker;

  public CommBle(Object context, IDataReadyExternal iSensorAgent, INotify iNotify) {
    mContext = (Context) context;
    this.iSensorAgent = iSensorAgent;
    this.iSensorState = (ISensorState) iSensorAgent;
    this.iNotify = iNotify;

    mGoogleClient = new GoogleApiClient.Builder(mContext)
      .addApi(Wearable.API)
      .addConnectionCallbacks(this)
      .addOnConnectionFailedListener(this)
      .build();
    mGoogleClient.connect();
    notifyConnectionState(ConnectionStates.CONNECTING);
    BleMsgReceiver.setIDataReadyExternal(iSensorAgent);
    currentThread = new DataSenderThread(currentThreadName);
    currentThread.start();
  }

  public void transmitData(String data) {

    if (currentThread == null) {
      currentThread = new DataSenderThread(currentThreadName);
      currentThread.start();
    }

    if (!currentThread.isAlive()) {
      currentThread.quit();
      currentThread = null;
      currentThread = new DataSenderThread(currentThreadName);
      currentThread.start();
    }

    currentThread.queueTask(data, false);
  }

  private void backupChannelData(String data) {
    long rowID = -1;
    try {
      JSONObject jsonObject = new JSONObject(data);

      if (databaseWorker == null) {
        databaseWorker = new DatabaseWorker(mContext);
      }

      if (buffer != null && buffer.size() > 0) {
        for (int i = 0; i < buffer.size(); i++) {
          databaseWorker.insert(buffer.get(i).data);
          buffer.remove(i);
        }
      }

      if (jsonObject.getString("uri").startsWith("Channel")) {
        databaseWorker.insert(data);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public ConnectionRole getConnectionRole() {
    return ConnectionRole.SERVER;
  }

  public ConnectionStates getConnectionStatus() {
    return ConnectionStates.CONNECTED;
  }

  public void post(ConnectionStates connectionStates) {

  }

  @Override
  public void onConnected(Bundle bundle) {

    new Thread(new Runnable() {
      @Override
      public void run() {
        NodeApi.GetConnectedNodesResult nodesResult = Wearable.NodeApi.getConnectedNodes(mGoogleClient).await();
        final List<Node> nodeList = nodesResult.getNodes();
        if (nodeList != null && nodeList.size() > 0) {
          notifyConnectionState(ConnectionStates.CONNECTED);
        } else {
          notifyConnectionState(ConnectionStates.DISCONNECTED);
        }
      }
    }).start();

    if (mGoogleClient != null && peerListener != null) {
      Wearable.NodeApi.removeListener(mGoogleClient, peerListener);
    }

    peerListener = new NodeApi.NodeListener() {
      @Override
      public void onPeerConnected(Node node) {
        notifyConnectionState(ConnectionStates.CONNECTED);
      }

      @Override
      public void onPeerDisconnected(Node node) {
        notifyConnectionState(ConnectionStates.DISCONNECTED);
      }
    };
    Wearable.NodeApi.addListener(mGoogleClient, peerListener);
  }

  @Override
  public void onConnectionSuspended(int i) {
    notifyConnectionState(ConnectionStates.CONNECTIONERROR);
  }

  @Override
  public void onConnectionFailed(ConnectionResult connectionResult) {
    notifyConnectionState(ConnectionStates.CONNECTIONERROR);
  }

  /**
   * notifyConnectionState will store the current state
   *
   * @param states
   */
  private void notifyConnectionState(ConnectionStates states) {
    connectionState = states;
    iNotify.notify(connectionState);
  }

  public void checkThread() {
    if (databaseWorker == null)
      databaseWorker = new DatabaseWorker(mContext);
    if (!databaseWorker.isEmpty() && iSensorState.getState() == States.OPERATIONAL && connectionState == ConnectionStates.CONNECTED) {
      if (dbThread == null) {
        dbThread = new DataSenderThread(dbThreadName);
        dbThread.start();
        dbThread.fetchDBContent();
        try {
          // android wear API takes 10 seconds to update the disconnection state
          dbThread.currentThread().sleep(Constants.CONNECTION_DELAY);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      } else {
        if (dbThread.messageCount == 0) {
          if (!dbThread.isAlive()) {
            dbThread.start();
          }
          dbThread.fetchDBContent();
        }
      }
    } else {
      clearThread();
    }
  }

  public void clearThread() {
    if (dbThread != null) {
      dbThread.mWorkerHandler.removeCallbacksAndMessages(null);
      dbThread.quit();
      dbThread = null;
      databaseWorker = null;
    }
  }

  public static class BleMsgReceiver extends WearableListenerService {

    private static final String TAG = BleMsgReceiver.class.getSimpleName();
    private static IDataReadyExternal sensorAgent;
    private SensorManager mSensorManager;
    private WearApplication wearApplication;

    public static void setIDataReadyExternal(IDataReadyExternal iSensorAgent) {
      sensorAgent = iSensorAgent;
    }

    @Override
    public void onCreate() {
      super.onCreate();
      mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    }

    @Override
    public void onDestroy() {
      super.onDestroy();
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
      String path = messageEvent.getPath();
      Log.e("--------path", path + "");
      Log.i(TAG, "!inside onMessageReceived " + messageEvent.getPath() + " " + new String
        (messageEvent.getData()));
//            LoggingService.d(getApplicationContext(), TAG, "!inside onMessageReceived, path: " + path);
      Constants.logString(TAG, new String(messageEvent.getData()));
//            Handler handler = new Handler(Looper.getMainLooper());
//            handler.post(new Runnable() {
//                @Override
//                public void run() {
//                    Toast.makeText(getApplicationContext(), "Received device instance", Toast.LENGTH_SHORT).show();
//                }
//            });


      if (path.startsWith("get_time")) {
        String data = new String(messageEvent.getData());
        Log.e(TAG, "Received sync response: " + data);
        wearApplication = (WearApplication) getApplicationContext();
        long time = Long.parseLong(data);

        wearApplication.setTime(time);

      }
      if ("sensors_list".equalsIgnoreCase(path)) {
        List<Sensor> sensorList = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        if (sensorList != null && sensorList.size() > 0) {
          String response = sensorList.get(0).getName();
          for (int i = 0, count = sensorList.size(); i < count; i++) {
            response += "|" + sensorList.get(i).getName();
          }
          Intent intent = new Intent(this, MessageSender.class);
          intent.putExtra(Constants.DATA, response);
          intent.putExtra(Constants.PATH, path);
          intent.putExtra(Constants.SOURCE, path);
          startService(intent);
        }
      } else if ("get_logs".equalsIgnoreCase(path)) {
        try {
          Log.e(TAG, "onMessageReceived");
          Intent intent = new Intent(this, MessageSender.class);
          intent.putExtra(Constants.DATA, "");
          intent.putExtra(Constants.PATH, "get_logs");
          intent.putExtra(Constants.SOURCE, "get_logs");
          startService(intent);

        } catch (Exception e) {
          e.printStackTrace();
        }
      } else if ("clear_logs".equalsIgnoreCase(path)) {
        File logFile = new File(Environment.getExternalStorageDirectory(),
          "ZetaSense/" + LoggingService.FILE_NAME);
        if (logFile.exists()) {
          boolean status = logFile.delete();
          Log.d(TAG, "!log file delete status: " + status);
        }
      } else if ("zeta_sense".equalsIgnoreCase(path)) {
        sensorAgent.dataReadyExternal(new String(messageEvent.getData()));
      }
    }
  }

  public class DataSenderThread extends HandlerThread {

    private int messageCount = 0;
    private Handler mWorkerHandler;

    public DataSenderThread(String name) {
      super(name);
    }

    @Override
    public synchronized void start() {
      super.start();
      prepareHandler();
    }

    public void queueTask(String data, boolean isFromDB) {
      messageCount++;
      mWorkerHandler.obtainMessage(isFromDB ? 1 : 0, data).sendToTarget();
    }

    private void prepareHandler() {
      mWorkerHandler = new Handler(getLooper(), new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
          messageCount--;
          String data = (String) msg.obj;
          boolean isFromDB = msg.what == 1;

          // connect GoogleClient
          if (!mGoogleClient.isConnected()) {
            ConnectionResult connectionResult = mGoogleClient.blockingConnect(30, TimeUnit.SECONDS);
            if (!connectionResult.isSuccess()) {
              Log.d(TAG, "Failed to connect to GoogleApiClient.");
              notifyConnectionState(ConnectionStates.CONNECTIONERROR);
              if (!isFromDB)
                backupChannelData(data);
              return true;
            }
          }

          // GoogleClient is connected
          NodeApi.GetConnectedNodesResult nodesResult = Wearable.NodeApi.getConnectedNodes(mGoogleClient).await();
          List<Node> nodeList = nodesResult.getNodes();

          // Check connected nodes
          if (nodeList != null && nodeList.size() > 0) {
            String wear_IDs = "";
            // try to send data to all connected node
            boolean nodeConnected = false;
            for (int i = 0, count = nodeList.size(); i < count; i++) {
              Node node = nodeList.get(i);
              MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleClient, node.getId(), "zeta_sense", data.getBytes()).await();
              Status status = result.getStatus();
              if (status.getStatusCode() != CommonStatusCodes.SUCCESS) {
                wear_IDs += "\nDevice-ID : " + node.getId();
                if (!isFromDB) {
                  backupChannelData(data);
                }
                notifyConnectionState(ConnectionStates.DISCONNECTED);
                continue;
              } else {

                if (isFromDB) {
                  if (databaseWorker != null) {
                    databaseWorker.deleteFirstRow();
                  } else {
                    //probably session stopped
                  }
                } else {
                  long currentTime = System.currentTimeMillis();
                  BufferData bufferData = new BufferData();
                  bufferData.data = data;
                  bufferData.timeStamp = currentTime;
                  buffer.add(bufferData);
                  for (int i1 = 0; i1 < buffer.size(); i1++) {
                    if ((currentTime - buffer.get(i1).timeStamp) < Constants.CONNECTION_DELAY) {
                      buffer.remove(i1);
                    } else {
                      break;
                    }
                  }
                }

                LoggingService.d(mContext, "TAG", data);
              }
            }

            // Send data failed node ids
            if (wear_IDs.length() > 0) {
              notifyConnectionState(ConnectionStates.DISCONNECTED);
              Log.e(TAG, "Data transmission failed for\n" + wear_IDs);
            }
          } else {
            // Nodes not connected, no further process is needed but saving it
            notifyConnectionState(ConnectionStates.DISCONNECTED);
            if (!isFromDB) {
              backupChannelData(data);
            }
            return true;
          }

          checkThread();

          return true;
        }
      });
    }

    private void fetchDBContent() {
      if (dbThread != null) {
        queueTask(databaseWorker.getFirstRow(), true);
      }
    }
  }

  public class BufferData {
    public String data = "";
    public long timeStamp = 0;
  }
}
