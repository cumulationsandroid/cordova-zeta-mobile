package com.at.communication;

import com.at.model.app.ConnectionRole;
import com.at.model.app.ConnectionStates;
import com.at.model.app.IDataReadyExternal;
import com.at.model.app.INotify;

public class ProxyComm implements IComm {

    private final CommBle mCommBle;
    private ConnectionRole connectionRole;
    private ConnectionStates connectionState;

    public ProxyComm(Object applicationContext, IDataReadyExternal iSensorAgent, INotify iNotify) {
        mCommBle = new CommBle(applicationContext, iSensorAgent, iNotify);
    }

    @Override
    public void transmitData(String data) {
        mCommBle.transmitData(data);
    }

    @Override
    public ConnectionStates getConnectionStatus() {
        return connectionState;
    }

    @Override
    public ConnectionRole getConnectionRole() {
        return connectionRole;
    }
}