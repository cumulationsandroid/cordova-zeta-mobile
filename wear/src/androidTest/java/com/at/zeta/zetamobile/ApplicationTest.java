package com.at.zeta.zetamobile;

import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<WearApplication> {
    public ApplicationTest() {
        super(WearApplication.class);
    }
}