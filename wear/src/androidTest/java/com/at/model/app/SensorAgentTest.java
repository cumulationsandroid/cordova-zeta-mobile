package com.at.model.app;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.MediumTest;
import android.util.Log;

import com.at.model.domain.Channel;
import com.at.model.domain.Device;
import com.at.model.domain.Sensor;
import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;
import com.at.model.meta.NodeType;
import com.at.utils.NodeGsonSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 * Tests that the parcelable interface is implemented correctly.
 */
@RunWith(AndroidJUnit4.class)
@MediumTest
public class SensorAgentTest {

    private static final String TAG = SensorAgentTest.class.getSimpleName();
    private SensorAgent mSensorAgent;
    private Node nodeObject;
    private Device mDevice;
    /*private String nodeJson = "{ \"data\":{ \"timeStamp\":0 }, \"description\":\"This is Moto360\", \"guid\":\"32efe742-6bff-4a01-950d-b2a87279fc45\", \"name\":\"Motorola metallica\", \"nodes\":[ { \"description\":\"android.sensor.accelerometer\", \"guid\":\"60273382-169c-4754-9958-2dbce48d9578\", \"name\":\"Accelerometer Sensor\", \"type\":\"OBJECT\", \"uri\":\"Sensor.android.sensor.accelerometer\", \"nodes\":[ { \"description\":\"Accelerometer x-axis\", \"guid\":\"a1b3f0c9-2441-4cfe-8546-3d21a37325c4\", \"name\":\"x-axis\", \"type\":\"OBJECT\", \"uri\":\"Channel.android.sensor.accelerometer.x\", \"samplingDuration\":0, \"nodes\":[ ], \"data\":{ \"dataType\":\"FLOAT\", \"guid\":\"f00db8af-b9e3-4b3f-878e-cb1a52913e8d\", \"name\":\"x-axis value\", \"timeStamp\":0, \"uri\":\"Data.android.sensor.accelerometer.x\" } }, { \"description\":\"Accelerometer y-axis\", \"guid\":\"e37c2de3-b311-46e2-95ff-e95f6fb2c2d2\", \"name\":\"y-axis\", \"type\":\"OBJECT\", \"uri\":\"Channel.android.sensor.accelerometer.y\", \"samplingDuration\":0, \"nodes\":[ ], \"data\":{ \"dataType\":\"FLOAT\", \"guid\":\"84857a78-2fa4-4ecd-9f0b-63659b204bfc\", \"name\":\"y-axis value\", \"timeStamp\":0, \"uri\":\"Data.android.sensor.accelerometer.y\" } }, { \"description\":\"Accelerometer z-axis\", \"guid\":\"a4e1d2cd-08c6-4610-9581-9cb8072a60f3\", \"name\":\"z-axis\", \"type\":\"OBJECT\", \"uri\":\"Channel.android.sensor.accelerometer.z\", \"samplingDuration\":0, \"nodes\":[ ], \"data\":{ \"dataType\":\"FLOAT\", \"guid\":\"9e1df978-a4f8-45d0-aecc-8ac638ace7e0\", \"name\":\"z-axis value\", \"timeStamp\":0, \"uri\":\"Data.android.sensor.accelerometer.z\" } } ], \"data\":{ \"timeStamp\":0 } } ], \"type\":\"OBJECT\", \"uri\":\"Device.Moto 360\" }";*/
    /*private String nodeJson = "{ \"name\":\"Accelerometer Sensor\", \"description\":\"android.sensor.accelerometer\", \"guid\":\"9fccfc98-23c8-4a0d-9141-6fe56cd0bcb4\", \"uri\":\"Sensor.android.sensor.accelerometer\", \"type\":\"OBJECT\", \"data\":{ \"timeStamp\":0 }, \"nodes\":[ { \"name\":\"x-axis\", \"description\":\"Accelerometer x-axis\", \"guid\":\"01e7145f-9676-4393-acfe-aa6096c8b8af\", \"uri\":\"Channel.android.sensor.accelerometer.x\", \"type\":\"OBJECT\", \"samplingDuration\":0, \"data\":{ \"name\":\"x-axis value\", \"guid\":\"d324fd57-17d1-4c24-aa8a-a8a3ff1cedff\", \"uri\":\"Data.android.sensor.accelerometer.x\", \"dataType\":\"FLOAT\", \"timeStamp\":0 }, \"nodes\":[ ] } ] }";*/
    /*private String nodeJson = "{ \"name\":\"x-axis\", \"description\":\"Accelerometer x-axis\", \"guid\":\"01e7145f-9676-4393-acfe-aa6096c8b8af\", \"uri\":\"Channel.android.sensor.accelerometer.x\", \"type\":\"OBJECT\", \"samplingDuration\":0, \"data\":{ \"name\":\"x-axis value\", \"guid\":\"d324fd57-17d1-4c24-aa8a-a8a3ff1cedff\", \"uri\":\"Data.android.sensor.accelerometer.x\", \"dataType\":\"FLOAT\", \"timeStamp\":0 }, \"nodes\":[ ] }";*/
    /*private String nodeJson = "{ \"name\":\"setConfiguration\", \"description\":\"SensorAgent setConfigurationMethod\", \"guid\":\"f616b565-0c82-48db-8e86-631fa9704843\", \"uri\":\"setConfiguration.SensorAgent\", \"type\":\"METHOD\", \"data\":{ \"timeStamp\":0 }, \"nodes\":[ ] }";*/
    private String nodeJson = "{ \"name\":\"SensorAgent\", \"description\":\"SensorAgent instance\", \"guid\":\"54985bb6-15fb-4242-8561-e25c77daf216\", \"uri\":\"SensorAgent\", \"type\":\"OBJECT\", \"data\":{ \"timeStamp\":0 }, \"nodes\":[ { \"name\":\"setConfiguration\", \"description\":\"SensorAgent setConfigurationMethod\", \"guid\":\"c3b9d897-a276-4faa-8d44-eca885d6e600\", \"uri\":\"setConfiguration.SensorAgent\", \"type\":\"METHOD\", \"data\":{ \"timeStamp\":0 }, \"nodes\":[ ] } ] }";
//    private String nodeJson = "{ \"name\": \"SensorAgent\", \"description\": \"SensorAgent instance\", \"guid\": \"de50f11a-3b52-4aa3-959d-9b568bd97809\", \"uri\": \"SensorAgent://42e63d67ad417fda.LGEplatina.SensorAgent\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"setConfiguration\", \"description\": \"SensorAgent setConfiguration\", \"guid\": \"d10c8f92-c17e-4ea9-a790-4f497b50b475\", \"uri\": \"Method://42e63d67ad417fda.LGEplatina.SensorAgent.setConfiguration\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"Node\", \"description\": \"setConfiguration param Node\", \"guid\": \"0ae1b57e-01af-4633-a1dd-183cd176fdf2\", \"uri\": \"Parameter://42e63d67ad417fda.LGEplatina.SensorAgent.setConfiguration.Node\", \"type\": \"PARAMETER\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"getConfiguration\", \"description\": \"SensorAgent getConfiguration\", \"guid\": \"b1c8b888-4b77-4954-b94e-41348538dc6f\", \"uri\": \"Method://42e63d67ad417fda.LGEplatina.SensorAgent.getConfiguration\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"start\", \"description\": \"SensorAgent start\", \"guid\": \"0b7c2894-3f03-4b76-9d46-fbd85f962941\", \"uri\": \"Method://42e63d67ad417fda.LGEplatina.SensorAgent.start\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"start\", \"description\": \"start param Node\", \"guid\": \"5cc691d4-1fb3-4403-8da3-a5ce43327cfc\", \"uri\": \"Parameter://42e63d67ad417fda.LGEplatina.SensorAgent.start.Node\", \"type\": \"PARAMETER\", \"data\": { \"timeStamp\": 0, \"dataType\": \"STRING\" }, \"nodes\": [] }] }, { \"name\": \"stop\", \"description\": \"SensorAgent stop\", \"guid\": \"56ec060e-ea8e-4d2f-943f-75fbad54060d\", \"uri\": \"Method://42e63d67ad417fda.LGEplatina.SensorAgent.stop\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"heartBeat\", \"description\": \"SensorAgent heartBeat\", \"guid\": \"cbc03d11-431c-4fd5-be73-aeaa2081eb77\", \"uri\": \"Method://42e63d67ad417fda.LGEplatina.SensorAgent.heartBeat\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"Node\", \"description\": \"heartBeat param Node\", \"guid\": \"d5ed66d5-fb6d-4f6c-87e7-65dff7a914a8\", \"uri\": \"Parameter://42e63d67ad417fda.LGEplatina.SensorAgent.heartBeat.Node\", \"type\": \"PARAMETER\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"getDeviceCapabilities\", \"description\": \"SensorAgent getDeviceCapabilities\", \"guid\": \"b5c309cb-23f0-4934-abfd-5bf2d93e7ffa\", \"uri\": \"Method://42e63d67ad417fda.LGEplatina.SensorAgent.getDeviceCapabilities\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"getLoggerLevel\", \"description\": \"SensorAgent getLoggerLevel\", \"guid\": \"77a899e7-7bf0-4843-9491-791b30f52195\", \"uri\": \"Method://42e63d67ad417fda.LGEplatina.SensorAgent.getLoggerLevel\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"setLoggerLevel\", \"description\": \"SensorAgent setLoggerLevel\", \"guid\": \"596595ae-2691-4947-83a9-7edd1005380c\", \"uri\": \"Method://42e63d67ad417fda.LGEplatina.SensorAgent.setLoggerLevel\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"notify\", \"description\": \"SensorAgent notify\", \"guid\": \"df49287b-2fe3-4280-8380-702d078b311b\", \"uri\": \"Method://42e63d67ad417fda.LGEplatina.SensorAgent.notify\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"getLoggerMessage\", \"description\": \"SensorAgent getLoggerMessage\", \"guid\": \"6a466f88-3b56-4fd8-8a09-8cb965bb1731\", \"uri\": \"Method://42e63d67ad417fda.LGEplatina.SensorAgent.getLoggerMessage\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"Notification\", \"description\": \"Notification Message\", \"guid\": \"e5cad652-1294-431e-83c5-4fa2a47e053a\", \"uri\": \"Notification://42e63d67ad417fda.LGEplatina.SensorAgent.Notification\", \"type\": \"OBJECT\", \"data\": { \"dataType\": \"ENUMERATION\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"LGE platina\", \"description\": \"This is LGE platina\", \"guid\": \"5ffd8009-5027-43c8-812c-91d2dcad876a\", \"uri\": \"Device://42e63d67ad417fda.LGEplatina\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"STMicro 3-axis Tilt Sensor\", \"description\": \"android.sensor.wrist_tilt_gesture\", \"guid\": \"cf5c3fee-e776-4065-8ac6-c91ef371bc0c\", \"uri\": \"Sensor://android.sensor.wrist_tilt_gesture\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"STMicro 3-axis Tilt Sensor Channel 1\", \"description\": \"android.sensor.wrist_tilt_gesture.1\", \"guid\": \"6f41ce1c-8167-4cc9-95f9-6e4c3c029674\", \"uri\": \"Channel://android.sensor.wrist_tilt_gesture.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Gyroscope\", \"description\": \"android.sensor.gyroscope\", \"guid\": \"e477e1e5-0939-4bbb-8cb1-d55ca6d5db12\", \"uri\": \"Sensor://android.sensor.gyroscope\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Gyroscope Channel 1\", \"description\": \"android.sensor.gyroscope.1\", \"guid\": \"3598a927-dd49-463a-8791-9e15d5cf9522\", \"uri\": \"Channel://android.sensor.gyroscope.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Gyroscope Channel 2\", \"description\": \"android.sensor.gyroscope.2\", \"guid\": \"914357f5-429a-4952-b658-ca3d8c84478a\", \"uri\": \"Channel://android.sensor.gyroscope.channel2\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Gyroscope Channel 3\", \"description\": \"android.sensor.gyroscope.3\", \"guid\": \"3eae50e0-5105-409b-a809-f851e5639b90\", \"uri\": \"Channel://android.sensor.gyroscope.channel3\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Raw Gyroscope\", \"description\": \"android.sensor.gyroscope_uncalibrated\", \"guid\": \"8310010f-c89c-47e7-a035-6c1e64951fd9\", \"uri\": \"Sensor://android.sensor.gyroscope_uncalibrated\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Raw Gyroscope Channel 1\", \"description\": \"android.sensor.gyroscope_uncalibrated.1\", \"guid\": \"e46e3934-2908-435b-b998-8357ab383b86\", \"uri\": \"Channel://android.sensor.gyroscope_uncalibrated.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Raw Gyroscope Channel 2\", \"description\": \"android.sensor.gyroscope_uncalibrated.2\", \"guid\": \"d8e64870-aa3f-4f50-b5da-836ce418ca93\", \"uri\": \"Channel://android.sensor.gyroscope_uncalibrated.channel2\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Raw Gyroscope Channel 3\", \"description\": \"android.sensor.gyroscope_uncalibrated.3\", \"guid\": \"e83adcaf-3074-43ad-8f0b-f17322c15540\", \"uri\": \"Channel://android.sensor.gyroscope_uncalibrated.channel3\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Raw Gyroscope Channel 4\", \"description\": \"android.sensor.gyroscope_uncalibrated.4\", \"guid\": \"b446c6b8-eed9-413c-9918-ee5c04d85c3f\", \"uri\": \"Channel://android.sensor.gyroscope_uncalibrated.channel4\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Raw Gyroscope Channel 5\", \"description\": \"android.sensor.gyroscope_uncalibrated.5\", \"guid\": \"6e55279b-0033-4f76-9d8d-c55e01954068\", \"uri\": \"Channel://android.sensor.gyroscope_uncalibrated.channel5\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Raw Gyroscope Channel 6\", \"description\": \"android.sensor.gyroscope_uncalibrated.6\", \"guid\": \"1e50a577-bc2b-437d-b7d7-deba49f0c76e\", \"uri\": \"Channel://android.sensor.gyroscope_uncalibrated.channel6\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Accelerometer\", \"description\": \"android.sensor.accelerometer\", \"guid\": \"aeb1bb0a-a949-4352-8dd8-4cb3fa77931f\", \"uri\": \"Sensor://android.sensor.accelerometer\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Accelerometer Channel 1\", \"description\": \"android.sensor.accelerometer.1\", \"guid\": \"4888b030-d753-4784-be42-8ab7a5ae74ac\", \"uri\": \"Channel://android.sensor.accelerometer.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Accelerometer Channel 2\", \"description\": \"android.sensor.accelerometer.2\", \"guid\": \"9ef211c3-37d5-42b5-a3b2-84583d4b23ec\", \"uri\": \"Channel://android.sensor.accelerometer.channel2\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Accelerometer Channel 3\", \"description\": \"android.sensor.accelerometer.3\", \"guid\": \"fd339689-aef9-4713-b710-0296581ae29c\", \"uri\": \"Channel://android.sensor.accelerometer.channel3\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Magnetic Field\", \"description\": \"android.sensor.magnetic_field\", \"guid\": \"6e2bd094-add7-495b-8270-c6909bd028f8\", \"uri\": \"Sensor://android.sensor.magnetic_field\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Magnetic Field Channel 1\", \"description\": \"android.sensor.magnetic_field.1\", \"guid\": \"3904564c-4674-450f-87b5-5b76bf50b7f8\", \"uri\": \"Channel://android.sensor.magnetic_field.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Magnetic Field Channel 2\", \"description\": \"android.sensor.magnetic_field.2\", \"guid\": \"cc4c6942-f618-4525-8b08-264e571908ea\", \"uri\": \"Channel://android.sensor.magnetic_field.channel2\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Magnetic Field Channel 3\", \"description\": \"android.sensor.magnetic_field.3\", \"guid\": \"d618bbda-3e98-4e0a-8b05-3cac9569df4b\", \"uri\": \"Channel://android.sensor.magnetic_field.channel3\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Raw Magnetic Field\", \"description\": \"android.sensor.magnetic_field_uncalibrated\", \"guid\": \"03aa1c2c-3446-4177-a30c-a256dbe0805c\", \"uri\": \"Sensor://android.sensor.magnetic_field_uncalibrated\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Raw Magnetic Field Channel 1\", \"description\": \"android.sensor.magnetic_field_uncalibrated.1\", \"guid\": \"051e0459-72a2-4d77-8680-1e976830c555\", \"uri\": \"Channel://android.sensor.magnetic_field_uncalibrated.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Raw Magnetic Field Channel 2\", \"description\": \"android.sensor.magnetic_field_uncalibrated.2\", \"guid\": \"5d268931-093c-4e30-8938-147a4dff3a4e\", \"uri\": \"Channel://android.sensor.magnetic_field_uncalibrated.channel2\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Raw Magnetic Field Channel 3\", \"description\": \"android.sensor.magnetic_field_uncalibrated.3\", \"guid\": \"978b7129-8fdb-42b7-b41e-ec0fc909b1c6\", \"uri\": \"Channel://android.sensor.magnetic_field_uncalibrated.channel3\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Raw Magnetic Field Channel 4\", \"description\": \"android.sensor.magnetic_field_uncalibrated.4\", \"guid\": \"81d4b7e5-04f7-4b05-8054-9ec1c465b78a\", \"uri\": \"Channel://android.sensor.magnetic_field_uncalibrated.channel4\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Raw Magnetic Field Channel 5\", \"description\": \"android.sensor.magnetic_field_uncalibrated.5\", \"guid\": \"99f28e87-c1d1-4e7c-9206-0b4775f7dc03\", \"uri\": \"Channel://android.sensor.magnetic_field_uncalibrated.channel5\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Raw Magnetic Field Channel 6\", \"description\": \"android.sensor.magnetic_field_uncalibrated.6\", \"guid\": \"675de107-9a68-4f98-ad78-94b9fc7fb4da\", \"uri\": \"Channel://android.sensor.magnetic_field_uncalibrated.channel6\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Orientation\", \"description\": \"android.sensor.orientation\", \"guid\": \"6d8d48a0-55ee-4e0d-b947-fbf8e094ac77\", \"uri\": \"Sensor://android.sensor.orientation\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Orientation Channel 1\", \"description\": \"android.sensor.orientation.1\", \"guid\": \"e3e0e109-48ef-4a62-b1d0-5e8f1415ad82\", \"uri\": \"Channel://android.sensor.orientation.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Orientation Channel 2\", \"description\": \"android.sensor.orientation.2\", \"guid\": \"517c9c82-e6f2-47ea-a22e-e6b3a8dd6960\", \"uri\": \"Channel://android.sensor.orientation.channel2\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Orientation Channel 3\", \"description\": \"android.sensor.orientation.3\", \"guid\": \"34a62240-31ca-4964-bd6f-8bb39bdf0428\", \"uri\": \"Channel://android.sensor.orientation.channel3\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Rotation Vector\", \"description\": \"android.sensor.rotation_vector\", \"guid\": \"82a0b26b-02c6-4c2f-8e40-79803206b069\", \"uri\": \"Sensor://android.sensor.rotation_vector\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Rotation Vector Channel 1\", \"description\": \"android.sensor.rotation_vector.1\", \"guid\": \"b9204a89-3edb-4d56-a4b8-b9b6873f88de\", \"uri\": \"Channel://android.sensor.rotation_vector.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Rotation Vector Channel 2\", \"description\": \"android.sensor.rotation_vector.2\", \"guid\": \"12caff45-1db5-4493-a455-41dce565700a\", \"uri\": \"Channel://android.sensor.rotation_vector.channel2\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Rotation Vector Channel 3\", \"description\": \"android.sensor.rotation_vector.3\", \"guid\": \"96d08869-4a15-4c20-be4a-29320ec5eaca\", \"uri\": \"Channel://android.sensor.rotation_vector.channel3\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Rotation Vector Channel 4\", \"description\": \"android.sensor.rotation_vector.4\", \"guid\": \"4dd476a7-f91a-4e8d-8d98-a2e8b146dfa6\", \"uri\": \"Channel://android.sensor.rotation_vector.channel4\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Rotation Vector Channel 5\", \"description\": \"android.sensor.rotation_vector.5\", \"guid\": \"08f54383-1bfe-4072-b58f-8fd99783c694\", \"uri\": \"Channel://android.sensor.rotation_vector.channel5\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Game Rotation Vector\", \"description\": \"android.sensor.game_rotation_vector\", \"guid\": \"5aaa1660-3dbc-402d-8474-955a2cba3b5a\", \"uri\": \"Sensor://android.sensor.game_rotation_vector\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Game Rotation Vector Channel 1\", \"description\": \"android.sensor.game_rotation_vector.1\", \"guid\": \"f6d074a2-2778-49a3-a3ec-780fd89ef00e\", \"uri\": \"Channel://android.sensor.game_rotation_vector.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Game Rotation Vector Channel 2\", \"description\": \"android.sensor.game_rotation_vector.2\", \"guid\": \"3507656e-b563-4238-aba7-2a7f8173c524\", \"uri\": \"Channel://android.sensor.game_rotation_vector.channel2\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Game Rotation Vector Channel 3\", \"description\": \"android.sensor.game_rotation_vector.3\", \"guid\": \"76d06e54-ed74-4849-bc85-6d5f1780ee29\", \"uri\": \"Channel://android.sensor.game_rotation_vector.channel3\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Game Rotation Vector Channel 4\", \"description\": \"android.sensor.game_rotation_vector.4\", \"guid\": \"452c5597-825d-426f-b26b-11c8c72a4d7a\", \"uri\": \"Channel://android.sensor.game_rotation_vector.channel4\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Linear Acceleration\", \"description\": \"android.sensor.linear_acceleration\", \"guid\": \"d4e867dd-6306-4692-a960-01628e65158a\", \"uri\": \"Sensor://android.sensor.linear_acceleration\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Linear Acceleration Channel 1\", \"description\": \"android.sensor.linear_acceleration.1\", \"guid\": \"88b3fca1-0431-4303-96bb-9462c01aea57\", \"uri\": \"Channel://android.sensor.linear_acceleration.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Linear Acceleration Channel 2\", \"description\": \"android.sensor.linear_acceleration.2\", \"guid\": \"68d027f1-2909-4b3b-a25d-c147ce0142df\", \"uri\": \"Channel://android.sensor.linear_acceleration.channel2\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Linear Acceleration Channel 3\", \"description\": \"android.sensor.linear_acceleration.3\", \"guid\": \"92cf807e-1cde-4d4a-afd1-791bc2f945a6\", \"uri\": \"Channel://android.sensor.linear_acceleration.channel3\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Gravity\", \"description\": \"android.sensor.gravity\", \"guid\": \"9f442ada-a91a-45a0-a4f1-98dca8ecbb4c\", \"uri\": \"Sensor://android.sensor.gravity\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Gravity Channel 1\", \"description\": \"android.sensor.gravity.1\", \"guid\": \"857e9dc1-9134-40ae-9e04-812ae82c7385\", \"uri\": \"Channel://android.sensor.gravity.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Gravity Channel 2\", \"description\": \"android.sensor.gravity.2\", \"guid\": \"0460092a-c7a9-402e-b2f4-96d06ed471f0\", \"uri\": \"Channel://android.sensor.gravity.channel2\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Gravity Channel 3\", \"description\": \"android.sensor.gravity.3\", \"guid\": \"5c19d81f-388f-4c5a-afa3-a38a20162c8b\", \"uri\": \"Channel://android.sensor.gravity.channel3\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Significant Motion\", \"description\": \"android.sensor.significant_motion\", \"guid\": \"ee45d3a7-1bfb-40fd-99bd-0d27e81b48c7\", \"uri\": \"Sensor://android.sensor.significant_motion\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Significant Motion Channel 1\", \"description\": \"android.sensor.significant_motion.1\", \"guid\": \"2523456d-4216-4a31-95af-8c8fdcc7a397\", \"uri\": \"Channel://android.sensor.significant_motion.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Step Detector\", \"description\": \"android.sensor.step_detector\", \"guid\": \"a89674bf-595a-4dac-8188-2356cd6b5af7\", \"uri\": \"Sensor://android.sensor.step_detector\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Step Detector Channel 1\", \"description\": \"android.sensor.step_detector.1\", \"guid\": \"4398a2f1-a6b8-4c9d-b959-d7484c9f9000\", \"uri\": \"Channel://android.sensor.step_detector.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Step Counter\", \"description\": \"android.sensor.step_counter\", \"guid\": \"5c1799fa-3283-4ed9-85bf-f79af6de9543\", \"uri\": \"Sensor://android.sensor.step_counter\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Step Counter Channel 1\", \"description\": \"android.sensor.step_counter.1\", \"guid\": \"b16104c1-6956-4080-a793-1dcc10917694\", \"uri\": \"Channel://android.sensor.step_counter.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }, { \"name\": \"MPL Geomagnetic Rotation Vector\", \"description\": \"android.sensor.geomagnetic_rotation_vector\", \"guid\": \"6f37a576-aea2-4ac4-b62e-552f78d002f4\", \"uri\": \"Sensor://android.sensor.geomagnetic_rotation_vector\", \"type\": \"OBJECT\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"name\": \"MPL Geomagnetic Rotation Vector Channel 1\", \"description\": \"android.sensor.geomagnetic_rotation_vector.1\", \"guid\": \"75b7890e-656d-4da5-a1c1-470c99c63be2\", \"uri\": \"Channel://android.sensor.geomagnetic_rotation_vector.channel1\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Geomagnetic Rotation Vector Channel 2\", \"description\": \"android.sensor.geomagnetic_rotation_vector.2\", \"guid\": \"f4525d9b-a9bd-48ab-8a65-30a83546e5ce\", \"uri\": \"Channel://android.sensor.geomagnetic_rotation_vector.channel2\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Geomagnetic Rotation Vector Channel 3\", \"description\": \"android.sensor.geomagnetic_rotation_vector.3\", \"guid\": \"c5ea4e35-34f0-4252-b6ed-26842f198633\", \"uri\": \"Channel://android.sensor.geomagnetic_rotation_vector.channel3\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Geomagnetic Rotation Vector Channel 4\", \"description\": \"android.sensor.geomagnetic_rotation_vector.4\", \"guid\": \"7ee29ffd-f190-45c6-826e-20a8777ce2ed\", \"uri\": \"Channel://android.sensor.geomagnetic_rotation_vector.channel4\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }, { \"name\": \"MPL Geomagnetic Rotation Vector Channel 5\", \"description\": \"android.sensor.geomagnetic_rotation_vector.5\", \"guid\": \"abbc9029-6378-4df6-910f-8cad4e8beffc\", \"uri\": \"Channel://android.sensor.geomagnetic_rotation_vector.channel5\", \"type\": \"OBJECT\", \"samplingDuration\": 5000000, \"isEnabled\": true, \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [] }] }] }] }";

    @Before
    public void initiatingTest() {
        mDevice = (Device) getDevice();

        nodeObject = getSensorAgent();
        mSensorAgent = new SensorAgent(InstrumentationRegistry.getTargetContext(), nodeObject);

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(SensorAgent.class, new NodeGsonSerializer());
        builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
        Gson gsonExt = builder.create();

        nodeJson = gsonExt.toJson(this.nodeObject);
    }

    @Test
    public void doTesting() {
        String json = mSensorAgent.serialize(nodeObject);
        Log.d(TAG, "serialized data: " + "\n" + json + "\n" + nodeJson);
        assertEquals(json, nodeJson);

        Node node = null;
        node = mSensorAgent.objectify(nodeJson);
        //Assert.assertThat(nodeObject, new ReflectionEquals(node));
        Log.d(TAG, "!node uri: " + node.getUri() + " , nodeObject uri: " + nodeObject.getUri());
        Assert.assertTrue(EqualsBuilder.reflectionEquals(node, nodeObject));
    }

    private Node getDevice() {
        Device device = new Device();
        device.setName("Motorola metallica");
        device.setDescription("This is Moto360");
        device.setGuid(UUID.fromString("32efe742-6bff-4a01-950d-b2a87279fc45"));
        device.setUri("Device.Moto 360");
        device.setType(NodeType.OBJECT);
        Data data = new Data();
        device.setData(data);

        Sensor sensor = new Sensor();
        sensor.setName("Accelerometer Sensor");
        sensor.setDescription("android.sensor.accelerometer");
        sensor.setGuid(UUID.fromString("60273382-169c-4754-9958-2dbce48d9578"));
        sensor.setUri("Sensor.android.sensor.accelerometer.x");
        sensor.setType(NodeType.OBJECT);

        Data sensorData = new Data();
        sensor.setData(sensorData);

        Channel channelX = new Channel();
        channelX.setName("x-axis");
        channelX.setDescription("android.sensor.accelerometer.x");
        channelX.setGuid(UUID.fromString("a1b3f0c9-2441-4cfe-8546-3d21a37325c4"));
        channelX.setUri("Channel.android.sensor.accelerometer.x");
        channelX.setType(NodeType.OBJECT);

        Data channelXData = new Data();
        channelXData.setDataType(DataType.FLOAT);
        channelXData.setTimeStamp(0);
        channelX.setData(channelXData);

        Channel channelY = new Channel();
        channelY.setName("y-axis");
        channelY.setDescription("Accelerometer y-axis");
        channelY.setGuid(UUID.fromString("e37c2de3-b311-46e2-95ff-e95f6fb2c2d2"));
        channelY.setUri("Channel.android.sensor.accelerometer.y");
        channelY.setType(NodeType.OBJECT);

        Data channelYData = new Data();
        channelYData.setDataType(DataType.FLOAT);
        channelYData.setTimeStamp(0);
        channelY.setData(channelYData);

        Channel channelZ = new Channel();
        channelZ.setName("z-axis");
        channelZ.setDescription("Accelerometer z-axis");
        channelZ.setGuid(UUID.fromString("a4e1d2cd-08c6-4610-9581-9cb8072a60f3"));
        channelZ.setUri("Channel.android.sensor.accelerometer.z");
        channelZ.setType(NodeType.OBJECT);

        Data channelZData = new Data();
        channelZData.setDataType(DataType.FLOAT);
        channelZData.setTimeStamp(0);
        channelZ.setData(channelZData);

        ArrayList<Channel> channelList = new ArrayList<>();
        channelList.add(channelX);
        channelList.add(channelY);
        channelList.add(channelZ);
        sensor.setNodes(channelList);

        ArrayList<Sensor> sensors = new ArrayList<>();
        device.setNodes(sensors);

        return device;
    }

    private  Node getSensor() {
        Sensor sensor = new Sensor();
        sensor.setDescription("android.sensor.accelerometer");
        sensor.setUri("Sensor.android.sensor.accelerometer");
        sensor.setType(NodeType.OBJECT);
        sensor.setName("Accelerometer Sensor");
        Data sensorData = new Data();
        sensor.setData(sensorData);
        sensor.setGuid(UUID.fromString("d399c3ec-ce62-46a1-afdb-73d1b917c190"));
        Channel channel = new Channel();
        channel.setDescription("Accelerometer x-axis");
        Data channelData = new Data();
        channelData.setTimeStamp(0);
        channelData.setDataType(DataType.FLOAT);
        channel.setData(channelData);
        channel.setGuid(UUID.fromString("6f3b0984-dceb-43a2-b67c-b2ae88084687"));
        channel.setName("x-axis");
        channel.setType(NodeType.OBJECT);
        channel.setUri("Channel.android.sensor.accelerometer.x");
        ArrayList<Channel> channelList = new ArrayList<>();
        channelList.add(channel);
        sensor.setNodes(channelList);
        return sensor;
    }

    private Node getChannel() {
        Channel channel = new Channel();
        channel.setDescription("Accelerometer x-axis");
        Data channelData = new Data();
        channelData.setTimeStamp(0);
        channelData.setDataType(DataType.FLOAT);
        channel.setData(channelData);
        channel.setGuid(UUID.fromString("6f3b0984-dceb-43a2-b67c-b2ae88084687"));
        channel.setName("x-axis");
        channel.setType(NodeType.OBJECT);
        channel.setUri("Channel.android.sensor.accelerometer.x");
        return channel;
    }

    private SensorAgent getSensorAgent() {
        SensorAgent sensorAgent = new SensorAgent(InstrumentationRegistry.getTargetContext(),mDevice);
        sensorAgent.setName("SensorAgent");
        sensorAgent.setDescription("SensorAgent instance");
        sensorAgent.setGuid(UUID.fromString("54985bb6-15fb-4242-8561-e25c77daf216"));
        sensorAgent.setUri(SensorAgent.class.getSimpleName());
        sensorAgent.setType(NodeType.OBJECT);
        Data data = new Data();
        sensorAgent.setData(data);
        ArrayList<Node> result = new ArrayList<>();
        Node setConfigurationNode = new Node();
        setConfigurationNode.setName("setConfiguration");
        setConfigurationNode.setDescription("SensorAgent setConfigurationMethod");
        setConfigurationNode.setGuid(UUID.fromString("c3b9d897-a276-4faa-8d44-eca885d6e600"));
        setConfigurationNode.setUri("setConfiguration" + "." + SensorAgent.class.getSimpleName());
        setConfigurationNode.setType(NodeType.METHOD);
        setConfigurationNode.setData(new Data());
        result.add(setConfigurationNode);
        sensorAgent.setNodes(result);
        return sensorAgent;
    }

    public void storeData() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                databaseWorker.getDbHelper().dropTrigger();
//                databaseWorker.getDbHelper().setTrigger();
//                String c1 = "{\"name\":\"Accelerometer Sensor Channel 1\",\"description\":\"android.sensor.accelerometer.1\",\"guid\":\"05f88d8b-f03e-4e7d-b7e5-86b93a02af48\",\"uri\":\"Channel://android.sensor.accelerometer.channel1\",\"type\":\"OBJECT\",\"samplingDuration\":3000000,\"isEnabled\":true,\"data\":{\"dataType\":\"FLOAT\",\"timeStamp\":1480924550412,\"values\":\"QRsAKQ\\u003d\\u003d\"},\"nodes\":[]}";
//                String c2 = "{\"name\":\"Accelerometer Sensor Channel 2\",\"description\":\"android.sensor.accelerometer.2\",\"guid\":\"05f88d8b-f03e-4e7d-b7e5-86b93a02af48\",\"uri\":\"Channel://android.sensor.accelerometer.channel2\",\"type\":\"OBJECT\",\"samplingDuration\":3000000,\"isEnabled\":true,\"data\":{\"dataType\":\"FLOAT\",\"timeStamp\":1480924550412,\"values\":\"QRsAKQ\\u003d\\u003d\"},\"nodes\":[]}";
//                String c3 = "{\"name\":\"Accelerometer Sensor Channel 3\",\"description\":\"android.sensor.accelerometer.3\",\"guid\":\"05f88d8b-f03e-4e7d-b7e5-86b93a02af48\",\"uri\":\"Channel://android.sensor.accelerometer.channel3\",\"type\":\"OBJECT\",\"samplingDuration\":3000000,\"isEnabled\":true,\"data\":{\"dataType\":\"FLOAT\",\"timeStamp\":1480924550412,\"values\":\"QRsAKQ\\u003d\\u003d\"},\"nodes\":[]}";
//                for (int i = 0; i < 30; i++) {
//                    databaseWorker.insert(c1);
//                    databaseWorker.insert(c2);
//                    databaseWorker.insert(c3);
//                }
//                databaseWorker.getAllStoredChannels();
//                databaseWorker.getItemCount();
//            }
//        }).start();
    }

}