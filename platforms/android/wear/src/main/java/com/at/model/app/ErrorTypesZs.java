package com.at.model.app;

/**
 * Possible errors thrown in the APIs defined
 */
public enum ErrorTypesZs {
    ERR_NO_ERROR, ERR_GENERIC, ERR_OBJECTIFY_FAILED, ERR_SERIALIZE_FAILED
}