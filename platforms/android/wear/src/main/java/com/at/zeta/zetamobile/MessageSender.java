package com.at.zeta.zetamobile;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.at.utils.Constants;
import com.at.utils.LoggingService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MessageSender extends IntentService {

    private static final String TAG = MessageSender.class.getSimpleName();
    GoogleApiClient mGoogleClient;
    Handler handler = new Handler(Looper.getMainLooper());

    public MessageSender() {
        super("MessageSender");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent != null) {
            String source = intent.getStringExtra(Constants.SOURCE);
            String data = intent.getStringExtra(Constants.DATA);
            final String path = intent.getStringExtra(Constants.PATH);

            mGoogleClient = new GoogleApiClient.Builder(this)
                    .addApi(Wearable.API)
                    .build();
            ConnectionResult connectionResult = mGoogleClient.blockingConnect(30, TimeUnit.SECONDS);
            if (!connectionResult.isSuccess()) {
                LoggingService.d(getApplicationContext(), TAG, "Failed to connect to GoogleApiClient.");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Failed to connect. Try again!!", Toast.LENGTH_SHORT).show();
                    }
                });
                return;
            }

            NodeApi.GetConnectedNodesResult nodesResult = Wearable.NodeApi.getConnectedNodes(mGoogleClient).await();
            List<Node> nodeList = nodesResult.getNodes();
            if (nodeList == null || nodeList.size() == 0) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Device is not connected. Please check it!", Toast.LENGTH_SHORT).show();
                    }
                });
                return;
            }

            File logFile = new File(Environment.getExternalStorageDirectory(),
                    "ZetaSense/" + LoggingService.FILE_NAME);

            FileInputStream fileInputStream = null;
            try {
                fileInputStream = new FileInputStream(logFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            int parts_count = 0;
            try {
                byte[] buffer = new byte[50 * 1024];
                int len;
                while ((len = fileInputStream.read(buffer)) != -1) {
                    parts_count++;
                    for (int i = 0, count = nodeList.size(); i < count; i++) {
                        Node node = nodeList.get(i);
                        MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleClient, node.getId(), path, buffer).await();
                        Status status = result.getStatus();
                        if (status.getStatusCode() != CommonStatusCodes.SUCCESS) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Device is not connected. Please check it!", Toast.LENGTH_SHORT).show();
                                }
                            });
                            return;
                        }
                        Log.d(TAG, "!data transmit status msg: " + status.getStatusMessage() + " , code: " + status.getStatusCode());
                        //necessary to clear the old byte array
                    }
                    buffer = new byte[50 * 1024];
                }

                if (parts_count > 0) {
                    for (int i = 0, count = nodeList.size(); i < count; i++) {
                        Node node = nodeList.get(i);
                        MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleClient, node.getId(), path + "_end", buffer).await();
                        Status status = result.getStatus();
                        Log.d(TAG, "transmit end status msg: " + status.getStatusMessage() + " , code: " + status.getStatusCode());
                    }
                }

                PrintWriter writer = new PrintWriter(logFile);
                writer.print(LoggingService.headerName+"\n");
                writer.close();

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (fileInputStream != null)
                    try {
                        fileInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }

            handler.post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "File is transmitted", Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}

