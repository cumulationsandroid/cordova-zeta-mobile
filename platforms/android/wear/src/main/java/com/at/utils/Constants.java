package com.at.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

public class Constants {

    public static final String DATA = "data";
    public static final String PATH = "path";
    public static final String SOURCE = "source";

    public static final String SENSORS_LIST = "sensors_list";

    public static final String DEVICE_SERIALIZED_DATA_DEFAULT = "device_serialized_data_default";
    public static final String DEVICE_SERIALIZED_DATA_OPERATIONAL = "device_serialized_data_operational";
    public static final String SENSOR_AGENT_SERIALIZED_DATA = "sensor_agent_serialized_data";

    public static final String SCHEME_IDENTIFIER = "://";

    public static final int DEFAULT_SAMPLING_DURATION = 5000000; //mentioned in microseconds
    public static final boolean DEFAULT_ENABLED_STATE = true;
    public static final int POPULATION_SAMPLING_DURATION = 100000;

    public static final long SENSOR_POPULATION_DELAY = 5000000; //mentioned in microseconds
    public static final int MAX_SKIP = 4;
    public static final long CONNECTION_DELAY = 10 * 1000; // mentioned in milliseconds
    public static final String EXTRA_SESSIONID = "extra_session_id";
    public static final String ACTION_FORCE_STOP = "zetaActionForceStop";
    public static final String MESSAGE_WARNING_BATTERY = "BATTERY LOW";
    public static final String MESSAGE_WARNING_STOPPED = "SESSION STOPPED DUE TO LOW BATTERY";
    public static final String MESSAGE_WARNING_STOPPED_U = "SESSION STOPPED DUE TO USER ACTION";
    private static final float MAX_STEP_PER_MINUTE = 300f;
    public static final int BATTERY_MIN = 10;
    public static final int BATTERY_MAX = 20;
    /**
     * Transmission may takes milliseconds to transfer data. To make sure that we have increased transmission delay to 5 seconds
     */
    public static long TRANSMISSION_DELAY_OFFSET = 5000; // mentioned in milliseconds

    public static boolean isHaveStepCounter(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            return packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER);
        } catch (Exception e) {
            return false;
        }
    }

    public static void logString(String TAG, String data) {
        if (data.length() > 4000) {
            Log.v(TAG, "sb.length = " + data.length());
            int chunkCount = data.length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= data.length()) {
                    Log.v(TAG, data.substring(4000 * i));
                } else {
                    Log.v(TAG, data.substring(4000 * i, max));
                }
            }
        } else {
            Log.v(TAG, data);
        }
    }

    public static boolean isStepCountOK(long previousTime, long currentTime, int oldStepCount, int newStepCount) throws Exception {

        if (oldStepCount > newStepCount)
            return false;

        if (previousTime < 1)
            return true;

        float diffTime = Math.abs(previousTime - currentTime);

        int diffStepCount = Math.abs(oldStepCount - newStepCount);

        float diff = (diffTime / (60 * 1000));

        if (diffStepCount / diff <= Constants.MAX_STEP_PER_MINUTE) {
            return true;
        }

        return false;
    }

    public String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }
}
