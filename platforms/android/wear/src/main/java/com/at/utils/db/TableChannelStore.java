package com.at.utils.db;

/**
 * Created by uday on 7/6/16.
 */
public class TableChannelStore {
    public static final String KEY_ID = "id";
    public static final String KEY_CHANNEL = "channelvalue";
    public static final String CHANNEL_TABLE_NAME = "StoredChannels";
    public static final String CHANNEL_TRIGGER_NAME = "ChannelStoreLimit";
}
