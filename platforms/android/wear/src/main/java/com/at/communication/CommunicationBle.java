package com.at.communication;

import android.content.Context;
import android.util.Log;

import com.at.utils.NodeGsonDeserializer;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class CommunicationBle extends Proxy {

    private static final String TAG = CommunicationBle.class.getSimpleName();
    private final Context mContext;
    private final GoogleApiClient mGoogleClient;

    public CommunicationBle(Context context) {
        mContext = context;
        mGoogleClient = new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .build();
    }

    @Override
    public void dataReceive(String data) {

    }

    @Override
    public void dataTransmit(final String data) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!mGoogleClient.isConnected()) {
                    ConnectionResult connectionResult =
                            mGoogleClient.blockingConnect(30, TimeUnit.SECONDS);
                    if (!connectionResult.isSuccess()) {
                        Log.d(TAG, "Failed to connect to GoogleApiClient.");
                        return;
                    }
                }

                NodeApi.GetConnectedNodesResult nodesResult = Wearable.NodeApi.getConnectedNodes(mGoogleClient).await();
                List<Node> nodeList = nodesResult.getNodes();
                if (nodeList != null) {

                    // Logging code
                    GsonBuilder builder = new GsonBuilder();
                    builder.registerTypeAdapter(com.at.model.meta.Node.class, new NodeGsonDeserializer());
                    Gson gsonExt = builder.create();
                    com.at.model.meta.Node nodeObject = new Gson().fromJson(data, com.at.model.meta.Node.class);
                    long timePrev = System.currentTimeMillis();
                    String logMsg = "!time before transmitting " + nodeObject.getUri() + " : " + timePrev;
                    Log.d(TAG, logMsg);

                    for (int i = 0, count = nodeList.size(); i < count; i++) {
                        Node node = nodeList.get(i);
                        MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleClient,
                                node.getId(), "wear_data", data.getBytes()).await();
                        Status status = result.getStatus();
                        Log.d(TAG, "!data transmit status msg: " +
                                status.getStatusMessage() + " , code: " + status.getStatusCode());
                    }
                    long timePost = System.currentTimeMillis();
                    logMsg = "!time after transmitting " + nodeObject.getUri() + " : " +
                            timePost + " , time taken to transmit: " + (timePost - timePrev);
                    Log.d(TAG, logMsg);
                }
            }
        }).start();
    }
}
