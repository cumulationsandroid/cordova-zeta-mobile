package com.at.model.app;

/**
 * Created by uday on 9/6/16.
 */
public interface ISensorState {
    States getState();
    void setState(States states);
}
