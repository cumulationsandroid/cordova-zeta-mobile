package com.at.model.app;

/**
 * Defines current connection state of the phone with the wear
 */
public enum ConnectionStates {
    CONNECTED, CONNECTING, DISCONNECTED, CONNECTIONERROR
}