package com.at.model.app;

public interface INotify {

    public void notify(ErrorTypesZs errorTypes);
    public void notify(ConnectionStates connectionStates);
}