package com.at.model.domain;

import com.at.model.meta.Node;

/**
 * Sensor device running on wearable device for which data is collected
 */
public class Sensor extends Node {

}