package com.at.zeta.zetamobile;

import android.app.IntentService;
import android.content.Intent;

import com.at.model.meta.Node;
import com.at.utils.LoggingService;
import com.at.utils.NodeGsonDeserializer;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class DataSender extends IntentService {

    private static final String TAG = DataSender.class.getSimpleName();
    GoogleApiClient mGoogleClient;

    public DataSender() {
        super("DataSender");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Node channel = (Node) intent.getSerializableExtra("data");
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(Node.class, new NodeGsonDeserializer());
            Gson gsonExt = builder.create();
            String data = gsonExt.toJson(channel, Node.class);

            ConnectionResult connectionResult =
                    mGoogleClient.blockingConnect(30, TimeUnit.SECONDS);
            if (!connectionResult.isSuccess()) {
                LoggingService.d(getApplicationContext(), TAG, "Failed to connect to GoogleApiClient.");
                return;
            }
            NodeApi.GetConnectedNodesResult nodesResult = Wearable.NodeApi.getConnectedNodes(mGoogleClient).await();
            List<com.google.android.gms.wearable.Node> nodeList = nodesResult.getNodes();
            if (nodeList != null) {
                for (int i = 0, count = nodeList.size(); i < count; i++) {
                    com.google.android.gms.wearable.Node node = nodeList.get(i);
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleClient,
                            node.getId(), "wear_data", data.getBytes()).await();
                    Status status = result.getStatus();
                    LoggingService.d(getApplicationContext(), TAG, "!data transmit status msg: " +
                            status.getStatusMessage() + " , code: " + status.getStatusCode());
                }
            }
            mGoogleClient.disconnect();
        }
    }
}