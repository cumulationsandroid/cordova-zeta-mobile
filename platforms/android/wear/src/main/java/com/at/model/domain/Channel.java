package com.at.model.domain;

import com.at.model.app.ErrorTypesZs;
import com.at.model.meta.Node;

/**
 * Individual component of @Sensor hardware for which data is
 * collected by specifying sampling duration.
 */
public class Channel extends Node {

    private int samplingDuration;
    private boolean isEnabled;

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public int getSamplingDuration() {
        return samplingDuration;
    }

    public void setSamplingDuration(int samplingDuration) {
        this.samplingDuration = samplingDuration;
    }

    public ErrorTypesZs sampleData() {
        return ErrorTypesZs.ERR_NO_ERROR;
    }
}