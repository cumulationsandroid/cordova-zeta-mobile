package com.at.utils;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import com.at.zeta.zetamobile.MessageSender;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Wearable;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.NtpV3Packet;
import org.apache.commons.net.ntp.TimeInfo;
import org.apache.commons.net.ntp.TimeStamp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public final class NTPClient implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

  private static NTPClient ntpClient;
  private GoogleApiClient mGoogleApiClient;
  private Context context;


  private List<String> servers;
  private TimeStamp refNtpTime;
  private TimeStamp origNtpTime;
  private TimeStamp rcvNtpTime;
  private TimeStamp xmitNtpTime;
  private TimeStamp destNtpTime;

  private long offset = -10;
  private long delay = -10;
  protected void onCreate(Bundle savedInstanceState) {

  }
  NTPClient() {

    servers = new ArrayList<String>();
    //servers.add("time.windows.com");
    servers.add("0.europe.pool.ntp.org");
    servers.add("1.europe.pool.ntp.org");
    servers.add("2.europe.pool.ntp.org");
    fetchNetworkTime();


   // LoggingService.d(getApplicationContext(), TAG, "!data transmit status msg: " +
     // status.getStatusMessage() + " , code: " + status.getStatusCode());
  }

  public static NTPClient getInstance() {
    if (ntpClient == null) {
      ntpClient = new NTPClient();
    }
    return ntpClient;
  }

  private void fetchNetworkTime(TimeInfo info) {
    NtpV3Packet message = info.getMessage();

    refNtpTime = message.getReferenceTimeStamp();

    // Originate Time is time request sent by client (t1)
    origNtpTime = message.getOriginateTimeStamp();

    long destTime = info.getReturnTime();
    // Receive Time is time request received by server (t2)
    rcvNtpTime = message.getReceiveTimeStamp();

    // Transmit time is time reply sent by server (t3)
    xmitNtpTime = message.getTransmitTimeStamp();

    // Destination time is time reply received by client (t4)
    destNtpTime = TimeStamp.getNtpTime(destTime);

    info.computeDetails(); // compute offset/delay if not already done
    offset = info.getOffset() == null ? 0 : info.getOffset();
    delay = info.getDelay() == null ? 0 : info.getDelay();


  }

  private void fetchNetworkTime() {

    if (servers.size() == 0) {
      System.err.println("Pass the server list");
      System.exit(1);
    }

    NTPUDPClient client = new NTPUDPClient();
    // We want to timeout if a response takes longer than 10 seconds
    client.setDefaultTimeout(10000);
    try {
      client.open();
      for (String host : servers) {
        System.out.println();
        try {
          InetAddress hostAddr = InetAddress.getByName(host);
          //System.out.println("> " + hostAddr.getHostName() + "/" + hostAddr.getHostAddress());
          TimeInfo info = client.getTime(hostAddr);
          fetchNetworkTime(info);
          break;
        } catch (IOException ioe) {
          continue;
        }
      }
    } catch (SocketException e) {
      e.printStackTrace();
    }

    client.close();
  }

  public long getOffset() {
    return offset;
  }

  public long getCorrentTime() {
    return System.currentTimeMillis() + offset;
  }

  public long getCorrentTime(long time) {
    return time + offset;
  }


  private class Featcher extends AsyncTask<Void, Void, Void> {


    protected Void doInBackground(Void... voids) {
      Void res = null;
      fetchNetworkTime();
      return res;
    }

    protected void onProgressUpdate(Integer... progress) {
      //setProgressPercent(progress[0]);
    }

    protected void onPostExecute(Long result) {
      //showDialog("Downloaded " + result + " bytes");
    }

  }


  @Override
  public void onConnected(Bundle connectionHint) {
    Log.i("MESSAGE", "onConnected");
    // We are now connected!
  }

  @Override
  public void onConnectionSuspended(int cause) {
    Log.i("MESSAGE", "onConnectionSuspended");
  }

  @Override
  public void onConnectionFailed(ConnectionResult result) {
    Log.i("MESSAGE", "onConnectionFailed");
  }
}

