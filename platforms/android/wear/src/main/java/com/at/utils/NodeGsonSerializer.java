package com.at.utils;

import android.util.Base64;
import android.util.Log;

import com.at.model.app.SensorAgent;
import com.at.model.domain.Channel;
import com.at.model.domain.Device;
import com.at.model.domain.Sensor;
import com.at.model.meta.Data;
import com.at.model.meta.Node;
import com.at.model.meta.Notification;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by mathan on 23/11/15.
 */
public class NodeGsonSerializer implements JsonSerializer {

    private static final String TAG = NodeGsonSerializer.class.getSimpleName();

    @Override
    public JsonElement serialize(Object object, Type typeOfSrc, JsonSerializationContext context) {
        if (object instanceof Channel) {
            /*Log.d(TAG, "Object " + Channel.class.getSimpleName());*/
            Channel channel = (Channel) ((Node) object);
            JsonObject jsonObject = new JsonObject();
            jsonObject = getNodeJsonObject(jsonObject, channel);
            JsonObject dataObject = new JsonObject();
            Data data = channel.getData();
            if (data != null) {
                dataObject.addProperty("dataType", data.getDataType().toString());
                dataObject.addProperty("timeStamp", data.getTimeStamp());
                if (data.getValues() != null) {
                    Log.d(TAG, "Converted Base 64" + Base64.encodeToString(data.getValues(), Base64
                            .NO_WRAP));
                    JsonElement valuesElement = new JsonPrimitive(Base64.encodeToString(data.getValues(), Base64
                            .NO_WRAP));
                    dataObject.add("values", valuesElement);
                }
                jsonObject.add("data", dataObject);
            }
            jsonObject.add("nodes", getNodesArray(channel, context));
            return jsonObject;
        } else if (object instanceof Sensor) {
            /*Log.d(TAG, "Object " + Sensor.class.getSimpleName());*/
            Sensor sensor = (Sensor) ((Node) object);
            JsonObject jsonObject = new JsonObject();
            jsonObject = getNodeJsonObject(jsonObject, sensor);
            JsonObject dataObject = new JsonObject();
            Data data = sensor.getData();
            if (data != null) {
                dataObject.addProperty("timeStamp", data.getTimeStamp());
                jsonObject.add("data", dataObject);
            }
            jsonObject.add("nodes", getNodesArray(sensor, context));
            return jsonObject;
        } else if (object instanceof Device) {
            JsonObject jsonObject = new JsonObject();
            /*Log.d(TAG, "Object " + Device.class.getSimpleName());*/
            Device device = (Device) ((Node) object);
            jsonObject = getNodeJsonObject(jsonObject, device);
            JsonObject dataObject = new JsonObject();
            Data data = device.getData();
            if (data != null) {
                dataObject.addProperty("timeStamp", data.getTimeStamp());
                jsonObject.add("data", dataObject);
            }
            jsonObject.add("nodes", getNodesArray(device, context));
            return jsonObject;
        } else if (object instanceof SensorAgent) {
            /*Log.d(TAG, "Object " + SensorAgent.class.getSimpleName());*/
            Node node = (Node) object;
            JsonObject jsonObject = new JsonObject();
            jsonObject = getNodeJsonObject(jsonObject, node);
            JsonObject dataObject = new JsonObject();
            Data data = node.getData();
            if (data != null) {
                dataObject.addProperty("timeStamp", data.getTimeStamp());
                jsonObject.add("data", dataObject);
            }
            jsonObject.add("nodes", getNodesArray(node, context));
            return jsonObject;
        } else if (object instanceof Notification) {
            /*Log.d(TAG, "Object " + Notification.class.getSimpleName());*/
            Node node = (Node) object;
            JsonObject jsonObject = new JsonObject();
            jsonObject = getNodeJsonObject(jsonObject, node);
            JsonObject dataObject = new JsonObject();
            Data data = node.getData();
            if (data != null) {
                dataObject.addProperty("dataType", data.getDataType().toString());
                dataObject.addProperty("timeStamp", data.getTimeStamp());
                if (data.getValues() != null) {
                    Log.d(TAG, "Converted Base 64" + Base64.encodeToString(data.getValues(), Base64
                            .NO_WRAP));
                    JsonElement valuesElement = new JsonPrimitive(Base64.encodeToString(data.getValues(), Base64
                            .NO_WRAP));
                    dataObject.add("values", valuesElement);
                }
                jsonObject.add("data", dataObject);
            }
            jsonObject.add("nodes", getNodesArray(node, context));
            return jsonObject;
        } else if (object instanceof Node) {
            /*Log.d(TAG, "Object " + Node.class.getSimpleName());*/
            Node node = (Node) object;
            JsonObject jsonObject = new JsonObject();
            jsonObject = getNodeJsonObject(jsonObject, node);
            JsonObject dataObject = new JsonObject();
            Data data = node.getData();
            if (data != null) {
                dataObject.addProperty("timeStamp", data.getTimeStamp());
                if (data.getDataType() != null) {
                    dataObject.add("dataType", new JsonPrimitive(data.getDataType().toString()));
                }
                if (data.getValues() != null) {
                    dataObject.add("values", new JsonPrimitive(Base64.encodeToString(data.getValues(), Base64.NO_WRAP)));
                }
                jsonObject.add("data", dataObject);
            }
            jsonObject.add("nodes", getNodesArray(node, context));
            return jsonObject;
        }

        /*Log.d(TAG, "Excited " + object.getClass().getSimpleName());*/
        return context.serialize(object, typeOfSrc);
    }

    private JsonObject getNodeJsonObject(JsonObject jsonObject, Node node) {
        if (node == null) {
            return null;
        }
        jsonObject.addProperty("name", node.getName());
        jsonObject.addProperty("description", node.getDescription());
        jsonObject.addProperty("guid", node.getGuid().toString());
        jsonObject.addProperty("uri", node.getUri());
        jsonObject.addProperty("type", node.getType().toString());
        if (node instanceof Channel) {
            Channel channel = (Channel) node;
            jsonObject.addProperty("samplingDuration", channel.getSamplingDuration());
            jsonObject.addProperty("isEnabled", channel.isEnabled());
        }

        return jsonObject;
    }

    private JsonArray getNodesArray(Node node, JsonSerializationContext context) {
        ArrayList<? extends Node> nodes = node.getNodes();
        JsonArray array = new JsonArray();
        for (int i = 0; i < nodes.size(); i++) {
            JsonElement jsonElement = context.serialize(nodes.get(i), Node.class);
            if(jsonElement != null && !jsonElement.isJsonNull()) {
                JsonObject channelObject = jsonElement.getAsJsonObject();
                array.add(channelObject);
            }
        }
        return array;
    }

}
