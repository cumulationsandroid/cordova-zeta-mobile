package com.at.utils.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by uday on 7/6/16.
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String MAX_VALUE = "max_range";
    public static final long AVERAGE_CHANNEL_SIZE = 700;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "zetasense.db";
    private static final String TAG = "DataBaseHelper";
    private long MAX_RANGE;

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        MAX_RANGE = mSharedPrefs.getLong(MAX_VALUE, 1000);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_CMD = "CREATE TABLE " + TableChannelStore.CHANNEL_TABLE_NAME + "("
                + TableChannelStore.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + TableChannelStore.KEY_CHANNEL + " TEXT )";
        db.execSQL(CREATE_TABLE_CMD);
        String CREATE_TABLE_CMD_MW = "CREATE TABLE " + TableChannelStore.CHANNEL_TABLE_NAME_MW + "("
                + TableChannelStore.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + TableChannelStore.KEY_CHANNEL + " TEXT )";
        db.execSQL(CREATE_TABLE_CMD_MW);
    }

    /**
     * This will make sure that the total number of row is not exceeded {@link #MAX_RANGE}
     */
    public void setTrigger() {
        if (MAX_RANGE < 100)
            MAX_RANGE = 100;
        Log.e(TAG, "Trigger is set");
        String CREATE_TRIGGER_CMD = "CREATE TRIGGER " + TableChannelStore.CHANNEL_TRIGGER_NAME + " INSERT ON " + TableChannelStore.CHANNEL_TABLE_NAME + " WHEN (select count(*) from " + TableChannelStore.CHANNEL_TABLE_NAME + ")>=" + MAX_RANGE + " " +
                "BEGIN" +
                " DELETE FROM " + TableChannelStore.CHANNEL_TABLE_NAME + " WHERE " + TableChannelStore.KEY_ID + " IN  (SELECT " + TableChannelStore.KEY_ID + " FROM " + TableChannelStore.CHANNEL_TABLE_NAME + " ORDER BY " + TableChannelStore.KEY_ID + " ASC LIMIT (select count(*) -" + MAX_RANGE + " from " + TableChannelStore.CHANNEL_TABLE_NAME + "));" +
                " END;";
        getWritableDatabase().execSQL(CREATE_TRIGGER_CMD);
    }

    public void dropTrigger() {
        Log.e(TAG, "Trigger is dropped");
        String CREATE_TRIGGER_CMD = "DROP TRIGGER IF EXISTS " + TableChannelStore.CHANNEL_TRIGGER_NAME;
        getWritableDatabase().execSQL(CREATE_TRIGGER_CMD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TableChannelStore.CHANNEL_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableChannelStore.CHANNEL_TABLE_NAME_MW);
        onCreate(db);
    }

    public void resetSqlSequence(String tableName) {
        getWritableDatabase().execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + tableName + "'");
    }

}
