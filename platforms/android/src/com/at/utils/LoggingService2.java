package com.at.utils;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import com.at.model.app.SensorAgent;
import com.at.model.domain.Channel;
import com.at.model.domain.Device;
import com.at.model.meta.Node;
import com.at.plugin.ProxyCommPlugin;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.ByteBuffer;

/**
 * Created by mathan on 12/8/15.
 */


public class LoggingService2 extends IntentService {

    public static final String TAG = "LoggingService2";
    public static final String FILE_NAME = "ZMlogs.csv";
    public static final String LOG_TAG = "log_tag";
    public static final String LOG_MSG = "log_msg";
    public static String headerName = "\"created\",\"owner\",\"type\",\"sensor\",\"steps\",\"x\",\"y\",\"z\"";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public LoggingService2() {
        super(LoggingService2.class.getSimpleName());
    }

    public static void d(Context context, String tag, String message) {
        Log.d(tag, message);
        Intent intent = new Intent(context.getApplicationContext(), LoggingService2.class);
        intent.putExtra(LOG_TAG, tag);
        intent.putExtra(LOG_MSG, message);
        context.startService(intent);
    }

    public static void reset(Context context) {
        Intent intent = new Intent(context.getApplicationContext(), LoggingService2.class);
        intent.putExtra(LOG_TAG, "reset");
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String logMsg = intent.getStringExtra(LOG_MSG);
        String logTag = intent.getStringExtra(LOG_TAG);

        if (logTag.equalsIgnoreCase("reset")) {
            resetFile();
            return;
        }

        Log.d(logTag, "!inside onHandleIntent " + logMsg);
        Node node = null;
        try {
            node = objectify(logMsg);
        } catch (Exception e) {
            e.printStackTrace();
            saveLogFile("\"ExceptionWhileObjectifying\",,,,,,,,");
            return;
        }

        if (node != null && node instanceof Channel) {
            String formattedData = "\"" + node.getData().getTimeStamp() + "\",,,";
            String value = getValue(node.getData().getValues());
            String uri = node.getUri().toLowerCase();
            String preFix = "Wear - ";
            if (uri.contains("metawear")) {
                preFix = "MetaWear - ";
            }

            String[] temp = node.getDescription().split("\\.");
            String tempStr = temp[temp.length - 2];
            String sensorName = tempStr.substring(0, 1).toUpperCase() + tempStr.substring(1);

            if (node.getDescription().startsWith("android.sensor.step_counter")) {
                formattedData = formattedData + "\"" + preFix + sensorName + "\"" + ",\"" + value + "\",,,";
            } else if (node.getName().endsWith("x")) {
                formattedData = formattedData + "\"" + preFix + sensorName + "\"" + ",,\"" + value + "\",,";
            } else if (node.getName().endsWith("y")) {
                formattedData = formattedData + "\"" + preFix + sensorName + "\"" + ",,,\"" + value + "\",";
            } else {
                formattedData = formattedData + "\"" + preFix + sensorName + "\"" + ",,,,\"" + value + "\"";
            }

            saveLogFile(formattedData);
        }
    }

    private String getValue(byte[] values) {
        return ByteBuffer.wrap(values).getFloat() + "";
    }

    private synchronized void saveLogFile(String sBody) {

        try {
            File root = new File(Environment.getExternalStorageDirectory(), ProxyCommPlugin.FOLDER_NAME);
            boolean status = false;
            if (!root.exists()) {
                status = root.mkdirs();
            }
            File logFile = new File(root, FILE_NAME);
            if (!logFile.exists()) {
                status = logFile.createNewFile();
                sBody = headerName + "\n" + sBody;
            }

            FileWriter writer = new FileWriter(logFile, true);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(sBody + "\n");
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetFile() {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), ProxyCommPlugin.FOLDER_NAME);
            boolean status = false;
            if (!root.exists()) {
                status = root.mkdirs();
            }
            File logFile = new File(root, FILE_NAME);
            if (!logFile.exists()) {
                status = logFile.createNewFile();
            }

            PrintWriter writer = new PrintWriter(logFile);
            writer.print(LoggingService2.headerName+"\n");
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Converts json format of data to object node
     *
     * @param data input in json format
     * @return ERR_NO_ERROR if conversion is successful else throws error of type ErrorTypesZs
     */
    public Node objectify(String data) throws Exception {
        Node node = null;
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonDeserializer());
        builder.registerTypeAdapter(Channel.class, new NodeGsonDeserializer());
        Gson gsonExt = builder.create();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(data).getAsJsonObject();
        if (jsonObject.getAsJsonPrimitive("uri") != null) {
            String uri = jsonObject.getAsJsonPrimitive("uri").getAsString();
            if (uri.startsWith(Device.class.getSimpleName())) {
                node = gsonExt.fromJson(data, Device.class);
            } else if (uri.startsWith(SensorAgent.class.getSimpleName())) {
                node = gsonExt.fromJson(data, Node.class);
            } else if (uri.contains(SensorAgent.class.getSimpleName())) {
                node = gsonExt.fromJson(data, Node.class);
            } else if (uri.startsWith(Channel.class.getSimpleName())) {
                node = gsonExt.fromJson(data, Channel.class);
            }
            if (node != null) {
                Log.d(TAG, "!inside objectify, node uri: " + node.getUri());
            }
        }

        return node;
    }
}
