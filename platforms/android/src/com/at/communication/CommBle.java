package com.at.communication;

import android.content.Context;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.at.model.app.ConnectionRole;
import com.at.model.app.ConnectionStates;
import com.at.model.app.IDataReadyExternal;
import com.at.model.app.INotify;
import com.at.plugin.ProxyCommPlugin;
import com.at.plugin.ProxyCommPluginCallback;
import com.at.utils.Constants;
import com.at.utils.LoggingService2;
import com.at.utils.NTPClient;
import com.at.utils.NodeGsonDeserializer;
import com.at.utils.db.DatabaseWorker;
import com.at.zeta.zetamobile.ZetaApplication;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CommBle implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

  public static final int DATA_TRANSMISSION_SUCCESS = 0;
  public static final String FOLDER_NAME = "Zeta_Mobile";
  private static final String TAG = CommBle.class.getSimpleName();
  public static SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy-HH_mm_ss");
  static String fileName = "";
  private final Context mContext;
  private final GoogleApiClient mGoogleClient;
  private final IDataReadyExternal iSensorAgent;
  private final INotify iNotify;
  private final Handler mainHandler = new Handler();
  private final ProxyCommPluginCallback mProxyCommPlugin;
  private NodeApi.NodeListener peerListener;


  public CommBle(Object context, IDataReadyExternal iSensorAgent, INotify iNotify,
                 ProxyCommPluginCallback proxyCommPlugin) {
    mContext = (Context) context;
    this.iSensorAgent = iSensorAgent;
    this.iNotify = iNotify;
    this.mProxyCommPlugin = proxyCommPlugin;

    mGoogleClient = new GoogleApiClient.Builder(mContext)
      .addApi(Wearable.API)
      .addConnectionCallbacks(this)
      .addOnConnectionFailedListener(this)
      .build();
    mGoogleClient.connect();

    mProxyCommPlugin.sendConnectionStatusToPlugin(ConnectionStates.CONNECTING);
    BleMsgReceiver.setIDataReadyExternal(iSensorAgent);
    BleMsgReceiver.setProxyCommPlugin(proxyCommPlugin);
  }

  private static void logString(String data) {
    if (data.length() > 4000) {
      Log.v(TAG, "sb.length = " + data.length());
      int chunkCount = data.length() / 4000;     // integer division
      for (int i = 0; i <= chunkCount; i++) {
        int max = 4000 * (i + 1);
        if (max >= data.length()) {
          Log.v(TAG, data.substring(4000 * i));
        } else {
          Log.v(TAG, data.substring(4000 * i, max));
        }
      }
    } else {
      Log.v(TAG, data);
    }
  }

  public void transmitData(final String data) {
    new Thread(new Runnable() {
      @Override
      public void run() {
        if (!mGoogleClient.isConnected()) {
          ConnectionResult connectionResult =
            mGoogleClient.blockingConnect(30, TimeUnit.SECONDS);
          if (!connectionResult.isSuccess()) {
            Log.d(TAG, "Failed to connect to GoogleApiClient.");
            mainHandler.post(new Runnable() {
              @Override
              public void run() {
                mProxyCommPlugin.sendFailMessageToPlugin("Creating connection with Wear failed");
                mProxyCommPlugin.sendConnectionStatusToPlugin(ConnectionStates.CONNECTIONERROR);
              }
            });
            return;
          }
        }

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(com.at.model.meta.Node.class, new NodeGsonDeserializer());
        Gson gsonExt = builder.create();

        com.at.model.meta.Node nodeObject = null;
        try {
          nodeObject = gsonExt.fromJson(data, com.at.model.meta.Node.class);
        } catch (Exception e) {
          // not meant to catch the exception
          throw new IllegalStateException(e.toString() + "\nInput is -->" + data);
        }
        long timePrev = System.currentTimeMillis();
        String logMsg = "!time before transmitting " + nodeObject.getUri() + " : " + timePrev;
        Log.d(TAG, logMsg);

        Constants.logString(TAG, data);
        NodeApi.GetConnectedNodesResult nodesResult = Wearable.NodeApi.getConnectedNodes(mGoogleClient).await();
        List<Node> nodeList = nodesResult.getNodes();
        if (nodeList != null && nodeList.size() > 0) {

          // Logging code
          String wear_IDs = "";
          for (int i = 0, count = nodeList.size(); i < count; i++) {
            Node node = nodeList.get(i);
            MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleClient, node.getId(), "zeta_sense", data.getBytes()).await();
            Status status = result.getStatus();
            Log.d(TAG, "!data transmit status msg: " + status.getStatusMessage() + " , code: " + status.getStatusCode());
            if (status.getStatusCode() != DATA_TRANSMISSION_SUCCESS) {
              wear_IDs += " Device-ID : " + node.getId();
            }
          }
          if (wear_IDs.length() > 0) {
            final String finalWear_IDs = wear_IDs;
            mainHandler.post(new Runnable() {
              @Override
              public void run() {
                mProxyCommPlugin.sendFailMessageToPlugin("Data transmission failed for" + finalWear_IDs);
              }
            });
          }
        } else {
          mainHandler.post(new Runnable() {
            @Override
            public void run() {
              mProxyCommPlugin.sendFailMessageToPlugin("Wear device not found");
            }
          });

        }

        long timePost = System.currentTimeMillis();
        logMsg = "!time after transmitting " + nodeObject.getUri() + " : " + timePost + " , time taken to transmit: " + (timePost - timePrev);
        Log.d(TAG, logMsg);

      }
    }).start();
  }

  public ConnectionRole getConnectionRole() {
    return ConnectionRole.SERVER;
  }

  public ConnectionStates getConnectionStatus() {
    return ConnectionStates.CONNECTED;
  }

  public void post(ConnectionStates connectionStates) {

  }

  @Override
  public void onConnected(Bundle bundle) {

    new Thread(new Runnable() {
      @Override
      public void run() {
        NodeApi.GetConnectedNodesResult nodesResult = Wearable.NodeApi.getConnectedNodes(mGoogleClient).await();
        final List<Node> nodeList = nodesResult.getNodes();
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
          @Override
          public void run() {
            if (nodeList != null && nodeList.size() > 0) {
              mProxyCommPlugin.sendConnectionStatusToPlugin(ConnectionStates.CONNECTED);
            } else {
              mProxyCommPlugin.sendConnectionStatusToPlugin(ConnectionStates.DISCONNECTED);
            }
          }
        });
      }
    }).start();

    if (mGoogleClient != null && peerListener != null) {
      Wearable.NodeApi.removeListener(mGoogleClient, peerListener);
    }

    peerListener = new NodeApi.NodeListener() {
      @Override
      public void onPeerConnected(Node node) {
        mProxyCommPlugin.sendConnectionStatusToPlugin(ConnectionStates.CONNECTED);
      }

      @Override
      public void onPeerDisconnected(Node node) {
        mProxyCommPlugin.sendConnectionStatusToPlugin(ConnectionStates.DISCONNECTED);
      }
    };
    Wearable.NodeApi.addListener(mGoogleClient, peerListener);
  }

  @Override
  public void onConnectionSuspended(int i) {
    mProxyCommPlugin.sendConnectionStatusToPlugin(ConnectionStates.CONNECTIONERROR);
  }

  @Override
  public void onConnectionFailed(ConnectionResult connectionResult) {
    mProxyCommPlugin.sendConnectionStatusToPlugin(ConnectionStates.CONNECTIONERROR);
  }

  public static class BleMsgReceiver extends WearableListenerService {

    private static final String TAG = BleMsgReceiver.class.getSimpleName();
    private static IDataReadyExternal sensorAgent;
    private static ProxyCommPluginCallback mProxyCommPlugin;
    private SensorManager mSensorManager;
    private ConnectionResult connectionResult;
    private DatabaseWorker databaseWorker;

    public static void setIDataReadyExternal(IDataReadyExternal iSensorAgent) {
      sensorAgent = iSensorAgent;
    }

    public static void setProxyCommPlugin(ProxyCommPluginCallback proxyCommPlugin) {
      mProxyCommPlugin = proxyCommPlugin;
    }

    @Override
    public void onCreate() {
      super.onCreate();
      mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    }

    @Override
    public void onDestroy() {
      super.onDestroy();
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
      Log.e(TAG, "onMessageReceived");
      String path = messageEvent.getPath();
      Log.e(TAG, "Path: " + path + " | ");
      if (path.startsWith("get_logs")) {
        loadFileFromAsset(messageEvent.getData(), path);
        return;
      }

      if (path.startsWith("get_time")) {
        NTPClient ntpClient = NTPClient.getInstance();
        long time = ntpClient.getCorrentTime();
        /*
        ByteBuffer buffer = ByteBuffer.allocate(Long.bitCount(time));
        buffer.putLong(time);
        byte[] msg = buffer.array();
        */
        String msg = String.valueOf(time);
        GoogleApiClient mGoogleClient = new GoogleApiClient.Builder(this)
          .addApi(Wearable.API)
          .build();
        if (mGoogleClient != null && !(mGoogleClient.isConnected() || mGoogleClient.isConnecting()))
          mGoogleClient.connect();

        MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleClient,
          "time_sync", "get_time", msg.getBytes()).await();
        Status status = result.getStatus();
        Log.e(TAG, "Status " + status);

        return;
      }


      String data = new String(messageEvent.getData());
      logString("!inside onMessageReceived PATH:" + messageEvent.getPath() + ", DATA:" + data);

      if (mProxyCommPlugin != null) {
        databaseWorker = null;
        mProxyCommPlugin.sendMessageToPlugin(data);
      } else {
        sendStopNode(this);
        backupChannelData(data);
        return;
      }
      LoggingService2.d(BleMsgReceiver.this.getApplicationContext(), "received data", data);
    }

    private void sendStopNode(final Context mContext) {
      new Thread(new Runnable() {
        @Override
        public void run() {

          GoogleApiClient mGoogleClient = new GoogleApiClient.Builder(mContext)
            .addApi(Wearable.API)
            .build();
          ConnectionResult connectionResult = mGoogleClient.blockingConnect(30, TimeUnit.SECONDS);
          if (!connectionResult.isSuccess()) {
            try {
              mGoogleClient.disconnect();
            } catch (Exception e) {
              mGoogleClient = null;
            }
            return;
          }

          Constants.logString(TAG, Constants.STOP_SESSION);
          NodeApi.GetConnectedNodesResult nodesResult = Wearable.NodeApi.getConnectedNodes(mGoogleClient).await();
          List<Node> nodeList = nodesResult.getNodes();
          if (nodeList != null && nodeList.size() > 0) {
            for (int i = 0, count = nodeList.size(); i < count; i++) {
              Node node = nodeList.get(i);
              MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleClient, node.getId(), "zeta_sense", Constants.STOP_SESSION.getBytes()).await();
              Status status = result.getStatus();
              Log.d(TAG, "!data transmit status msg: " + status.getStatusMessage() + " , code: " + status.getStatusCode());
            }
          }
        }
      }).start();
    }

    private void backupChannelData(String data) {
      try {
        JSONObject jsonObject = new JSONObject(data);
        if (jsonObject.getString("uri").startsWith("Channel")) {
          if (databaseWorker == null) {
            databaseWorker = ((ZetaApplication) getApplication()).getDataBase();
          }
          databaseWorker.insert(data);
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    private void loadFileFromAsset(byte[] asset, String path) {

      if (path.equalsIgnoreCase("get_logs_end")) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override
          public void run() {
            Toast.makeText(getApplicationContext(), "Logs received", Toast.LENGTH_SHORT).show();
          }
        });

        File root = new File(Environment.getExternalStorageDirectory(), ProxyCommPlugin.FOLDER_NAME);
        File file = new File(root, "file_path.txt");

        StringBuilder text = new StringBuilder();
        String line = "";
        try {
          BufferedReader br = new BufferedReader(new FileReader(file));
          while ((line = br.readLine()) != null) {
            text.append(line);
          }
          line = text.toString();
          br.close();
        } catch (IOException e) {
        }

        File logFile2 = new File(root, LoggingService2.FILE_NAME);
        File logFile = new File(root, fileName);

        logFile2.renameTo(new File(root + "/" + line + "/", logFile2.getName()));
        logFile.renameTo(new File(root + "/" + line + "/", logFile.getName()));

        File[] files = new File(root, line).listFiles();
        if (files != null)
          for (int i = 0; i < files.length; i++) {
            Log.e(TAG, "FILE_PATH " + i + " " + files[i].getAbsolutePath());
          }

        fileName = "";

        return;
      }

      File mConfigurationFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), FOLDER_NAME);
      if (!mConfigurationFile.exists()) {
        mConfigurationFile.mkdir();
      }

      if (fileName == null || fileName.length() == 0) {
        fileName = "ZSlogs.csv";
      }

      final File logFile = new File(Environment.getExternalStorageDirectory(), FOLDER_NAME + "/" + fileName);
      if (!logFile.exists()) {
        try {
          logFile.createNewFile();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }

      FileWriter fw = null;
      BufferedWriter bw = null;
      PrintWriter out = null;
      try {
        fw = new FileWriter(logFile, true);
        bw = new BufferedWriter(fw);
        out = new PrintWriter(bw);
        out.append(new String(asset).trim());
        out.close();
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (out != null)
            out.close();
        } catch (Exception e) {
          e.printStackTrace();
        }
        try {
          if (bw != null)
            bw.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
        try {
          if (fw != null)
            fw.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
