package com.at.communication;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.at.data.DataAcquisitionMW2;
import com.at.model.app.ConnectionStates;
import com.at.model.app.INotify;
import com.at.model.app.ISensorState;
import com.at.model.app.States;
import com.at.utils.Constants;
import com.at.zeta.zetamobile.ZetaApplication;

import java.lang.reflect.Method;
import java.util.UUID;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class GenericBle extends Service {

    public static final String ACTION_START_READING = "com.at.utils.wakehelper.start";
    public static final String ACTION_STOP_READING = "com.at.utils.wakehelper.stop";
    public static final String ACTION_CONNECT = "com.at.utils.wakehelper.connect";
    public static final String ACTION_DISCONNECT = "com.at.utils.wakehelper.disconnect";

    public static final int NOTIFICATION_ID = 82;
    public static final String ACTION_FORCE_STOP_READING = "com.at.utils.wakehelper.forcestop";
    private static final int RETRY_COUNT = Integer.MAX_VALUE;
    private static final long RETRY_INTERVAL = 10000;
    private static final String TAG = "JL-" + GenericBle.class.getSimpleName();
    Handler handler = new Handler();
    BluetoothAdapter bluetoothAdapter;
    private DataAcquisitionMW2 dataAcquisition;
    private BluetoothGatt bluetoothGatt;
    private int retryCount = 0;
    private IBinder mBinder = new LocalBinder();
    private String macAddress;
    private BluetoothManager bluetoothManager;
    private BluetoothDevice bluetoothDevice;
    private INotify iNotify;
    private ISensorState iSensorState;
    private ServiceCallback serviceCallback;
    private boolean notifySensorAgent = true;
    private ConnectionStates pendingConnectionState = null;
    private int oldStatus = -1;
    private Runnable connectRunnable = new Runnable() {
        @Override
        public void run() {
            connect();
        }
    };
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        Log.e(TAG, "Bluetooth off");
                        handler.post(connectRunnable);
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.e(TAG, "Turning Bluetooth off");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.e(TAG, "Bluetooth on");
                        if (bluetoothGatt == null) {
                            handler.removeCallbacks(connectRunnable);
                            handler.post(connectRunnable);
                        } else {
                            int connectionState = bluetoothManager.getConnectionState(bluetoothGatt.getDevice(), BluetoothProfile.GATT);
                            if (connectionState != BluetoothGatt.STATE_CONNECTED || connectionState != BluetoothGatt.STATE_CONNECTING) {
                                handler.removeCallbacks(connectRunnable);
                                handler.post(connectRunnable);
                            }
                        }
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.e(TAG, "Turning Bluetooth on");
                        break;
                }
            }
        }
    };
    private States pendingSensorState;
    private boolean isForeground = false;
    private BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d(TAG, "!inside onConnectionStateChange, status: " + status + " , new state: " + newState);
            if (status == 0) {
                notifySensorAgent = true;
                oldStatus = -1;
                switch (newState) {
                    case BluetoothProfile.STATE_CONNECTED:
                        handler.removeCallbacks(connectRunnable);
                        retryCount = 0;
                        bluetoothGatt = gatt;
                        dataAcquisition.onConnectionStateChange(gatt, status, newState);
                        break;

                    case BluetoothProfile.STATE_DISCONNECTED:
                        dataAcquisition.onConnectionStateChange(gatt, status, newState);
                        if (retryCount < RETRY_COUNT) {
                            handler.removeCallbacks(connectRunnable);
                            handler.post(connectRunnable);
                            break;
                        }
                        break;
                }
            } else {
                if (notifySensorAgent || oldStatus != status) {
                    dataAcquisition.onConnectionStateChange(gatt, status, newState);
                }
                oldStatus = status;
                notifySensorAgent = false;
                retryCount = 0;
                handler.removeCallbacks(connectRunnable);
                handler.postDelayed(connectRunnable, RETRY_INTERVAL / 4); // reconnection should be faster
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            dataAcquisition.onServicesDiscovered(gatt, status);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            dataAcquisition.onCharacteristicRead(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            dataAcquisition.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            dataAcquisition.onCharacteristicChanged(gatt, characteristic);
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            dataAcquisition.onDescriptorRead(gatt, descriptor, status);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            dataAcquisition.onDescriptorWrite(gatt, descriptor, status);
        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            dataAcquisition.onReliableWriteCompleted(gatt, status);
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            dataAcquisition.onReadRemoteRssi(gatt, rssi, status);
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dataAcquisition.onMtuChanged(gatt, mtu, status);
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null || intent.getAction() == null)
            return START_NOT_STICKY;
        if (intent.getAction().equals(ACTION_START_READING)) {
            // received command to start data reading from sensor
        } else if (intent.getAction().equals(ACTION_STOP_READING)) {
            // received command to stop data reading from sensor
            stopForegroundCUS(true);
            if (serviceCallback != null)
                serviceCallback.onStop();
        } else if (intent.getAction().equals(ACTION_FORCE_STOP_READING)) {
            // received command to stop data reading from sensor
            sendBroadcast(new Intent(Constants.ACTION_FORCE_STOP));
            stopForegroundCUS(true);
            if (serviceCallback != null)
                serviceCallback.onStop();
        } else if (intent.getAction().equals(ACTION_CONNECT)) {
            String macAddress = intent.getStringExtra(Constants.EXTRA_MAC_ADDRESS);
            if (macAddress != null && macAddress.length() > 0) {
                this.macAddress = macAddress;
            }
            handler.removeCallbacks(connectRunnable);
            handler.post(connectRunnable);
        } else if (intent.getAction().equals(ACTION_DISCONNECT)) {
            if (dataAcquisition.canClose()) {
                notifyConnectionState(ConnectionStates.DISCONNECTED);
                disconnect();
                ((ZetaApplication) getApplication()).getSensorAgent("", null);
                stopSelf();
            }
        }
        return START_NOT_STICKY;
    }

    public void setDataAcquisition(DataAcquisitionMW2 dataAcquisition) {
        this.dataAcquisition = dataAcquisition;
    }

    public BluetoothGattService getService(UUID serviceUuid) {
        return bluetoothGatt != null ? bluetoothGatt.getService(serviceUuid) : null;
    }

    public boolean writeCharacteristic(BluetoothGattCharacteristic gattCharacteristic) {
        return bluetoothGatt != null && bluetoothGatt.writeCharacteristic(gattCharacteristic);
    }

    public boolean enableCharNotification(BluetoothGattCharacteristic gattCharacteristic) {
        boolean status = false;
        if (gattCharacteristic != null && bluetoothGatt != null) {
            bluetoothGatt.setCharacteristicNotification(gattCharacteristic, true);
            BluetoothGattDescriptor gattDescriptor = gattCharacteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
            if (gattDescriptor != null) {
                gattDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                status = bluetoothGatt.writeDescriptor(gattDescriptor);

                Log.d(TAG, "!writing descriptor, status: " + status + " , service uuid: " +
                        gattCharacteristic.getService().getUuid().toString() +
                        " , char uuid: " + gattCharacteristic.getUuid().toString() +
                        " , desc uuid: " + gattDescriptor.getUuid());
            }
        }
        return status;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void connect(String macAddress) {
        Log.e(TAG, "Connect(str) Invoked");
        if (/*!notifySensorAgent ||*/ (this.macAddress != null && !this.macAddress.equals(macAddress))) {
            // TODO : it's better to stop foreground service for new mac address
            stopForegroundCUS(true);
        }
        this.macAddress = macAddress;
        handler.removeCallbacks(connectRunnable);
        handler.post(connectRunnable);
    }

    private void connect() {
        Log.e(TAG, "connect Invoked. RetryCount is " + retryCount);
        handler.removeCallbacks(connectRunnable);

        try {

            disconnect();

            if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled() || macAddress == null || macAddress.length() == 0) {
                retryCount = 0;
                notifyConnectionState(ConnectionStates.CONNECTIONERROR);
                Log.e(TAG, "-----check-----\nmacAddress\nbluetooth connection\n--------------");
                return;
            }

            notifyConnectionState(ConnectionStates.CONNECTING);

            bluetoothDevice = bluetoothAdapter.getRemoteDevice(this.macAddress);
            int connectionState = bluetoothManager.getConnectionState(bluetoothDevice, BluetoothProfile.GATT);

            if (connectionState != BluetoothProfile.STATE_CONNECTED && connectionState != BluetoothProfile.STATE_CONNECTING) {
                bluetoothGatt = bluetoothDevice.connectGatt(this, false, bluetoothGattCallback);
            }

        } catch (Exception e) {
            e.printStackTrace();
            notifyConnectionState(ConnectionStates.CONNECTIONERROR);

        }

        if (retryCount < RETRY_COUNT) {
            handler.postDelayed(connectRunnable, RETRY_INTERVAL);
            retryCount++;
        } else {
            retryCount = 0;
        }

    }

    public void setINotify(INotify iNotify) {
        this.iNotify = iNotify;
        if (pendingConnectionState != null) {
            notifyConnectionState(pendingConnectionState);
            pendingConnectionState = null;
        }
    }

    public void notifyConnectionState(ConnectionStates connectionStates) {
        if (iNotify != null) {
            iNotify.notify(ConnectionStates.CONNECTIONERROR);
        } else {
            pendingConnectionState = connectionStates;
        }
    }

    /**
     * This returns the {@link #bluetoothManager }
     *
     * @return The value of {@link #bluetoothManager }
     */
    public BluetoothManager getBluetoothManager() {
        return bluetoothManager;
    }

    /**
     * This returns the {@link #bluetoothDevice }
     *
     * @return The value of {@link #bluetoothDevice }
     */
    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    /**
     * This returns the {@link #serviceCallback }
     *
     * @return The value of {@link #serviceCallback }
     */
    public ServiceCallback getServiceCallback() {
        return serviceCallback;
    }

    /**
     * Sets the serviceCallback value
     *
     * @param serviceCallback The value will set to {@link #serviceCallback}
     */
    public void setServiceCallback(ServiceCallback serviceCallback) {
        this.serviceCallback = serviceCallback;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mReceiver, filter);
    }

    public void reset() {
        // for now nothing to do here
    }

    private boolean refreshDeviceCache(BluetoothGatt gatt) {
        try {
            BluetoothGatt localBluetoothGatt = gatt;
            Method localMethod = localBluetoothGatt.getClass().getMethod("refresh", new Class[0]);
            if (localMethod != null) {
                boolean bool = ((Boolean) localMethod.invoke(localBluetoothGatt, new Object[0])).booleanValue();
                return bool;
            }
        } catch (Exception localException) {
            Log.e(TAG, "An exception occurred while refreshing BluetoothGatt");
        }
        return false;
    }

    public void disconnect() {
        try {
            if (bluetoothGatt != null) {
                refreshDeviceCache(bluetoothGatt);
                bluetoothGatt.close();
                bluetoothGatt = null;
            }
        } catch (Exception e) {
            bluetoothGatt = null;
        }
    }

    public void setISensorState(ISensorState iSensorState) {
        this.iSensorState = iSensorState;
        if (pendingSensorState != null) {
            notifySensorState(pendingSensorState);
            pendingSensorState = null;
        }
    }

    private void notifySensorState(States states) {
        if (iSensorState != null) {
            iSensorState.setState(states);
        } else {
            pendingSensorState = states;
        }
    }

    public void startForegroundCUS(int id, Notification notification) {
        isForeground = true;
        startForeground(id, notification);
    }

    public void stopForegroundCUS(boolean removeNotification) {
        isForeground = false;
        stopForeground(removeNotification);
    }

    public boolean isForeground() {
        return isForeground;
    }

    public interface ServiceCallback {
        void onStart();

        void onStop();

        void onDestroy();
    }

    /**
     * {@code LocalBinder} is used to bind the service from outside
     */
    public class LocalBinder extends Binder {
        public GenericBle getService() {
            return GenericBle.this;
        }
    }

}
