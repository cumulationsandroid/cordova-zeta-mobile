package com.at.communication;

import com.at.model.app.ConnectionRole;
import com.at.model.app.ConnectionStates;

public interface IComm {

    public void transmitData(String data);
    public ConnectionStates getConnectionStatus();
    public ConnectionRole getConnectionRole();
}