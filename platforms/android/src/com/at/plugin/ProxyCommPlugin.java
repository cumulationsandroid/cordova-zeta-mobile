package com.at.plugin;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.at.communication.CommBle;
import com.at.communication.ProxyComm;
import com.at.model.app.ConnectionStates;
import com.at.model.app.IDataReadyExternal;
import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;
import com.at.model.meta.NodeType;
import com.at.model.meta.Notification;
import com.at.utils.Constants;
import com.at.utils.LoggingService2;
import com.at.utils.NTPClient;
import com.at.utils.db.DatabaseWorker;
import com.at.zeta.zetamobile.DeviceListActivity;
import com.at.zeta.zetamobile.ZetaApplication;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 *
 */
public class ProxyCommPlugin extends CordovaPlugin implements ProxyCommPluginCallback {

  public static final String FOLDER_NAME = "Zeta_Mobile";
  private static final String TAG = "JL-" + ProxyCommPlugin.class.getSimpleName();
  private final String SHOW_BLE_SCAN_UI_NODE = "{\"name\": \"showDeviceListing\",\"description\": \"Show BLE scan UI\",\"guid\": \"804e1d1f-2a92-459c-8be8-56e6f116f2f0\",\"uri\": \"Method://MetaWear.showDeviceListing\",\"type\": \"METHOD\",\"data\": {\"timeStamp\": 0},\"nodes\": []}";
  private ProxyComm mProxyComm;
  private CallbackContext mCallbackContext = null;
  private String BASE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
  private String FILE_NAME = "configuration_string.txt";
  private File mConfigurationFile;
  private String DEFAULT_CONFIGURATION = "{ \"name\":\"setConfiguration\", \"description\":\"SensorAgent setConfiguration\",\"guid\":\"c5c02dc1-d325-4419-9eb8-5f3916896477\", \"uri\":\"Method://3d0523bd64000f81.Motorolametallica.SensorAgent.setConfiguration\", \"type\":\"METHOD\", \"data\":{ \"timeStamp\":0 }, \"nodes\":[ { \"data\":{ \"timeStamp\":0 }, \"description\":\"This is Motorola metallica\", \"guid\":\"7c84034d-0764-477f-8e7b-7f78e5c90ecc\", \"name\":\"Motorola metallica\", \"nodes\":[ { \"name\":\"Accelerometer Sensor\", \"description\":\"android.sensor.accelerometer\", \"guid\":\"12b17c34-9293-46e5-899d-8362022425b6\", \"uri\":\"Sensor://3d0523bd64000f81.Motorolametallica.Accelerometer\", \"type\":\"OBJECT\", \"data\":{ \"timeStamp\":0 }, \"nodes\":[ { \"name\":\"x-axis\", \"description\":\"Accelerometer x-axis\", \"guid\":\"7edbcd7d-dfb0-417a-9bfa-74764ce7af47\", \"uri\":\"Channel://3d0523bd64000f81.Motorolametallica.Accelerometer.x\", \"type\":\"OBJECT\", \"samplingDuration\":5000000, \"isEnabled\":true, \"data\":{ \"dataType\":\"FLOAT\", \"timeStamp\":0 }, \"nodes\":[ ] }, { \"name\":\"y-axis\", \"description\":\"Accelerometer y-axis\", \"guid\":\"3daded2c-abc9-4305-83e6-cf23e23c3cbe\", \"uri\":\"Channel://3d0523bd64000f81.Motorolametallica.Accelerometer.y\", \"type\":\"OBJECT\", \"samplingDuration\":5000000, \"isEnabled\":true, \"data\":{ \"dataType\":\"FLOAT\", \"timeStamp\":0 }, \"nodes\":[ ] }, { \"name\":\"z-axis\", \"description\":\"Accelerometer z-axis\", \"guid\":\"b31d9a60-47c5-4751-b0ff-2370cd89f399\", \"uri\":\"Channel://3d0523bd64000f81.Motorolametallica.Accelerometer.z\", \"type\":\"OBJECT\", \"samplingDuration\":5000000, \"isEnabled\":true, \"data\":{ \"dataType\":\"FLOAT\", \"timeStamp\":0 }, \"nodes\":[ ] } ] }, { \"name\":\"Gyro Sensor\", \"description\":\"android.sensor.gyroscope\", \"guid\":\"3610b799-b450-4e15-bb6c-b4fcab5d4887\", \"uri\":\"Sensor://3d0523bd64000f81.Motorolametallica.Gyrometer\", \"type\":\"OBJECT\", \"data\":{ \"timeStamp\":0 }, \"nodes\":[ { \"name\":\"x-axis\", \"description\":\"Gyrometer x-axis\", \"guid\":\"be39cfac-887d-4edc-8f18-7d6aac35ba52\", \"uri\":\"Channel://3d0523bd64000f81.Motorolametallica.Gyrometer.x\", \"type\":\"OBJECT\", \"samplingDuration\":5000000, \"isEnabled\":true, \"data\":{ \"dataType\":\"FLOAT\", \"timeStamp\":0 }, \"nodes\":[ ] }, { \"name\":\"y-axis\", \"description\":\"Gyrometer y-axis\", \"guid\":\"0a1c95de-d063-4e80-847a-c8bc4088e332\", \"uri\":\"Channel://3d0523bd64000f81.Motorolametallica.Gyrometer.y\", \"type\":\"OBJECT\", \"samplingDuration\":5000000, \"isEnabled\":true, \"data\":{ \"dataType\":\"FLOAT\", \"timeStamp\":0 }, \"nodes\":[ ] }, { \"name\":\"z-axis\", \"description\":\"Gyrometer z-axis\", \"guid\":\"7d920cee-f140-4f8c-b497-9ea2807dfe4f\", \"uri\":\"Channel://3d0523bd64000f81.Motorolametallica.Gyrometer.z\", \"type\":\"OBJECT\", \"samplingDuration\":5000000, \"isEnabled\":true, \"data\":{ \"dataType\":\"FLOAT\", \"timeStamp\":0 }, \"nodes\":[ ] } ] }, { \"name\":\"Compass Sensor\", \"description\":\"android.sensor.magnetic_field\", \"guid\":\"9bba34ea-a219-4838-88cc-0714530685c6\", \"uri\":\"Sensor://3d0523bd64000f81.Motorolametallica.Magnetometer\", \"type\":\"OBJECT\", \"data\":{ \"timeStamp\":0 }, \"nodes\":[ { \"name\":\"x-axis\", \"description\":\"Magnetometer x-axis\", \"guid\":\"91ffcdd3-942f-411b-aef2-b7e5d7943c6a\", \"uri\":\"Channel://3d0523bd64000f81.Motorolametallica.Magnetometer.x\", \"type\":\"OBJECT\", \"samplingDuration\":5000000, \"isEnabled\":true, \"data\":{ \"dataType\":\"FLOAT\", \"timeStamp\":0 }, \"nodes\":[ ] }, { \"name\":\"y-axis\", \"description\":\"Magnetometer y-axis\", \"guid\":\"52e0a47a-c130-4659-bd77-21ff0dd3c748\", \"uri\":\"Channel://3d0523bd64000f81.Motorolametallica.Magnetometer.y\", \"type\":\"OBJECT\", \"samplingDuration\":5000000, \"isEnabled\":true, \"data\":{ \"dataType\":\"FLOAT\", \"timeStamp\":0 }, \"nodes\":[ ] }, { \"name\":\"z-axis\", \"description\":\"Magnetometer z-axis\", \"guid\":\"e272b1f6-64fb-4fa8-9dba-640be71cf942\", \"uri\":\"Channel://3d0523bd64000f81.Motorolametallica.Magnetometer.z\", \"type\":\"OBJECT\", \"samplingDuration\":5000000, \"isEnabled\":true, \"data\":{ \"dataType\":\"FLOAT\", \"timeStamp\":0 }, \"nodes\":[ ] } ] } ], \"type\":\"OBJECT\", \"uri\":\"Device://3d0523bd64000f81.Motorolametallica.\" } ] }";
  //    private String DEFAULT_CONFIGURATION =  "{ \"name\": \"setConfiguration\", \"description\": \"SensorAgent setConfiguration\", \"guid\": \"5c2f4ed5-39f9-4162-b91f-b034f38d9e9d\", \"uri\": \"Method://3d0523bd64000f81.MetaWear.SensorAgent.setConfiguration\", \"type\": \"METHOD\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"description\": \"This is MetaWear\", \"type\": \"OBJECT\", \"guid\": \"AECD4EB7-FBAA-4CB6-8368-212AA733E6C2\", \"name\": \"MetaWear\", \"uri\": \"Device:\\\\/\\\\/3d0523bd64000f81.MetaWear\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"description\": \"metawear.sensor.temperature\", \"type\": \"OBJECT\", \"guid\": \"BFA39631-3EBF-4F1B-A5B3-A9B8DA97B7A0\", \"name\": \"Temperature Sensor\", \"uri\": \"Sensor:\\\\/\\\\/metawear.sensor.temperature\", \"data\": { \"timeStamp\": 0 }, \"nodes\": [{ \"description\": \"metawear.sensor.temperature.1\", \"isEnabled\": true, \"samplingDuration\": 4000, \"guid\": \"14A0B0EC-9E9C-477A-B432-B31AAED76E35\", \"name\": \"Temperature Sensor Channel 1\", \"type\": \"OBJECT\", \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [], \"uri\": \"Channel:\\\\/\\\\/metawear.sensor.temperature.channel1\" }] }, { \"description\": \"metawear.sensor.heartrate\", \"type\": \"OBJECT\", \"guid\": \"BFA39631-3EBF-4F1B-A5B3-A9B8DA97B7A0\", \"name\": \"Heartrate Sensor\", \"data\": { \"timeStamp\": 0 }, \"uri\": \"Sensor:\\\\/\\\\/metawear.sensor.heartrate\", \"nodes\": [{ \"description\": \"metawear.sensor.heartrate.1\", \"isEnabled\": true, \"samplingDuration\": 7000, \"guid\": \"14A0B0EC-9E9C-477A-B432-B31AAED76E35\", \"name\": \"Heartrate Sensor Channel 1\", \"type\": \"OBJECT\", \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [], \"uri\": \"Channel:\\\\/\\\\/metawear.sensor.heartrate.channel1\" }] }, { \"description\": \"metawear.sensor.gyroscope\", \"type\": \"OBJECT\", \"guid\": \"BFA39631-3EBF-4F1B-A5B3-A9B8DA97B7A0\", \"name\": \"Gyroscope Sensor\", \"data\": { \"timeStamp\": 0 }, \"uri\": \"Sensor:\\\\/\\\\/metawear.sensor.gyroscope\", \"nodes\": [{ \"description\": \"metawear.sensor.gyroscope.1\", \"isEnabled\": true, \"samplingDuration\": 5000, \"guid\": \"14A0B0EC-9E9C-477A-B432-B31AAED76E35\", \"name\": \"Gyroscope Sensor Channel 1\", \"type\": \"OBJECT\", \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [], \"uri\": \"Channel:\\\\/\\\\/metawear.channel.gyroscope.channel1\" }, { \"description\": \"metawear.sensor.gyroscope.2\", \"isEnabled\": true, \"samplingDuration\": 5000, \"guid\": \"14A0B0EC-9E9C-477A-B432-B31AAED76E35\", \"name\": \"Gyroscope Sensor Channel 2\", \"type\": \"OBJECT\", \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [], \"uri\": \"Channel:\\\\/\\\\/metawear.channel.gyroscope.channel2\" }, { \"description\": \"metawear.sensor.gyroscope.3\", \"isEnabled\": true, \"samplingDuration\": 5000, \"guid\": \"14A0B0EC-9E9C-477A-B432-B31AAED76E35\", \"name\": \"Gyroscope Sensor Channel 3\", \"type\": \"OBJECT\", \"data\": { \"dataType\": \"FLOAT\", \"timeStamp\": 0 }, \"nodes\": [], \"uri\": \"Channel:\\\\/\\\\/metawear.channel.gyroscope.channel3\" }] }] }] }";
  private String connectionState = null;
  private String pendingCStateMW = null;
  private String pendingStateMW = null;
  private IDataReadyExternal iSensorAgent;
  private SharedPreferences mSharedPrefs;
  private String macAddress;
  private int requestCode = 82;
  private DataSenderThread dbThread;
  private DatabaseWorker databaseWorker;
  private String dbThreadName = "ZMDatabaseThread";
  private int isConnNotified = -1;

  @Override
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);
    Log.d(TAG, "Initialized");

    mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(cordova.getActivity().getApplicationContext());
    mProxyComm = new ProxyComm(cordova.getActivity().getApplicationContext(), null, null, this);

    mConfigurationFile = new File(BASE_PATH, FOLDER_NAME);

    if (!mConfigurationFile.exists()) {
      mConfigurationFile.mkdir();
    }

    macAddress = mSharedPrefs.getString(Constants.PREF_KEY_MAC_ADDRESS, "");
    if (macAddress != null && macAddress.length() > 0) {
      startSensorAgent(macAddress);
    }

    mConfigurationFile = new File(mConfigurationFile, FILE_NAME);

    if (!mConfigurationFile.exists()) {
    	Log.d(TAG,"mConfigurationFile doesn't exists");
      writeToFile(DEFAULT_CONFIGURATION, mConfigurationFile);
    }
  }

  public void startSensorAgent(String macAddress) {
  	Log.d(TAG,"startSensorAgent, macAddress "+macAddress);
    this.macAddress = macAddress;
    iSensorAgent = ((ZetaApplication) cordova.getActivity().getApplication()).getSensorAgent(macAddress, this);
    if (iSensorAgent!=null) {
    	Log.d(TAG,"startSensorAgent, iSensorAgent found");
    }
  }

  public void sendDataToSensorAgent(String node) {
  	Log.d(TAG,"sendDataToSensorAgent node "+node);
    if (iSensorAgent != null) {
    	Log.d(TAG,"startSensorAgent, iSensorAgent not null");
      if (mSharedPrefs.getString(Constants.PREF_KEY_MAC_ADDRESS, "").length() > 0) {
      	Log.d(TAG,"sendDataToSensorAgent Bluetooth device selected");
        iSensorAgent.dataReadyExternal(node);
      } else {
        sendMessageToPlugin(Constants.serialize(Constants.buildErrorNode("ERROR_DEVICE_NOT_SELECTED")));
      }
    } else {
    	Log.d(TAG,"startSensorAgent, iSensorAgent null");
      if (mSharedPrefs.getString(Constants.PREF_KEY_MAC_ADDRESS, "").length() > 0) {
        Log.e(TAG, "Sensor Agent is not initiated");
      } else {
        sendMessageToPlugin(Constants.serialize(Constants.buildErrorNode("ERROR_DEVICE_NOT_SELECTED")));
      }
    }
  }

  private void writeToFile(String data, File file) {
    try {
      FileOutputStream fis = new FileOutputStream(file);
      fis.write(data.getBytes());
      fis.close();
    } catch (IOException e) {
      Log.e(TAG, "File write failed: " + e.toString());
    }
  }

  @Override
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
  	Log.e(TAG,"execute action "+action+" JSONArray args "+args);
    if (action.equals("getClockOffset")) {
      try {
        NTPClient ntpClient = NTPClient.getInstance();
        JSONObject result = new JSONObject();
        result.put("offset", ntpClient.getOffset());
        Log.d(TAG,"callbackContext success");
        callbackContext.success(result);
        return true;
      } catch (Exception e) {
      	Log.d(TAG,"callbackContext error");
        callbackContext.error(e.getStackTrace().toString());
        return false;
      }
      //return true;
    } else {
      Log.e(TAG, "execute args.getString(1) " + args.getString(1));
      if (args.getString(1).contains("shareFiles")) {
        emailFiles();
        callbackContext.success();
        return true;
      }

      mCallbackContext = callbackContext;
      try {
        final String methodName = args.getString(0);
        Log.d(TAG,"methodName (args.getString(0)) "+methodName);
        String uri;
        Log.d(TAG,"new JSONObject args.getString(1) "+args.getString(1));
        try {
          JSONObject jsonObject = new JSONObject(args.getString(1));
          uri = jsonObject.getString("uri");
          Log.d(TAG,"execute uri "+uri);
        } catch (Exception e) {
        	Log.e(TAG,"execute Exception");
          uri = "";
        }

        if (pendingCStateMW != null && pendingCStateMW.length() > 0) {
          sendMessageToPlugin(pendingCStateMW);
          pendingCStateMW = "";
        }

        if (pendingStateMW != null && pendingStateMW.length() > 0) {
          sendMessageToPlugin(pendingStateMW);
          pendingStateMW = "";
        }

        if (connectionState != null && connectionState.length() > 0) {
          sendMessageToPlugin(connectionState);
          connectionState = null;
        }

        // check node uri
        if (uri.contains("MetaWear") || uri.contains("Metawear") || uri.contains("metawear")) {
          Log.e(TAG, "metawear sent " + args.getString(1));
          if (uri.contains("showDeviceListing")) {
            cordova.setActivityResultCallback(this);
            cordova.getActivity().startActivityForResult(new Intent(cordova.getActivity(), DeviceListActivity.class), requestCode);
          } else {
            sendDataToSensorAgent(args.getString(1));
          }
        } else {
          if (uri.contains("getConnectionStates") || uri.contains("ConnectionStates")) {
            setOnConnectionChangeListener(callbackContext);
          } else {

            Log.e(TAG, "androidwear sent " + args.getString(1));
            if (uri.endsWith("start")) {
            	Log.d(TAG,"uri.endsWith start ");
              checkThread();
              LoggingService2.reset(cordova.getActivity().getApplication());
            } else if (uri.endsWith("stop")) {
            	Log.d(TAG,"uri.endsWith stop ");
              clearThread();
            }

            Class params[] = new Class[0];
            Object[] objects = null;
            if (args.length() > 1) {
              params = new Class[1];
              params[0] = String.class;
              String message;
              message = args.getString(1);
              objects = new Object[args.length() - 1];
              objects[0] = message;
            }
//                Log.d(TAG, "Parameter size " + params.length);
            Method method = mProxyComm.getClass().getDeclaredMethod(methodName, params);
            if (method.getReturnType().equals(Void.TYPE)) {
              method.invoke(mProxyComm, objects);
            } else if (method.getReturnType().equals(String.class)) {
              method.invoke(mProxyComm, objects).toString();
            }
          }
        }
        return true;
      } catch (Exception e) {
      	Log.e(TAG,"Exception "+e.getMessage()+" sendPluginResult Error");
        PluginResult result = new PluginResult(PluginResult.Status.ERROR, "Error " + e.getLocalizedMessage());
        result.setKeepCallback(true);
        callbackContext.sendPluginResult(result);
        e.printStackTrace();
        return true;
      }
    }
  }

  private void emailFiles() {
    File root = new File(Environment.getExternalStorageDirectory(), ProxyCommPlugin.FOLDER_NAME);
    File file = new File(root, "file_path.txt");
    StringBuilder text = new StringBuilder();
    String line = "";
    try {
      BufferedReader br = new BufferedReader(new FileReader(file));
      while ((line = br.readLine()) != null) {
        text.append(line);
      }
      line = text.toString();
      br.close();
    } catch (IOException e) {
    }

    File logFile = new File(root, line);
    if (!logFile.exists()) {
      Toast.makeText(cordova.getActivity(), "Session folder not found", Toast.LENGTH_SHORT).show();
      return;
    }

    //need to "send multiple" to get more than one attachment
    final Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
    emailIntent.setType("text/csv");
    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{""});
    emailIntent.putExtra(android.content.Intent.EXTRA_CC, new String[]{""});
    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Zeta log(" + new Date() + ")");
    ArrayList<String> text11 = new ArrayList<String>();
    text11.add("");
    emailIntent.putExtra(Intent.EXTRA_TEXT, text11);

    //has to be an ArrayList
    ArrayList<Uri> uris = new ArrayList<Uri>();

    //convert from paths to Android friendly Parcelable Uri's
    File[] files = logFile.listFiles();
    for (int i = 0; i < files.length; i++) {
      uris.add(Uri.fromFile(files[i]));
    }
    emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);

    cordova.getActivity().startActivity(Intent.createChooser(emailIntent, "Send mail").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
  }

  public String readSavedData(File file) {
    String s = null;
    FileInputStream fin = null;
    try {
      // create FileInputStream object
      fin = new FileInputStream(file);

      byte fileContent[] = new byte[(int) file.length()];

      // Reads up to certain bytes of data from this input stream into an array of bytes.
      fin.read(fileContent);
      //create string from byte array
      s = new String(fileContent);
      System.out.println("readSavedData File content s: " + s);
    } catch (FileNotFoundException e) {
      System.out.println("readSavedData File not found" + e.getMessage());
    } catch (IOException ioe) {
      System.out.println("readSavedData Exception while reading file " + ioe.getMessage());
    } finally {
      // close the streams using close method
      try {
        if (fin != null) {
          fin.close();
        }
      } catch (IOException ioe) {
        System.out.println("readSavedData Error while closing stream: " + ioe.getMessage());
      }
    }
    return s;
  }

  public void sendMessageToPlugin(final String message) {
  	Log.d(TAG,"sendMessageToPlugin message "+message);
    cordova.getThreadPool().execute(new Runnable() {
      @Override
      public void run() {
        PluginResult result = new PluginResult(PluginResult.Status.OK, message);
        result.setKeepCallback(true);
        if (mCallbackContext != null) {
          Log.e(TAG, "sendMessageToPlugin mCallbackContext != null sendPluginResult(result) OK - " + message);
          mCallbackContext.sendPluginResult(result);
        } else {
          Log.e(TAG, "sendMessageToPlugin mCallbackContext null stored - " + message);
          JSONObject jsonObject = null;
          try {
            jsonObject = new JSONObject(message);
            if (jsonObject != null && jsonObject.getString("uri").endsWith("getConnectionStates")) {
              if (pendingCStateMW == null) {
                pendingCStateMW = message;
                Log.d(TAG,"pendingCStateMW "+pendingCStateMW);
              }
            } else if (jsonObject != null && jsonObject.getString("uri").endsWith("getState")) {
              if (pendingStateMW == null) {
                pendingStateMW = message;
                Log.d(TAG,"pendingStateMW "+pendingStateMW);
              }
            }

          } catch (JSONException e) {
            e.printStackTrace();
          }
        }
      }
    });
  }

  public void sendFailMessageToPlugin(final String message) {
  	Log.e(TAG,"sendFailMessageToPlugin message "+message);
    cordova.getThreadPool().execute(new Runnable() {
      @Override
      public void run() {
        Log.e(TAG, " getThreadPool execute run");
        PluginResult result = new PluginResult(PluginResult.Status.ERROR, "Error : " + message);
        result.setKeepCallback(true);
        if (mCallbackContext != null)
          mCallbackContext.sendPluginResult(result);
      }
    });
  }

  public void sendConnectionStatusToPlugin(final ConnectionStates message) {
  	Log.d(TAG,"sendConnectionStatusToPlugin message "+message);
    connectionState = null;
    cordova.getThreadPool().execute(new Runnable() {
      @Override
      public void run() {
      	Log.i(TAG,"sendConnectionStatusToPlugin getThreadPool execute run");
        if (mCallbackContext == null) {
          Log.e(TAG, "Wear ConnectionStatus Changed stored - " + message);
          connectionState = Constants.serialize(getConnectionStates(message));
          return;
        }
        Log.e(TAG, "Wear ConnectionStatus Changed sent- " + message);
        PluginResult result = new PluginResult(PluginResult.Status.OK, Constants.serialize(getConnectionStates(message)));
        result.setKeepCallback(true);
        mCallbackContext.sendPluginResult(result);
      }
    });
  }

  public void setOnConnectionChangeListener(CallbackContext mConnectionCallback) {
  	Log.i(TAG,"setOnConnectionChangeListener mConnectionCallback"+mConnectionCallback);
    this.mCallbackContext = mConnectionCallback;
    if (connectionState != null && connectionState.length() > 0) {
    	Log.i(TAG,"setOnConnectionChangeListener connectionState "+connectionState);
      sendMessageToPlugin(connectionState);
      connectionState = null;
    }
  }

  public Node getConnectionStates(ConnectionStates connectionStates) {
    Node node = new Notification();
    node.setName("ConnectionStates");
    node.setDescription("The connection state with the external device");
    node.setType(NodeType.OBJECT);
    node.setGuid(UUID.randomUUID());
    node.setUri("Notification://ZetaMobile.CommBle.getConnectionStates");
    Data date = new Data();
    date.setDataType(DataType.ENUMERATION);
    date.setTimeStamp(System.currentTimeMillis());
    date.setValues(connectionStates.name().getBytes());
    node.setData(date);
    return node;
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    super.onActivityResult(requestCode, resultCode, intent);
    if (resultCode == Activity.RESULT_OK && this.requestCode == requestCode) {
    	Log.i(TAG,"onActivityResult RESULT_OK requestCode "+requestCode+" startSensorAgent");
      startSensorAgent(intent.getStringExtra(Constants.EXTRA_MAC_ADDRESS));
    }
  }

  @Override
  public void onDestroy() {
    // NOTE: Without this, memory leak may happen
    ((ZetaApplication) cordova.getActivity().getApplication()).getSensorAgent(null, null);
    CommBle.BleMsgReceiver.setProxyCommPlugin(null);
    super.onDestroy();
  }

  public void checkThread() {
  	Log.i(TAG,"checkThread");
    if (databaseWorker == null)
      databaseWorker = ((ZetaApplication) cordova.getActivity().getApplication()).getDataBase();
    if (!databaseWorker.isEmpty()) {
    	Log.i(TAG,"checkThread databaseWorker not empty");
      if (dbThread == null) {
        dbThread = new DataSenderThread(dbThreadName);
        dbThread.start();
        dbThread.fetchDBContent();
        Log.i(TAG,"checkThread dbThread start fetchDBContent");
        try {
          dbThread.currentThread().sleep(Constants.START_DELAY);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      } else {
        if (dbThread.messageCount == 0) {
        	Log.i(TAG,"checkThread dbThread messageCount is 0");
          if (!dbThread.isAlive()) {
            dbThread.start();
          }
          dbThread.fetchDBContent();
        }
      }
    } else {
      clearThread();
    }
  }

  public void clearThread() {
  	Log.i(TAG,"clearThread");
    if (dbThread != null) {
    	Log.i(TAG,"clearThread dbThread not null");
      dbThread.mWorkerHandler.removeCallbacksAndMessages(null);
      dbThread.quit();
      dbThread = null;
      databaseWorker = null;
    }
  }

  public class DataSenderThread extends HandlerThread {

    private int messageCount = 0;
    private Handler mWorkerHandler;

    public DataSenderThread(String name) {
      super(name);
    }

    @Override
    public synchronized void start() {
      super.start();
      Log.i(TAG,"DataSenderThread start");
      prepareHandler();
    }

    public void queueTask(String data, boolean isFromDB) {
    	Log.i(TAG,"DataSenderThread queueTask data "+data+" isFromDB "+isFromDB+" messageCount++");
      messageCount++;
      mWorkerHandler.obtainMessage(isFromDB ? 1 : 0, data).sendToTarget();
    }

    private void prepareHandler() {
    	Log.i(TAG,"prepareHandler");
      mWorkerHandler = new Handler(getLooper(), new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
        	Log.i(TAG,"mWorkerHandler handleMessage msg "+msg.toString()+" messageCount--");
          messageCount--;
          String data = (String) msg.obj;
          Log.i(TAG,"mWorkerHandler handleMessage String data "+data);
          if (mCallbackContext != null) {
            sendMessageToPlugin(data);
            databaseWorker.deleteFirstRow();
            LoggingService2.d(cordova.getActivity(), "TAG", data);
            checkThread();
          } else {
            clearThread();
          }
          return true;
        }
      });
    }

    private void fetchDBContent() {
      // TODO : Need to remove delay and create Thread at cordova layer
      if (dbThread != null) {
        queueTask(databaseWorker.getFirstRow(), true);
        try {
          dbThread.currentThread().sleep(Constants.TRANSMISSION_DELAY);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
