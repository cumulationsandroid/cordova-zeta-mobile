package com.at.plugin;


import com.at.model.app.ConnectionStates;

/**
 * Created by vasanth on 9/8/16.
 */
public interface ProxyCommPluginCallback {

    void sendConnectionStatusToPlugin(ConnectionStates connectionStates);

    void sendFailMessageToPlugin(String message);

    void sendMessageToPlugin(String message);
}
