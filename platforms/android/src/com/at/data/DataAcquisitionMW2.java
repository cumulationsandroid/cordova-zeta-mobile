package com.at.data;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.at.communication.GenericBle;
import com.at.model.app.ConnectionStates;
import com.at.model.app.IDataReadyInternal;
import com.at.model.app.INotify;
import com.at.model.app.ISensorState;
import com.at.model.app.States;
import com.at.model.domain.Channel;
import com.at.model.domain.Device;
import com.at.model.domain.Sensor;
import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;
import com.at.model.meta.NodeType;
import com.at.model.meta.Notification;
import com.at.utils.Constants;
import com.at.utils.NTPClient;
import com.at.utils.wakehelper.ZetaWakeLock;
import com.at.zeta.zetamobile.ZetaApplication;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class DataAcquisitionMW2 extends DataAcquisition {

  final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
  private static final int SHOW_TOAST = 0;
  private static final long CHECK_DURATION = 8000;
  private static final String METAWEAR_BATTERY_CHAR = "00002a19-0000-1000-8000-00805f9b34fb";
  private final Context mContext;
  private final IDataReadyInternal iDataReadyInternal;
  private final INotify iNotify;
  private final ISensorState iSensorState;
  private long CHECK_DURATION_OFFSET = 5000;
  private String TAG = "JL-" + DataAcquisitionMW2.class.getSimpleName();
  private GenericBle genericBle;
  private String macAddress;
  private Device mDevice = null;
  private MainHandler mainHandler;
  private LinkedList<ArrayList<Channel>> tickerChannels = new LinkedList<ArrayList<Channel>>();
  private LinkedList<RequestItem> requestItemList = new LinkedList<RequestItem>();
  private HashMap<ArrayList<Channel>, Integer> tickerChannelMap = new HashMap<ArrayList<Channel>, Integer>();
  private Object populatedSensorChannel;
  private RequestItem lastRequestItem;
  private int needToSetDevice = -1;
  private boolean isExecutorON = false;
  private NTPClient ntpClient = NTPClient.getInstance();
  private Runnable timeOutChecker = new Runnable() {
    @Override
    public void run() {
      if ((ntpClient.getCorrentTime() - lastRequestItem.startedON) >= (lastRequestItem.timeOut)) {
        Log.e(TAG, "Request Timeout");
        iSensorState.setState(States.ERROR);
      }
    }
  };
  private String sessionID = "";
  private Runnable queueExecutor = new Runnable() {
    @Override
    public void run() {

      isExecutorON = true;
      if (lastRequestItem != null && lastRequestItem.isNeedToWait) {
        Log.v(TAG, "waiting for response, from the hardware for the last sent command : " + bytesToHex(lastRequestItem.data));
        isExecutorON = false;
        return;
      }

      Log.i(TAG, "request queue size is " + requestItemList.size());
      lastRequestItem = requestItemList.pollFirst();
      if (lastRequestItem != null) {
        if (lastRequestItem.type == RequestType.SEND_DATA) {
          lastRequestItem.startedON = ntpClient.getCorrentTime();
          writeCharacteristic(lastRequestItem.data);
        } else if (lastRequestItem.type == RequestType.UPDATE_STATE) {
          if (lastRequestItem.states == States.OPERATIONAL && needToSetDevice == 0) {
            needToSetDevice = -1;
            setDevice(getDevice());
            isExecutorON = true;
            mainHandler.post(queueExecutor);
          } else if (lastRequestItem.states == States.READY && genericBle.isForeground()) {
            start(null);
            isExecutorON = true;
            mainHandler.post(queueExecutor);
          } else {
            iSensorState.setState(lastRequestItem.states);
            isExecutorON = true;
            mainHandler.post(queueExecutor);
          }
        } else if (lastRequestItem.type == RequestType.UPDATE_CONNECTION) {
          if (lastRequestItem.connectionStates == ConnectionStates.DISCONNECTED) {
            if (iSensorState != null) {
              iSensorState.setState(lastRequestItem.states);
            }
            if (iNotify != null) {
              iNotify.notify(lastRequestItem.connectionStates);
            }
            ((ZetaApplication) mContext).getSensorAgent("", null);
            genericBle.disconnect();
            genericBle.stopSelf();
          }
        }
      } else {
        Log.i(TAG, "end of request queue");
        isExecutorON = false;
      }
    }
  };
  private GenericBle.ServiceCallback serviceCallback = new GenericBle.ServiceCallback() {
    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
      iDataReadyInternal.dataReadyInternal(getStopNode());
      stop();
    }

    @Override
    public void onDestroy() {

    }
  };

  private ServiceConnection myConnection = new ServiceConnection() {

    public void onServiceConnected(ComponentName className, IBinder service) {
      GenericBle.LocalBinder binder = (GenericBle.LocalBinder) service;
      genericBle = binder.getService();
      genericBle.setDataAcquisition(DataAcquisitionMW2.this);
      genericBle.setINotify(iNotify);
      genericBle.setISensorState(iSensorState);
      genericBle.setServiceCallback(serviceCallback);
      genericBle.connect(macAddress);
    }

    public void onServiceDisconnected(ComponentName arg0) {
      genericBle.stopForegroundCUS(true);
      genericBle = null;
    }
  };
  private int oldLevel = -1;

  public DataAcquisitionMW2(Context context, INotify notify, IDataReadyInternal dataReadyInternal, String macAddress) {
    mContext = context;
    mContext.startService(new Intent(mContext, GenericBle.class));
    this.macAddress = macAddress;
    iDataReadyInternal = dataReadyInternal;
    iSensorState = (ISensorState) dataReadyInternal;
    iNotify = notify;
    mainHandler = new MainHandler(Looper.getMainLooper());
    mContext.bindService(new Intent(mContext, GenericBle.class), myConnection, Context.BIND_AUTO_CREATE | Context.BIND_WAIVE_PRIORITY);
  }

  public static String bytesToHex(byte[] bytes) {
    char[] hexChars = new char[0];
    if (bytes != null) {
      hexChars = new char[bytes.length * 2];
      for (int j = 0; j < bytes.length; j++) {
        int v = bytes[j] & 0xFF;
        hexChars[j * 2] = hexArray[v >>> 4];
        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
      }
    }
    return new String(hexChars);
  }

  @Override
  public Device getDevice() {
    return mDevice;
  }

  @Override
  public void setDevice(Device device) {
    Log.i(TAG, "Invoked setDevice(Device)");
    needToSetDevice = -1;
    mDevice = device;
    tickerChannels.clear();
    tickerChannelMap.clear();
    RequestItem requestItem = new RequestItem();
    requestItem.type = RequestType.UPDATE_STATE;
    requestItem.states = States.RECEIVED_CONFIGURATION;
    requestItemList.add(requestItem);
    processDevice();
    enableNextChannel();
  }

  public void start(Node node) {
    if (node != null) {
      sessionID = new String(node.getData().getValues());
    }

    Log.i(TAG, "starting session " + sessionID);

    int connectionState = genericBle.getBluetoothManager().getConnectionState(genericBle.getBluetoothDevice(), BluetoothProfile.GATT);
    if (connectionState == BluetoothGatt.STATE_CONNECTED) {
      genericBle.startForegroundCUS(GenericBle.NOTIFICATION_ID, ZetaWakeLock.createNotification(mContext));
      Object[] tickerIDs = tickerChannelMap.keySet().toArray(new Object[0]);
      if (tickerIDs != null) {
        for (int i = 0; i < tickerIDs.length; i++) {
          ArrayList<Channel> channels = (ArrayList<Channel>) tickerIDs[i];
          if (channels.get(0).getUri().matches("(.*)gyroscope(.*)")) {

            RequestItem requestItem = new RequestItem();
            requestItem.data = new byte[]{0x09, 0x03, 0x01};
            requestItemList.add(requestItem);

            requestItem = new RequestItem();
            requestItem.data = new byte[]{0x09, 0x07, tickerChannelMap.get(tickerIDs[i]).byteValue(), 0x01};
            requestItemList.add(requestItem);

            requestItem = new RequestItem();
            requestItem.data = new byte[]{0x13, 0x02, 0x01, 0x00};
            requestItemList.add(requestItem);

            requestItem = new RequestItem();
            requestItem.data = new byte[]{0x13, 0x01, 0x01};
            requestItemList.add(requestItem);

            continue;
          }

          RequestItem requestItem = new RequestItem();
          requestItem.data = new byte[]{0x0C, 0x03, tickerChannelMap.get(tickerIDs[i]).byteValue()};
          requestItemList.add(requestItem);

        }

        if (tickerIDs.length > 0) {
          RequestItem requestItem = new RequestItem();
          requestItem.type = RequestType.UPDATE_STATE;
          requestItem.states = States.OPERATIONAL;
          requestItemList.add(requestItem);
        } else {
          Log.i(TAG, "Nothing to start");
          RequestItem requestItem = new RequestItem();
          requestItem.type = RequestType.UPDATE_STATE;
          requestItem.states = States.READY;
          requestItemList.add(requestItem);
        }
      }
      if (!isExecutorON) {
        isExecutorON = true;
        mainHandler.post(queueExecutor);
      }
    } else {
      iNotify.notify(ConnectionStates.DISCONNECTED);
      // TODO: Better to not show any UI element from SDK part
      mainHandler.obtainMessage(SHOW_TOAST, "Device not connected").sendToTarget();
    }
  }

  public void stop() {

    Log.e(TAG, "Invoked stop");

    genericBle.stopForegroundCUS(true);
    int connectionState = genericBle.getBluetoothManager().getConnectionState(genericBle.getBluetoothDevice(), BluetoothProfile.GATT);
    if (connectionState == BluetoothGatt.STATE_CONNECTED) {
      Object[] tickerIDs = tickerChannelMap.keySet().toArray(new Object[0]);
      if (tickerIDs != null) {
        for (int count = 0; count < tickerIDs.length; count++) {
          ArrayList<Channel> channels = (ArrayList<Channel>) tickerIDs[count];
          if (channels.get(0).getUri().matches("(.*)gyroscope(.*)")) {

            RequestItem requestItem = new RequestItem();
            requestItem.data = new byte[]{0x13, 0x1, 0x0};
            requestItemList.add(requestItem);

            requestItem = new RequestItem();
            requestItem.data = new byte[]{0x13, 0x2, 0x0, 0x1};
            requestItemList.add(requestItem);

            requestItem = new RequestItem();
            requestItem.data = new byte[]{0x09, 0x07, tickerChannelMap.get(tickerIDs[count]).byteValue(), 0x00};
            requestItemList.add(requestItem);

            requestItem = new RequestItem();
            requestItem.data = new byte[]{0x09, 0x03, 0x00};
            requestItemList.add(requestItem);

//                        FIXME : including below command resets the ticker id
//                        requestItem = new RequestItem();
//                        requestItem.data = new byte[]{0x09, 0x06, 0x06};
//                        requestItemList.add(requestItem);

            continue;
          }
          RequestItem requestItem = new RequestItem();
          requestItem.data = new byte[]{0x0C, 0x04, tickerChannelMap.get(tickerIDs[count]).byteValue()};
          requestItemList.add(requestItem);
        }
      }

      if (tickerIDs.length > 0) {
        RequestItem requestItem = new RequestItem();
        requestItem.type = RequestType.UPDATE_STATE;
        requestItem.states = States.READY;
        requestItemList.add(requestItem);
      } else {
        Log.e(TAG, "Nothing to stop");
        RequestItem requestItem = new RequestItem();
        requestItem.type = RequestType.UPDATE_STATE;
        requestItem.states = States.READY;
        requestItemList.add(requestItem);
      }

      Log.i(TAG, "ticker id length is " + tickerIDs.length);

      if (!isExecutorON) {
        isExecutorON = true;
        mainHandler.post(queueExecutor);
      }

    } else {
      iNotify.notify(ConnectionStates.DISCONNECTED);

      Log.e(TAG, "Nothing to stop");
      RequestItem requestItem = new RequestItem();
      requestItem.type = RequestType.UPDATE_STATE;
      requestItem.states = States.READY;
      requestItemList.add(requestItem);

      if (!isExecutorON) {
        isExecutorON = true;
        mainHandler.post(queueExecutor);
      }
//            mainHandler.obtainMessage(SHOW_TOAST, "Metawear not connected").sendToTarget();
    }
  }

  public void resetDevice() {

    Log.i(TAG, "resetDevice Invoked");

    requestItemList.clear();
    mDevice = null;
    lastRequestItem = null;
    mainHandler.removeCallbacksAndMessages(null);

    RequestItem requestItem1 = new RequestItem();
    requestItem1.data = new byte[]{(byte) 0xFE, 0x01};
    requestItem1.type = RequestType.SEND_DATA;
    requestItemList.add(requestItem1);

    RequestItem requestItem2 = new RequestItem();
    requestItem2.type = RequestType.UPDATE_STATE;
    requestItem2.states = States.STARTUP;
    requestItemList.add(requestItem2);

    if (!isExecutorON) {
      isExecutorON = true;
      mainHandler.post(queueExecutor);
    }

    tickerChannels.clear();
    tickerChannelMap.clear();

    genericBle.reset();

    iNotify.notify(ConnectionStates.DISCONNECTED);
  }

  private void processDevice() {
    if (mDevice != null) {
      ArrayList<Sensor> sensors = (ArrayList<Sensor>) mDevice.getNodes();
      if (sensors != null) {
        for (int i = 0; i < sensors.size(); i++) {
          processSensor(sensors.get(i));
        }
      }
    }
  }

  private void processSensor(Sensor sensor) {
    if (sensor != null) {
      ArrayList<Channel> channels = (ArrayList<Channel>) sensor.getNodes();
      ArrayList<Channel> channelGroup = new ArrayList<Channel>();
      if (channels != null) {
        for (int i = 0; i < channels.size(); i++) {
          boolean isEnabled = processChannel(channels.get(i));
          if (isEnabled) {
            channelGroup.add(channels.get(i));
          }
        }

        if (channelGroup.size() > 0) {
          tickerChannels.addFirst(channelGroup);
        }
      }
    }
  }

  private boolean processChannel(Channel channel) {
    if (channel != null && channel.getUri() != null && channel.isEnabled()) {
      return true;
    }
    return false;
  }

  private void enableNextChannel() {
    if (tickerChannels.peekFirst() == null)
      return;

    for (int i = 0; i < tickerChannels.size(); i++) {
      Channel channel = tickerChannels.get(i).get(0);
      if (channel != null) {
        if (channel.getUri() != null && channel.getUri().trim().matches("(.*)heartrate(.*)")) {

          RequestItem requestItem1 = new RequestItem();
          requestItem1.data = new byte[]{0x05, 0x02, 0x01};
          requestItem1.type = RequestType.SEND_DATA;
          requestItemList.add(requestItem1);

          byte[] duration = ByteBuffer.allocate(2).putShort((short) (channel.getSamplingDuration() / 1000)).array();
          RequestItem requestItem2 = new RequestItem();
          requestItem2.data = new byte[]{0x0C, (byte) 0x02, duration[1], duration[0], 0x00, 0x00, (byte) 0xFF, (byte) 0xFF, 0x01};
          requestItem2.type = RequestType.SEND_DATA;
          requestItem2.isNeedToWait = true;
          requestItemList.add(requestItem2);

          // RequestItem3 created with RequestItem2 response

          RequestItem requestItem4 = new RequestItem();
          requestItem4.data = new byte[]{0x0A, 0x03, 0x00};
          requestItem4.type = RequestType.SEND_DATA;
          requestItem4.isNeedToWait = true;
          requestItemList.add(requestItem4);

        } else if (channel.getUri() != null && channel.getUri().trim().matches("(.*)temperature(.*)")) {

          RequestItem requestItem0 = new RequestItem();
          requestItem0.data = new byte[]{0x12, 0x04, 0x01, 0x01};
          requestItem0.type = RequestType.SEND_DATA;
          requestItemList.add(requestItem0);

          byte[] duration = ByteBuffer.allocate(2).putShort((short) (channel.getSamplingDuration() / 1000)).array();
          RequestItem requestItem1 = new RequestItem();
          requestItem1.data = new byte[]{0x0C, (byte) 0x02, duration[1], duration[0], 0x00, 0x00, (byte) 0xFF, (byte) 0xFF, 0x01};
          requestItem1.type = RequestType.SEND_DATA;
          requestItem1.isNeedToWait = true;
          requestItemList.add(requestItem1);

          // RequestItem2 created with RequestItem1 response

          RequestItem requestItem3 = new RequestItem();
          requestItem3.data = new byte[]{0x0A, 0x03, 0x03};
          requestItem3.type = RequestType.SEND_DATA;
          requestItem3.isNeedToWait = true;
          requestItemList.add(requestItem3);

        } else if (channel.getUri().trim().matches("(.*)gyroscope(.*)")) {

          RequestItem requestItem1 = new RequestItem();
          requestItem1.data = new byte[]{0x13, 0x3, 0x26, 0x0};
          requestItem1.type = RequestType.SEND_DATA;
          requestItemList.add(requestItem1);

          RequestItem requestItem2 = new RequestItem();
          requestItem2.data = new byte[]{0x13, 0x3, 0x26, 0x4};
          requestItem2.type = RequestType.SEND_DATA;
          requestItemList.add(requestItem2);

          byte[] duration = ByteBuffer.allocate(2).putShort((short) (channel.getSamplingDuration() / 1000)).array();
          RequestItem requestItem3 = new RequestItem();
          requestItem3.data = new byte[]{0x09, 0x02, 0x13, 0x05, (byte) 0xFF, (byte) 0xA0, 0x08, 0x05, duration[1], duration[0], 0x00, 0x00};
          requestItem3.type = RequestType.SEND_DATA;
          requestItem3.isNeedToWait = true;
          requestItemList.add(requestItem3);

        } else {
          Log.i(TAG, "Implementation is done for " + channel.getUri());
        }
      }
    }
    if (requestItemList != null && requestItemList.size() > 0) {
      RequestItem requestItem = new RequestItem();
      requestItem.type = RequestType.UPDATE_STATE;
      requestItem.states = States.READY;
      requestItemList.add(requestItem);
      if (!isExecutorON) {
        isExecutorON = true;
        mainHandler.post(queueExecutor);
      }
    } else {
      Log.e(TAG, "Nothing to set");
    }
  }

  public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
    Log.d(TAG, "!inside onConnectionStateChange, status: " + status + " , new state: " + newState);
    oldLevel = -1;
    if (status == 0) {
      switch (newState) {
        case BluetoothProfile.STATE_CONNECTED:
          iNotify.notify(ConnectionStates.CONNECTING);
          gatt.discoverServices();
          break;

        case BluetoothProfile.STATE_DISCONNECTED:
          iNotify.notify(ConnectionStates.DISCONNECTED);
          break;

        default:
          iNotify.notify(ConnectionStates.CONNECTIONERROR);
          break;
      }
      isExecutorON = false;
      requestItemList.clear();
      mainHandler.removeCallbacks(queueExecutor);
    } else {
      if (status == 133) {
        needToSetDevice = 0;
        requestItemList.clear();
        mainHandler.removeCallbacks(queueExecutor);
        iNotify.notify(ConnectionStates.CONNECTIONERROR);
        mainHandler.obtainMessage(SHOW_TOAST, "Connection state change failed with status: " + status).sendToTarget();
      } else {
        mainHandler.obtainMessage(SHOW_TOAST, "Connection state change failed with status: " + status).sendToTarget();
        requestItemList.clear();
        mainHandler.removeCallbacks(queueExecutor);
        iNotify.notify(ConnectionStates.CONNECTIONERROR);
      }
    }
  }

  public void onServicesDiscovered(BluetoothGatt gatt, int status) {
    List<BluetoothGattService> services = gatt.getServices();
    Log.d(TAG, "!inside onServicesDiscovered, services count: " + services.size() + " , status: " + status);
    enableNotification1();
  }

  public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
    byte[] characteristicValue = characteristic.getValue();
    Log.d(TAG, "!inside onCharacteristicRead, service uuid: " + characteristic.getService().getUuid() +
      " , characteristic uuid: " + characteristic.getUuid() + " , hex bytes: " +
      bytesToHex(characteristicValue));
  }

  public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
    byte[] characteristicValue = characteristic.getValue();
    Log.d(TAG, "!inside onCharacteristicWrite, service uuid: " + characteristic.getService().getUuid() + " , characteristic uuid: " + characteristic.getUuid() + " , hex bytes: " + bytesToHex(characteristicValue));

    if (!lastRequestItem.isNeedToWait) {
      // start to send next ReQuestItem
      Log.i(TAG, "queueExecutor invoke - onCharacteristicWrite");
      isExecutorON = true;
      mainHandler.post(queueExecutor);
    } else {
      isExecutorON = false;
      mainHandler.postDelayed(timeOutChecker, lastRequestItem.timeOut);
    }
  }

  public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

    byte[] value = characteristic.getValue();
    Log.d(TAG, "!inside onCharacteristicChanged, service uuid: " + characteristic.getService().getUuid() + " , characteristic uuid: " + characteristic.getUuid() + " , hex bytes: " + bytesToHex(value));

    if (iSensorState.getState() == States.STARTUP) {
      requestItemList.clear();
      mainHandler.removeCallbacks(queueExecutor);
      iSensorState.setState(States.ERROR);
      return;
    }

    if (iSensorState.getState() == States.OPERATIONAL && genericBle.isForeground()) {
//            needToSetDevice = -1;
//            requestItemList.clear();
//            mainHandler.removeCallbacks(queueExecutor);
    }

    if (characteristic.getUuid().toString().equalsIgnoreCase(METAWEAR_BATTERY_CHAR)) {
      broadcast(characteristic.getValue()[0]);
      return;
    }

    if (lastRequestItem != null && lastRequestItem.isNeedToWait) {
      boolean dataProcessed = true;
      if (value.length == 3 && value[0] == 0x09 && value[1] == 0x02) {
        tickerChannelMap.put(tickerChannels.pollFirst(), (int) value[2]);
      } else if (value.length == 3 && value[0] == 0x0C && value[1] == 0x02) {
        RequestItem dynamicRequest = new RequestItem();
        Channel channel = tickerChannels.peekFirst().get(0);
        if (channel != null) {
          if (channel.getUri() != null && channel.getUri().trim().matches("(.*)temperature(.*)")) {
            dynamicRequest.data = new byte[]{0x0A, 0x02, 0x0C, 0x06, value[2], 0x04, (byte) 0x81, 0x01};
          } else if (channel.getUri() != null && channel.getUri().trim().matches("(.*)heartrate(.*)")) {
            dynamicRequest.data = new byte[]{0x0A, 0x02, 0x0C, 0x06, value[2], 0x05, (byte) 0x87, 0x01};
          }
        }
        requestItemList.addFirst(dynamicRequest);
      } else if (value.length == 3 && value[0] == 0x0A && value[1] == 0x02) {
        tickerChannelMap.put(tickerChannels.pollFirst(), (int) value[2]);
      } else {
        dataProcessed = false;// received data is not meant for last sent command, skip executor
      }

      if (dataProcessed) {
        // got response from the hardware, so just change it to not wait
        lastRequestItem.isNeedToWait = false;
        mainHandler.removeCallbacks(timeOutChecker);
        if (!isExecutorON) {
          isExecutorON = true;
          mainHandler.post(queueExecutor);
        }
        return;
      }
    }

    long curTimeStamp = ntpClient.getCorrentTime();
    boolean dataParsed = false;
    if (value.length == 5 && value[0] == 0x04 && value[1] == (byte) 0x81) {
      // Temperature sensor's output
      byte[] data = new byte[2];
      data[0] = value[4];
      data[1] = value[3];
      short shortA = ByteBuffer.wrap(data).getShort(); // big-endian by default
      float aShort = Constants.scaleTemperature(shortA);
      Log.i(TAG, "sensor value " + aShort + "");
      ArrayList<Channel> temp = getChannelForRegister(value[1]);
      if (temp == null || temp.size() == 0 || temp.get(0) == null) {
        resetDevice();
        return;
      } else {
        Channel channel = temp.get(0);
        Channel result = populateChannel(channel, ByteBuffer.allocate(4).putFloat(aShort).array(), curTimeStamp);
        iDataReadyInternal.dataReadyInternal(result);
      }
      dataParsed = true;
    } else if (value.length == 5 && value[0] == 0x05 && value[1] == (byte) 0x87) {
      // Pulse sensor's output
      byte[] data = new byte[2];
      data[0] = value[4];
      data[1] = value[3];
      short aShort = ByteBuffer.wrap(data).getShort();
      Log.i(TAG, "sensor value " + aShort + "");
      ArrayList<Channel> temp = getChannelForRegister(value[1]);
      if (temp == null || temp.size() == 0 || temp.get(0) == null) {
        resetDevice();
        return;
      } else {
        Channel channel = temp.get(0);
        Channel result = populateChannel(channel, ByteBuffer.allocate(4).putFloat((float) aShort).array(), curTimeStamp);
        iDataReadyInternal.dataReadyInternal(result);
      }
      dataParsed = true;
    } else if (value.length == 9 && value[0] == 0x09 && value[1] == 0x03) {
      // Gyro sensor's output
      ArrayList<Channel> channels = getChannelForRegister(value[0]);
      if (channels == null || channels.size() == 0) {
        resetDevice();
        return;
      }
      // removed timestamp validation which caused channel missing issue
      for (int i = 0; i < channels.size(); i++) {
        Channel channel = channels.get(i);
        if (channel != null && channel.isEnabled() && channel.getUri() != null) {
          byte[] data = new byte[2];
          if (channel.getUri().endsWith("gyroscope.x")) {
            data[0] = value[4];
            data[1] = value[3];
          } else if (channel.getUri().endsWith("gyroscope.y")) {
            data[0] = value[6];
            data[1] = value[5];
          } else if (channel.getUri().endsWith("gyroscope.z")) {
            data[0] = value[8];
            data[1] = value[7];
          }
          short shortA = ByteBuffer.wrap(data).getShort();
          float aShort = Constants.scaleGyro(shortA);
          Log.i(TAG, "sensor value " + aShort + "");
          Channel result = populateChannel(channel, ByteBuffer.allocate(4).putFloat(aShort).array(), curTimeStamp);
          iDataReadyInternal.dataReadyInternal(result);
          dataParsed = true;
        }
      }
    }

    if (dataParsed && (iSensorState.getState() != States.OPERATIONAL)) {
      stop();
    }
  }

  public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
    Log.d(TAG, "!inside onDescriptorRead, status: " + status);
  }

  public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
    BluetoothGattCharacteristic characteristic = descriptor.getCharacteristic();
    BluetoothGattService service = characteristic.getService();
    Log.d(TAG, "!inside onDescriptorWrite, service uuid: " + service.getUuid() +
      " , characteristic uuid: " + characteristic.getUuid() + " , descriptor uuid: " +
      descriptor.getUuid() + " , characteristic hex bytes: " + bytesToHex(characteristic.getValue()) +
      " , descriptor hex bytes: " + bytesToHex(descriptor.getValue()));

    if (service.getUuid().toString().equalsIgnoreCase("0000180f-0000-1000-8000-00805f9b34fb") && characteristic.getUuid().toString().equalsIgnoreCase(METAWEAR_BATTERY_CHAR)) {
      enableNotification2();
    } else if (service.getUuid().toString().equalsIgnoreCase("326a9000-85cb-9195-d9dd-464cfbbae75a") &&
      characteristic.getUuid().toString().equalsIgnoreCase("326a9006-85cb-9195-d9dd-464cfbbae75a")) {

      iNotify.notify(ConnectionStates.CONNECTED);

      Log.i(TAG, "All characteristic enabled.. Current state is " + iSensorState.getState());
      Log.i(TAG, "needToSetDevice is " + needToSetDevice);
      Log.i(TAG, "isForeground running " + genericBle.isForeground());

      requestItemList.clear();

      if (iSensorState.getState() == States.STARTUP ||
        iSensorState.getState() == States.WAITING_CONFIGURATION ||
        iSensorState.getState() == States.ERROR) {

        if (getDevice() != null) {
          setDevice(getDevice());
        } else {
          mainHandler.removeCallbacksAndMessages(null);
          RequestItem requestItem = new RequestItem();
          requestItem.type = RequestType.UPDATE_STATE;
          requestItem.states = States.WAITING_CONFIGURATION;
          requestItemList.add(requestItem);
          mainHandler.postDelayed(queueExecutor, CHECK_DURATION);
        }
        return;

      } else if (iSensorState.getState() == States.RECEIVED_CONFIGURATION) {
        if (getDevice() != null) {
          setDevice(getDevice());
        } else {
          iSensorState.setState(States.WAITING_CONFIGURATION);
        }
        return;
      } else if (iSensorState.getState() == States.READY) {
        // don't know what to do
        if (needToSetDevice == 0) {
          if (getDevice() != null) {
            setDevice(getDevice());
          } else {
            iSensorState.setState(States.WAITING_CONFIGURATION);
          }
        }
      } else if (iSensorState.getState() == States.OPERATIONAL) {
//                wait for some time to make sure not overriding the hardware
        mainHandler.removeCallbacksAndMessages(null);
        RequestItem requestItem = new RequestItem();
        requestItem.type = RequestType.UPDATE_STATE;
        requestItem.states = States.OPERATIONAL;
        requestItemList.add(requestItem);
        mainHandler.post(queueExecutor);
      }
      // need to enable timer after this, enable timer is done after setConfiguration is done
    }
  }

  public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
    Log.d(TAG, "!inside onReliableWriteCompleted, status: " + status);
  }

  public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
    Log.d(TAG, "!inside onReadRemoteRssi, status: " + status);
  }

  public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
    Log.d(TAG, "!inside onMtuChanged, status: " + status);
  }

  private void enableNotification1() {
    UUID serviceUuid = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
    BluetoothGattService gattService = genericBle.getService(serviceUuid);
    if (gattService != null) {
      BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(UUID.fromString(METAWEAR_BATTERY_CHAR));
      if (gattCharacteristic != null) {
        boolean status = genericBle.enableCharNotification(gattCharacteristic);
      }
    }
  }

  private void enableNotification2() {
    UUID serviceUuid = UUID.fromString("326a9000-85cb-9195-d9dd-464cfbbae75a");
    BluetoothGattService gattService = genericBle.getService(serviceUuid);
    if (gattService != null) {
      BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(UUID.fromString("326a9006-85cb-9195-d9dd-464cfbbae75a"));
      if (gattCharacteristic != null) {
        boolean status = genericBle.enableCharNotification(gattCharacteristic);
      }
    }
  }

  private boolean writeCharacteristic(byte[] data) {
    boolean status = false;
    BluetoothGattService gattService = genericBle.getService(UUID.fromString("326a9000-85cb-9195-d9dd-464cfbbae75a"));
    if (gattService != null) {
      BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(UUID.fromString("326a9001-85cb-9195-d9dd-464cfbbae75a"));
      if (gattCharacteristic != null) {
        gattCharacteristic.setValue(data);
        gattCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
        status = genericBle.writeCharacteristic(gattCharacteristic);

        Log.d(TAG, "!writing characteristic, status: " + status + " , service uuid: " +
          gattService.getUuid().toString() + " , char uuid: " +
          gattCharacteristic.getUuid().toString() + " , write type: " +
          BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE + " , value: " + bytesToHex(data));
      }
    }
    return status;
  }

  private synchronized ArrayList<Channel> getChannelForRegister(byte registerId) {
    ArrayList<Channel> result = null;
    Object[] channels = tickerChannelMap.keySet().toArray(new Object[0]);

    for (int i = 0; i < channels.length; i++) {
      ArrayList<Channel> channelsList = (ArrayList<Channel>) channels[i];
      Channel channel = channelsList.get(0);
      if (registerId == (byte) 0x81 && channel.getUri() != null &&
        channel.getUri().trim().matches("(.*)temperature(.*)")) {
        result = channelsList;
        break;
      } else if (registerId == (byte) 0x87 && channel.getUri() != null &&
        channel.getUri().trim().matches("(.*)heartrate(.*)")) {
        result = channelsList;
        break;
      } else if (registerId == (byte) 0x09 && channel.getUri() != null &&
        channel.getUri().trim().matches("(.*)gyroscope(.*)")) {
        result = channelsList;
        break;
      }
    }

    if (result != null) {
      for (int i = 0; i < result.size(); i++) {
        Channel channel = result.get(i);
        String channelDescription = channel.getDescription();
        if (channelDescription.contains("||")) {
          channelDescription = channelDescription.split("\\|\\|")[1];
        }
        if (sessionID != null && sessionID.length() > 0) {
          channelDescription = sessionID + "||" + channelDescription;
        }
        channel.setDescription(channelDescription);
      }
    }

    return result;
  }

  private Channel populateChannel(Channel temp, byte[] channelData, long timeStamp) {
    Channel channel = new Channel();
    channel.setName(temp.getName());
    channel.setDescription(temp.getDescription());
    channel.setUri(temp.getUri());
    channel.setGuid(temp.getGuid());
    channel.setType(temp.getType());
    channel.setSamplingDuration(temp.getSamplingDuration());
    channel.setIsEnabled(temp.isEnabled());

    Data data = new Data();
    data.setDataType(DataType.FLOAT);
    data.setTimeStamp(timeStamp);
    data.setValues(channelData);

    channel.setData(data);
    temp.setData(data);

    return channel;
  }

  public Object getPopulatedSensorChannel() {
    return populatedSensorChannel;
  }

  public void setPopulatedSensorChannel(Object populatedSensorChannel) {
    this.populatedSensorChannel = populatedSensorChannel;
  }

  /**
   * This returns the stop {@link Node}
   *
   * @return The value of stop {@link Node}
   */
  public Node getStopNode() {
    Node node = new Notification();
    node.setName("stop");
    node.setDescription("The sensor reading is stopped");
    node.setType(NodeType.METHOD);
    node.setGuid(UUID.randomUUID());
    node.setUri("Notification://" + Constants.META_WEAR_HOSTNAME + ".SensorAgent.stop");
    Data date = new Data();
    date.setTimeStamp(ntpClient.getCorrentTime());
    node.setData(date);
    return node;
  }

  public boolean canClose() {
    if (iSensorState.getState() == States.OPERATIONAL) {
      iDataReadyInternal.dataReadyInternal(getStopNode());
      stop();
      RequestItem requestItem = new RequestItem();
      requestItem.type = RequestType.UPDATE_CONNECTION;
      requestItem.states = States.STARTUP;
      requestItem.connectionStates = ConnectionStates.DISCONNECTED;
      requestItemList.add(requestItem);
      if (!isExecutorON) {
        isExecutorON = true;
        mainHandler.post(queueExecutor);
      }
      return false;
    }
    return true;
  }

  public void broadcast(int batteryLevel) {

    Log.d(TAG, "Metawear battery - " + batteryLevel);

    int batterySTATUS = BatteryManager.BATTERY_STATUS_CHARGING;
    if (oldLevel != -1) {
      batterySTATUS = -1;
      if (oldLevel <= batteryLevel) {
        batterySTATUS = BatteryManager.BATTERY_STATUS_CHARGING;
      }

      if (batteryLevel == 100) {
        batterySTATUS = BatteryManager.BATTERY_STATUS_FULL;
      }
    }
    Intent intent = new Intent(Constants.ACTION_BATTERY_LEVEL);
    intent.putExtra(BatteryManager.EXTRA_STATUS, batterySTATUS);
    intent.putExtra(BatteryManager.EXTRA_LEVEL, batteryLevel);
    mContext.sendBroadcast(intent);
    oldLevel = batteryLevel;
  }

  enum RequestType {
    SEND_DATA, UPDATE_STATE, UPDATE_CONNECTION
  }

  private class RequestItem {
    long timeOut = 3000;
    long startedON;
    RequestType type = RequestType.SEND_DATA;
    byte[] data;
    States states;
    ConnectionStates connectionStates;
    boolean isNeedToWait = false;
  }

  private class MainHandler extends Handler {

    public MainHandler() {
      super();
    }

    public MainHandler(Looper looper) {
      super(looper);
    }

    @Override
    public void handleMessage(Message msg) {
      switch (msg.what) {
        case SHOW_TOAST:
          Log.d(TAG, "Toast: " + msg.obj.toString());
          break;
      }
    }
  }
}
