package com.at.data;

import com.at.model.app.ConnectionStates;

public interface IDeviceState {

    public void notify(ConnectionStates connectionStates);
}