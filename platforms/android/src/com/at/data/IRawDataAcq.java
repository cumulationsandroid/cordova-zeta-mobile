package com.at.data;

public interface IRawDataAcq {
    public void rawDataAcquired(byte[] data);
}