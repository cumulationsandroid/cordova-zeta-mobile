package com.at.model;

import java.io.Serializable;

/**
 * Created by mathan on 8/12/15.
 */
public class MessageEvent implements Serializable{
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    private String path;
    private String data;
}
