package com.at.model.app;

/**
 * Defines current state of Wear
 */
public enum States {
    STARTUP, WAITING_CONFIGURATION, RECEIVED_CONFIGURATION, READY, OPERATIONAL, ERROR
}