package com.at.model.app;

/**
 * Created by mathan on 30/11/15.
 */
public interface IDataReadyExternal {
    public void dataReadyExternal(String data);
}
