package com.at.model.app;

import com.at.model.meta.Node;

/**
 * Created by mathan on 30/11/15.
 */
public interface IDataReadyInternal {
    public void dataReadyInternal(Node node);
}