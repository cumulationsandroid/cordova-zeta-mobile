package com.at.model.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.at.communication.Proxy;
import com.at.data.IDataAcq;
import com.at.data.ProxyDataAcquisition;
import com.at.model.domain.Channel;
import com.at.model.domain.Device;
import com.at.model.meta.Data;
import com.at.model.meta.DataType;
import com.at.model.meta.Node;
import com.at.model.meta.NodeType;
import com.at.model.meta.Notification;
import com.at.plugin.ProxyCommPluginCallback;
import com.at.utils.Constants;
import com.at.utils.LoggingService2;
import com.at.utils.NodeGsonDeserializer;
import com.at.utils.NodeGsonSerializer;
import com.at.utils.db.DatabaseWorker;
import com.at.utils.db.TableChannelStore;
import com.at.zeta.zetamobile.ZetaApplication;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.UUID;

/**
 * @SensorAgent is a service that is running on the wear which
 * acts as communication gateway between wear and phone
 */
public class SensorAgent extends Node implements IDataReadyExternal, IDataReadyInternal, INotify, ISensorState {

    private static final String tableName = TableChannelStore.CHANNEL_TABLE_NAME_MW;
    private static final String TAG = "JL-" + SensorAgent.class.getSimpleName();
    private static final String currentThreadName = "ZetaCommunicationMainThread";
    private static final String dbThreadName = "ZetaCommunicationSecondaryThread";
    private final IDataAcq mIDataAcq;
    private final Context mContext;
    private final String macAddress;
    public Node stopperMsg = null;
    DataSenderThread currentThread;
    DataSenderThread dbThread = null;
    Handler handler = new Handler(Looper.getMainLooper());
    private DatabaseWorker databaseWorker;
    private ProxyCommPluginCallback proxyCommPlugin;
    private States mState;
    private transient Notification mNotification;
    private ConnectionStates connectionStates = ConnectionStates.DISCONNECTED;
    private long interval = -1;
    private Runnable stopper = new Runnable() {
        @Override
        public void run() {
            if (mState == States.OPERATIONAL && connectionStates == ConnectionStates.CONNECTED) {
//                stop();
                Log.e(TAG, "ping - stopped session");
            } else {
                // to make sure that the messageQueue has only one stopper runnable
                handler.removeCallbacks(stopper);
                if (mState == States.OPERATIONAL) {
                    handler.postDelayed(stopper, interval);
                    Log.e(TAG, "ping - postponed ping");
                }
            }
        }
    };
    private BroadcastReceiver receiver = null;

    public SensorAgent(Context applicationContext, ProxyCommPluginCallback proxyCommPlugin, Node sensorAgentNode, String macAddress) {
        mContext = applicationContext;
        mState = States.STARTUP;
        this.proxyCommPlugin = proxyCommPlugin;
        mIDataAcq = new ProxyDataAcquisition(applicationContext, this, this, macAddress);
        databaseWorker = ((ZetaApplication) mContext).getDataBase();
        this.macAddress = macAddress;
        populateSensorAgentNodes(sensorAgentNode);
    }

    private void populateSensorAgentNodes(Node node) {
        setName(node.getName());
        setDescription(node.getDescription());
        setGuid(node.getGuid());
        setUri(node.getUri());
        setType(node.getType());
        setData(node.getData());
        ArrayList<Node> nodes = (ArrayList<Node>) node.getNodes();
        if (mIDataAcq.getDevice() != null) {
            nodes.add(mIDataAcq.getDevice());
        }
        setNodes(nodes);
    }

    /**
     * Returns the list of sensors available
     */
    public String getDeviceCapabilities() {
        return serialize(this);
    }

    /**
     * Gets the device node
     */
    public String getConfiguration() {
        if (mIDataAcq.getDevice() != null) {
            return serialize(mIDataAcq.getDevice());
        } else {
            return serialize(buildErrorNode(ErrorTypesZs.ERR_SERIALIZE_FAILED.name()));
        }
    }

    /**
     * Sets the configuration for a given node
     *
     * @param node
     */
    public void setConfiguration(Node node) {

        if (node instanceof Device) {

            // checking for States.STARTUP, because for the first time device node will be null
            if (mState == States.STARTUP) {
                dataReadyInternal(buildErrorNode("ERROR_DEVICE_NOT_READY_YET"));
                Log.e(TAG, "Device not ready");
                return;
            }

            if (mState == States.ERROR) {
                dataReadyInternal(buildErrorNode("ERROR_DEVICE_RESET_NEEDED"));
                Log.e(TAG, "hardware is sending the sensor data but the ticker is not there");
                return;
            }

            if (mState == States.OPERATIONAL) {
                stop();
            }

//            if (checkDeviceEqual(node)) {
//                // device is same, no need to set again
////                dataReadyInternal(buildErrorNode("ERROR_DEVICE_SAME"));
//                Log.e(TAG, "Device obj is same");
//                return;
//            }

            mIDataAcq.setDevice((Device) node);
            boolean gotDeviceNode = false;
            ArrayList<Node> nodes = (ArrayList<Node>) getNodes();
            for (int size = nodes.size() - 1; size >= 0; size--) {
                if (nodes.get(size) instanceof Device) {
                    nodes.set(size, node);
                    setNodes(nodes);
                    gotDeviceNode = true;
                    break;
                }
            }

            if (!gotDeviceNode) {
                nodes.add(node);
                setNodes(nodes);
            }
            // TODO : If we wish to start, we have to do it here.
            //start();
        } else {
            dataReadyInternal(buildErrorNode("ERROR_UNEXPECTED_NODE"));
            Log.e(TAG, "not a device node");
        }
    }

    /**
     * @param node
     * @return
     */
    private boolean checkDeviceEqual(Node node) {

        if (mIDataAcq.getDevice() == null)
            return false;

        String device1 = serialize(node);
        // FIXME : Find a better way to clone a Device node
        Device deviceNode = (Device) objectify(serialize(mIDataAcq.getDevice()));
        if (deviceNode != null) {
            for (int i = 0; i < deviceNode.getNodes().size(); i++) {
                ArrayList<? extends Node> sensorNode = deviceNode.getNodes().get(i).getNodes();
                for (int i1 = 0; i1 < sensorNode.size(); i1++) {
                    Node ChannelNode = sensorNode.get(i1);
                    Data data = ChannelNode.getData();
                    data.setValues(null);
                    data.setDataType(DataType.FLOAT);
                    data.setTimeStamp(0);
                }
            }
        }
        String device2 = serialize(deviceNode);
        if (device1.length() == device2.length() && device1.equals(device2)) {
            return true;
        }
        return false;
    }

    /**
     * Starts the data collection for a given node
     */
    public void start(Node node) {

        stopperMsg = null;

        if (mState == States.ERROR) {
            dataReadyInternal(buildErrorNode("ERROR_DEVICE_RESET_NEEDED"));
            Log.e(TAG, "hardware is sending the sensor data but the ticker is not there");
            return;
        }

        if (mState != States.READY) {
            dataReadyInternal(buildErrorNode("ERROR_STATE_NOT_MATCHING"));
            return;
        }
        mIDataAcq.start(node);
    }

    /**
     * Stops the data collection for a given node
     */
    public void stop() {

        handler.removeCallbacks(stopper);
        interval = -1;
        if (mState == States.ERROR) {
            dataReadyInternal(buildErrorNode("ERROR_DEVICE_RESET_NEEDED"));
            Log.e(TAG, "hardware is sending the sensor data but the ticker is not there");
            return;
        }

        if (mState != States.OPERATIONAL) {
            dataReadyInternal(buildErrorNode("ERROR_STATE_NOT_MATCHING"));
            return;
        }

        mIDataAcq.stop();
    }

    /**
     * Resets the external device
     */
    public void reset() {
        mIDataAcq.reset();
    }

    /**
     * Sends heart-beat message for a given node to check the status of node
     *
     * @param node
     */
    public void heartBeat(Node node) {

    }

    /**
     * Converts json format of data to object node
     *
     * @param data input in json format
     * @return ERR_NO_ERROR if conversion is successful else throws error of type ErrorTypesZs
     */
    public Node objectify(String data) {
        Node node = null;
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonDeserializer());
        builder.registerTypeAdapter(Channel.class, new NodeGsonDeserializer());
        Gson gsonExt = builder.create();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(data).getAsJsonObject();
        if (jsonObject.getAsJsonPrimitive("uri") != null) {
            String uri = jsonObject.getAsJsonPrimitive("uri").getAsString();
            if (uri.startsWith(Device.class.getSimpleName())) {
                node = gsonExt.fromJson(data, Device.class);
            } else if (uri.startsWith(SensorAgent.class.getSimpleName())) {
                node = gsonExt.fromJson(data, SensorAgent.class);
            } else if (uri.contains(Channel.class.getSimpleName())) {
                node = gsonExt.fromJson(data, Channel.class);
            } else {
                node = gsonExt.fromJson(data, Node.class);
            }
            if (node != null) {
                Log.d(TAG, "!inside objectify, node uri: " + node.getUri());
            }
        }

        return node;
    }

    /**
     * Converts node object to json format
     *
     * @param node object to be serialized
     * @return ERR_NO_ERROR if conversion is successful else throws error of type ErrorTypesZs
     */
    public String serialize(Node node) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Node.class, new NodeGsonSerializer());
        if (node instanceof SensorAgent) {
            builder.registerTypeAdapter(SensorAgent.class, new NodeGsonSerializer());
        } else if (node instanceof Device) {
            builder.registerTypeAdapter(Device.class, new NodeGsonSerializer());
        } else if (node instanceof Channel) {
            builder.registerTypeAdapter(Channel.class, new NodeGsonSerializer());
        }
        Gson gsonExt = builder.create();
        String temp = gsonExt.toJson(node, Node.class);
        Log.d(TAG, "!serialized data: " + temp);
        return temp;
    }

    /**
     * @return proxy of the SensorAgent
     */
    public Proxy getProxy() {
        return null;
    }

    /**
     * @param proxy set proxy for SensorAgent
     */
    public void setProxy(Proxy proxy) {

    }

    /**
     * Get current connection state of the wear
     *
     * @return
     */
    @Override
    public States getState() {
        return mState;
    }

    /**
     * Set current connection state of the wear
     *
     * @param state
     */
    @Override
    public void setState(States state) {

        Log.e(TAG, "setState Invoked. State is " + state);

        if (state == mState) {
            return;
        }

        if (state == States.OPERATIONAL) {
            registerBattery();
        } else {
            unregisterBattery();
        }

        this.mState = state;
        if (this.mState == States.WAITING_CONFIGURATION
                || this.mState == States.READY) {
            dataReadyInternal(getStateNode(this.mState));
        } else if (this.mState == States.ERROR) {
            reset();
            dataReadyInternal(getStateNode(States.ERROR));
        } else {
            // this block is for testing purpose
            dataReadyInternal(getStateNode(this.mState));
        }
    }

    @Override
    public void notify(ErrorTypesZs errorTypes) {

    }

    @Override
    public void notify(ConnectionStates connectionStates) {
        Log.e(TAG, "notify Invoked. connectionState is " + connectionStates);
        if (this.connectionStates == connectionStates) {
            return;
        }
        this.connectionStates = connectionStates;
        dataReadyInternal(getConnectionStates());
        if (connectionStates == ConnectionStates.DISCONNECTED && mState == States.OPERATIONAL && interval != -1) {
            handler.removeCallbacks(stopper);
            Log.e(TAG, "ping - posponded ping");
            handler.postDelayed(stopper, interval);
        } else if (connectionStates == ConnectionStates.CONNECTED && mState == States.OPERATIONAL && interval != -1) {
            handler.removeCallbacks(stopper);
            Log.e(TAG, "ping - created ping");
            handler.postDelayed(stopper, interval);
        }
    }

    private void logString(String data) {
        if (data.length() > 4000) {
            Log.v(TAG, "sb.length = " + data.length());
            int chunkCount = data.length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= data.length()) {
                    Log.v(TAG, data.substring(4000 * i));
                } else {
                    Log.v(TAG, data.substring(4000 * i, max));
                }
            }
        } else {
            Log.v(TAG, data);
        }
    }

    public void ping(Node node) {
        handler.removeCallbacks(stopper);
        interval = ByteBuffer.wrap(node.getData().getValues()).getLong();
        if (interval <= 0)
            throw new IllegalArgumentException("Interval should be greater then zero");
        interval += (Constants.CONNECTION_DELAY + Constants.TRANSMISSION_DELAY_OFFSET);
        if (mState == States.OPERATIONAL) {
            handler.postDelayed(stopper, interval);
            Log.e(TAG, "ping - ping set for " + interval);
        } else {
            if (stopperMsg != null) {
                Log.e(TAG, "ping - session not running, notification sent");
                dataReadyInternal(stopperMsg);
                stopperMsg = null;
            }
            Log.e(TAG, "ping - session not running, notification already sent");
        }
    }

    @Override
    public void dataReadyInternal(final Node node) {
        String data = serialize(node);
        if (currentThread == null) {
            currentThread = new DataSenderThread(currentThreadName);
            currentThread.start();
        }
        if (!currentThread.isAlive()) {
            currentThread.quit();
            currentThread = null;
            currentThread = new DataSenderThread(currentThreadName);
            currentThread.start();
        }
        currentThread.queueTask(data, false);
    }

    @Override
    public void dataReadyExternal(String data) {

        if (connectionStates != ConnectionStates.CONNECTED) {
            dataReadyInternal(buildErrorNode("ERROR_DEVICE_NOT_CONNECTED"));
            return;
        }

        Node node = objectify(data);
        if (node instanceof Node) {
            try {
                Class params[] = new Class[node.getNodes().size()];
                if (node.getNodes().size() > 0) {
                    params[0] = Node.class;
                }
                Object[] objects = null;
                if (node.getNodes().size() > 0) {
                    ArrayList<? extends Node> nodeList = node.getNodes();
                    objects = new Object[nodeList.size()];
                    for (int i = 0; i < nodeList.size(); i++) {
                        Node objectNode = nodeList.get(i);
                        objects[i] = objectNode;
                    }
                }
                Method method = this.getClass().getDeclaredMethod(node.getName(), params);
                if (method != null) {
                    if (method.getReturnType().equals(Void.TYPE)) {
                        method.invoke(this, objects);
                    } else if (method.getReturnType().equals(String.class)) {
                        String name = (String) method.invoke(this, objects);
                        if (proxyCommPlugin != null)
                            proxyCommPlugin.sendMessageToPlugin(name);
                    } else if (method.getReturnType().equals(States.class)) {
                        States curState = getState();
                        if (curState == States.WAITING_CONFIGURATION || curState == States.READY) {
                            dataReadyInternal(getStateNode(curState));
                        } else {
                            // this block is for testing purpose
                            dataReadyInternal(getStateNode(curState));
                        }
                    } else if (method.getReturnType().equals(Node.class)) {
                        Node connectionNode = (Node) method.invoke(this, objects);
                        String serializedState = serialize(connectionNode);
                        if (proxyCommPlugin != null)
                            proxyCommPlugin.sendMessageToPlugin(serializedState);
                    }

                    Log.d(TAG, "Invoked " + node.getName());
                } else {
                    Log.d(TAG, "Method not available " + node.getName());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This returns the {@link #connectionStates } in {@link Node}
     *
     * @return The value of {@link #connectionStates } in {@link Node}
     */
    public Node getConnectionStates() {
        Node node = new Notification();
        node.setName("ConnectionStates");
        node.setDescription("The connection state with the external device");
        node.setType(NodeType.METHOD);
        node.setGuid(UUID.randomUUID());
        node.setUri("Notification://" + Constants.META_WEAR_HOSTNAME + ".SensorAgent.getConnectionStates");
        Data date = new Data();
        date.setDataType(DataType.ENUMERATION);
        date.setTimeStamp(System.currentTimeMillis());
        date.setValues(connectionStates.name().getBytes());
        node.setData(date);
        return node;
    }

    /**
     * Sets the connectionStates value
     *
     * @param connectionStates The value will set to {@link #connectionStates}
     */
    public void setConnectionStates(ConnectionStates connectionStates) {
        this.connectionStates = connectionStates;
    }

    /**
     * This returns the {@link #connectionStates } in {@link Node}
     *
     * @return The value of {@link #connectionStates } in {@link Node}
     */
    public Node getStateNode(States state) {
        Node node = new Notification();
        node.setName("getState");
        node.setDescription("The functional state of the SensorAgent");
        node.setType(NodeType.OBJECT);
        node.setGuid(UUID.randomUUID());
        node.setUri("Notification://" + Constants.META_WEAR_HOSTNAME + ".SensorAgent.getState");
        Data date = new Data();
        date.setDataType(DataType.ENUMERATION);
        date.setTimeStamp(System.currentTimeMillis());
        date.setValues(state.name().getBytes());
        node.setData(date);
        return node;
    }

    public Node buildErrorNode(String state) {
        Node node = new Notification();
        node.setName("ErrorTypesZs");
        node.setDescription("The error type");
        node.setType(NodeType.OBJECT);
        node.setGuid(UUID.randomUUID());
        node.setUri("Notification://" + Constants.META_WEAR_HOSTNAME + ".SensorAgent.ErrorTypesZs");
        Data date = new Data();
        date.setDataType(DataType.ENUMERATION);
        date.setTimeStamp(System.currentTimeMillis());
        date.setValues(state.getBytes());
        node.setData(date);
        return node;
    }

    private void onDestroy() {
//        FIXME: find a way to send callback to DataAcquisition
    }

    public void setCallback(ProxyCommPluginCallback proxyCommPlugin) {
        this.proxyCommPlugin = proxyCommPlugin;
    }

    public void checkThread() {
        if (databaseWorker == null)
            databaseWorker = ((ZetaApplication) mContext).getDataBase();
        if (databaseWorker != null
                && !databaseWorker.isEmpty(tableName)
                && getState() == States.OPERATIONAL
                && connectionStates == ConnectionStates.CONNECTED
                && proxyCommPlugin != null) {
            if (dbThread == null) {
                dbThread = new DataSenderThread(dbThreadName);
                dbThread.start();
                dbThread.fetchDBContent();
            } else {
                if (dbThread.messageCount == 0) {
                    if (!dbThread.isAlive()) {
                        dbThread.start();
                    }
                    dbThread.fetchDBContent();
                }
            }
        } else {
            clearThread();
        }
    }

    private void backupChannelData(String data) {
        long rowID = -1;
        try {
            JSONObject jsonObject = new JSONObject(data);
            if (jsonObject.getString("uri").startsWith("Channel")) {
                if (databaseWorker == null) {
                    databaseWorker = ((ZetaApplication) mContext).getDataBase();
                }
                databaseWorker.insert(data, tableName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearThread() {
        if (dbThread != null) {
            dbThread.mWorkerHandler.removeCallbacksAndMessages(null);
            dbThread.quit();
            dbThread = null;
            databaseWorker = null;
        }
    }

    public String getMacAddress() {
        return macAddress;
    }

    private void validateBattery(Intent batteryStatus) {

        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);

        if (!isCharging) {
            if (level > 10 && level <= 20) {
                stopperMsg = buildBattery(Constants.MESSAGE_WARNING_BATTERY);
                dataReadyInternal(stopperMsg);
            } else if (level <= 10) {
                stopperMsg = buildBattery(Constants.MESSAGE_WARNING_STOPPED);
                dataReadyInternal(stopperMsg);
                stop();
            }
        }
    }

    private Node buildBattery(String msg) {
        Node node = null;
        ArrayList<Node> nodes = (ArrayList<Node>) getNodes();
        for (int i = nodes.size() - 1; i >= 0; i--) {
            if (nodes.get(i).getName().equalsIgnoreCase("Notification")) {
                node = nodes.get(i);
                break;
            }
        }
        if (node == null) {
            node = new Notification();
            node.setName("Notification");
            node.setDescription("Notification message");
            node.setType(NodeType.OBJECT);
            node.setGuid(UUID.randomUUID());
            node.setUri(getUri().replace("SensorAgent:", "Notification:") + ".Notification");
            nodes.add(node);
        }

        Data data = new Data();
        data.setDataType(DataType.STRING);
        data.setTimeStamp(System.currentTimeMillis());
        data.setValues(msg.getBytes());

        node.setData(data);

        return node;
    }

    private void registerBattery() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_BATTERY_LEVEL);
        intentFilter.addAction(Constants.ACTION_FORCE_STOP);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction() != Constants.ACTION_FORCE_STOP) {
                    validateBattery(intent);
                } else {
                    stopperMsg = buildBattery(Constants.MESSAGE_WARNING_STOPPED_U);
                    dataReadyInternal(stopperMsg);
                }
            }
        };

        mContext.registerReceiver(receiver, intentFilter);
    }

    private void unregisterBattery() {
        if (receiver != null) {
            mContext.unregisterReceiver(receiver);
            receiver = null;
        }
    }

    public class DataSenderThread extends HandlerThread {

        public int messageCount = 0;
        private Handler mWorkerHandler;

        public DataSenderThread(String name) {
            super(name);
        }

        @Override
        public synchronized void start() {
            super.start();
            prepareHandler();
        }

        public void queueTask(String data, boolean isFromDB) {
            messageCount++;
            mWorkerHandler.obtainMessage(isFromDB ? 1 : 0, data).sendToTarget();
        }

        private void prepareHandler() {
            mWorkerHandler = new Handler(getLooper(), new Handler.Callback() {

                @Override
                public boolean handleMessage(Message msg) {
                    messageCount--;
                    String data = (String) msg.obj;
                    boolean isFromDB = msg.what == 1;

                    // connect GoogleClient
                    if (proxyCommPlugin == null) {
                        if (!isFromDB) {
                            backupChannelData(data);
                        }
                        if (mState == States.OPERATIONAL) {
                            SensorAgent.this.stop();
                        }
                        return true;
                    }

                    // try to send data to all connected node
                    proxyCommPlugin.sendMessageToPlugin(data);
                    LoggingService2.d(mContext, "TAG", data);

                    if (isFromDB) {
                        databaseWorker.deleteFirstRow(tableName);
                    }

                    checkThread();

                    return true;
                }
            });
        }

        private void fetchDBContent() {
            // TODO : Dont wait, just send the data to callback
            if (dbThread != null) {
                queueTask(databaseWorker.getFirstRow(tableName), true);
                try {
                    dbThread.currentThread().sleep(Constants.TRANSMISSION_DELAY);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
